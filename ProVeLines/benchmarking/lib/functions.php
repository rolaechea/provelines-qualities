<?php

$path = "..";
$fPromela = "$path/src/snip";
$spin = "$path/benchmarking/lib/Spin/Src5.2.5/spin";
$TVLParser = "$path/src/lib/tvl/TVLParser.jar";
$validProducts;

/* 
 * Benchmarks any command execution. 
 */
function texec($command, $logfileprefix, &$stdout, &$stderr, &$time, $printerrors = true) {
	global $repetitions;
	
	if(strtolower(PHP_OS) == 'linux') $command = "time -p -o $logfileprefix.time.txt $command > $logfileprefix.stdout.txt 2> $logfileprefix.stderr.txt";
	else $command = "(time -p $command > $logfileprefix.stdout.txt 2> $logfileprefix.stderr.txt) 2>> $logfileprefix.time.txt";
	
	$totalTime = array();
	$totalTime["real"] = array();
	$totalTime["real"][0] = 0;
	$totalTime["real"][1] = 0;
	$totalTime["user"] = array();
	$totalTime["user"][0] = 0;
	$totalTime["user"][1] = 0;
	$totalTime["sys"] = array();
	$totalTime["sys"][0] = 0;
	$totalTime["sys"][1] = 0;
	$i = $repetitions;
	while($i > 0) {
		shell_exec($command);
		$stdout = file("$logfileprefix.stdout.txt");
		$stderr = file("$logfileprefix.stderr.txt");
		$tout = file($logfileprefix.'.time.txt');
		$time = array();
		foreach($tout as $l) {
				// $tstr = trim(substr($l, strrpos($l, "\t")+1));
			// @sscanf($tstr, '%dm%fs', $min, $sec);
			$tstr = trim(substr($l, strrpos($l, " ")+1));
			@sscanf($tstr, '%f', $sec);
			if(trim($l) != '') $time[substr($l, 0, strpos($l, " "))] = array($tstr, $sec);
		}
		$totalTime["real"][0] += $time["real"][0];
		$totalTime["user"][0] += $time["user"][0];
		$totalTime["sys"][0] += $time["sys"][0];
		$totalTime["real"][1] += $time["real"][1];
		$totalTime["user"][1] += $time["user"][1];
		$totalTime["sys"][1] += $time["sys"][1];
		$i--;
	}
	$time["real"][0] = $totalTime["real"][0]/$repetitions;
	$time["user"][0] = $totalTime["user"][0]/$repetitions;
	$time["sys"][0] = $totalTime["sys"][0]/$repetitions;
	$time["real"][1] = $totalTime["real"][1]/$repetitions;
	$time["user"][1] = $totalTime["user"][1]/$repetitions;
	$time["sys"][1] = $totalTime["sys"][1]/$repetitions;
}

/* 
 * Extracts the features of a .pml file.
 * 
 * Parameters:
 * 	- $file is the path to the .pml file.
 * 
 * Returns: a two-cells array that contains:
 * 	- Each text line contained in the file, in the right order.
 * 	- An array of which each cell:
 * 		- is indexed by a distinct feature name
 * 		- contains the line number at which the feature is declared.
 */
function parsePromelaFile($file) {
	$lines = file($file);
	$features = array();
	$featuresList = array();
	$fcounter = 0;
	$i = 0;
	while($i < count($lines)) {
		$val = $lines[$i];
   		if(ereg('typedef features',strtolower($val))) {
   			while($i < count($lines) && !ereg('}',$val)) {
   				$val = $lines[$i];
   				$tok = strtok($val," ;");
   				while($tok !== false) {
   					if(ereg('bool',$tok)) {
   						$tok = strtok(" ;");
   						$feat = trim($tok);
   						$features[$feat] = $i;
   						$featuresList[$fcounter++] = $feat;
   					}
   					$tok = strtok(" }");
   				}
   				$i++;
   			}
   		}
   		$i++;
	} 
	return array($lines, $features, $featuresList);
}

/*
 * Produces a .pml file with its features initialized.
 * 
 * Parameters:
 * 	- $path is the path at which the file will be created.
 * 	- $lines is an array containing each line of the .pml file, in the right order.
 * 	- $values is an array of which each cell:
 * 		- is indexed by a distinct feature name
 * 		- contains the value (0 or 1) of the indexing feature
 * 
 * Returns: Nothing.
 */
function initPromelaFile($path, $lines, $values) {
	$inF = fopen($path,"w");
	$i = 0;
	while($i < count($lines)) {
		$val = $lines[$i];
   		if(($values != null) && ereg('typedef features',strtolower($val))) {
   			fputs($inF,"typedef features {\n");
   			$first = true;
   			while(list($feat,$value) = each($values)) {
   				if($first) {
   					fputs($inF, "\t bool ".$feat." = ".$value);
   					$first = false;
   				}
   				else fputs($inF, ";\n\t bool ".$feat." = ".$value);
   			}
   			fputs($inF, "\n");
			while(!ereg('}', $lines[$i]))	
				$i++;
			$val = $lines[$i];
   		}
   		fputs($inF,$val);
   		$i++;
	} 
}

/*
 * Checks whether or not the .pml file is compilable by Spin.
 * 
 * Parameters:
 * 	- $fPromelaModel is the path to the .pml file.
 * 
 * Returns: True iff an executable can be created by using Spin on the .pml file.
 */
function isCompilableBySpin($fPromelaModel) {
	global $spin;
	shell_exec("rm pan* >/dev/null 2>/dev/null");
	shell_exec("$spin -a ".$fPromelaModel." >stdout");
	$output = file(stdout);
	$i = 0;
	while($i < count($output))
		echo $output[$i++];
	shell_exec("gcc -o pan pan.c >stdout");
	$output = file(stdout);
	$i = 0;
	while($i < count($output))
		echo $output[$i++];
	shell_exec("rm stdout >/dev/null 2>/dev/null");
	return (file_exists("pan") == 1);
}

/*
 * Checks whether or not Spin found an error (a deadlock, an assert violation or a violation of the property).
 * The checking is based on the Spin output contained in a file.
 * 
 * Parameters:
 * 	- $file is the path to the file containing the output of Spin.
 * 
 * Returns true iff the number of errors indicated by the output of Spin is greater than 0.
 */
function spinOutputIsGood($file) {
	$lines = file($file);
	$i = 0;
	while($i < count($lines)) {
		if(ereg('errors: ',$lines[$i])) {
			if(ereg('errors: 0',$lines[$i])) return true;
			else return false;
		}
		$i++;
	}
	return true;
}

/*
 * Gets the boolean expression of the products that violates the property thanks to Snip output.
 * 
 * Parameters:
 * 	- $file is the path to the file that contains the Snip output.
 * 	- $exhaustive indicates if Snip has been launched in exhaustive mode (value 1) or not (value 0).
 * 
 * Returns: A boolean expression indicating the violating products, found by Snip.
 */
function getFallibleProducts($file, $exhaustive) {
	$lines = file($file);
	$i = 0;
	while($i < count($lines)) {
		if($exhaustive == 0 && ereg('Violated by all products',$lines[$i])) {
			return "All products";
		}
		if($exhaustive == 0 && ereg('Products by which it is violated',$lines[$i])) {
			$prods = trim($lines[$i+1],"\n ");
			if(strcmp($prods,"All the products") == 0) return "All products";
			else if(strcmp($prods,"None") == 0) return "None";
			else return $prods;
		}
		if($exhaustive == 1 && ereg('Exhaustive search finished',$lines[$i])) {
			if(ereg('covering every product',$lines[$i+1]))	return "All products";
			$prods = trim($lines[$i+2],"\n ");
			if(strcmp($prods,"All the products") == 0) return "All products";
			else if(strcmp($prods,"None") == 0) return "None";
			else return $prods;
		}
		$i++;
	}
	return "Property satisfied";
}

/*
 * Checks whether or not Snip found an error (a deadlock, an assert violation or a violation of the property).
 * The checking is based on the Snip output contained in a file.
 * 
 * Parameters:
 * 	- $file is the path to the file containing the output of Snip.
 * 
 * Returns true iff the output of Snip indicates a deadlock, an assert violation or a violation of the property.
 */
function fPromelaOutputIsGood($file) {
	$lines = file($file);
	$i = 0;
	$satisfied = true;
	while($i < count($lines)) {
		if(ereg('Found deadlock',$lines[$i]) || ereg('violated',$lines[$i]) || ereg('Violated',$lines[$i]))
			$satisfied = false;
		$i++;
	}
	return $satisfied;
}

/*
 * Returns the number of states explored by Snip, based on its output contained in a file.
 * 
 * Parameters:
 * 	- $filepath is the path to the file that contains the output of Snip.
 * 
 * Returns: The number of states explored by Snip.
 */
function getExploredfPromela($filepath) {
	$lines = file($filepath);
	$i = 0;
	$explored = 0;
	while($i < count($lines)) {
		if(ereg('\[explored',$lines[$i]))
			preg_match_all('/(\d+)/',$lines[$i],$arr_codes);
		$i++;
	}
	return $arr_codes[0];
}

/*
 * Returns the number of states explored by Spin, based on its output contained in a file.
 * 
 * Parameters:
 * 	- $filepath is the path to the file that contains the output of Spin.
 * 
 * Returns: The number of states explored by Spin.
 */
function getExploredSpin($filepath) {
	$lines = file($filepath);
	$i = 0;
	$explored = 0;
	$reexplored = 0;
	while($i < count($lines)) {
		if(ereg('states, stored',$lines[$i])) {
			preg_match_all('/(\d+)/',$lines[$i],$arr_codes);
			$explored = $arr_codes[0][0];
		}
		$i++;
	}
	return array($explored, $reexplored);
}


/*
 * Benchmarks Snip on a specified Promela model and a given LTL property.
 * 
 * Parameters:
 * 	- $fPromelaModel is the path to the .pml file that defines the model.
 * 	- $property is a string expressing the LTL property.
 *  - $exhaustive must be equal to 1 for Snip to be benchmarks in exhaustive mode.
 *  
 * Returns: an array containing, at index 0, 1 and 2 respectively:
 * 	- The execution time of Snip.
 * 	- The number of explored states.
 * 	- The number of reexplored states.
 */
function benchmark_fPromela($fPromelaModel, $property, $exhaustive) {
	global $fdlc; 
	global $dac;
	global $fPromela;
	shell_exec("rm __fPromela.* >/dev/null 2>/dev/null");
	$options = "";
	if($property !== null) $options .= " -ltl $property";
	else if($fdlc === true) $options .= " -fdlc";
	if($exhaustive == 1) $options .= " -exhaustive";
	$cmd = "$fPromela -nt -check $options -fmdimacs __clauses.tmp __mapping.tmp $fPromelaModel";
	texec("$cmd", "__fPromela", $stdout, $stderr, $time);
	$explored = getExploredfPromela("__fPromela.stdout.txt");
	$output[0] = $time["user"][1];
	$output[1] = $explored[0];
	$output[2] = $explored[1];
	return $output;
	
}


/*
 * Computes all the valid products defined by a given TVL model.
 * 
 * Parameters:
 * 	- $featuresModel is the path to the file containing the TVL specification.
 * 
 * Returns a three-cells array containing:
 *  - a two-dimensional array whose each line cells represents a valid product and each cell of
 * a given line is the name of a feature of the product;
 *  - the time needed for the TVL library to enumerate all the valid products;
 *  - the name of the root feature.
 */
function parseValidProducts($featuresModel) {
	global $TVLParser;
	$cmd = "java -jar $TVLParser -uprods $featuresModel > __valid.tmp";
	texec("$cmd", "__TVL", $lines, $stderr, $time);
	$features = array();
	$i = 0;
	$k = 0;
	$root;
	while($i < count($lines)) {
		$val = $lines[$i];
		if(strpos($val,'-') !== false) {
			$features[$k] = array();
			$tok = strtok($val,",");
   			$j = 0;
   			while($tok !== false) {
   				$feat = trim($tok);
   				$feat = trim($feat, "-, ");
   				$features[$k][$j] = $feat;
   				if(!isset($root)) $root = $feat; // the first feature is always the root.
   				$tok = strtok(",");
   				$j++;
   			}
   			$k++;
		}
   		$i++;
	} 
	shell_exec("rm __valid.tmp >/dev/null 2>/dev/null");
	$result = array();
	$result[0] = $features;
	$result[1] = $time["user"][1];
	$result[2] = $root;
	return $result;
}

/*
 * Returns the first valid product of the TVL model.
 * 
 * Parameters:
 * 	- $features is an array whose each cells is indexed by the name of a feature.
 * 
 * Returns:
 * 	- An array whose each cells is indexed by the name of a feature and has a value of 0 or 1,
 * 	if the feature is enabled or not, respectively.
 */
function getFirstValidProduct($features) {
	return getIthValidProduct($features, 0);
}

/*
 * Returns the i-th valid product of the TVL model.
 * 
 * Parameters:
 * 	- $features is an array whose each cells is indexed by the name of a feature.
 * 
 * Returns:
 * 	- An array whose each cells is indexed by the name of a feature and has a value of 0 or 1,
 * 	if the feature is enabled or not, respectively.
 */
function getIthValidProduct($features, $i) {
	global $validProducts;
	$val = $validProducts[$i];
	// Reinitialise the value of each feature.
	if($val == null) return null;
	$features = firstProduct($features); 
	// Set to one the value of each feature of the valid product.
	foreach($val as $positiveFeature) {
		$features[$positiveFeature] = 1;
	}
	return $features;
}

/*
 * Parses the feature expression returned by Snip (thanks to the CUDD library)
 * and transforms it into a two-dimensional array whose each line represents a minterm
 * and each element of a given line is an element of the conjunction of the corresponding minterm.
 * 
 * Parameters:
 * 	- $fexp is a string containing the feature expression returned by Snip (using CUDD format).
 * 
 * Returns: An array whose each cell is another array. Each cell of this array contains
 * either the name of the feature or its negation.
 */
function parseFeaturesExp($fexp) {
	$disjunctions = explode(" | ",$fexp);
	$products = array();
	for($i = 0; $i < count($disjunctions); $i++) {
		$products[$i] = array();
		$products[$i] = explode(" & ", trim($disjunctions[$i], "()"));
	}
	return $products;
}

/*
 * Parses the feature expression returned by Snip (thanks to the CUDD library) and determines
 * the cardinal of the set of products represented by this feature expression.
 * 
 * Parameters:
 * 	- $fexp is a string containing the feature expression returned by Snip.
 * 	- $fPromelaModel is the path to the .pml file.
 * 
 * Returns: The cardinal of the set of products represented by $fexp.
 */
function getFailProductsFromFeaturesExp($fexp, $fPromelaModel) {
	global $validProducts;

	if(strcmp(trim($fexp), "All products") == 0) return count($validProducts);
	if(strcmp(trim($fexp), "None") == 0) return 0;
	
	$badproducts = parseFeaturesExp($fexp);
	$parsed = parsePromelaFile($fPromelaModel);
	$features = $parsed[1];
	$i = 0;
	$bad = 0;
	$features = getIthValidProduct($features, $i);
	while($features != null) {
		$foundBad = false;
		$j = 0;
		while(!$foundBad && ($j < count($badproducts))) {
			$curr_exp = $badproducts[$j];
			$foundGood = false;
			$k = 0;
			while(!$foundGood && $k < count($curr_exp)) {
				if(strstr($curr_exp[$k], '!') !== false) {
					if($features[trim($curr_exp[$k],'!')] != 0)
						$foundGood = true;
				}
				else if($features[$curr_exp[$k]] != 1)
					$foundGood = true;
				$k++;
			}
			if(!$foundGood) $foundBad = true;
			$j++;
		}
		if($foundBad) $bad++;
		$i++;
		$features = getIthValidProduct($features, $i);
	}	
	return $bad;
}


/*
 * Returns the first (possibly invalid) product, i.e. the product which has no feature.
 * 
 * Parameters:
 * 	- $features is an array whose each cell is indexed by a feature name.
 * 
 * Returns: The same array than $features, but with every cell having a value of zero.
 */
function firstProduct($features) {
	foreach ($features as $feat => $value)
   		$features[$feat] = 0;
   	return $features;
}

/*
 * Returns the next (possibly invalid) product of a given product.
 * 
 * Parameters:
 * 	- $features is an array whose each cell:
 * 		- is indexed by a feature name
 * 		- has a value of 0 or 1 
 * 
 * Returns: The array can be see as a binary number 'b'. The function returns the same array, except that
 * 	the values are such that the array can be seed as a binary number equal to 'b+1'. When 'b' is maximal
 * (i.e. all the feature are set), then the function returns null.
 */
function nextProduct($features) {
	$stop = false;
	$i = 0;
	foreach ($features as $feat => $value) {
		if($value == 1)
			$features[$feat] = 0;
		else {
			$features[$feat] = 1;
			return $features;
		}
	}
	return null;
}

/*
 * Benchmarks Snip in 'spin mode' on some specified Promela and TVL models and a given LTL property.
 * 
 * Parameters:
 * 	- $fPromelaModel is the path to the .pml file that defines the Promela model.
 * 	- $featuresModel is the path to the .tvl file that defines the TVL model.
 * 	- $property is a string expressing the LTL property.
 *  - $exhaustive must be equal to 1 for Snip to be benchmarks in exhaustive mode.
 *  
 * Returns: an array containing, at index 0, 1 and 2 respectively:
 * 	- The execution time of Snip.
 * 	- The number of explored states.
 * 	- The number of reexplored states (always 0).
 */
function benchmark_dumbfPromela($fPromelaModel, $featuresModel, $property, $exhaustive) {
	global $fPromela;
	$parsed = parsePromelaFile($fPromelaModel);
	$lines = $parsed[0];
	$features = $parsed[1];
	$features = getFirstValidProduct($features);
	$options = "";
	if($property !== null) $options .= " -ltl $property";
	else if($fdlc === true) $options .= " -fdlc";
	$cmd = "$fPromela -ospin -nt -check $options __checking.pml";
	$output[0] = 0.00; // Time
	$output[1] = 0; // Explored
	$output[2] = 0; // Bad products number
	$i = 1;
	while($features !== null) {
		shell_exec("rm __dumbfPromela.* >/dev/null 2>/dev/null");
		initPromelaFile("__checking.pml", $lines, $features);
		texec("$cmd", "__dumbfPromela", $stdout, $stderr, $time);
		$explored = getExploredfPromela("__dumbfPromela.stdout.txt");
		$output[0] += $time["user"][1];
		$output[1] += $explored[0];
		$output[2] += $explored[1];
		if(!fPromelaOutputIsGood("__dumbfPromela.stdout.txt")) {
			$output[2]++;
			if($exhaustive == 0) return $output;
		}
		$features = getIthValidProduct($features, $i);	
		$i++;
	}
	return $output;
}

/*
 * Benchmarks Spin on some specified Promela and TVL models and a given LTL property.
 * 
 * Parameters:
 * 	- $fPromelaModel is the path to the .pml file that defines the Promela model.
 * 	- $featuresModel is the path to the .tvl file that defines the TVL model.
 * 	- $property is a string expressing the LTL property.
 *  - $exhaustive must be equal to 1 for Snip to be benchmarks in exhaustive mode.
 *  
 * Returns: an array containing, at index 0, 1, 2 and 3 respectively:
 * 	- The generation time + the execution time of Spin.
 * 	- The compilation time of the file generated by Spin.
 * 	- The number of explored states.
 * 	- The number of reexplored states (always 0).
 */
function benchmark_spin($fPromelaModel, $featuresModel, $property, $exhaustive) {
	global $spin, $noreduce, $panDFSlimit;
	$parsed = parsePromelaFile($fPromelaModel);
	$paramSpin = "";
	$paramComp = "";
	$paramPan = "";
	
	if($noreduce === true) {
		$paramSpin = " -o1 -o2 -o3 -o5";
		$paramComp = " -DNOREDUCE";
	}
	
	if($property !== null) {
		$lines = array_merge($parsed[0],$property);
		$paramPan .= " -a";
	} else {
		$lines = $parsed[0];
	}
	
	if($panDFSlimit != '') {
		$paramPan .= ' -m'.$panDFSlimit;
	}
	
	$features = $parsed[1];
	$features = getFirstValidProduct($features);
	$totalTime = array();
	$totalTime[0] = 0;
	$totalTime[1] = 0;
	$explored = 0;
	$failnumber = 0;
	$i = 1;
	while($features !== null) {
		initPromelaFile("__checking.pml", $lines, $features);
		texec("$spin -a __checking.pml", "__compile", $stdout, $stderr, $time);
		$totalTime[0] += $time["user"][1];
		texec("gcc -o pan $paramComp pan.c", "__compile", $stdout, $stderr, $time);
		$totalTime[1] += $time["user"][1];
		shell_exec("rm __spin.* >/dev/null 2>/dev/null");
		texec("./pan $paramPan", "__spin", $stdout, $stderr, $time);
		$totalTime[0] += $time["user"][1];
		$output = getExploredSpin("__spin.stdout.txt");
		$explored += $output[0];
		if(!spinOutputIsGood("__spin.stdout.txt"))
			if($exhaustive == 0) return array($totalTime[0], $totalTime[0]+$totalTime[1], $explored, 1);
			else $failnumber++;
		$features = getIthValidProduct($features, $i);
		$i++;	
	}
	$totalTime[1] = $totalTime[1] + $totalTime[0];
	return array($totalTime[0], $totalTime[1], $explored, $failnumber);
}

/*
 * Gets the number of features in a TVL model.
 * 
 * Parameters:
 *  - $featuresModel is the path to the .tvl file that defines the TVL model.
 *  
 * Returns: an array containing the name of the leaves of a feature diagram.
 */
function getFeaturesNumber($featuresModel) {
	global $TVLParser;
	$cmd = "java -jar $TVLParser -c $featuresModel > __valid.tmp";
	texec("$cmd", "__TVL", $lines, $stderr, $time);
	return $lines[0];
}

/*
 * Benchmarks Snip, Snip in 'spin mode' and Spin on some specified Promela and TVL models 
 * and some given LTL properties. The results of the benchmark are displayed on standard out.
 * 
 * Parameters:
 * 	- $fPromelaModel is the path to the .pml file that defines the Promela model.
 * 	- $featuresModel is the path to the .tvl file that defines the TVL model.
 * 	- $property is an array of string such that every cell expresses an LTL property.
 */
function benchmark($fPromelaModel, $featuresModel, $properties) {
	global $spin, $TVLParser, $validProducts, $dac, $incremental;
	$res = parseValidProducts($featuresModel);
	$validProducts = $res[0];
	$TVLtime = $res[1];
	$root = $res[2];
	echo "\nTVL library (listing valid products): ".$TVLtime."\n";
	
	if($properties === null || $dac === true) $properties[-1] = null;
	foreach ($properties as $property) {
		if($property !== null) {
			$property = "\"".$property."\"";
			$property = str_replace('\\','',$property);
			echo "\nChecking property: ".$property."\n\n";
			$notproperty = "'!(".trim($property, "\"'").")'";
			shell_exec("$spin -f $notproperty > __prop.txt 2>/dev/null");
			$parseProp = file("__prop.txt");
			$parsed = parsePromelaFile($fPromelaModel);
			$lines = array_merge($parsed[0], $parseProp);
		}
		else {
			echo "\nChecking deadlocks and asserts.\n\n";
			$parseProp = null;
			$parsed = parsePromelaFile($fPromelaModel);
			$lines = $parsed[0];
		}
		$features = $parsed[2];
		initPromelaFile("__checking.pml", $lines, null);
		if($incremental) $numberOfFeatures = 1;
		else $numberOfFeatures = count($features);
		
		while($numberOfFeatures <= count($features)) {
			printf("Considering %d/%d features (Last added: %s)\n", $numberOfFeatures, count($features), $features[$numberOfFeatures-1]);
			copy($featuresModel, "__featuresModel.tmp");
			$fp = fopen("__featuresModel.tmp", "a");
			fputs($fp, "\n $root {");
			$constraints = "";
			$removedFeature = count($features)-1;
			while($removedFeature >= $numberOfFeatures) {
				if($removedFeature == $numberOfFeatures)
					$constraints = $constraints."!$features[$removedFeature];";
				else 
					$constraints = $constraints."!$features[$removedFeature] && ";
				$removedFeature--;
			}
			fputs($fp, $constraints);
			fputs($fp, "}");
			fclose($fp);
			$cmdParse = "java -jar $TVLParser -dimacs __mapping.tmp __clauses.tmp __featuresModel.tmp";
			shell_exec($cmdParse);
			$res = parseValidProducts("__featuresModel.tmp");
			$validProducts = $res[0];
			if(isCompilableBySpin("__checking.pml") === true) {
				printf("Time\t#States\tReexpl.\tTime\t#States\tReexpl.\tTool\n");
				$output = benchmark_fPromela($fPromelaModel, $property, 0);
				$outputAll = benchmark_fPromela($fPromelaModel, $property, 1);
				printf("%4.2f\t%7d\t%7d\t%4.2f\t%7d\t%7d\t%s\n", $output[0], $output[1], $output[2], $outputAll[0], $outputAll[1], $outputAll[2], "SNIP (standard mode)");
				$output = benchmark_dumbfPromela($fPromelaModel, "__featuresModel.tmp", $property, 0);
				$outputAll = benchmark_dumbfPromela($fPromelaModel, "__featuresModel.tmp", $property, 1);
				printf("%4.2f\t%7d\t%7d\t%4.2f\t%7d\t%7d\t%s\n", $output[0], $output[1], 0, $outputAll[0], $outputAll[1], 0, "SNIP (spin mode)");
				$snipfails = $outputAll[2]; 
				$output = benchmark_spin($fPromelaModel, "__featuresModel.tmp", $parseProp, 0);
				$outputAll = benchmark_spin($fPromelaModel, "__featuresModel.tmp", $parseProp, 1);
				printf("%4.2f\t%7d\t%7d\t%4.2f\t%7d\t%7d\t%s\n", $output[1], $output[2], 0, $outputAll[1], $outputAll[2], 0, "Spin (w/ compilation)");
				printf("%4.2f\t%7d\t%7d\t%4.2f\t%7d\t%7d\t%s\n", $output[0], $output[2], 0, $outputAll[0], $outputAll[2], 0, "Spin (w/o compilation)");
				$spinfails = $outputAll[3];
			
				printf("\n");
				if($snipfails == 0 && $snipfails == $spinfails)
					if($property !== null) printf(" - The property is satisfied.\n");
					else printf(" - No deadlock or violated assertion found.\n");
				else  {
					$fexp = getFallibleProducts("__fPromela.stdout.txt",1);
					printf(" - %-55s %s\n","SNIP (standard mode) found the bad expression:", $fexp);
					$snipfailsprod = getFailProductsFromFeaturesExp($fexp, $fPromelaModel);
					printf(" - %-55s %d good, %d bad\n","SNIP (standard mode) found the good and bad products:", count($validProducts) - $snipfailsprod, $snipfailsprod);
					printf(" - %-55s %d good, %d bad\n","SNIP (spin mode) found the good and bad products:", count($validProducts) - $snipfails, $snipfails);
					printf(" - %-55s %d good, %d bad\n\n","Spin found the good and bad products:", count($validProducts) - $spinfails, $spinfails);
				}
			}
			else 
				echo "\n\t - Spin cannot compile the promela model.\n";
			$numberOfFeatures++;
		}
	}
	shell_exec("rm pan* >/dev/null 2>/dev/null");
	shell_exec("rm __* >/dev/null 2>/dev/null");
}



			/*printf("\n First violation\n");
			printf(" %-24s  .  %10s  .  %10s  .  %s\n", "", "Time", "Explored", "Reexplored");
			$output = benchmark_fPromela($fPromelaModel, $property, 0);
			printf(" %-24s  .  %7d.%2s  .  %10d  .  %10d\n", "- SNIP (standard mode)", $output[0], str_pad(intval(100*($output[0]-intval($output[0]))), 2, 0, STR_PAD_RIGHT), $output[1], $output[2]);
			$output = benchmark_dumbfPromela($fPromelaModel, $featuresModel, $property, 0);
			$snipfails = $output[2];
			printf(" %-24s  .  %7d.%2s  .  %10d  .  %10d\n", "- SNIP (spin mode)", $output[0], str_pad(intval(100*($output[0]-intval($output[0]))), 2, 0, STR_PAD_RIGHT), $output[1], 0);
			$output = benchmark_spin($fPromelaModel, $featuresModel, $parseProp, 0);
			$spinfails = $output[3];
			printf(" %-24s  .  %7d.%2s  .  %10d  .  %10d\n", "- Spin (w/ compilation)", $output[1], str_pad(intval(100*($output[1]-intval($output[1]))), 2, 0, STR_PAD_RIGHT), $output[2], 0);
			printf(" %-24s  .  %7d.%2s\n", "- Spin (w/o compilation)", $output[0], str_pad(intval(100*($output[0]-intval($output[0]))), 2, 0, STR_PAD_RIGHT));
			
			printf("\n");
			if($snipfails == 0 && $snipfails == $spinfails)
				if($property !== null) printf(" - The property is satisfied.\n");
				else printf(" - No deadlock or violated assertion found.\n");
			else  {
				printf(" - %-60s %s\n","SNIP (standard mode) found 1 bad product:", getFallibleProducts("__fPromela.stdout.txt",0));
				printf(" - %-60s\n","SNIP (spin mode) found ".$snipfails." bad product.");
				printf(" - %-60s\n","Spin found ".$spinfails." bad product.");
			}
			
			printf("\n\n All violations\n");
			printf(" %-24s  .  %10s  .  %10s  .  %s\n", "", "Time", "Explored", "Reexplored");
			$output = benchmark_fPromela($fPromelaModel, $property, 1);
			printf(" %-24s  .  %7d.%2s  .  %10d  .  %10d\n", "- SNIP (standard mode)", $output[0], str_pad(intval(100*($output[0]-intval($output[0]))), 2, 0, STR_PAD_RIGHT), $output[1], $output[2]);
			$output = benchmark_dumbfPromela($fPromelaModel, $featuresModel, $property, 1);
			$snipfails = $output[2];
			printf(" %-24s  .  %7d.%2s  .  %10d  .  %10d\n", "- SNIP (spin mode)", $output[0], str_pad(intval(100*($output[0]-intval($output[0]))), 2, 0, STR_PAD_RIGHT), $output[1], 0);
			$output = benchmark_spin($fPromelaModel, $featuresModel, $parseProp, 1);
			$spinfails = $output[3];
			printf(" %-24s  .  %7d.%2s  .  %10d  .  %10d\n", "- Spin (w/ compilation)", $output[1], str_pad(intval(100*($output[1]-intval($output[1]))), 2, 0, STR_PAD_RIGHT), $output[2], 0);
			printf(" %-24s  .  %7d.%2s\n", "- Spin (w/o compilation)", $output[0], str_pad(intval(100*($output[0]-intval($output[0]))), 2, 0, STR_PAD_RIGHT));
			*/



?>
