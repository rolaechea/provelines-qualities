echo "Start Minepump Benchmarks"
echo "-------------------------"

if [ "$1" = '' ]; then
	TS=$(date +%Y%m%d)
else
	TS=$1
fi;

PML=../test/minepump.pml
TVL=../test/minepump.tvl
OUT=minepump-results-$TS.txt

php run.php $PML $TVL \
		-dac \
		'!([]<> (stateReady && highWater && userStart))' \
		'!([]<> stateReady)                            ' \
		'!([]<> stateRunning)                          ' \
		'!([]<> stateStopped)                          ' \
		'!([]<> stateMethanestop)                      ' \
		'!([]<> stateLowstop)                          ' \
		'!([]<> readCommand)                           ' \
		'!([]<> readAlarm)                             ' \
		'!([]<> readLevel)                             ' \
		'!(([]<> readCommand) && ([]<> readAlarm) && ([]<> readLevel)) ' \
		'!([]<>  pumpOn)                               ' \
		'!([]<> !pumpOn)                               ' \
		'!(([]<> pumpOn) && ([]<> !pumpOn))            ' \
		'!([]<>  methane)                              ' \
		'!([]<> !methane)                              ' \
		'!(([]<> methane) && ([]<> !methane))          ' \
		'[] (!pumpOn || stateRunning)                  ' \
		\
		'[] (methane ->  (<> stateMethanestop))        ' \
		'[] (methane -> !(<> stateMethanestop))        ' \
		'[] (pumpOn || !methane)                       ' \
		'[] ((pumpOn && methane) -> <> !pumpOn)        ' \
		'(([]<> readCommand) && ([]<> readAlarm) && ([]<> readLevel)) -> [] ((pumpOn && methane) -> <> !pumpOn)    ' \
		'!<>[] (pumpOn && methane)                     ' \
		'(([]<> readCommand) && ([]<> readAlarm) && ([]<> readLevel)) -> !<>[] (pumpOn && methane)                 ' \
		'[] ((!pumpOn && methane && <>!methane) -> ((!pumpOn) U !methane))                                         ' \
		\
		'[] ((highWater && !methane) -> <>pumpOn)      ' \
		'!(<> (highWater && !methane))                 ' \
		'(([]<> readCommand) && ([]<> readAlarm) && ([]<> readLevel)) -> ([] ((highWater && !methane) -> <>pumpOn))' \
		'[] ((highWater && !methane) -> !<>pumpOn)     ' \
		'!<>[] (!pumpOn && highWater)                  ' \
		'(([]<> readCommand) && ([]<> readAlarm) && ([]<> readLevel)) -> (!<>[] (!pumpOn && highWater))            ' \
		'!<>[] (!pumpOn && !methane && highWater)      ' \
		'(([]<> readCommand) && ([]<> readAlarm) && ([]<> readLevel)) -> (!<>[] (!pumpOn && !methane && highWater))' \
		'[] ((pumpOn && highWater && <>lowWater) -> (pumpOn U lowWater))                                           ' \
		'!<> (pumpOn && highWater && <>lowWater)       ' \
		\
		'[] (lowWater -> (<>!pumpOn))                  ' \
		'(([]<> readCommand) && ([]<> readAlarm) && ([]<> readLevel)) -> ([] (lowWater -> (<>!pumpOn)))            ' \
		'!<>[] (pumpOn && lowWater)                    ' \
		'(([]<> readCommand) && ([]<> readAlarm) && ([]<> readLevel)) -> (!<>[] (pumpOn && lowWater))              ' \
		'[] ((!pumpOn && lowWater && <>highWater) -> ((!pumpOn) U highWater))                                      ' \
		'!<> (!pumpOn && lowWater && <>highWater)      ' \
		\
		| tee $OUT


echo "End"
echo "-------------------------"
