echo "Start Elevator Benchmarks"
echo "-------------------------"

if [ "$1" = '' ]; then
	TS=$(date +%Y%m%d)
else
	TS=$1
fi;

PML=../test/elevator.pml
TVL=../test/elevator.tvl
OUT=elevator-results-$TS.txt

php run.php $PML $TVL \
		-dac \
		-m10000000 \
		'![]<> progress' \
		'![]<>f0 || ![]<>f1 || ![]<>f2 || ![]<>f3' \
		'![]<>p0at0 || ![]<>p0at1 || ![]<>p0at2 || ![]<>p0at3' \
		\
		'[] (fb2 -> (<> f2))' \
		'[]<> progress -> ([] (fb2 -> (<> f2)))' \
		'[]<> progress -> ([] (fb2 -> (<> (f2 && dopen))))' \
		\
		'[]<> progress -> (!<>[] f2)' \
		'[]<> (progress || waiting) -> (!<>[] f2)' \
		'[]<> (progress || waiting) -> (!<>[] f0)' \
		\
		'!<> ((cb0 || cb1 || cb2 || cb3) && !(p0in || p1in) && dclosed)' \
		\
		'[]<> progress -> (!<>[] dclosed)' \
		'[]<> progress -> (!<>[] (p0to3 && dclosed))' \
		\
		'[]<> progress -> (!<>[] dopen)' \
		'[]<> (progress || waiting) -> (!<>[] dopen)' \
		'(([]<> (progress || waiting)) && ([]<> (fb0 || fb1 || fb2 || fb3))) -> (!<>[] dopen)' \
		\
		'!<> (p0in && p1in && dclosed)' \
		\
		'!<>[] (p0in && dclosed)' \
		'[]<> progress -> (!<>[] (p0in && dclosed))' \
		\
		| tee $OUT


echo "End"

echo  "-------------------------"
