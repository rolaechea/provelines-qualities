echo "Start CFDP Benchmarks"
echo "---------------------"

if [ "$1" = '' ]; then
	TS=$(date +%Y%m%d)
else
	TS=$1
fi;

PML=../test/cfdp.pml
TVL=../test/cfdp.tvl
OUT=cfdp-results-$TS.txt

php run.php $PML $TVL \
		-incremental -dac -repeat 5\
		'<>fileReceived' \
		'(<>eofReceived) -> <>fileReceived'\
		'((<>eofReceived) && (<>nakReceived)) -> <>fileReceived' \
		'((<>eofReceived) && ([]<>nakReceived)) -> <>fileReceived' \
		'[](finSend -> fileReceived)' \
		\
		| tee $OUT

echo "End"
echo "---------------------"
