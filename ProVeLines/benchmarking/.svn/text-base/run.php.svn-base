<?php 

/* WARNING: The command must be launched in the 'src' directory. */

$dac = false;
$fdlc = false;
$noreduce = false;
$panDFSlimit = '';
$incremental = false;
$repetitions = 1;

include "lib/functions.php";

function print_error($error) {
	echo "$error";
	echo " --\n";
	echo "Usage: php run.php file.pml file.tvl options LTLproperties \n";
	echo " where \n";
	echo "  - file.pml is the file containing the fPromela model \n";
	echo "  - file.tvl is the file containing the TVL model\n";
	echo "  - options is a (possible empty) series of parameters:\n";
	echo "  \t'-dac' is an optional parameter that enables the checking of only deadlock and assert.\n";
	echo "  \t'-fdlc' is an optional parameter that, if -dac is active, enables the 'full deadlock checking' option for Snip.\n";
	echo "  \t'-incremental' is an optional parameter that carries out the benchmarks with an increasing number of features.\n";
	echo "  \t'-mXXXX' is an optional parameter that sets the DFS limit of the SPIN process analyser.\n";
	echo "  \t'-noreduce' is an optional parameter that disables some optimisations made by Spin.\n";
	echo "  \t'-repeat' followed by an integer, allows to repeat the benchmark the number of times given by this integer.\n";
	echo "  - LTLproperties is a (possibly empty) series of LTL properties separated by a space\n";
	echo "    and written between simple quotes (e.g. '<>x' \n";
	exit(1);
}

if(count($argv) < 3) print_error("Arguments missing.\n");
else {
	array_shift($argv);
	$fPromelaModel = array_shift($argv);
	$fFeatureModel = array_shift($argv);
	
	if(strrchr($fPromelaModel, '.') == '.tvl') {
		$t = $fPromelaModel;
		$fPromelaModel = $fFeatureModel;
		$fFeatureModel = $t;
	}
	
	$props = null;
	$count = count($argv);
	$i = 0;
	while($count > 0) {
		if(trim($argv[$i]) == "-fdlc") {
			$fdlc = true;
			$count--;
			array_shift($argv);
		} else if(trim($argv[$i]) == "-dac") {
			$dac = true;
			$count--;
			array_shift($argv);
		} else if(trim($argv[$i]) == "-noreduce") {
			$noreduce = true;
			$count--;
			array_shift($argv);
		} else if(trim($argv[$i]) == "-incremental") {
			$incremental = true;
			$count--;
			array_shift($argv);
		} else if(trim($argv[$i]) == "-repeat") {
			$count--;
			array_shift($argv);
			if($count < 1)
				print_error("The number of repetitions must be specified after the '-repeat' option.");
			else {
				$repetitions = trim($argv[$i]);
				if(!is_numeric($repetitions))
					print_error("The number of repetitions must be specified after the '-repeat' option.");
				array_shift($argv);
				$count--;
			}
		} else if(substr(trim($argv[$i]), 0, 2) == '-m') {
			$panDFSlimit = (int) substr(trim($argv[$i]), 2);
			$count--;
			array_shift($argv);
		} else if(substr(trim($argv[$i]), 0, 1) == '-') {
			print_error("Not a valid parameter: '{$argv[$i]}'.\n");
		} else {
			$props = $argv;
			$count = 0;
		}
	}
	
	if(!file_exists($fPromelaModel)) print_error("Not a valid file path: '$fPromelaModel'.\n");
	else if(!file_exists($fFeatureModel)) print_error("Not a valid file path: '$fFeatureModel'.\n");
	else benchmark($fPromelaModel, $fFeatureModel, $props);
}

?>