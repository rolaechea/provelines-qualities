/*
 * GENERIC HASHTABLE
 * * * * * * * * * * * * * * * * * * * * * * * */

typedef unsigned int   tHtKey;
typedef unsigned int* ptHtKey;

struct hashtable * createHashtable();
void destroyEntries(struct hashtable * table, ptList keys);
void destroyHashtable(struct hashtable * table);
void htInsert(struct hashtable * table, tHtKey key, void * value);
ptList htSearch(struct hashtable * table, tHtKey key);
ptList htFind(struct hashtable * table, tHtKey key, void * value);
void htRemove(struct hashtable * table, tHtKey key, void * value);
