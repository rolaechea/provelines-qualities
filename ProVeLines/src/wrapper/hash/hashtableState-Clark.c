#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include "error.h"
#include "main.h"
#include "list.h"
#include "stack.h"
#include "boolFct.h"
#include "symbols.h"
#include "automata.h"
#ifdef CLOCK
	#include "clockZone.h"
	#include "federation.h"
#endif
#include "state.h"
#include "execution.h"
#include "hashtable.h"
#include "hashtable_itr.h"
#include "hashState.h"
#include "hashtableState.h"


#ifdef PROFILE_HT
	PROFILER_REGISTER(pr_htVisitedStatesInsert, "htVisitedStatesInsert", 10);
	PROFILER_REGISTER(pr_htVisitedStatesFind, "htVisitedStatesFind", 200);
#endif


long unsigned int htVisitedStatesNbFilledBuckets;
long unsigned int htVisitedStatesNbRecords;

DEFINE_HASHTABLE_INSERT(insert_some, tHtKey, struct value_);
DEFINE_HASHTABLE_INS_DEBUG(insert_some_debug, tHtKey, struct value_);
DEFINE_HASHTABLE_SEARCH(search_some, tHtKey, struct value_);
DEFINE_HASHTABLE_REMOVE(remove_some, tHtKey, struct value_);
DEFINE_HASHTABLE_ITERATOR_SEARCH(search_itr_some, tHtKey);

static unsigned int hashfromkey(void* k) {
    return *((ptHtKey) k);
}

static int equalkeys(void* k1, void* k2) {
    return *((ptHtKey) k1) == *((ptHtKey) k2);
}

static struct hashtable* _table;
static struct hashtable* _tableOuter;
static struct hashtable* _tableTranspose;

void htVisitedStatesInit() {
	_table = create_hashtable(2^16, hashfromkey, equalkeys);
}

void htOuterStatesInit() {
	_tableOuter = create_hashtable(2^16, hashfromkey, equalkeys);
}

struct hashtable* create_hashtableWrapper(){
	return  create_hashtable(2^16, hashfromkey, equalkeys);
}


/*
 *
 * Copy a hasthable.
 * Also copies the inner table but still share the hashtable elements.
 *
 */
struct hashtable* shallowHashTableCopy(struct hashtable* abstractTable){

	    struct hashtable *h;

	    h = (struct hashtable *)malloc(sizeof(struct hashtable));
	    if (NULL == h)
	    	failure("Out of memory (copying hashtable).\n");

	    h->tablelength = abstractTable->tablelength;

	    h->table = (struct entry **)malloc(sizeof(struct entry*) * h->tablelength);

	    if (NULL == h->table)
	    	failure("Out of memory (copying hashtable -- allocating inner table).\n");

	    memset(h->table, 0, h->tablelength * sizeof(struct entry *));

	    memcpy(h->table,  abstractTable->table, h->tablelength * sizeof(struct entry *));

	    h->primeindex   = abstractTable->primeindex;
	    h->entrycount   = abstractTable->entrycount;
	    h->hashfn       = abstractTable->hashfn;
	    h->eqfn         = abstractTable->eqfn;
	    h->loadlimit    = abstractTable->loadlimit;

	    return h;

}

void _htInsert(struct hashtable* table, tHtKey key, ptState state, byte foundInDFS) {
	if(!state) return;
	ptHtKey keyP = (ptHtKey) malloc(sizeof(tHtKey));
	if(!keyP) failure("Out of memory (creating hashtable key).\n");
	*keyP = key;
	ptValue valP;
	valP = remove_some(table, keyP);
	if(!valP) {
		valP = (ptValue) malloc(sizeof(tValue));
		if(!valP) failure("Out of memory (creating hashtable value).\n");
		valP->list = NULL;
		if(table == _table) htVisitedStatesNbFilledBuckets++;
	}
	if(table == _table) htVisitedStatesNbRecords++;

	ptHtState hs = (ptHtState) malloc(sizeof(tHtState));
	hs->payload = state->payload;
	hs->payloadSize = state->payloadSize;
	hs->originalState = state;
	hs->foundInDFS = foundInDFS;
#ifdef CLOCK
	hs->outerFed = addElement(NULL, state->features, state->zone, NULL, NULL);
	hs->innerFed = NULL;
#else
	hs->outerFeatures = state->features;
	hs->innerFeatures = NULL;
#endif
	valP->list = listAdd(valP->list, hs);
	hs = NULL;

	if(!insert_some(table, keyP, valP)) failure("Out of memory (inserting state in hashtable)\n");
}



/*
 *
 * Inserts a state into given table but storing a
 * given feature expression into OuterFeatures  instead of the one stored in
 * state->features
 *
 * Debug Version
 *
 */
void htAbstractInsertExplicitFeaturesDebug(struct hashtable* table, tHtKey key, ptState state, ptBoolFct featureExpression,
		struct hashtable* toPrinttable){
	if(!state) return;
	ptHtKey keyP = (ptHtKey) malloc(sizeof(tHtKey));

	if(!keyP)
		failure("Out of memory (creating hashtable key).\n");

	*keyP = key;
	ptValue valP;

//	printf("Before Remove Some \n ");
//	printHashTableInnerDetails(toPrinttable);
//	printf("\n");

	valP = remove_some(table, keyP);

//	printf("After Remove Some , val = NULL %s \n", valP == NULL ? "Yes" : "No");
//	printHashTableInnerDetails(toPrinttable);
//	printf("-- \n");


	if(!valP) {
		valP = (ptValue) malloc(sizeof(tValue));
		if(!valP) failure("Out of memory (creating hashtable value).\n");
		valP->list = NULL;
	}




	ptHtState hs = (ptHtState) malloc(sizeof(tHtState));
	if(!hs)
		failure("Out of memory (creating hashtate)");

	hs->payload = state->payload;
	hs->payloadSize = state->payloadSize;
	hs->foundInDFS = DFS_OUTER;

	hs->outerFeatures = featureExpression;
	hs->innerFeatures = NULL;

	valP->list = listAdd(valP->list, hs);
	hs = NULL;

//	printf("Before Insert Some \n");
//	printHashTableInnerDetails(toPrinttable);
//	printf("\n");

	if(!insert_some_debug(table, keyP, valP, toPrinttable))
		failure("Out of memory (inserting state in hashtable)\n");

//	printf("After Insert Some \n");
//	printHashTableInnerDetails(toPrinttable);
//	printf("\n");



}


/*
 *
 * Inserts a state into given table but storing a
 * given feature expression into OuterFeatures  instead of the one stored in
 * state->features
 *
 */
void _htInsertExplicitFeatures(struct hashtable* table, tHtKey key, ptState state, ptBoolFct featureExpression) {
	if(!state) return;
	ptHtKey keyP = (ptHtKey) malloc(sizeof(tHtKey));
	if(!keyP)
		failure("Out of memory (creating hashtable key).\n");

	*keyP = key;
	ptValue valP;
	valP = remove_some(table, keyP);

//	printf("_htInsertExplicitFeatures  valP  found null %s \n", valP == NULL ? "Yes" : "No");

	if(!valP) {
		valP = (ptValue) malloc(sizeof(tValue));
		if(!valP) failure("Out of memory (creating hashtable value).\n");
		valP->list = NULL;
	}




	ptHtState hs = (ptHtState) malloc(sizeof(tHtState));
	if(!hs)
		failure("Out of memory (creating hashtate)");

	hs->payload = state->payload;
	hs->payloadSize = state->payloadSize;
	hs->foundInDFS = DFS_OUTER;


	hs->originalState = state;

	hs->outerFeatures = featureExpression;
	hs->innerFeatures = NULL;

	valP->list = listAdd(valP->list, hs);
	hs = NULL;

	if(!insert_some(table, keyP, valP))
		failure("Out of memory (inserting state in hashtable)\n");
}


void _htRemove(struct hashtable * table, tHtKey key, ptHtState htState) {
	if(!htState) return;
	ptHtKey keyP = &key;
	ptValue valP = search_some(_table, keyP);
	valP->list = listRemove(valP->list, htState);
	if(!valP->list)
		remove_some(_table, keyP);
	free(valP);
}

/**
 * Inserts an element into the states hashtable.
 *
 * This function does NOT check whether the state is already
 * there, it blindly inserts it.
 */
void htVisitedStatesInsert(tHtKey key, ptState state, byte foundInDFS) {
	#ifdef PROFILE_HT
		PROFILER_START(pr_htVisitedStatesInsert);
	#endif
	_htInsert(_table, key, state, foundInDFS);
	#ifdef PROFILE_HT
		PROFILER_END(pr_htVisitedStatesInsert);
	#endif
}

void htVisitedStatesRemove(tHtKey key, ptHtState state) {
	_htRemove(_table, key, state);
}

/**
 * Inserts an element into the outer states hashtable.
 */
void htOuterStatesInsert(tHtKey key, ptState state) {
	_htInsert(_tableOuter, key, state, DFS_OUTER);
}


/**
 * Returns true in case the state was already visited.  That is, if there is s2
 * so that stateCompare(s, s2) == STATES_SAME_S1_VISITED.  The foundInDFS parameter
 * specifies on which DFS the state was supposedly visited.
 *
 * Possible scenarios:
 *
 * (A) If the state was visited during the specified DFS, the function returns true.
 *
 * (B) If the state was not visited at all, the return value is false and foundState is NULL.
 *
 * (C) If the state was visited during the specified DFS, but with less features, then the function
 *     returns false and writes a reference to the previously visited state into foundState.
 *
 * (D) If DFS_INNER is required and the state was already visited during DFS_OUTER, then
 *     the function returns false and writes a reference to the previously visited state
 *     into foundState.  The foundState->foundOnDFS is DFS_OUTER.  The caller can then set
 *     the foundState->innerFeatures and change foundState->foundInDFS to DFS_INNER
 *
 * To distinguish C from D in the case where DFS_INNER is required (not necessary for DFS_OUTER),
 * the foundState->foundInDFS has to be tested.
 */
byte htVisitedStatesFind(tHtKey key, ptState state, byte foundInDFS, ptHtState* foundState) {
#ifdef PROFILE_HT
		PROFILER_START(pr_htVisitedStatesFind);
#endif
	if(foundState)
		*foundState = NULL;

	ptHtKey keyP = (ptHtKey) malloc(sizeof(tHtKey));

	if(!keyP)
		failure("Out of memory (creating hashtable key).\n");

	*keyP = key;
	ptValue valP;

	valP = search_some(_table, keyP);
	free(keyP);

	if(valP) {
		ptList listElement = valP->list;
		byte compare;
		ptHtState candidateState;

		while(listElement) {
			candidateState = (ptHtState) listElement->value;
#ifdef CLOCK
			compare = stateCompare(state, candidateState->payload, NULL);
#else
			compare = stateCompare(state, candidateState->payload, foundInDFS == DFS_OUTER ? candidateState->outerFeatures : candidateState->innerFeatures);
#endif
			if(compare != STATES_DIFF) {
#ifdef CLOCK
				if(foundState) {
					*foundState = candidateState;
					return 0;
				}
#else
				// Cases A, C
				if(foundInDFS == candidateState->foundInDFS || (foundInDFS == DFS_OUTER && candidateState->foundInDFS == DFS_INNER)) {
					if(compare == STATES_SAME_S1_VISITED) {
						#ifdef PROFILE_HT
							PROFILER_END(pr_htVisitedStatesFind);
						#endif
						return 1;
					} else if(compare == STATES_SAME_S1_FRESH) {
						*foundState = candidateState;
						#ifdef PROFILE_HT
							PROFILER_END(pr_htVisitedStatesFind);
						#endif
						return 0;
					}
				} else if(foundInDFS == DFS_INNER /* && candidateState->foundInDFS == DFS_OUTER */) {
					*foundState = candidateState;
					#ifdef PROFILE_HT
						PROFILER_END(pr_htVisitedStatesFind);
					#endif
					return 0;
				}
#endif
			}
			listElement = listElement->next;
		}
	}
	#ifdef PROFILE_HT
		PROFILER_END(pr_htVisitedStatesFind);
	#endif
	return 0;
}

/**
 * Checks whether a state is in the backlinks hashtable.
 *
 * For this check it is sufficient to check that the state exists; the set of
 * products of the state in the hashtable will necessarily include the set of
 * products of the state to be tested since we're testing a path in which the
 * states in the hashtable are all ancestors of the state to be tested.
 */
ptHtState htOuterStatesFind(tHtKey key, ptState state) {
	ptHtKey keyP = (ptHtKey) malloc(sizeof(tHtKey));
	if(!keyP) failure("Out of memory (creating hashtable key).\n");
	*keyP = key;
	ptValue valP;
	valP = search_some(_tableOuter, keyP);
	free(keyP);
	if(valP) {
		ptList listElement = valP->list;
		while(listElement) {
			// Here we pass NULL as the feature expression of the candidate state instead
			// of the actual feature expression.  This avoids a implies() check because we
			// know that state->features necessarily implies candidate->features (if they
			// are both equal).
			if(stateCompare(state, ((ptHtState) listElement->value)->payload, NULL) == STATES_SAME_S1_VISITED) return (ptHtState) listElement->value;
			listElement = listElement->next;
		}
	}
	return NULL;
}


/**
 * Removes a state from the backlinks hashtable.
 */
void htOuterStatesRemove(tHtKey key, void* payload, unsigned int payloadSize) {
	if(payload) {
		ptHtKey keyP = (ptHtKey) malloc(sizeof(tHtKey));
		if(!keyP) failure("Out of memory (creating hashtable key).\n");
		*keyP = key;
		ptValue valP = search_some(_tableOuter, keyP); // Getting the list of htStates registered at the given key.
		if(valP) {
			ptList stateList = valP->list;
			ptList previous = NULL;
			ptHtState currentHtState;
			byte stop = 0;
			byte empty = 0;
			while(stateList && !stop) { // stop holds iff the searched htState has been found and removed.
				currentHtState = (ptHtState) stateList->value;
				if(memcmp(payload, currentHtState->payload, payloadSize) == 0) {
					/*if(previous) previous->next = stateList->next; // removing the found htState from the list.
					else if(stateList->next) valP->list = stateList->next; // The searched state was the first element.
					else empty = 1; // The list will be empty after the removal because there was only the searched state in it.
					free(currentHtState);
					free(stateList);
					stop = 1;*/
					valP->list = listRemove(valP->list, currentHtState);
					free(currentHtState);
					stop = 1;
				} else {
					previous = stateList;
					stateList = stateList->next;
				}
			}
			if(!valP->list) { // The list registered at the given key is now empty: it can be removed from the hashtable.
				valP = remove_some(_tableOuter, keyP);
				free(valP);
			}
		}
		free(keyP);
	}
}


/* Start Symbolic DFS Hasthable */

static struct hashtable* _tableBlackOrGrey;

/*
 * Initialize a hash table containing s, f such that state has been or is beeing visited with f.
 *
 */
void htBlackOrGreyInit() {
	_tableBlackOrGrey = create_hashtable(2^16, hashfromkey, equalkeys);
}


void htTranposeInit(){
	_tableTranspose = create_hashtable(2^16, hashfromkey, equalkeys);;
}

struct hashtable* getBlackOrGreyTable(){
	return _tableBlackOrGrey;
}


/*
 * Inserts an element into a Black or Grey Table
 *
 */
void htBlackOrGreyInsert(tHtKey key, ptState state){
	_htInsert(_tableBlackOrGrey, key, state, DFS_OUTER);
}

void htAbstractInsertExplicitFeatures(tHtKey key, ptState state, ptBoolFct FeatureExpression, struct hashtable* _abstractTable){
	_htInsertExplicitFeatures(_abstractTable, key, state, FeatureExpression);
}
/*
 * STATE COMPARISON
 * * * * * * * * * * * * * * * * * * * * * * * */

/**
 * Compares s1 a newly reached state
 *     with s2 a state known to be reachable
 * to see whether s1 is a state that was already visited.
 *
 * When s1 was not yet visited, then we say it's "fresh".
 *
 * Returns:
 * 	- STATES_DIFF 			 if s1 and s2 are totally different states, meaning s1 is fresh.
 * 	- STATES_SAME_S1_VISITED if s1 and s2 are identical but s2 is reachable by more products; hence, s1 adds nothing new
 *  - STATES_SAME_S1_FRESH	 if s1 and s2 are identical but s1 has products that were not explored with s2; hence, s1 is fresh
 */

byte stateCompareAbstract(ptState s1, void* s2Payload,	ptBoolFct s1Features, ptBoolFct s2Features){
	if(!s1 && !s2Payload){
		return STATES_SAME_S1_VISITED;
	}

	if((s1 && !s2Payload) || (!s1 && s2Payload))
		return STATES_DIFF;

	if(memcmp(s1->payload, s2Payload, s1->payloadSize) != 0)
		return STATES_DIFF;

	// Convention: NULL means 'true'.
	if(!s2Features){
		return STATES_SAME_S1_VISITED;
	}
	/* ------  */
	if(!s1Features){
		return STATES_SAME_S1_FRESH;	// Here we do not check the case in which s2->features != NULL but still a tautology;
													// There is a compilation parameter CHECK_TAUTOLOGY that can be set to check for
													// tautologies before they end up here.
	}

	if(implies(s1Features, s2Features)){
		return STATES_SAME_S1_VISITED;
	}
	else {
		// Avoid returning STATES_DIFF by no return (C defaults to 0=STATES_DIFF)
		// if we reach here, then s1,s2 payloads are equivalent and s1 doesn't imply s2 features
		// so s1 with s1 features  is fresh !
		return STATES_SAME_S1_FRESH;
	}

}



/*
 *
 * Replace the given state, stateFeatures hashState by a copy, and return a pointer to such copy.
 *
 * Precondition:
 *	htAbstractFind must have returned  false and foundState ! = NULL
 *
 *
 */
bool  htAbstractReplaceByCopy(tHtKey key,
		ptState state,
		ptBoolFct stateFeatures,
		ptHtState* foundState,
		struct hashtable* _abstractTable){

	if(foundState)
			*foundState = NULL;

	ptHtKey keyP = (ptHtKey) malloc(sizeof(tHtKey));

	if(!keyP)
		failure("Out of memory (creating hashtable key).\n");

	*keyP = key;

	ptValue valP;

	valP = search_some(_abstractTable, keyP);

	free(keyP);

	if(valP) {
		ptList listElement = valP->list;
		byte compare;
		ptHtState candidateState;

		while(listElement) {
			candidateState = (ptHtState) listElement->value;

			/*
			 *
			 * Check - state/feature excluded
			 */
			compare = stateCompareAbstract(state, candidateState->payload,
						stateFeatures,  candidateState->outerFeatures);

			if(compare != STATES_DIFF) {
				// Cases B, C
					if(compare == STATES_SAME_S1_VISITED) {

						return true;

					} else if(compare == STATES_SAME_S1_FRESH) {

						*foundState = candidateState;

						ptHtState hs = (ptHtState) malloc(sizeof(tHtState));

						memcpy(hs, candidateState,  sizeof(tHtState));

						*foundState = hs;


						listElement->value = hs;
						/* will now perform such copy */

						return false;
					}
			}

			listElement = listElement->next;
		}
	}
	return false;
}



/*
 *
 * Prints the inner details of a hash-table.
 *
 * --- Wanna detect infinite loop in a table
 *
 */
void printHashTableInnerDetails(struct hashtable* h){

    struct entry *e;
    unsigned int hashvalue, index;


    int i = 0;
    int countLoop ;
    printf("\n --- Hashtable -- \n ");

    for(i = 0; i < h->tablelength; i++ ){
        printf("At e, h=%p , index = %d \n", (void *) h , i);
        countLoop = 0;

        e = h->table[i];

        printf(" e absolute address is  %p, stored at %p , with h->table %p \n", e, (void *)(&h->table[i]), (void *)h->table);
        while (NULL != e)
        {
            /* Check hash value to short circuit heavier comparison */
        	printf("At %d,  %p, key %u , countLoop %d \n", i,  (void *) e, *((unsigned int *) e->k), countLoop);
        	countLoop++;
        	if(countLoop > 150 ){
        		failure("Exit Apparent Infinite Loop");
        	}
            e = e->next;
        }
    }

}


/*
 *
 * Check if a state has been visited under a given Reachability Exclusion Relationship.
 * three cases
 * 		A- State Completely White (not visited) at all
 * 			return false and foundState =  NULL
 * 		B - State visited with f', but not current->f -> f' === current->f & ~f' is satisfiable (compare gives STATES_SAME_S1_FRESH )
 *			return false and foundstate = PREVIOUS state
 *		C - State visited with f', and current->f -> f' -- (compare gives STATES_SAME_S1_VISITED)
 *			return true, and foundState = NULL.
 *
 *
 */
bool htAbstractFind(tHtKey key,
		ptState state,
		ptBoolFct stateFeatures,
		ptHtState* foundState,
		struct hashtable* _abstractTable){

	if(foundState)
		*foundState = NULL;

	ptHtKey keyP = (ptHtKey) malloc(sizeof(tHtKey));

	if(!keyP)
		failure("Out of memory (creating hashtable key).\n");

	*keyP = key;

	ptValue valP;

	valP = search_some(_abstractTable, keyP);

	free(keyP);

	if(valP) {
		ptList listElement = valP->list;
		byte compare;
		ptHtState candidateState;

		while(listElement) {
			candidateState = (ptHtState) listElement->value;

			/*
			 *
			 * Check - state/feature excluded
			 */
			compare = stateCompareAbstract(state, candidateState->payload,
						stateFeatures,  candidateState->outerFeatures);


			if(compare != STATES_DIFF) {
				// Cases B, C
					if(compare == STATES_SAME_S1_VISITED) {

						return true;

					} else if(compare == STATES_SAME_S1_FRESH) {

						*foundState = candidateState;

						return false;
					}
			}

			listElement = listElement->next;
		}
	}
	return false;
}

/*
 *
 * Check if a state has been visited under a symbolic DFS.
 * three cases
 * 		A- State Completely White (not visited) at all
 * 			return false and foundState =  NULL
 * 		B - State visited with f', but not current->f -> f' === current->f & ~f' is satisfiable (compare gives STATES_SAME_S1_FRESH )
 *			return false and foundstate = PREVIOUS state
 *		C - State visited with f', and current->f -> f' -- (compare gives STATES_SAME_S1_VISITED)
 *			return true, and foundState = NULL.
 *
 *
 */
bool htBlackOrGreyFind(tHtKey key, ptState state, ptHtState* foundState) {

	if(foundState)
		*foundState = NULL;

	ptHtKey keyP = (ptHtKey) malloc(sizeof(tHtKey));

	if(!keyP)
		failure("Out of memory (creating hashtable key).\n");

	*keyP = key;
	ptValue valP;

	valP = search_some(_tableBlackOrGrey, keyP);
	free(keyP);

	if(valP) {
		ptList listElement = valP->list;
		byte compare;
		ptHtState candidateState;

		while(listElement) {
			candidateState = (ptHtState) listElement->value;

			compare = stateCompare(state, candidateState->payload,  candidateState->outerFeatures);

			if(compare != STATES_DIFF) {
				// Cases B, C
					if(compare == STATES_SAME_S1_VISITED) {

						return true;

					} else if(compare == STATES_SAME_S1_FRESH) {

						*foundState = candidateState;

						return false;
					}
			}

			listElement = listElement->next;
		}
	}

	return false;
}
/* End Symbolic DFS Hasthable */


/*
* Checks in the given table if the state exists.
* 	Ignores features.
* 	Depends directly on payload ptr, payload size and key.
*
* 	Preconditions
*
*
*/
bool htFindByKeyAndPayloadNoFeatureCheck(tHtKey key, void * payload,
		unsigned int payloadSize,  ptHtState* foundState,
		struct hashtable* _abstractTable){
	if(foundState)
		*foundState = NULL;

	ptHtKey keyP = (ptHtKey) malloc(sizeof(tHtKey));

	if(!keyP)
		failure("Out of memory (creating hashtable key).\n");

	*keyP = key;
	ptValue valP;

	valP = search_some(_abstractTable, keyP);
	free(keyP);


	if(valP) {
			ptList listElement = valP->list;
			byte compare;
			ptHtState candidateState;

			while(listElement) {
				candidateState = (ptHtState) listElement->value;

				compare = CompareIgnoreFeaturesByKeyAndPayload(payload, payloadSize,
						candidateState->payload,
						candidateState->payloadSize);

				if(compare == true){
					*foundState = candidateState;
					return true;
				}

				listElement = listElement->next;
			}
	}
	return false;
}


/*
 *
 * Checks in the given table if the state exists.
 * 	Ignores features.
 *
 *
 * 	Depends on state only through payloadSize
 *
 */
bool htFindNoFeatureCheck(tHtKey key, ptState state,  ptHtState* foundState,  struct hashtable* _abstractTable){
	if(foundState)
		*foundState = NULL;

	ptHtKey keyP = (ptHtKey) malloc(sizeof(tHtKey));

	if(!keyP)
		failure("Out of memory (creating hashtable key).\n");

	*keyP = key;
	ptValue valP;

	valP = search_some(_abstractTable, keyP);
	free(keyP);


	if(valP) {
			ptList listElement = valP->list;
			bool compare;
			ptHtState candidateState;

			while(listElement) {
				candidateState = (ptHtState) listElement->value;

				compare = stateCompareIgnoreFeatures(state, candidateState->payload);

				if(compare == true){
					*foundState = candidateState;
					return true;
				}

				listElement = listElement->next;
			}
	}

	return false;
}



/*
 *
 * Insert state into table _abstractTable.
 * 	Add a reverse link from state to  ...
 *
 * Precondition: state doesn't exists in table  _abstractTable
 *
 */
void htInsertNoFeaturesAndLink(tHtKey key, ptState state, ptState stateLinkedTo,
		ptBoolFct featuresTransition,
		int transitionWeight,
		struct hashtable* _abstractTable){

	if(!state)
		return;

	ptHtKey keyP = (ptHtKey) malloc(sizeof(tHtKey));
	if(!keyP) failure("Out of memory (creating hashtable key).\n");
	*keyP = key;

	ptValue valP;
	valP = remove_some(_abstractTable, keyP);

	if(!valP) {
		valP = (ptValue) malloc(sizeof(tValue));

		if(!valP)
			failure("Out of memory (creating hashtable value).\n");

		valP->list = NULL;
	}


	ptHtState hs = (ptHtState) malloc(sizeof(tHtState));
	hs->payload = state->payload;
	hs->payloadSize = state->payloadSize;
	hs->foundInDFS = 0;

	hs->originalState = state;
	hs->outerFeatures = state->features;
	hs->innerFeatures = NULL;

	/**** add reverse link right here */
	ptTransAndFExp tmpTrans = (ptTransAndFExp) malloc(sizeof(tTransAndFexp));
	if(!tmpTrans)
		failure("Out of memory (creating transition/fexp for transpose).\n");

	tmpTrans->State = stateLinkedTo ;
	tmpTrans->features = copyBool(featuresTransition);
	tmpTrans->Weight = transitionWeight;
	hs->transposeLinks = listAdd(NULL, tmpTrans);


	valP->list = listAdd(valP->list, hs);
	hs = NULL;

	if(!insert_some(_abstractTable, keyP, valP))
		failure("Out of memory (inserting state in hashtable)\n");


}



/*
 *
 * Insert state into table _abstractTable.
 *
 * Precondition: state doesn't exists in table  _abstractTable
 *
 */
void htInsertNoFeatures(tHtKey key, ptState state, 	struct hashtable* _abstractTable){

	if(!state)
		return;

	ptHtKey keyP = (ptHtKey) malloc(sizeof(tHtKey));
	if(!keyP) failure("Out of memory (creating hashtable key).\n");
	*keyP = key;

	ptValue valP;
	valP = remove_some(_abstractTable, keyP);

	if(!valP) {
		valP = (ptValue) malloc(sizeof(tValue));

		if(!valP)
			failure("Out of memory (creating hashtable value).\n");

		valP->list = NULL;
	}


	ptHtState hs = (ptHtState) malloc(sizeof(tHtState));
	hs->payload = state->payload;
	hs->payloadSize = state->payloadSize;
	hs->foundInDFS = 0;

	hs->originalState = state;
	hs->outerFeatures = state->features;
	hs->innerFeatures = NULL;

	hs->abstractTransposeLinks = NULL;

	valP->list = listAdd(valP->list, hs);
	hs = NULL;

	if(!insert_some(_abstractTable, keyP, valP))
		failure("Out of memory (inserting state in hashtable)\n");
}



/*
 *
 * Search if the reverse(transpose) link  already exists for the given state.
 * If it doesn't then adds it.
 *
 * TODO:
 * 	CHeck if the generated State Machine can have transition (u,v) with same states but diff. weights/features
 *
 */
void addReverseLink(ptHtState foundState, ptState linkToState,
		int currentTransitionWeight,  ptBoolFct featuresTransition){

	ptList Existinglinks =  foundState->transposeLinks;
	bool alreadyPresent = false;

	while(Existinglinks != NULL && alreadyPresent==false){
		ptTransAndFExp currentLinkTo = (ptTransAndFExp) Existinglinks->value;

		if (stateCompareIgnoreFeatures(linkToState, currentLinkTo->State->payload)){
			alreadyPresent = true;
		}
		Existinglinks = Existinglinks->next;
	}

	if(alreadyPresent == false){
		ptTransAndFExp tmpTrans = (ptTransAndFExp) malloc(sizeof(tTransAndFexp));

		if(!tmpTrans)
			failure("Out of memory (creating transition/fexp for transpose).\n");

		tmpTrans->State = linkToState ;
		tmpTrans->features = copyBool(featuresTransition);
		tmpTrans->Weight = currentTransitionWeight;

		foundState->transposeLinks = listAdd(foundState->transposeLinks, tmpTrans);
	}
}


