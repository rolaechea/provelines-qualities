/*
 * STATE HASHTABLE
 * * * * * * * * * * * * * * * * * * * * * * * */
#ifndef WRAPPER_HASH_HASHTABLESTATE_H_
#define WRAPPER_HASH_HASHTABLESTATE_H_

#include <stdbool.h>
#include "hashtable.h"

#include "main.h"
#include "wrapper/features/boolFct.h"
#include "semantics/state.h"


typedef unsigned int   tHtKey;
typedef unsigned int* ptHtKey;

#define DFS_OUTER 1
#define DFS_INNER 2

// A state in the hashtable is a reduced version of a "normal" state
// TODO: we need to keep the payload length here as well if two states have same hash but
// different payloadlength, the memcmp on them will result in a segmentation fault
struct htState {
	void * payload;
	unsigned int payloadSize;
	byte foundInDFS;			// Indicates on which search the state was found: DFS_OUTER or DFS_INNER

								// The inner DFS starts with the deepest violating state of the outer DFS.
								// It is thus guaranteed that all states found during the inner DFS have already been visited by the outer DFS.
								// This is because before it can even reach states NOT visited by the outer search, it must "hit" the prefix
								// of the outer search, and thus stop (because a violation was found).

								// Therefore, the foundInDFS flag DFS_INNER is used to mean that the state was visited by *both*, the inner
								// and the outer DFS.
#ifdef CLOCK
	//ptClockZone zone;
	ptFederation outerFed;
	ptFederation innerFed;
#else
	ptBoolFct outerFeatures;	// The features required to reach the state on the outer DFS
	ptBoolFct innerFeatures;	// The features required to reach the state on the inner DFS
#endif

	ptState originalState;		// Storing a pointer to original state
	ptList transposeLinks;
	ptList abstractTransposeLinks; // Stores a list of abstract transitions.
};

typedef struct htState tHtState;
typedef struct htState * ptHtState;

struct transAndFexp {
	ptState State;
	ptBoolFct features;
	int Weight;
};

typedef struct transAndFexp tTransAndFexp;
typedef struct transAndFexp * ptTransAndFExp;


struct value_ {
    ptList list;
};
typedef struct value_   tValue;
typedef struct value_* ptValue;

int insert_some (struct hashtable *h, tHtKey *k, struct value_ *v);
struct value_ * search_some (struct hashtable *h, tHtKey *k);
struct value_ * remove_some (struct hashtable *h, tHtKey *k);

/*
 * API
 * * * * * * * * * * * * * * * * * * * * * * * */

// Hashtable keeping track of the visited states
void htVisitedStatesInit();
void htVisitedStatesInsert(tHtKey key, ptState state, byte foundInDFS);
byte htVisitedStatesFind(tHtKey key, ptState state, byte foundInDFS, ptHtState* foundState);

// These values should only be read
extern long unsigned int htVisitedStatesNbFilledBuckets;
extern long unsigned int htVisitedStatesNbRecords;

// Hashtable containing states visited during the outer search to get to a violating state
void htOuterStatesInit();
void htOuterStatesInsert(tHtKey key, ptState state);
void htOuterStatesRemove(tHtKey key, void* payload, unsigned int payloadSize);
ptHtState htOuterStatesFind(tHtKey key, ptState state);



// Symbolic DFS  - BlackOrWhite Visited States
void htBlackOrGreyInit();
void htBlackOrGreyInsert(tHtKey key, ptState state);
bool htBlackOrGreyFind(tHtKey key, ptState state, ptHtState* foundState);
struct hashtable* getBlackOrGreyTable();


void printHashTableInnerDetails(struct hashtable* h);

// Find Functions
bool htAbstractFind(tHtKey key, ptState state, ptBoolFct stateFeatures,  ptHtState* foundState,  struct hashtable* _abstractTable);
bool htFindNoFeatureCheck(tHtKey key, ptState state,  ptHtState* foundState,
		struct hashtable* _abstractTable);
bool htFindByKeyAndPayloadNoFeatureCheck(tHtKey key, void * payload,
		unsigned int payloadSize,  ptHtState* foundState,
		struct hashtable* _abstractTable);

void htInsertNoFeaturesAndLink(tHtKey key,
		ptState state,
		ptState stateLinkedTo,
		ptBoolFct featuresTransition,
		int transitionWeight,
		struct hashtable* _abstractTable);

void htInsertNoFeatures(tHtKey key, ptState state,
		struct hashtable* _abstractTable);

void addReverseLink(ptHtState foundState, ptState linkToState,
		int currentTransitionWeight, ptBoolFct featuresTransition);


struct hashtable* create_hashtableWrapper();


/* SCC Computation */

void htAbstractInsertExplicitFeatures(tHtKey key, ptState state,
		ptBoolFct FeatureExpression, struct hashtable* _abstractTable);
void htAbstractInsertExplicitFeaturesDebug(struct hashtable* table, tHtKey key, ptState state, ptBoolFct featureExpression,
		struct hashtable* toPrinttable);

bool  htAbstractReplaceByCopy(tHtKey key,
		ptState state,
		ptBoolFct stateFeatures,
		ptHtState* foundState,
		struct hashtable* _abstractTable);


struct hashtable* shallowHashTableCopy(struct hashtable* abstractTable);

//void htSCCFind(tHtKey key, ptState state, ptBoolFct	Features, ptHtState* foundState, struct hashtable* _abstractTable);

#endif
