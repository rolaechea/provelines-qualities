struct fcz_ {
	ptBoolFct features;
	ptClockZone zone;
};
typedef struct fcz_ tFcz;
typedef struct fcz_ * ptFcz;

typedef ptList ptFederation;

ptFederation createFederation(ptBoolFct features, ptClockZone zone);
void destroyFederation(ptFederation f, byte preserve);
ptFederation addElement(ptFederation f, ptBoolFct features, ptClockZone zone, byte * fresh, ptBoolFct * freshProducts);
ptFederation disjunctFederation(ptFederation f1, ptFederation f2, byte preserveLeft, byte preserveRight);
ptFederation copyFederation(ptFederation f, byte fullCopy);
byte equalsFederation(ptFederation f1, ptFederation f2);
