#define ZONE_INCOMPARABLE	0
#define ZONE_SUBSET			1
#define ZONE_EQUAL			2
#define ZONE_SUPSET			3



typedef void * ptClockZone;
typedef int _clock;

ptClockZone initClockZone();
ptClockZone zoneIntersect(ptClockZone z1, ptClockZone z2, byte preserve);
ptClockZone zoneReset(ptClockZone z, char * c, byte preserve);
ptClockZone zoneResetAll(ptClockZone z);
ptClockZone zoneFuture(ptClockZone z, byte preserve);
ptClockZone zonePast(ptClockZone z, byte preserve);
ptClockZone zoneConstraint(ptClockZone z, char * c1, char * c2, int bound, byte strict, byte preserve);
byte zoneEmpty(ptClockZone z);
void zoneDestroy(ptClockZone z);
ptClockZone zoneCopy(ptClockZone z);
void printZone(ptClockZone z);

byte zoneCompare(ptClockZone z1, ptClockZone z2);

void encodeClocks(ptList clocks);
void encodeClocksInc(ptList clocks);
