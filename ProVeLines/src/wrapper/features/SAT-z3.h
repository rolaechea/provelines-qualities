/*
 * SAT-z3.h
 *
 *  Created on: Apr 23, 2015
 *      Author: rafaelolaechea
 */

#ifndef WRAPPER_FEATURES_SAT_Z3_H_
#define WRAPPER_FEATURES_SAT_Z3_H_

#include <memory.h>
#include <setjmp.h>
#include "error.h"
#include "main.h"
#include "boolFct.h"
#include "test_capi.h"
#include "tvl.h"
#include "SAT.h"

Z3_ast getBaseFD();

Z3_ast getFD();

Z3_ast getAsserts();

Z3_ast createExistingVariableFromConst();


#endif /* WRAPPER_FEATURES_SAT_Z3_H_ */
