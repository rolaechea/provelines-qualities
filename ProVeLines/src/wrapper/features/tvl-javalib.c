
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include "error.h"
#ifdef Z3
#include <z3.h>
#include "test_capi.h"
#endif
#include "main.h"
#include "list.h"
#include "boolFct.h"
#include "tvl.h"



typedef struct NodeFeatureAttributes {
        char * featureName;
        char * attributeName;
        int value;
        struct NodeFeatureAttributes *next;
} NodeFeatureAttributes;

//#ifdef Z3
//Z3_ast _QualityConstraints = NULL;
//#endif


ptBoolFct _featureModelClauses;
ptLList _featureIDMapping;
int _nbFeatures = 0;
int _nbVars = 0;
int maxId = -1;

//#ifdef Z3
//void loadQualityAttributes(char *attributesFile, char *constraintsFile){
//        if(attributesFile != NULL && constraintsFile != NULL){
//                FILE *  fpAttributesFile = fopen(attributesFile,"r");
//                FILE *  fpConstraintsFile = fopen(constraintsFile,"r");
//
//                if (fpAttributesFile == NULL || fpConstraintsFile == NULL){
//                        _QualityConstraints = NULL;
//                } else {
//
//                	/* Load attributes and constraints  */
//                    int numberOfAttributes = 0;
//                    char buffer[1024];
//                    NodeFeatureAttributes *LLRoot = NULL;
//
//                        while(fgets(buffer, 1024, fpAttributesFile) != NULL){
//                                int attributeValue;
//
//                                char *attributeName = malloc(sizeof(char) * 60);
//                                char *featureName = malloc(sizeof(char) * 60);
//                                int numberRead = sscanf(buffer, "%s%s%d", featureName, attributeName, &attributeValue);
//                                if(numberRead==3){
//                                        //printf("Parsing attribute \n");
//                                        NodeFeatureAttributes *LLNode;
//                                        /* Create a Node and push it at the end of the list */
//                                        if(LLRoot==NULL){
//                                                LLRoot = malloc(sizeof(NodeFeatureAttributes));
//                                                LLNode = LLRoot;
//                                        }else {
//                                                NodeFeatureAttributes *iterateNode = LLRoot;
//                                                while(iterateNode->next != NULL){
//                                                        iterateNode = iterateNode->next;
//                                                }
//                                                LLNode =malloc(sizeof(NodeFeatureAttributes));
//                                                iterateNode->next = LLNode;
//                                        }
//                                        LLNode->featureName = featureName;
//                                        LLNode->attributeName = attributeName;
//                                        LLNode->value = attributeValue;
//                                        LLNode->next = NULL;
//                                        numberOfAttributes++;
//                                } else{
//                                        failure("Each line on the Attribute file should have 3 fields separated by space");
//                                        //printf("Skipping Attribute, only read %d ", numberRead);
//                                }
//                        }
//
//                        /* Create Quality Attributes and Constraints */
//                   char cBuffer[1024];
//                   while(fgets(cBuffer, 1024, fpConstraintsFile) != NULL){
//
//                                char *constraintAttributeName = malloc(sizeof(char) * 60);
//                                char *constraintType = malloc(sizeof(char) * 60);
//                                int constraintValue;
//
//                                int constraintFieldsRead = sscanf(cBuffer, "%s%s%d", constraintAttributeName, constraintType, &constraintValue);
//                                if(constraintFieldsRead==3){
//
//                                        // Count Number of Attributed Feature with such atttributeName
//                                        NodeFeatureAttributes *iNode = LLRoot;
//                                        int numberAttributedFeatures = 0;
//                                        while(iNode != NULL){
//                                                if(strcmp(iNode->attributeName, constraintAttributeName)==0){
//                                                        numberAttributedFeatures++;
//                                                        //printf("Adding Attribute i=%d ", numberAttributedFeatures);
//                                                } else {
//                                                        //printf("Discarding Attribute i=%d ", numberAttributedFeatures);
//                                                }
//                                                iNode = iNode->next;
//                                        }
//
//                                        Z3_ast *args = malloc(sizeof(Z3_ast)*numberAttributedFeatures);
//
//                                        int i =0;
//                                        iNode = LLRoot;
//                                        while(iNode != NULL){
//                                                if(strcmp(iNode->attributeName, constraintAttributeName)==0){
//                                                        args[i] = Z3_mk_ite(ctx,
//                                                        		createVariable(iNode->featureName),
//                                                        		mk_int(ctx, iNode->value),
//																mk_int(ctx, 0));
//                                                        i++;
//                                                }
//                                                iNode = iNode->next;
//                                        }
//
//                                        /* Add Constraint about Quality Attributes*/
//                                        if(strcmp(constraintType, "LT") == 0){
//                                            _QualityConstraints = Z3_mk_lt(ctx, Z3_mk_add(ctx, i, args), mk_int(ctx, constraintValue));
//                                        }else if(strcmp(constraintType, "GT") == 0){
//                                            _QualityConstraints = Z3_mk_gt(ctx, Z3_mk_add(ctx, i, args), mk_int(ctx, constraintValue));
//                                        } else {
//                                                failure("Constraints have to be either less than (LT) or greater than (GT), instead got %s \n", constraintType);
//                                        }
//                                }
//                   }
//                }
//        }
//        return ;
//}
//#endif




void loadFeatureModelDimacs(char* clauseFile, char* mappingFile) {

	FILE* fMappingFile = fopen(mappingFile, "r");
	char buffer[1024];
	int id;
	int* pId;
	char* fname;
	if(fMappingFile == NULL){
		failure("Could not find dimacs mapping file.\n");
	}else {
		while(fgets(buffer, 1024, fMappingFile) != NULL) {
			fname = malloc(sizeof(char) * 40);
			pId = malloc(sizeof(int));
			if(!fname || !pId) failure("Out of memory (parsing dimacs file).\n");
			if(sscanf(buffer, "%d %s", &id, fname) == 2) {
				*pId = id;
				maxId = maxId >= id ? maxId : id;
				_featureIDMapping = llistAdd(_featureIDMapping, fname, pId);
				_nbFeatures++;
			} else {
				_nbVars++;
				free(fname);
				free(pId);
			}
		}
		fclose(fMappingFile);
	}

	_featureModelClauses = dimacsFileToBool(clauseFile);

	if(_featureModelClauses == NULL)
		failure("Could not find dimacs clauses file.\n");


}

int loadFeatureModel(char* filename, char* filter) {
	if(copyFile(filename, "__workingfile.tvl")) {
		if(filter != NULL) {
			FILE* fWorkingFile = fopen("__workingfile.tvl", "r");
			char* rootName;
			char buffer[1024];
			byte foundRoot = false;
			while(!foundRoot && fgets(buffer, 1024, fWorkingFile) != NULL) {
				rootName = malloc(sizeof(char) * 40);
				if(!rootName) failure("Out of memory (loading feature model).\n");
				if(sscanf(buffer, "root %s {", rootName) == 1) foundRoot = true;
				else free(rootName);
			}
			fclose(fWorkingFile);

			if(!foundRoot) failure("Could not find the root feature in your TVL file.  Please try again after moving it to the top of the file.\n");
			else {
				char newLine[9 + strlen(rootName) + strlen(filter)];
				sprintf(newLine, "\n%s {\n\t%s;\n}\n", rootName, filter);
				FILE* fWorkingFile = fopen("__workingfile.tvl", "a");
				fwrite(newLine, sizeof(char), 9 + strlen(rootName) + strlen(filter), fWorkingFile);
				fclose(fWorkingFile);
				free(rootName);
			}
		}

		char command[90 + strlen(path) + 1];
		sprintf(command, "java -jar %slib/tvl/TVLParser.jar -dimacs __mapping.tmp __clauses.tmp __workingfile.tvl", path);

		if(system(command) != 0) {
			if(!keepTempFiles) remove("__workingfile.tvl");
			return 0;
		} else {
			loadFeatureModelDimacs("__clauses.tmp", "__mapping.tmp");
			if(!keepTempFiles) remove("__workingfile.tvl");
			if(!keepTempFiles) remove("__mapping.tmp");
			if(!keepTempFiles) remove("__clauses.tmp");
			return 1;
		}
	}
}

ptBoolFct getFeatureModelClauses() {
	return _featureModelClauses;
}



bool  hasNextFeature(ptLList node ){
	return node != NULL;
}

/*
 * Precondition: node !=NULL  === hasNextFeature(node) == true.
 *
 */
char *  getCurrentFeatureLabel(ptLList node ){
	return node->label;
}

/*
 * Precondition: node !=NULL  === hasNextFeature(node) == true.
 *
 */
int  getCurrentFeatureId(ptLList node ){
	return *((int*) (node->value));
}


/*
 *
 * Checks that a feature is not an Artificial feature.
 *
 * Precondition: node !=NULL  === hasNextFeature(node) == true
 *
 */
bool isOriginalFeature(ptLList node){
	char const * featureName = getCurrentFeatureLabel(node);

	return strncmp(featureName, "Artificial", strlen("Artificial"))!=0 ;
}

/*
 *
 * Checks that a feature is not an Artificial feature, nor the dummy root feature (MinePump or Simple for our cases).
 *
 * Precondition: node !=NULL  === hasNextFeature(node) == true
 *
 */
bool isOriginalFeatureNonRoot(ptLList node){
	char const * featureName = getCurrentFeatureLabel(node);

	return strncmp(featureName, "Artificial", strlen("Artificial"))!=0 &&
			strncmp(featureName, "MinePump", strlen("MinePump"))!=0  &&
			strncmp(featureName, "Simple", strlen("Simple"))!=0;
}



/*
 * Precondition: node !=NULL  === hasNextFeature(node) == true.
 *
 */
ptLList  AdvanceFeature(ptLList node ){
	return node->next;
}


ptLList  getfeatureIDMapping(){
	return _featureIDMapping;
}


int getFeatureID(char* name) {
	ptLList id = llistFind(_featureIDMapping, name);
	if(id == NULL) return -1;
	else return *((int*) (id->value));
}

char* getFeatureIDName(int id) {
	ptLList node = _featureIDMapping;
	while(node != NULL) {
		if(*(int *) node->value == id) return node->label;
		node = node->next;
	}
	return NULL;
}

int getNbFeatures() {
	return _nbFeatures;
}

int getNbVariables() {
	return _nbVars;
}

int createMapping(char * name) {
	int id = getFeatureID(name);
	if(id != -1) {
		return id;
	}
	int * pId = (int *) malloc(sizeof(int));
	*pId = maxId+1;
	maxId++;
	int length = strlen(name);
	char * pname = (char*)malloc((length+1)*sizeof(char));
	strcpy(pname,name);
	_featureIDMapping = llistAdd(_featureIDMapping, pname, pId);
	return *pId;
}
