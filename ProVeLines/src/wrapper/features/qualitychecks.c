/*
 * qualitychecks.c
 *
 *  Created on: Apr 23, 2015
 *      Author: rafaelolaechea
 */
#include <stdbool.h>

#include "SAT.h"
#include "tvl.h"
#include "boolFct.h"
#include "test_capi.h"
#include "SAT-z3.h"
#include "qualityparser.h"
#include "qualitychecks.h"


Z3_ast _readBBDFormula(const char *filename, int numberOfLines);



bool getStringToBool(char const  *valZ3String){
	bool ret_value;

	if (valZ3String == NULL){
		failure("Received Null Value for a feature value (unassigned)\n");
	} else if (strcmp(valZ3String, "true")==0){
		ret_value = true;
	} else if (strcmp(valZ3String, "false")==0){
		ret_value = false;
	} else {
		failure("Received Unrecognized value for a feature (neither true nor false)\n");
	}

	return ret_value;
}




Z3_ast _readBBDFormula(const char *filename, int numberOfLines) {
		printf("Readding bdd file %s \n", filename);
		FILE * stream = fopen(filename, "r");
		char c = 'c';
		char * feature;
		char * operator;
		int i = 0;
		byte nextliteral = 0;
		byte nextminterm = 0;



		bool allClausesInit = false;
		ptBoolFct allClauses = NULL;

		ptBoolFct currentClause = getTrue();

		printf("(");
		while(c != EOF && fscanf(stream, "%c", &c) != EOF) {
			if(c == '0') {

				feature = getFeatureIDName(i);

				if(nextliteral){
					operator = (" & !");
					currentClause = addConjunction(currentClause, negateBool(createVariable(feature)),0, 0);
				}
				else if (nextminterm){
					operator = (") | (!");
					if(allClausesInit==false){
						allClauses = currentClause;
						currentClause = addConjunction(getTrue(), negateBool(createVariable(feature)),0, 0);
						allClausesInit = true;
					}else {
						allClauses = addDisjunction(allClauses, currentClause,0, 0);
						currentClause = addConjunction(getTrue(), negateBool(createVariable(feature)),0, 0);
					}
				}else {
					operator = "!";
					currentClause = addConjunction(currentClause, negateBool(createVariable(feature)),0, 0);
				}


				printf("%s%s", operator, feature);

				nextliteral = 1;
				nextminterm = 0;
				i++;
			} else if (c == '1') {
				feature = getFeatureIDName(i);

				if(nextliteral){
					operator = (" & ");
					currentClause = addConjunction(currentClause, createVariable(feature),0, 0);
				}
				else if (nextminterm){
					operator = (") | (");
					if(allClausesInit==false){
						allClauses = currentClause;
						currentClause = addConjunction(getTrue(), createVariable(feature),0, 0);
						allClausesInit = true;
					}else {
						allClauses = addDisjunction(allClauses, currentClause,0, 0);
						currentClause = addConjunction(getTrue(), createVariable(feature),0, 0);
					}
				}
				else {
					operator = "";
					currentClause = addConjunction(currentClause, createVariable(feature),0, 0);
				}


				printf("%s%s", operator, feature);

				i++;
				nextliteral = 1;
				nextminterm = 0;
			} else if(c == ' ') {

				while(c != '\n' && fscanf(stream, "%c", &c) != EOF);

				nextliteral = 0;
				nextminterm = 1;
				i = 0;
			} else {
				i++;
			}
		}
		printf(")");

		if 	(allClausesInit == true){
			allClauses = addDisjunction(allClauses, currentClause,0, 0);
		} else {
			allClauses =  currentClause;
		}

		printf("\n Z3 Equivalent\n");
		printBool(allClauses);
		printf("\n");
		fclose(stream);

		return allClauses;
}

/*
 * Enumerates all products that Satisfy the Quality Constraints.
 *
 *byte enumerateAllSatisfyingProducts(ptBoolFct formula);
*
 *Enumerates all products that violate the Quality Constraints.
*
 *byte enumerateAllVioatingProducts(ptBoolFct formula){
 */
void enumerateAllSatisfyingProducts(const char *CombineFilename, int numberOfLines){
#ifdef DEBUG
        printf("Begin isSatFD-With-QualityConstraints\n");
#endif
#ifdef TIME
        satCalls++;
        clock_t c = clock();
#endif

        if(!ctx)
                initBoolFct();

        if(!slv)
                initSolverWithFD(NULL);


        Z3_solver_push(ctx, slv);

        Z3_ast BaseFD = getBaseFD();

        Z3_solver_assert(ctx, slv, BaseFD);

        Z3_ast FD = getFD();

        if(FD)
                Z3_solver_assert(ctx, slv, FD);


        Z3_ast qualityConstraints = getFeatureModelQualityClauses();

        byte QualityConstraintsAsserted = false;
        if (qualityConstraints){
                QualityConstraintsAsserted = true;
                Z3_solver_assert(ctx, slv,  qualityConstraints);
        }

        Z3_ast productSet = NULL;

        if (CombineFilename!=NULL){
        	productSet =  _readBBDFormula(CombineFilename, 1);
        	Z3_solver_assert(ctx, slv,  productSet);
        }

        Z3_lbool res = Z3_solver_check(ctx, slv);




		// Now generate all products.
		byte satCallHasFailed = false;
		int j=0;
		while( ! satCallHasFailed ){


			if (res == Z3_L_TRUE){
				j++;

//				printf("Instance %d \n", j);

				Z3_model m =  Z3_solver_get_model(ctx, slv);

				int i = 0;

				ptBoolFct allClauses = getTrue();

				ptLList allNodes = getfeatureIDMapping();

				while(hasNextFeature(allNodes)){
					char const * featureName = getCurrentFeatureLabel(allNodes);
					if(strncmp(featureName, "Artificial", strlen("Artificial"))!=0){
						Z3_ast curClause = NULL;


			             Z3_ast featureValueAst = NULL;
			             Z3_model_eval(ctx,  m, createExistingVariableFromConst(featureName), Z3_L_TRUE, &featureValueAst);

			             bool fValue= getStringToBool(Z3_ast_to_string(ctx, featureValueAst)) ;


			         	if (	fValue  == true ) {
			         		curClause = Z3_simplify(ctx, createExistingVariableFromConst(featureName));
			         	} else {
			         		curClause = Z3_simplify(ctx, negateBool(Z3_simplify(ctx, createExistingVariableFromConst(featureName))));
			         	}

			         	allClauses = addConjunction(allClauses, curClause,  0, 0);

					} else {
						// skip Artificial Feature
					}

					allNodes = AdvanceFeature(allNodes);
				}

//				printf("Asserting negation of found instance \n");

				allClauses =  negateBool(allClauses);

//				printBool(allClauses);

				Z3_solver_assert(ctx, slv, allClauses);


				res = Z3_solver_check(ctx, slv);


			} else {
				satCallHasFailed =  true;
			}
		}

		printf("Total Instance %d \n", j);
}



void printInstance(){
	/*
    if (res == Z3_L_TRUE){

        Z3_model m =  Z3_solver_get_model(ctx, slv);

        int numVariables = Z3_get_model_num_constants(ctx, m);
        printf("Model numVariables=%d, \n %s", numVariables , Z3_model_to_string(ctx, m));

        int i =0;


        for (i; i <numVariables ; i++){
			Z3_func_decl fnDecl = Z3_get_model_constant(ctx, m, i);
			Z3_symbol fnName = Z3_get_decl_name(ctx, fnDecl);


			printf("Symbol Name %s \n", Z3_get_symbol_string(ctx, fnName));
			Z3_ast fnVal = Z3_model_get_const_interp(ctx, m, fnDecl);
			printf("Symbol Value = <<%s>> \n ", Z3_ast_to_string(ctx, fnVal));



        }
        //printf("Model \n %s", Z3_model_to_string(ctx, m));

    } */

}



byte isSatisfiableWithBaseFDAndQualityConstraints(ptBoolFct formula){
#ifdef DEBUG
        printf("Begin isSatFD-With-QualityConstraints\n");
#endif
#ifdef TIME
        satCalls++;
        clock_t c = clock();
#endif

        if(!ctx)
                initBoolFct();

        if(!slv)
                initSolverWithFD(NULL);


        Z3_solver_push(ctx, slv);

        Z3_ast BaseFD = getBaseFD();

        Z3_solver_assert(ctx, slv, BaseFD);

        Z3_ast FD = getFD();

        if(FD)
                Z3_solver_assert(ctx, slv, FD);

        if(formula!=NULL)
                Z3_solver_assert(ctx, slv, formula);

        Z3_ast qualityConstraints = getFeatureModelQualityClauses();

        byte QualityConstraintsAsserted = false;
        if (qualityConstraints){
                QualityConstraintsAsserted = true;
                Z3_solver_assert(ctx, slv,  qualityConstraints);
        }

        Z3_lbool res = Z3_solver_check(ctx, slv);

        Z3_solver_pop(ctx, slv, 1);


        if (res == Z3_L_TRUE){

             Z3_model m =  Z3_solver_get_model(ctx, slv);

             int numVariables = Z3_get_model_num_constants(ctx, m);
             printf("Number of Features = %d\n", getNbFeatures());

             printf("Model numVariables=%d, \n %s", numVariables , Z3_model_to_string(ctx, m));


             printf("Evaluating MethaneQuery");
             Z3_ast v = NULL;
             Z3_model_eval(ctx,  m, createVariable("MethaneQuery"), Z3_L_TRUE, &v);

             printf("\nZ3 ast to string  is  %s \n", Z3_ast_to_string(ctx, v));


             printBool(v);

        }

#ifdef TIME
        satTime += (clock() - c);
#endif
        return (res == Z3_L_TRUE);

}
