#ifndef WRAPPER_FEATURES_BOOLFCT_H_
#define WRAPPER_FEATURES_BOOLFCT_H_
#include "main.h"

typedef void * ptBoolFct;

#ifdef Z3
	#include <z3.h>
	Z3_context ctx;
#endif

void initBoolFct();

ptBoolFct dimacsFileToBool(char * filePath);

ptBoolFct createLiteral(int lit);
ptBoolFct createVariable(char * name);
#ifdef ATTR
	ptBoolFct createConstraint(ptBoolFct attr, int value, int type);
	ptBoolFct createConstraintVar(ptBoolFct var1, ptBoolFct var2, int type);
	ptBoolFct createAttribute(char * name);
	ptBoolFct createConstant(int value);
#endif
ptBoolFct addConjunction(ptBoolFct leftFct, ptBoolFct rightFct, byte preserveLeft, byte preserveRight);
ptBoolFct addDisjunction(ptBoolFct leftFct, ptBoolFct rightFct, byte preserveLeft, byte preserveRight);
ptBoolFct copyBool(ptBoolFct formula);
ptBoolFct negateBool(ptBoolFct formula);
ptBoolFct getTrue();
void destroyBool(ptBoolFct formula);
void printBool(ptBoolFct formula);

#endif
