#ifndef WRAPPER_FEATURES_TVL_H
#define WRAPPER_FEATURES_TVL_H

#include <stdbool.h>
#include "list.h"


void loadFeatureModelDimacs(char* clauseFile, char* mappingFile);
int loadFeatureModel(char* filename, char* filter);
ptBoolFct getFeatureModelClauses();
int getFeatureID(char* name);
char* getFeatureIDName(int id);
int getNbFeatures();
int getNbVariables();
int createMapping(char * name);

bool  hasNextFeature(ptLList node );
bool isOriginalFeature(ptLList node);
bool isOriginalFeatureNonRoot(ptLList node);
char *  getCurrentFeatureLabel(ptLList node );
int  getCurrentFeatureId(ptLList node );


ptLList  AdvanceFeature(ptLList node );
ptLList  getfeatureIDMapping();


#endif
