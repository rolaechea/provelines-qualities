#include <memory.h>
#include <setjmp.h>
#include "error.h"
#include "main.h"
#include "boolFct.h"
#include "test_capi.h"
#include "tvl.h"
#include "SAT.h"
#include "SAT-z3.h"

static Z3_ast FD = NULL;
static Z3_ast BaseFD;
static Z3_ast asserts = NULL;


Z3_ast getBaseFD(){
	return BaseFD;
}

Z3_ast getFD(){
	return FD;
}

Z3_ast getAsserts(){
	return asserts;
}


void initSolverWithFD(ptBoolFct formula) {
        if(!ctx)
        	initBoolFct();

        slv = Z3_mk_solver(ctx);

        FD = getTrue();

        FD = addConjunction(getFeatureModelClauses(), FD, 0, 0);
        BaseFD = FD;
        Z3_solver_inc_ref(ctx, slv);
}

byte addConstraintToFD(ptBoolFct formula) {
#ifdef DEBUG
	printf("Begin addConstraintToFD\n");
#endif
	if(!ctx) initBoolFct();
	if(!FD) FD = formula;
	else FD = addConjunction(FD, formula, 0, 0);
	return isSatisfiableWrtFD(getTrue());
}


byte isSatisfiable(ptBoolFct formula) {
#ifdef DEBUG
	printf("Begin isSat\n");
#endif
#ifdef TIME
	satCalls++;
	clock_t c = clock();
#endif
	if(!ctx) initBoolFct();
	if(!slv) initSolverWithFD(NULL);
	Z3_solver_push(ctx, slv);
	if(formula)
		Z3_solver_assert(ctx, slv, formula);
	Z3_lbool res = Z3_solver_check(ctx, slv);
	Z3_solver_pop(ctx, slv, 1);
#ifdef TIME
	satTime += (clock() - c);
#endif
	return (res == Z3_L_TRUE);
}

byte isSatisfiableWrtFD(ptBoolFct formula) {
#ifdef DEBUG
	printf("Begin isSatFD\n");
#endif
#ifdef TIME
	satCalls++;
	clock_t c = clock();
#endif
	if(!ctx) initBoolFct();
	if(!slv) initSolverWithFD(NULL);
	Z3_solver_push(ctx, slv);
	Z3_solver_assert(ctx, slv, BaseFD);
	if(FD)
		Z3_solver_assert(ctx, slv, FD);
	if(formula)
		Z3_solver_assert(ctx, slv, formula);
	Z3_lbool res = Z3_solver_check(ctx, slv);
	Z3_solver_pop(ctx, slv, 1);
#ifdef TIME
	satTime += (clock() - c);
#endif
	return (res == Z3_L_TRUE);
}

byte isSatisfiableWrtBaseFD(ptBoolFct formula) {
#ifdef DEBUG
	printf("Begin isSatBaseFD\n");
#endif
#ifdef TIME
	satCalls++;
	clock_t c = clock();
#endif
	if(!ctx) initBoolFct();
	if(!slv) initSolverWithFD(NULL);
	Z3_solver_push(ctx, slv);
	Z3_solver_assert(ctx, slv, BaseFD);
	if(formula)
		Z3_solver_assert(ctx, slv, formula);
	Z3_lbool res = Z3_solver_check(ctx, slv);
	Z3_solver_pop(ctx, slv, 1);
#ifdef TIME
	satTime += (clock() - c);
#endif
	return (res == Z3_L_TRUE);
}


byte implies(ptBoolFct fct_a, ptBoolFct fct_b) {
#ifdef DEBUG
	printf("Begin implies: %s => %s\n",fct_a ? Z3_ast_to_string(ctx, fct_a) : "NULL", fct_b ? Z3_ast_to_string(ctx, fct_b) : "NULL");
#endif
#ifdef TIME
	junCalls += 2;
	clock_t c = clock();
#endif
	Z3_ast notImplies = Z3_mk_not(ctx, Z3_mk_implies(ctx, fct_a, fct_b));
#ifdef TIME
	junTime += (clock() - c);
#endif
	return !isSatisfiable(notImplies);
}

byte impliesWrtFD(ptBoolFct fct_a, ptBoolFct fct_b) {
#ifdef DEBUG
	printf("Begin impliesFD\n");
#endif
#ifdef TIME
	junCalls += 2;
	clock_t c = clock();
#endif
	Z3_ast notImplies = Z3_mk_not(ctx, Z3_mk_implies(ctx, fct_a, fct_b));
#ifdef TIME
	junTime += (clock() - c);
#endif
	return !isSatisfiableWrtFD(notImplies);
}

byte impliesWrtBaseFD(ptBoolFct fct_a, ptBoolFct fct_b) {
#ifdef DEBUG
	printf("Begin impliesBaseFD\n");
#endif
#ifdef TIME
	junCalls += 2;
	clock_t c = clock();
#endif
	Z3_ast notImplies = Z3_mk_not(ctx, Z3_mk_implies(ctx, fct_a, fct_b));
#ifdef TIME
	junTime += (clock() - c);
#endif
	return !isSatisfiableWrtBaseFD(notImplies);
}

byte equals(ptBoolFct fct_a, ptBoolFct fct_b) {
	if(!fct_b) return (!fct_a || isTautology(fct_b));
	if(!fct_a) return isTautology(fct_b);

	byte result = 0;
	ptBoolFct notB = negateBool(fct_b);
	ptBoolFct notA = negateBool(fct_a);
	ptBoolFct a_notImplies_b = addConjunction(fct_a, notB, 1, 0);
	ptBoolFct b_notImplies_a = addConjunction(fct_b, notA, 1, 0);
	ptBoolFct a_notEquals_b = addDisjunction(a_notImplies_b, b_notImplies_a, 0, 0);
	if(a_notEquals_b && !isSatisfiable(a_notEquals_b)) result = 1;
	destroyBool(a_notImplies_b);

	return result;
}

byte equalsWrtFD(ptBoolFct fct_a, ptBoolFct fct_b) {
	if(!fct_b) return (!fct_a || isTautologyWrtFD(fct_b));
	if(!fct_a) return isTautologyWrtFD(fct_b);

	byte result = 0;
	ptBoolFct notB = negateBool(fct_b);
	ptBoolFct notA = negateBool(fct_a);
	ptBoolFct a_notImplies_b = addConjunction(fct_a, notB, 1, 0);
	ptBoolFct b_notImplies_a = addConjunction(fct_b, notA, 1, 0);
	ptBoolFct a_notEquals_b = addDisjunction(a_notImplies_b, b_notImplies_a, 0, 0);
	if(a_notEquals_b && !isSatisfiableWrtFD(a_notEquals_b)) result = 1;
	destroyBool(a_notImplies_b);

	return result;
}

byte equalsWrtBaseFD(ptBoolFct fct_a, ptBoolFct fct_b) {
	if(!fct_b) return (!fct_a || isTautologyWrtBaseFD(fct_b));
	if(!fct_a) return isTautologyWrtBaseFD(fct_b);

	byte result = 0;
	ptBoolFct notB = negateBool(fct_b);
	ptBoolFct notA = negateBool(fct_a);
	ptBoolFct a_notImplies_b = addConjunction(fct_a, notB, 1, 0);
	ptBoolFct b_notImplies_a = addConjunction(fct_b, notA, 1, 0);
	ptBoolFct a_notEquals_b = addDisjunction(a_notImplies_b, b_notImplies_a, 0, 0);
	if(a_notEquals_b && !isSatisfiableWrtBaseFD(a_notEquals_b)) result = 1;
	destroyBool(a_notImplies_b);

	return result;
}


byte isTautology(ptBoolFct formula) {
#ifdef DEBUG
	printf("Begin Tautology\n");
#endif
	ptBoolFct neg = negateBool(formula);
	return !isSatisfiable(neg);
}

byte isTautologyWrtFD(ptBoolFct formula) {
#ifdef DEBUG
	printf("Begin TautologyFD\n");
#endif
	ptBoolFct neg = negateBool(formula);
	return !isSatisfiableWrtFD(neg);
}

byte isTautologyWrtBaseFD(ptBoolFct formula) {
#ifdef DEBUG
	printf("Begin TautologyBase\n");
#endif
	ptBoolFct neg = negateBool(formula);
	return !isSatisfiableWrtBaseFD(neg);
}
