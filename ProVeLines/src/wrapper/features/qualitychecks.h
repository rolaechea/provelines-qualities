/*
 * qualitychecks.h
 *
 *  Created on: Apr 23, 2015
 *      Author: rafaelolaechea
 */

#ifndef WRAPPER_FEATURES_QUALITYCHECKS_H_
#define WRAPPER_FEATURES_QUALITYCHECKS_H_

#include <stdbool.h>
#include "main.h"
#include "boolFct.h"

byte isSatisfiableWithBaseFDAndQualityConstraints(ptBoolFct formula);
void enumerateAllSatisfyingProducts(const char *CombineFilename, int numberOfLines);
bool getStringToBool(char const  *valZ3String);



#endif /* WRAPPER_FEATURES_QUALITYCHECKS_H_ */
