#ifndef WRAPPER_FEATURES_SAT_H_
#define WRAPPER_FEATURES_SAT_H_
#include "boolFct.h"

#ifdef Z3
	#include <z3.h>
	Z3_solver slv;
#endif

/**
 * Initializes the solver with a formula.
 */
void initSolverWithFD(ptBoolFct formula);


/**
 * Modifies the FD formula by adding a constraint to it.
 * The resulting FD formula will be equal to: 'old.FD' && 'formula'.
 * WARNING: The formula of the constraint is NOT preserved.
 *
 * Returns:
 * 	- 1 iff the resulting FD is satisfiable (i.e. iff ('old.FD' && 'formula') is satisfiable)
 * 	- 0 otherwise.
 */
byte addConstraintToFD(ptBoolFct formula);


/**
 * Checks whether or not a formula is satisfiable.
 */
byte isSatisfiable(ptBoolFct boolfct);
byte isSatisfiableWrtFD(ptBoolFct boolfct);
byte isSatisfiableWrtBaseFD(ptBoolFct boolfct);


/**
 * Returns:
 * 	- 1 iff a -> b,
 *  - 0 otherwise.
 */
byte implies(ptBoolFct fct_a, ptBoolFct fct_b);
byte impliesWrtFD(ptBoolFct fct_a, ptBoolFct fct_b);
byte impliesWrtBaseFD(ptBoolFct fct_a, ptBoolFct fct_b);


/**
 * Returns:
 * 	- 1 iff a <-> b,
 *  - 0 otherwise.
 */
byte equals(ptBoolFct fct_a, ptBoolFct fct_b);
byte equalsWrtFD(ptBoolFct fct_a, ptBoolFct fct_b);
byte equalsWrtBaseFD(ptBoolFct fct_a, ptBoolFct fct_b);


/**
 * Checks whether a formula is a tautology (negates it, which is costly).
 */
byte isTautology(ptBoolFct boolfct);
byte isTautologyWrtFD(ptBoolFct boolfct);
byte isTautologyWrtBaseFD(ptBoolFct boolfct);


#endif
