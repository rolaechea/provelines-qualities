/*
 * Generic lists
 * * * * * * * * * * * * * * * * * * * * * * * */
#ifndef HELPER_LIST_H
#define HELPER_LIST_H

struct list_ {
	void* value;
	struct list_* next;	// Points to next node; the last node in a list has this pointer set to NULL.
	struct list_* prev;	// Points to previous node; the first node in a list has this pointer pointing to the last node!
};
typedef struct list_   tList;
typedef struct list_* ptList;

struct labeledList_ {
	char* label;
	void* value;
	struct labeledList_* next;	// Points to next node; the last node in a list has this pointer set to NULL.
	struct labeledList_* prev;	// Points to previous node; the first node in a list has this pointer pointing to the last node!
};
typedef struct labeledList_   tLList;
typedef struct labeledList_* ptLList;

ptList listAdd(ptList list, void* value);
void listAddAfterCurrentItemNonEmptyList(ptList list, void* value);
ptList listAddHead(ptList list, void* value);
ptList listRemove(ptList list, void* value);
ptList listFind(ptList list, void* value);
void listDestroy(ptList list);
void listPrint(ptList list);
int listCount(ptList list);
ptList listConcat(ptList a, ptList b);
ptList listCopy(ptList list);

ptLList llistAdd(ptLList list, char* label, void* value);
ptLList llistRemoveLabel(ptLList list, char* label);
ptLList llistRemove(ptLList list, void* value);
ptLList llistFind(ptLList list, char* label);
void llistDestroy(ptLList list);
void llistPrint(ptLList list);
int llistCount(ptLList list);
ptLList llistMerge(ptLList a, ptLList b);

ptLList list2llist(ptList list);
ptLList list2llistConcat(ptLList prevList, ptList newList);

#endif
