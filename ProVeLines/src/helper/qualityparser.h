/*
 * qualityparser.h
 *
 *  Created on: Apr 20, 2015
 *      Author: rafaelolaechea
 */

#ifndef HELPER_QUALITYPARSER_H_
#define HELPER_QUALITYPARSER_H_
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "error.h"

#include <z3.h>
#include "boolFct.h"
#include "test_capi.h"


void loadQualityAttributes(char *attributesFile);
void createConstraints(char *constraintsFile);
Z3_ast getFeatureModelQualityClauses();

#endif /* HELPER_QUALITYPARSER_H_ */
