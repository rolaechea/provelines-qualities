
// Generic bug display function
#ifndef HELPER_ERROR_H_
#define HELPER_ERROR_H_
void failure(char* msg, ...);
void warning(char* msg, ...);
#endif
