/*
 * qualityparser.c
 *
 *  Created on: Apr 20, 2015
 *      Author: rafaelolaechea
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "error.h"

#include <z3.h>
#include "boolFct.h"
#include "qualityparser.h"
#include "test_capi.h"



typedef struct NodeFeatureAttributes {
        char * featureName;
        char * attributeName;
        int value;
        struct NodeFeatureAttributes *next;
} NodeFeatureAttributes;

NodeFeatureAttributes *LLRoot = NULL;


Z3_ast _QualityConstraints = NULL;


Z3_ast getFeatureModelQualityClauses() {
	return _QualityConstraints;
}

void loadQualityAttributes(char *attributesFile){

        if(attributesFile != NULL ){
                FILE *  fpAttributesFile = fopen(attributesFile,"r");

                if (fpAttributesFile == NULL ){
#ifdef Z3
                        _QualityConstraints = NULL;
#endif
                } else {
                	/* Load attributes  */
                    int numberOfAttributes = 0;
                    char buffer[1024];
					while(fgets(buffer, 1024, fpAttributesFile) != NULL){
							int attributeValue;

							char *attributeName = malloc(sizeof(char) * 60);
							char *featureName = malloc(sizeof(char) * 60);
							int numberRead = sscanf(buffer, "%s%s%d", featureName, attributeName, &attributeValue);
							if(numberRead==3){
									//printf("Parsing attribute \n");
									NodeFeatureAttributes *LLNode;

									/* Create a Node and push it at the end of the list */
									if(LLRoot==NULL){
											LLRoot = malloc(sizeof(NodeFeatureAttributes));
											LLNode = LLRoot;
									}else {
											NodeFeatureAttributes *iterateNode = LLRoot;
											while(iterateNode->next != NULL){
													iterateNode = iterateNode->next;
											}
											LLNode =malloc(sizeof(NodeFeatureAttributes));
											iterateNode->next = LLNode;
									}

									LLNode->featureName = featureName;
									LLNode->attributeName = attributeName;
									LLNode->value = attributeValue;
									LLNode->next = NULL;
									numberOfAttributes++;
							} else{
									failure("Each line on the Attribute file should have 3 fields separated by space");
									//printf("Skipping Attribute, only read %d ", numberRead);
							}
					} // End-While
                } // End-else If Fp-AttributesFile
        } // End-if attributesFile!= NULL
}



void createConstraints(char *constraintsFile){
    char cBuffer[1024];
    FILE *  fpConstraintsFile = fopen(constraintsFile,"r");

    while(fgets(cBuffer, 1024, fpConstraintsFile) != NULL){
		 char *constraintAttributeName = malloc(sizeof(char) * 60);
		 char *constraintType = malloc(sizeof(char) * 60);
		 int constraintValue;

		 int constraintFieldsRead = sscanf(cBuffer, "%s%s%d", constraintAttributeName, constraintType, &constraintValue);

		 if(constraintFieldsRead==3){
			 // Count Number of Attributed Feature with such atttributeName
			 NodeFeatureAttributes *iNode = LLRoot;
			 int numberAttributedFeatures = 0;
			 while(iNode != NULL){
					 if(strcmp(iNode->attributeName, constraintAttributeName)==0){
							 numberAttributedFeatures++;
					 } else {
					 }
					 iNode = iNode->next;
			 }

			 Z3_ast *args = malloc(sizeof(Z3_ast)*numberAttributedFeatures);

			 int i =0;
			 iNode = LLRoot;
			 while(iNode != NULL){
					 if(strcmp(iNode->attributeName, constraintAttributeName)==0){
							 args[i] = Z3_mk_ite(ctx, createVariable(iNode->featureName), mk_int(ctx, iNode->value), mk_int(ctx, 0));
							 i++;
					 }
					 iNode = iNode->next;
			 }
			 /* Add Constraint about Quality Attributes*/
			 if(strcmp(constraintType, "LT") == 0){
				 _QualityConstraints = Z3_mk_lt(ctx, Z3_mk_add(ctx, i, args), mk_int(ctx, constraintValue));
			 }else if(strcmp(constraintType, "GT") == 0){
				 _QualityConstraints = Z3_mk_gt(ctx, Z3_mk_add(ctx, i, args), mk_int(ctx, constraintValue));
			 } else {
					 failure("Constraints have to be either less than (LT) or greater than (GT), instead got %s \n", constraintType);
			 }
		 }
    }
}







