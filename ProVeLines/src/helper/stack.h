/*
 * STACK
 * * * * * * * * * * * * * * * * * * * * * * * */
#ifndef HELPER_STACK_H
#define HELPER_STACK_H

struct stack_ {
	void * value;
	struct stack_ * prev;
};

typedef struct stack_ stack;
typedef struct stack_ * ptStack;

/* API */
void destroyStack(ptStack stackPtr);
void * top(ptStack stackPtr);
ptStack push(ptStack stackPtr, void * value);
ptStack getEmptyStack();
void * pop(ptStack * stackPtr);
unsigned char empty(ptStack stackPtr);
unsigned char onStack(ptStack stackPtr, void *, int length);
int countStack(ptStack stack);
ptStack reverse(ptStack stackPtr, unsigned char preserve);

#endif
