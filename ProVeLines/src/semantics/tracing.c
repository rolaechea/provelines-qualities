#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include "transitions.pb-c.h"
#include "error.h"
#include "main.h"
#include "list.h"
#include "stack.h"
#include "hashtableGen.h"
#include "boolFct.h"
#include "symbols.h"
#include "automata.h"
#include "SAT.h"
#include "tvl.h"
#ifdef CLOCK
	#include "clockZone.h"
#endif
#include "state.h"
#include "execution.h"
#include "boolFct.h"

static 		unsigned int _nbErrorsDiscard = 0;

static ptFsmTrans *allTransitionsByUniqueId = NULL;
static ATransition  ** _allReadTransitions = NULL ;
static int  _numberAllReadTransitions = 0;

static void setUpTransitionLookUpTable(ptSymTabNode globalSymTab, ptMTypeNode mtypes,
		ATransition **AllTransitionArray, int numTransitions );

int getNumberOfTransitions() {
	return _numberAllReadTransitions;
}



ptFsmTrans getTransitionInProvelinesFormatFromIdentifier(int Identifier){
	return allTransitionsByUniqueId[Identifier-1];
}

/*
 * Builds a formula representing products that are compatible with a list of transitions.
 *
 */
void CheckCompatibleProducts(ptList transitionIds, ptSymTabNode globalSymTab, ptMTypeNode mtypes){
	ptBoolFct CompatibleProducts = NULL;


	while(transitionIds!=NULL){
		int transitionId =  *((int *)transitionIds->value);

		ptFsmTrans trans = getTransitionInProvelinesFormatFromIdentifier(transitionId);


		CompatibleProducts = addConjunction(CompatibleProducts,trans->features, true, true);

		transitionIds = transitionIds->next;
	}

	printf("Compatible Products\n");
	if (CompatibleProducts){
		printBool(CompatibleProducts);
	} else {
		printf("All Products");
	}
	printf("\n");

}


/*
 * Parses a list of comma separated transition identifiers (integers) and returns a list of integers representing them.
 */
ptList parseListOfTransitionIds(char *TraceFile){
	ptList ptListTransitionsIdentifiers = NULL;

	if (!TraceFile){
		failure("No trace file given, unable to parse it. \n");
	}

	FILE*  file = fopen(TraceFile, "r");

	if (!file){
		failure("Unable to open trace file %s\n" , TraceFile == NULL ? "Null" : TraceFile );
	}



	fseek(file, 0, SEEK_END);
	size_t  size = ftell(file);
	fseek(file, 0, SEEK_SET);

	int numberCharacters = size + 10 ;

	char *rawLine = malloc(sizeof(char) *numberCharacters );

	fgets(rawLine,numberCharacters,file);

	char * pch=strtok(rawLine,",");

	while (pch != NULL)
	  {
	    int Identifier =  atoi(pch);

	    int *IdentifierOnHeap = malloc(sizeof(int));
	    *IdentifierOnHeap = Identifier;

		ptListTransitionsIdentifiers = listAdd(ptListTransitionsIdentifiers, IdentifierOnHeap);

	    pch = strtok (NULL, ",");
	 }

	return ptListTransitionsIdentifiers;
}

/*
 * Counts the number of available transitions.
 *
 */
int countTotalTransitions(ptSymTabNode globalSymTab, ptMTypeNode mtypes){
	ptSymTabNode cur = globalSymTab;
	int numberTransitionsTotal = 0;

	if(cur != NULL) {
		while(cur != NULL) {
			if(cur->type == T_PROC || cur->type == T_NEVER) {
				if(cur->fsm != NULL){
					int nbtrans = listCount(cur->fsm->transitions);
					numberTransitionsTotal += nbtrans;
				}
			}
			cur = cur->next;
		}
	}
	return numberTransitionsTotal;
}

// Initializes Transition lookup table and serializes a list of all available transitions into a binary file.
void InitializeAndSerializeTransitions(ptSymTabNode globalSymTab, ptMTypeNode mtypes, byte serializeTransitions){
	ptSymTabNode cur = globalSymTab;

	AvailableTransitions allTransitions = AVAILABLE_TRANSITIONS__INIT;

	int numTransitions  = countTotalTransitions(globalSymTab, mtypes);

	allTransitions.n_transition = numTransitions;
	allTransitions.transition = malloc(sizeof (ATransition*) * allTransitions.n_transition);

	allTransitionsByUniqueId =  malloc(sizeof(ptFsmTrans) * allTransitions.n_transition);

	/* Go through each transition and add it to the list */
	int i=0;
	int uniqueId = 1;
	ptSymTabNode init_cur = cur;

	if(cur != NULL) {
		while(cur != NULL) {
			if(cur->type == T_PROC || cur->type == T_NEVER) {
				if(cur->fsm != NULL){
					ptList transitions = cur->fsm->transitions;

					while(transitions!=NULL){
						ptFsmTrans trans = (ptFsmTrans) transitions->value;

						allTransitions.transition[i] = malloc(sizeof(ATransition));
						atransition__init(allTransitions.transition[i]);

						allTransitions.transition[i]->uniqueidentifier = uniqueId;
						allTransitions.transition[i]->expressiontype = trans->expression ? trans->expression->type : -1;
						allTransitions.transition[i]->linenb = trans->lineNb;
						allTransitions.transition[i]->sourcelinenb = trans->source->lineNb;
						allTransitions.transition[i]->targetlinenb = trans->target ? trans->target->lineNb : -1  ;
						allTransitions.transition[i]->processname =  cur->name;

						allTransitionsByUniqueId[uniqueId-1] = trans;
						transitions = transitions->next;
						uniqueId++;
						i++;
					}
				}
			}
			cur = cur->next;
		}
	}


	if(serializeTransitions){
		unsigned len = available_transitions__get_packed_size(&allTransitions);
		void *buf = malloc (len);

		available_transitions__pack(&allTransitions,buf);

		FILE *traceFile = fopen("transitions-trace.bin", "wb");
		fwrite (buf, len, 1, traceFile);
		fclose(traceFile);
	}

	/* Now off to set up transition data structure */
	setUpTransitionLookUpTable(globalSymTab,  mtypes,
			allTransitions.transition, numTransitions);

}

// Prints a single transition
void printATransition(ptSymTabNode cur, ptFsmTrans trans){
	printf("TL%03d, %s, ", trans->lineNb, trans->expression ? getExpTypeName(trans->expression->type) : "no expression");


	// Print Child Node of  Expression - Wrapper
	if (trans->expression && trans->expression->type == E_STMNT_EXPR){

		if (trans->expression->children[0]){
			printf(" Child=%s ,",  getExpTypeName(trans->expression->children[0]->type));
		} else {
			printf(" Empty ,");
		}
	} else {
		printf(" None ,");
	}


	// Print Child Node of Assignment.
	if (trans->expression && trans->expression->type == E_STMNT_ASGN){

		if (trans->expression->children[0]){
			if (trans->expression->children[0]->type==E_VARREF){
				printf(" Child=%s - Ival=%d ,",  getExpTypeName(trans->expression->children[0]->type),	trans->expression->children[0]->lineNb );
			} else {
				printf(" Child=%s ,",  getExpTypeName(trans->expression->children[0]->type));
			}

		} else {
			printf(" Empty ,");
		}
	} else {
		printf(" None ,");
	}


	if (trans->features){
		printBool(trans->features);
	} else {
		printf("no feature expression");
	}
	printf(", %d, %d, %s \n",  trans->source->lineNb,
					trans->target == NULL ? -1 : trans->target->lineNb,
					cur->name);
}

// Prints a list of all Transitions
void printTransitions(ptSymTabNode globalSymTab, ptMTypeNode mtypes){

	printf("Id, TL#, expressionType, ChildTypIfWrapper, ChildTypeIfAssignment, features, sourceLineNbr, targetLineNbr, processName\n");
	int uniqueId = 1;
	ptSymTabNode cur = globalSymTab;
	if(cur != NULL) {
		while(cur != NULL) {
			if(cur->type == T_PROC || cur->type == T_NEVER) {
				if(cur->fsm != NULL){
					ptList transitions = cur->fsm->transitions;
					while(transitions!=NULL){
						ptFsmTrans trans = (ptFsmTrans) transitions->value;
						printf("%d, ", uniqueId);
						printATransition(cur, trans);
						transitions = transitions->next;
						uniqueId++;
					}
				}
			}
			cur = cur->next;
		}
	}
}

void printTransitionList(ptList	executableList){
	while(executableList != NULL){
		ptProcessTransition nextTrans = ((ptProcessTransition) executableList->value);
		printATransition(nextTrans->process->process, nextTrans->trans);
		executableList  = executableList->next;
	}
}

void replayTrace(ptSymTabNode globalSymTab, ptMTypeNode mtypes){

}


int PrintCmp = 0;

// 	Returns 0 if elements are equal,  > 0  if a is greater, < 0 if b is greater.
int cmpFuncTransitionWithoutIdentifier(const void * a, const void * b)
{
	ATransition *transitionA =  ((ATransition **) a)[0];
	ATransition *transitionB =  ((ATransition **) b)[0];


/*
		printf("Comparing Line Numbers %d, %d \n"
				,transitionA->linenb,
				transitionB->linenb );
*/


	if ( 	(transitionA->linenb == transitionB->linenb) &&
			(transitionA->expressiontype == transitionB->expressiontype) &&
			(strcmp(transitionA->processname, transitionB->processname)==0) &&
			(transitionA->sourcelinenb == transitionB->sourcelinenb) &&
			(transitionA->targetlinenb == transitionB->targetlinenb)){
		return 0;
	} else if( (transitionA->linenb > transitionB->linenb) ||
				( (transitionA->linenb == transitionB->linenb) && (
						transitionA->expressiontype	> transitionB->expressiontype )) ||
				( (transitionA->linenb == transitionB->linenb) &&
				  (transitionA->expressiontype == transitionB->expressiontype) &&
				  ((strcmp(transitionA->processname, transitionB->processname) > 0))) ||
				( (transitionA->linenb == transitionB->linenb) &&
				  (transitionA->expressiontype == transitionB->expressiontype) &&
				  (strcmp(transitionA->processname, transitionB->processname)==0) &&
				  (transitionA->sourcelinenb	> transitionB->sourcelinenb )) ||
				( (transitionA->linenb == transitionB->linenb) &&
				  (transitionA->expressiontype == transitionB->expressiontype) &&
				  (strcmp(transitionA->processname, transitionB->processname)==0) &&
				  (transitionA->sourcelinenb == transitionB->sourcelinenb) &&
				  (transitionA->targetlinenb	> transitionB->targetlinenb ))
			){
		return 1;
	} else {
		return -1;
	}
}

// Sets up a sorted array of transition objects.
void setUpTransitionLookUpTable(ptSymTabNode globalSymTab, ptMTypeNode mtypes,
		ATransition **AllTransitionArray, int numTransitions ){

	_allReadTransitions = AllTransitionArray;
	_numberAllReadTransitions = numTransitions;

	qsort(_allReadTransitions,  _numberAllReadTransitions, sizeof(ATransition *),  &cmpFuncTransitionWithoutIdentifier);
}


// Returns the unique identifier of a specific transition
/*
 * Searches across a data structure for a transition matching trans and returns its identifier.
 * The transitions are stored as a sorted array.
 *
 */
int getTransitionIdentifier(ATransition *lookupTransition){
	ATransition **equivalentTranstionWithIdentifier = (ATransition **) bsearch(&lookupTransition,
			_allReadTransitions,
			_numberAllReadTransitions ,
			sizeof(ATransition*),
			&cmpFuncTransitionWithoutIdentifier );


	if(equivalentTranstionWithIdentifier == NULL){
		return -1;
	} else {
		return equivalentTranstionWithIdentifier[0]->uniqueidentifier;
	}

}


//	Creates a serialized transitions from a ProveLines Transition and Process

ATransition * createSerializedTransitionRepresentation(ptSymTabNode cur, ptFsmTrans trans){
	ATransition *serializedTranstion = malloc(sizeof(ATransition));
	atransition__init(serializedTranstion);


	serializedTranstion->expressiontype = trans->expression ? trans->expression->type : -1;
	serializedTranstion->linenb = trans->lineNb;
	serializedTranstion->sourcelinenb = trans->source->lineNb;
	serializedTranstion->targetlinenb = trans->target ? trans->target->lineNb : -1  ;
	serializedTranstion->processname =  cur->name;

	serializedTranstion->uniqueidentifier = getTransitionIdentifier(serializedTranstion);

	return serializedTranstion;
}



void startTraceGeneration(ptSymTabNode globalSymTab, ptMTypeNode mtypes, int MaxNumberOfSteps) {
	initSolverWithFD(getFeatureModelClauses());
	htVisitedStatesInit();

	// Create initial state
	ptState init = stateCreateInitial(globalSymTab, mtypes);
	init->payloadHash = hashState(init);

	ATransition transition = ATRANSITION__INIT;

	void *buf;                     // Buffer to store serialized data
	unsigned len;                  // Length of serialized data

	/* Iterate through transitions and print each transition */

	ptList ptLSerializedTransitions = NULL;

	ptBoolFct noOutgoing = NULL;
	byte resetExclusivity = false;
	byte hasDeadlock = 0;

	int stepsTaken = 0;
	ptList	executableList = executables(globalSymTab,
					mtypes,
					init,
					1,
					_nbErrorsDiscard,
					&hasDeadlock,
					NULL,
					NULL,
					&noOutgoing,
					&resetExclusivity);
	ptState curState = init;


	while(executableList != NULL && stepsTaken < MaxNumberOfSteps){


		ptProcessTransition nextTrans = selectTransition(executableList);

		byte assertViolation = 0;
		ptState newState  = apply(globalSymTab,
				mtypes,
				curState,
				nextTrans,
				1,
				&assertViolation);

		/* Add Transition to list of transitions */
		ATransition * serializedTranstionTemp = createSerializedTransitionRepresentation(nextTrans->process->process, nextTrans->trans);
		ptLSerializedTransitions = listAdd(ptLSerializedTransitions, serializedTranstionTemp);
		/* End Add Transition to list of transitions */


		executableList = executables(globalSymTab,
				mtypes,
				newState,
				1,
				_nbErrorsDiscard,
				&hasDeadlock,
				NULL,
				NULL,
				&noOutgoing,
				&resetExclusivity);
		curState = newState;
		stepsTaken++;

		printf("%d\n",	serializedTranstionTemp->uniqueidentifier);
		printState(mtypes, newState, NULL);
	}

	/* Prints Unique identifiers */
	while(ptLSerializedTransitions != NULL){
		ATransition * serializedTranstion = (ATransition *) ptLSerializedTransitions->value;
		if(ptLSerializedTransitions->next == NULL){
			printf("%d\n", serializedTranstion->uniqueidentifier);
		}
		else{
			printf("%d,", serializedTranstion->uniqueidentifier);
		}
		ptLSerializedTransitions = ptLSerializedTransitions->next;
	}

}
