/*
 * channels.h
 *
 *  Created on: Aug 20, 2015
 *      Author: rafaelolaechea
 */

#ifndef SEMANTICS_CHANNELS_H_
#define SEMANTICS_CHANNELS_H_
#include "symbols.h"
#include "state.h"

void initializeChannelReferences(ptSymTabNode globalSymTab, ptState newState);



#endif /* SEMANTICS_CHANNELS_H_ */
