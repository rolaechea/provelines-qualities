/*
 * qualitycomputation.h
 *
 *  Created on: Mar 24, 2015
 *      Author: rafaelolaechea
 */
#ifndef SEMANTICS_QUALITYCOMPUTATION_H_
#define SEMANTICS_QUALITYCOMPUTATION_H_

#include <../wrapper/features/boolFct.h>
#include <stdbool.h>

struct featureExpressionCosts {
	int cost;							// Cost of transitions mapping this feature Expression
										// provided otherCost = false
	bool isOtherNode ;
	int otherCost;

	ptBoolFct fExpression ;

	struct featureExpressionCosts * next;		// first; these things will be optimised away
};



typedef struct featureExpressionCosts tFeatureExpressionCosts;
typedef struct featureExpressionCosts * ptFeatureExpressionCosts;


struct AccumulativeCosts {
	int cost;							//  cost for any product matching fExpression


	ptBoolFct fExpression ;			// fExpression. Null means any product.

	struct AccumulativeCosts * next;		// first; these things will be optimised away

};

typedef struct AccumulativeCosts tAccumulativeCosts;
typedef struct AccumulativeCosts * ptAccumulativeCosts;


ptAccumulativeCosts buildEmptyAccumulativeCosts();

void setOtherCost(int transitionNumber, int otherCost);
void setFeatureExpressionCost(int transitionNumber, int costValue, char *FeatureExpression);
void parseTransitionCost(char * fileTransition);
ptFeatureExpressionCosts getFeatureExpressionCostsFromdIdentifier(int TransitionIdentifier);
bool isTimeTickTransition(int currentTransitionIdentifier);
bool hasQualityMapping(int TransitionIdentifier);
ptAccumulativeCosts addCostFeatureExpressionMapping(ptAccumulativeCosts AllDynamicCosts, int cost, ptBoolFct FeatureExpression, ptAccumulativeCosts LastNode);
void computeCosts(ptList TransitionList);
void printMapping(ptAccumulativeCosts AllDynamicCosts);
int countNodesMapping(ptAccumulativeCosts AllDynamicCosts);
ptAccumulativeCosts initializeEmptyAccumulation(ptAccumulativeCosts AllDynamicCosts, int currentTransitionIdentifier);

#endif /* SEMANTICS_QUALITYCOMPUTATION_H_ */
