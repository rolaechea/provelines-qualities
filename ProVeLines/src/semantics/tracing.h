/*
 * tracing.h
 *
 *  Created on: Mar 5, 2015
 *      Author: rafaelolaechea
 */
#include "transitions.pb-c.h"
#ifndef SEMANTICS_TRACING_H_
#define SEMANTICS_TRACING_H_

#include "../helper/list.h"
#include "../automata/symbols.h"
#include "../automata/automata.h"


ptList parseListOfTransitionIds(char *TraceFile);
int getNumberOfTransitions();
void CheckCompatibleProducts(ptList transitionIds, ptSymTabNode globalSymTab, ptMTypeNode mtypes);
ATransition *createSerializedTransitionRepresentation(ptSymTabNode cur, ptFsmTrans trans);
int getTransitionIdentifier(ptSymTabNode cur, ptFsmTrans trans);
ptFsmTrans getTransitionInProvelinesFormatFromIdentifier(int Identifier);
void startTraceGeneration(ptSymTabNode globalSymTab, ptMTypeNode mtypes, int MaxNumberOfSteps);
void InitializeAndSerializeTransitions(ptSymTabNode globalSymTab, ptMTypeNode mtypes, byte serializeTransitions);
void printTransitions(ptSymTabNode globalSymTab, ptMTypeNode mtypes);

#endif /* SEMANTICS_TRACING_H_ */
