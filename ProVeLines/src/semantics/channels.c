/*
 * channels.c
 *
 *  Created on: Aug 20, 2015
 *      Author: rafaelolaechea
 */
#include <stdio.h>      /* printf, scanf, NULL */
#include <stdlib.h>

#include "symbols.h"
#include "state.h"

#include "channels.h"

/*	 Iterate through each channel and create a list of channel refs from the initial state
 *
 * */
void initializeChannelReferences(ptSymTabNode globalSymTab, ptState newState){

	printf("In Channel References \n");

	ptSymTabNode variable = globalSymTab;

	while (variable) {
		printf("In-Channel Variables \n");
		if (variable->type == T_CHAN) {
			printf("With var-type == T_CHAN  \n");

			ptChannelRef oldRef = newState->chanRefs;
			newState->chanRefs = (ptChannelRef) malloc(sizeof(tChannelRef));
			if(!newState->chanRefs)
				failure("Out of memory (creating channelRef).\n");

			newState->chanRefs->channelOffset = newState->payloadSize;
			newState->chanRefs->symTab = variable;
			newState->chanRefs->next = oldRef;

			if (variable->capacity != 0)
				newState->payloadSize += (1 + variable->memSize * variable->capacity) * variable->bound;
			else
				newState->payloadSize += variable->bound;
		}
		else if (variable->type != T_PROC && variable->type != T_TDEF) {
			printf("With var-type != T_PROC  && T_TDEF !=TDEF \n");
			newState->payloadSize += variable->memSize * variable->bound;
		}
		variable = variable->next;
	}

}


