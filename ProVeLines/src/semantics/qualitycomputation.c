/*
 * qualitycomputation.c
 *
 *  Created on: Mar 24, 2015
 *      Author: rafaelolaechea
 */
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>

#include "tracing.h"
#include "cnf.h"
#include "error.h"
#include "qualitycomputation.h"
#include "../wrapper/features/boolFCT.h"

// Private  wrapper to a ptCNF, to include boolean flag is Unsat to prevent negating NULL.
struct otherComposedFormula {
	bool haveOtherFormula;
	bool NoMatchIsUnsat;
	ptCNF NoMatch ;
};

typedef struct otherComposedFormula tOtherComposedFormula;
typedef struct otherComposedFormula * ptOtherComposedFormula;

static ptFeatureExpressionCosts *UniqueIdentifierToListOfFeatureExpressionCost = NULL;
static ptOtherComposedFormula *UniqueIdentifierToOtherFormula = NULL;
static bool *UniqueIdentifierToHaveComputedOtherFormula = NULL;

int getVariableValue(char *variableName);




/*
 * Allocate an array of pointers to Feature Expressions/Other List for each transition and set them to null
 *
 */

void InitializeTransitionCostMapping(){
	UniqueIdentifierToListOfFeatureExpressionCost = malloc(sizeof(ptFeatureExpressionCosts) * getNumberOfTransitions());
	UniqueIdentifierToOtherFormula = malloc(sizeof(ptOtherComposedFormula) *  getNumberOfTransitions());
	UniqueIdentifierToHaveComputedOtherFormula = malloc(sizeof(bool *) *  getNumberOfTransitions());

	int i = 0;
	for ( i = 0; i  < getNumberOfTransitions() ; i++ ){
		UniqueIdentifierToListOfFeatureExpressionCost[i] = NULL;
		UniqueIdentifierToOtherFormula[i] = NULL;
		UniqueIdentifierToHaveComputedOtherFormula[i] = false;
	}
}



/*
 *
 * Checks if a transition has a mapping assigned to it.
 *
 */
bool hasQualityMapping(int TransitionIdentifier){
	return UniqueIdentifierToListOfFeatureExpressionCost[TransitionIdentifier-1] != NULL;
}


/*
 *
 * Returns the quality mapping for a given transition or NULL if id does not exist.
 *
 */
ptFeatureExpressionCosts getFeatureExpressionCostsFromdIdentifier(int TransitionIdentifier){
	return UniqueIdentifierToListOfFeatureExpressionCost[TransitionIdentifier-1];
}

int getVariableValue(char *variableValue){
	return 0;
}

/*
 * Print Parsed Transitions
 *
 */
void printParsedTransitions(){
	int i =1;
	for ( i = 1; i  <= getNumberOfTransitions() ; i++ ){
		if (hasQualityMapping(i)){
			ptFeatureExpressionCosts tmpMapping = getFeatureExpressionCostsFromdIdentifier(i);
			printf("Transition %d \n", i);

			while(tmpMapping!=NULL){
				if (tmpMapping->isOtherNode == false){
					printf("FeatureExpression \n\t\t");
					printCNF((ptCNF) tmpMapping->fExpression);
					printf("  \t Cost=%d, OtherCost=%d \n", tmpMapping->cost, tmpMapping->otherCost );
				} else {
					printf("Other  \n\t\t Cost=%d OtherCost=%d \n", tmpMapping->cost, tmpMapping->otherCost);

				}
				tmpMapping = tmpMapping->next;
			}

		}
	}

}


/*
 *
 * Initializes an array of feature expression for transitions. It reads the information from a file and populates such array.
 *
 * A Transition cost file contains
 *
 *
 * # Transition Id_1
 * # Feature Expression A_1
 * p cnf q
 * 212 32 0
 * X YY 0
 * # Cost
 *
 * # Feature Expression
 * # Cost ...
 *
 * # Other
 * # Cost
 *
 *	# Transition Id_2
 * ...
 *
 */
void parseTransitionCost(char * fileTransition){
	const char * const featureExpressionKeyword = "# Feature Expression";
	const char * const otherFeatureExpressionKeyword = "# Other";
	const char * const currentTransitionEndsKeyword = "# End Transition";
	const char * const costKeyword ="# Cost";


//	printf("Setting Transition Cost Mappings\n");
	InitializeTransitionCostMapping();
	// Find transition
	// Find Feature Expression Keyword
	// Read CNF Formula
	FILE* costValueMappingFile = fopen(fileTransition, "r");
	int transitionNumber = -1;

	const int maximumLineSize = 1000;

	char *lineBuffer = malloc(maximumLineSize*sizeof(char));


	int readTransitions = -1;
	while(fscanf(costValueMappingFile, "# Transition %d\n", &transitionNumber)==1){
//		printf("Start parsing values for Transition %d \n", transitionNumber);

		byte readingCurrentTransitionInfo=1;

		while(readingCurrentTransitionInfo){
			if (fgets(lineBuffer, maximumLineSize, costValueMappingFile )){
				if (strncmp(lineBuffer, featureExpressionKeyword,  strlen(featureExpressionKeyword)) != 0 &&
							strncmp(lineBuffer, otherFeatureExpressionKeyword,  strlen(otherFeatureExpressionKeyword)) != 0	){
						if (	strncmp(lineBuffer, currentTransitionEndsKeyword,  strlen(currentTransitionEndsKeyword))==0){
							readingCurrentTransitionInfo = 0;
						} else {
							failure("Incorrect Transition Cost Mapping file. Transition Declaration not followed by Feature Expression Keyword Or Other Expression, instead by <%s> \n", lineBuffer);
						}

					} else if (strncmp(lineBuffer, featureExpressionKeyword,  strlen(featureExpressionKeyword)) == 0){
						/*Feature Expression */
						char *lineFormula = malloc( maximumLineSize*sizeof(char));
						char *resultRead = fgets(lineFormula, maximumLineSize, costValueMappingFile );

						char *accumulatedFormula = malloc(maximumLineSize*sizeof(char));
						accumulatedFormula[0] = '\0';
						char *secondaryAccumulatedFormula = NULL;

						/* have to combine set of lines into a single string */
						while(resultRead!=NULL &&
								strncmp(lineFormula, costKeyword,  strlen(costKeyword)) != 0){


								int secondaryAccumulatedFormulaLenght = strlen(accumulatedFormula) + strlen(lineFormula) ;
								secondaryAccumulatedFormula =  malloc((secondaryAccumulatedFormulaLenght+1) *sizeof(char));
								strcpy(secondaryAccumulatedFormula, accumulatedFormula );

								strcat(secondaryAccumulatedFormula, lineFormula);

								accumulatedFormula = secondaryAccumulatedFormula;

								resultRead = fgets(lineFormula, maximumLineSize, costValueMappingFile );
						}

						if(strncmp(lineFormula, costKeyword,  strlen(costKeyword)) == 0){
							/* Expecting a cost */
						//	printf("Found Cost function after feature expression. Previous to it we found string <%s>\n", accumulatedFormula);
						//	printf(" Line Formula to extract cost from is <%s> \n", lineFormula);
							int costValue = -1;
							if (sscanf(lineFormula, "# Cost %d\n", &costValue )!= 1){
								failure("Failed extracting cost from a Cost Keywordd line \n");
							} else {
								setFeatureExpressionCost(transitionNumber, costValue, accumulatedFormula);
							}

						} else {
							failure("Failed Parsing. Formula after Feature Expression is not followed by a cost.\n");
						}
					}else {
						/* An  Other Expression, to be followed by Cost Expression */
						int otherCost;
//						printf("Found other line <%s> \n", lineBuffer);
						int readCostItem = fscanf(costValueMappingFile, "# Cost %d\n", &otherCost);

						if (readCostItem != 1){
							failure("Missing Cost after other in the Transition Cost Mapping file\n");
						} else {
							setOtherCost(transitionNumber, otherCost);
						}
					}
			} else {
				readingCurrentTransitionInfo=0;
			}
		}
	}
}

/*
 *
 * Sets the feature expression in an array where for each transition there is a list of tuples <Feature Expression, Cost> followed optionally by an other cost node.
 */
void setFeatureExpressionCost(int transitionNumber, int costValue, char *FeatureExpression){
//	printf("Setting Cost with Feature Expression: \n \tTransition %d \n\t %s\n \tCost %d\n", transitionNumber, FeatureExpression, costValue );
	ptCNF clausePtr = dimacsStringToCNF(FeatureExpression);

	ptFeatureExpressionCosts FeatureExpressionCostNode = malloc(sizeof(tFeatureExpressionCosts));
	FeatureExpressionCostNode->cost = costValue;
	FeatureExpressionCostNode->isOtherNode = false;
	FeatureExpressionCostNode->next = NULL;
	FeatureExpressionCostNode->fExpression = clausePtr;

	if(UniqueIdentifierToListOfFeatureExpressionCost[transitionNumber-1]==NULL){
		// Write in the front.
		UniqueIdentifierToListOfFeatureExpressionCost[transitionNumber-1] = FeatureExpressionCostNode;
	} else {
		//Append it to the back
		ptFeatureExpressionCosts currentNode = UniqueIdentifierToListOfFeatureExpressionCost[transitionNumber-1];
		while(currentNode->next!=NULL){
			currentNode = currentNode->next;
		}
		currentNode->next = FeatureExpressionCostNode;
	}

}

/*
 * Sets an other cost either by appending or putting as first item an Other Node.
 *
 */
void setOtherCost(int transitionNumber, int otherCost){
	//printf("Setting Other Cost for: \n \t Transition %d \n\t Cost %d\n", transitionNumber, otherCost );
	ptFeatureExpressionCosts OtherCostNode = malloc(sizeof(tFeatureExpressionCosts));
	OtherCostNode->cost = otherCost;
	OtherCostNode->otherCost = otherCost;
	OtherCostNode->isOtherNode = true;
	OtherCostNode->next = NULL;
	OtherCostNode->fExpression = NULL;


	if(UniqueIdentifierToListOfFeatureExpressionCost[transitionNumber-1]==NULL){
		// Set an Empty Node that is an other Cost.
		UniqueIdentifierToListOfFeatureExpressionCost[transitionNumber-1] = OtherCostNode;
	} else {
		//Append it to the back
		ptFeatureExpressionCosts currentNode = UniqueIdentifierToListOfFeatureExpressionCost[transitionNumber-1];
		while(currentNode->next!=NULL){
			currentNode = currentNode->next;
		}
		currentNode->next = OtherCostNode;
	}
}





/*
 *
 * Constructor to build an empty structure to accumulate mapping of product to cost.
 *
 */
ptAccumulativeCosts buildEmptyAccumulativeCosts() {
	ptAccumulativeCosts tmpCostStructure = malloc(sizeof(tAccumulativeCosts));
	tmpCostStructure->cost = 0;
	tmpCostStructure->fExpression = NULL;
	tmpCostStructure->next = NULL;

	return tmpCostStructure;
}




/*
 * Checks whether the mapping is empty (0, NULL, NULL)
 *
 */
bool isEmptyAccumulativeCosts(ptAccumulativeCosts M){
	bool ret = false;

	if (M->cost == 0 &&
		M->fExpression == NULL &&
		M->next == NULL){
		ret = true;
	} else {
		ret = false;
	}

	return ret;
}


/*
 * Add a cost feature expression Mapping to the end of the ptAccumulativeCosts and returns a pointer to the end of it.
 *
 */
ptAccumulativeCosts addCostFeatureExpressionMapping(ptAccumulativeCosts AllDynamicCosts, int cost, ptBoolFct FeatureExpression, ptAccumulativeCosts LastNode){
//	printf("Adding Cost Feature Expression\n");
	if (LastNode == AllDynamicCosts && isEmptyAccumulativeCosts(AllDynamicCosts))  {
//		printf("Replaced Empty Node\n");
		AllDynamicCosts->cost = cost;
		AllDynamicCosts->fExpression =  FeatureExpression;
	} else {
//		printf("Attached Extra  Node\n");
//		printf("First Node:  Cost = %d ,  AllDynamicCosts->next=%s \n", \
//				AllDynamicCosts->cost, AllDynamicCosts->next == NULL ? "NULL" : " Not Null" );

		ptAccumulativeCosts newNode = buildEmptyAccumulativeCosts();
		newNode->cost = cost;
		newNode->fExpression = FeatureExpression;
		LastNode->next = newNode;
		LastNode = newNode;
	}

	return LastNode;
}


int countNodesMapping(ptAccumulativeCosts AllDynamicCosts){
	int count=0;
	while(AllDynamicCosts!=NULL){
		count += 1;
		AllDynamicCosts = AllDynamicCosts->next;
	}
	return count;
}

/* Given a transition identifier return a formula for the other case (provided such formula exists).
 * Requires hasMapping(TransitionIdentifier) == true.
 *
 * It caches results to avoid computing over and over again the "OtherFormula".
 */
ptOtherComposedFormula getOtherFormula(int TransitionIdentifier){

	ptOtherComposedFormula OtherInstance = NULL;

	if(UniqueIdentifierToHaveComputedOtherFormula[TransitionIdentifier-1] == true){
		OtherInstance = UniqueIdentifierToOtherFormula[TransitionIdentifier-1];
	}else{
		OtherInstance = malloc(sizeof(tOtherComposedFormula));


		ptFeatureExpressionCosts tmpTransitionMapping =getFeatureExpressionCostsFromdIdentifier(TransitionIdentifier);

		bool otherNodeExists = false;

		ptCNF noExpressionMatch = NULL;
		bool noExpressionMatchIsUnsat = false;

		// Go through each feature expression to compute other formula.
		while(tmpTransitionMapping){
			if (tmpTransitionMapping->isOtherNode == false){
				// Building Up Other Expression
				// Not(FE_1) ^ Not(FE_2) ... ^ Not(FE_n)

				if(tmpTransitionMapping->fExpression==NULL){
					noExpressionMatchIsUnsat =  true;
				} else if (noExpressionMatchIsUnsat == false){

					ptCNF tmpNotCurrentFE = negateCNF( (ptCNF) tmpTransitionMapping->fExpression);

					noExpressionMatch = addCNFConjunction(noExpressionMatch,
							tmpNotCurrentFE,
							true, true);
				}
			} else { // Other Node so finish up.
				otherNodeExists = true;
			}

			tmpTransitionMapping = tmpTransitionMapping->next;
		}

		if(otherNodeExists == true){
			OtherInstance->haveOtherFormula = true;
			OtherInstance->NoMatchIsUnsat = noExpressionMatchIsUnsat;
			OtherInstance->NoMatch = noExpressionMatch;
		} else {
			// No other node.
			OtherInstance->haveOtherFormula = false;
			OtherInstance->NoMatchIsUnsat = false;
			OtherInstance->NoMatch = NULL;
		}

		// Cache results.
		UniqueIdentifierToOtherFormula[TransitionIdentifier-1] = OtherInstance;
		UniqueIdentifierToHaveComputedOtherFormula[TransitionIdentifier-1] = true;
	}

	return OtherInstance;

}




/*
 *
 * Initializes a previously empty AllDynamicCosts to the feature expression costs of a transition.
 *  Requires hasQualityMapping(currentTransitionIdentifier)==True and
 *  isEmptyAccumulativeCosts(AllDynamicCosts)==True.
 *
 *
 *  Returns the initialized AllDynamicCosts.
 *
 */
ptAccumulativeCosts initializeEmptyAccumulation(ptAccumulativeCosts AllDynamicCosts, int currentTransitionIdentifier){

	ptAccumulativeCosts AllDynamicCostsLastPtr = AllDynamicCosts;

	ptFeatureExpressionCosts tmpTransitionMapping =getFeatureExpressionCostsFromdIdentifier(currentTransitionIdentifier);

	ptCNF noExpressionMatch = NULL;
	bool noExpressionMatchIsUnsat = false;

	ptOtherComposedFormula ohterFormulaInstance = getOtherFormula(currentTransitionIdentifier);

	// Make M = amap, by iterating through each featureExpression, cost element of amap.
	while(tmpTransitionMapping){
		if (tmpTransitionMapping->isOtherNode == false){

			AllDynamicCostsLastPtr = addCostFeatureExpressionMapping(AllDynamicCosts,
					tmpTransitionMapping->cost,
					tmpTransitionMapping->fExpression,
					AllDynamicCostsLastPtr);

		} else {
			if (ohterFormulaInstance->haveOtherFormula &&
					ohterFormulaInstance->NoMatchIsUnsat == false){

				AllDynamicCostsLastPtr = addCostFeatureExpressionMapping(AllDynamicCosts,
						tmpTransitionMapping->otherCost,
						noExpressionMatch,
						AllDynamicCostsLastPtr);
			}
		}
		tmpTransitionMapping = tmpTransitionMapping->next;
	}
	// End M=amap

	return AllDynamicCosts;
}


bool isTimeTickTransition(int currentTransitionIdentifier){
	bool isTimeTick = false;
	if (currentTransitionIdentifier == 97)
		isTimeTick =  true;

	return isTimeTick;
}

/*
 *
 * Compute a set of cost for transitions
 *
 */
void computeComplexCosts(ptList TransitionList){
	ptAccumulativeCosts AllDynamicCosts = buildEmptyAccumulativeCosts();
	ptBoolFct CompatibleProducts = NULL; // NULL represents formula true.

	while(TransitionList != NULL){
			int currentTransitionIdentifier =  *((int*) TransitionList->value);
			ptFsmTrans trans = getTransitionInProvelinesFormatFromIdentifier(currentTransitionIdentifier);

			if (isTimeTickTransition(currentTransitionIdentifier)){

				bool isWarm  = getVariableValue("isWarm");
				bool isCold = getVariableValue("isWarm"); ;
				bool isHighway = getVariableValue("isWarm"); ;
				bool isCity = getVariableValue("isWarm"); ;


				int speed ;
				int previousSpeed ;



				if (isWarm && isHighway ){


					// Platoonin, Sunroof/Convertible, Gear - State.

	// 				ptFeatureExpressionCosts tmpTransitionMapping = LookUpRate(
	//										isWarm, isHighway, SportEngaged);
	// For each FExpression, rateOnSpeed in LookUpRate(isWarm, isHighway, SportModeEngagedOrNot)
	//  If M.isEmpty:
	//		M[FExpressionx] = rateOnSpeed	for all FExpression, rateOnSpeed.
	//  else if M.has(FExpression)
	//  	M[FExpression] += speed * rateOnSpeed
	// else
	//		For each fexp' in M.keys():
	//			if (fexp' && FExpression && CompatibleProducts) is satisfiable:
	//				M[fexp' && FExpression] += speed * rateOnSpeed

				} else if (isWarm && isCity ){

				} else if (isCold &&  isHighway){

				} else if (isCold && isCity){

				} else {
					// Error Condition
				}


/*
 * At t_i, t_i+1 we know the State Expression.
 *
 * Feature  Expression + State Expression ---> Cost
 * Feature  Expression + State Expression  ---> Cost
 *
 *
 *
 */

			}
			TransitionList = TransitionList->next;
	}
}

/*
 * Iterate through each transition pair and compute the set of costs.
 *
 *
 */
void computeCosts(ptList TransitionList){


	printf("Computing Cost for a Trace \n");
	ptAccumulativeCosts AllDynamicCosts = buildEmptyAccumulativeCosts();
	ptBoolFct CompatibleProducts = NULL; // NULL represents formula true.

	while(TransitionList != NULL){
		int currentTransitionIdentifier =  *((int*) TransitionList->value);
		ptFsmTrans trans = getTransitionInProvelinesFormatFromIdentifier(currentTransitionIdentifier);

		// Afexp = Afexp ^ gamma(s_i, a_i, s_i+1)
		CompatibleProducts = addConjunction(CompatibleProducts,	trans->features, true, true);

		// For each expression, cost in AccumulatedCost:
		//			if e ^ CompatibleProductFormula unsatisfiable then eliminate  <e, cost> from AccumulatedCost


		// if M = {}
		if(isEmptyAccumulativeCosts(AllDynamicCosts)){
			if(hasQualityMapping(currentTransitionIdentifier)){

				printf("Initializing Mapping \n");
				AllDynamicCosts = initializeEmptyAccumulation(AllDynamicCosts,
						currentTransitionIdentifier);

			} // End-If HasQualityMapping
		} else {// Else !isEmptyAccumulativeCosts(AllDynamicCosts)

			if(hasQualityMapping(currentTransitionIdentifier)){

				printf("ReUpdating Mapping \n");
				// Go through each element of  M
				// For each featureExpression TranstionFE in Amap
					// For each featureExpression PreviousFE in AllDynamicCostsM
						// AllDynamicCosts'(TranstionFE ^ PreviousFE) <- AllDynamicCosts(PreviousFE) + Amap(TranstionFE)

				ptFeatureExpressionCosts tmpTransitionMapping =getFeatureExpressionCostsFromdIdentifier(currentTransitionIdentifier);

				ptOtherComposedFormula ohterFormulaInstance = getOtherFormula(currentTransitionIdentifier);

				ptAccumulativeCosts AllDynamicCostsPrime =  buildEmptyAccumulativeCosts();
				ptAccumulativeCosts AllDynamicCostsLastPtrPrime = AllDynamicCostsPrime;


				while(tmpTransitionMapping){
					if (tmpTransitionMapping->isOtherNode == false){
						// Update AllDynamicCosts' based on TranstionFE ^ PreviousFE
						ptAccumulativeCosts AllDynamicCostsIterator = AllDynamicCosts;
						while(AllDynamicCostsIterator != NULL){

							ptCNF TFeAndPreviousFe = addCNFConjunction(  (ptCNF ) AllDynamicCostsIterator->fExpression,
									 (ptCNF ) tmpTransitionMapping->fExpression, true, true);

							// AllDynamicCosts'(TranstionFE ^ PreviousFE) <- AllDynamicCosts(PreviousFE) + Amap(TranstionFE)
							AllDynamicCostsLastPtrPrime = addCostFeatureExpressionMapping(AllDynamicCostsPrime,
									tmpTransitionMapping->cost + AllDynamicCostsIterator->cost,
									TFeAndPreviousFe,
									AllDynamicCostsLastPtrPrime);

							AllDynamicCostsIterator = AllDynamicCostsIterator->next;
						}
					} else { // else tmpTransitionMapping->isOtherNode == false
						if (ohterFormulaInstance->haveOtherFormula &&
								ohterFormulaInstance->NoMatchIsUnsat == false){

							// Update AllDynamicCosts' based on TranstionFE ^ PreviousFE
							ptAccumulativeCosts AllDynamicCostsIterator = AllDynamicCosts;
							while(AllDynamicCostsIterator != NULL){


								ptCNF TFeAndPreviousFe = addCNFConjunction(AllDynamicCostsIterator->fExpression,
																				ohterFormulaInstance->NoMatch, true, true);

								// AllDynamicCosts'(TranstionFE ^ PreviousFE) <- AllDynamicCosts(PreviousFE) + Amap(TranstionFE)
								AllDynamicCostsLastPtrPrime = addCostFeatureExpressionMapping(AllDynamicCostsPrime,
										tmpTransitionMapping->cost + AllDynamicCostsIterator->cost,
										TFeAndPreviousFe,
										AllDynamicCostsLastPtrPrime);

								AllDynamicCostsIterator = AllDynamicCostsIterator->next;
							}
						}
					} // end-if  tmpTransitionMapping->isOtherNode == false

					tmpTransitionMapping = tmpTransitionMapping->next;
				}// End-While 	tmpTransitionMapping

				AllDynamicCosts = AllDynamicCostsPrime;
			}// End-if hasQualityMapping
		} // End-if isEmptyAccumulativeCosts(AllDynamicCosts)


		TransitionList = TransitionList->next;
	}
	printf("End Computing Cost for a Trace \n");


	printf("Computed Cost is \n");
	printMapping(AllDynamicCosts);



}


void printMapping(ptAccumulativeCosts AllDynamicCosts){
	printf("Printing Mapping \n");
	while(AllDynamicCosts != NULL){
			printf("FeatureExpression: \n");
			printf("\t\n");
			printCNF((ptCNF)  AllDynamicCosts->fExpression);
			printf("\n");
			printf("\t Cost: %d \n",AllDynamicCosts->cost );
			AllDynamicCosts = AllDynamicCosts->next;
	}


}
