/*
 * ENCODING OF STATES
 * * * * * * * * * * * * * * * * * * * * * * * *
 *
 * A state consists of a set of active processes and of the values of the
 * variables of these processes.  It is thus represented by two elements:
 *
 *  - A list of active processes, and for each a pointer to its symbol table.
 *
 *  - A block of memory containing the variables of these processes.  This is
 *    more efficient than a linked list of values and basically corresponds to
 *    a run-time struct.
 *
 *    The block is divided into sub-blocks, one for each process.  The
 *    list of processes contains the offsets of the sub-block of each process.
 *    One sub-block holds all variable values in the order in which they
 *    were declared in the symbol table of the process.  For each variable, a
 *    predefined chunk of memory is allocated (all variable types are finite
 *    and cannot grow).
 *
 *
 * To test equivalence of two states, it is sufficient to test equivalence of
 * payloadSize, and if equivalent, the whole blocks.
 */
#ifndef SEMANTICS_STATE_H
#define SEMANTICS_STATE_H

#include <stdbool.h>
#include "../automata/symbols.h"
#include "../main.h"
#include "automata/automata.h"


struct context_ {
	char * name;
	ptExpNode expression;
	int index;
};
typedef struct context_ tContext;
typedef struct context_* ptContext;

// A state mask gives for every process the pid, a pointer to its symtab node
// and its offset in the payload
struct stateMask_ {
	ptSymTabNode process;
	int pid;					// List of processes and their offset in the state
	unsigned int offset; 		// Offset of the process in the chunk of memory.
								// The address of the process current node can be found in the payload at offset 'offset - sizeof(void *)'.
	ptContext context;			// FM context of the process.
	struct stateMask_* next;
};

typedef struct stateMask_   tStateMask;
typedef struct stateMask_* ptStateMask;


// A channel ref gives for every channel the offset.
struct channelRef_ {
	unsigned int channelOffset;
	ptSymTabNode symTab;
	struct channelRef_ * next;
};

typedef struct channelRef_    tChannelRef;
typedef struct channelRef_ * ptChannelRef;


// State
struct state_ {
	ptStateMask mask; 	// Points to the global (first) stateMask.
	ptStateMask last; 	// Points to the last stateMask.
	ptStateMask never; 	// If f is an LTL formula to be checked, never is the stateMask conversion of ~f.
								//	Furthermore, never->node returns the current node in this FSM. Note that pid and offset have no importance here.
								//	Also, never->next is supposed to be NULL.

	ptChannelRef chanRefs;		// References of the channel declared in a process or as a global variable.

	int nbProcesses; 			// Number of running processes.
	int lastStepPid; 			// pid of the process that fired transition that got us into this state. (NOT part of the actual state of the system, just a helper)
	void* payload; 				// Chunk of memory containing the data.
/*
 * Payload  contains:
 * 		OFFSET_EXCLUSIVE  -- T_BYTE referring  to whether a process is executing something atomic.
 * 							Its initialized to NO_PROCESS and only changed to a specific process id after
 * 							an execution when executing procTrans->trans->target && (procTrans->trans->target->flags & N_ATOMIC) == N_ATOMIC
 * 		OFFSET_HANDSHAKE  -- T_INT representing a "channel handshake"
 * 							Its initialized to NO_HANDSHAKE and can be changed based on channel ???
 *
 *	 Global Variables
 * 	 Process Variables
 *	 FSM Node Pointer
 *
 */



	unsigned int payloadSize;	// Number of bytes currently allocated to payload.
	unsigned int payloadHash;	// Hash of the state, used to avoid memcmp() when comparing states on the stack.  This hash is NOT maintained by
								// by the functions that change states, it is only updated in checking.c.

	ptBoolFct features;			// Boolean formula representing the products in which the state can be reached.

#ifdef CLOCK
	ptClockZone zone;			// Time zone in which the state is reachable.
	ptClockZone invariant;		// Invariant of this location (MUST NOT COUNT FOR STATE COMPARISON)
#endif
};

typedef struct state_   tState;
typedef struct state_* ptState;


/*
 * API
 * * * * * * * * * * * * * * * * * * * * * * * */

// Access and modification of variables
ptFsmNode getNodePointer(ptState state, ptStateMask mask);

/* Compute Global Variable Value */
int getGlobalVariableValue(ptState state, char *variableName);

void storeNodePointer(ptState state, ptStateMask mask, ptFsmNode pointer);
unsigned int getVarOffset(ptSymTabNode globalSymTab, ptMTypeNode mtypes, ptState state, ptStateMask process, unsigned int preOffset, struct expNode_* expression);
byte* readValues(void* chunk, unsigned int offset, int nb);
void storeValues(void* chunk, unsigned int offset, int nb, byte* values);
int stateGetValue(void* chunk, unsigned int offset, int type);
void stateSetValue(void* chunk, unsigned int offset, int type, int value);
ptStateMask maskLookup(ptState state, int pid);
ptSymTabNode getChannelSymTab(ptState state, unsigned int chanOffset);

// Access and modification of large state chunks
ptState stateCreateInitial(struct symTabNode_* globalSymTab, struct mTypeNode_* mtypes);
int  stateAddProctype(ptState state, struct symTabNode_* proctype, char * cname, ptExpNode cexp, int index); // returns pid
void stateAddNever(ptState state, struct symTabNode_ * neverSymTab);
void stateKillProctype(ptState state, int pid);
void stateClean(ptState state);
void stateDestroy(ptState state, byte keepPayloadAndFeatures);

// State printing
void printGlobalVariables(ptMTypeNode mtypes, ptState state, ptState diffState);
void printState(ptMTypeNode mtypes, ptState state, ptState diffState);
void printPayload(ptState state);

// State duplication
ptState stateDuplicate(ptState state);

// State comparison
#define STATES_DIFF 0				// s1 and s2 are totally different states, meaning s1 is fresh.
#define STATES_SAME_S1_VISITED 1	// s1 and s2 are identical but s2 is reachable by more products; hence, s1 adds nothing new
#define STATES_SAME_S1_FRESH 2		// s1 and s2 are identical but s1 has products that were not explored with s2; hence, s1 is fresh
byte stateCompare(ptState s1, void* s2Payload, ptBoolFct s2Features);
bool stateCompareIgnoreFeatures(ptState s1, void* s2Payload);


#endif
