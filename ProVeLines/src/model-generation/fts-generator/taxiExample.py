'''
Created on Oct 14, 2015

@author: rafaelolaechea
'''
import sys

from taxiPrinting import printFeatures, generateBasicDefines, \
generateExtensionDefines, printGlobalVariables, \
generateMandatoryTransitions, generateLicenseTransitions, \
generateFinalizationCode

if __name__ == '__main__':
    pass

#    for i in range(int(sys.argv[1]), int(sys.argv[1])+1):
#        print " creating feature x{}".format(i)
        
        
                    
    printFeatures(int(sys.argv[1]))

    
    maxDefineNumber = generateBasicDefines()

    for i in range(1, int(sys.argv[1])+1):
        maxDefineNumber = generateExtensionDefines(i, maxDefineNumber)
        

    printGlobalVariables()    
            
    generateMandatoryTransitions()
    
    for i in range(1, int(sys.argv[1])+1):
        generateLicenseTransitions(range(1, int(sys.argv[1])+1), i)
        
    generateFinalizationCode()


