'''
Created on Oct 15, 2015

@author: rafaelolaechea
'''
from sys import stdout

def printFeatures(maxNumber, streamToPrint=stdout):
    currentLicense = 1

    print >> streamToPrint , "typedef features {"
    print >> streamToPrint , "    bool Shuttle;"
    print >> streamToPrint , "   bool Taxi",
    while(currentLicense <= maxNumber):
        print >> streamToPrint , ";\n bool License{}".format(currentLicense),
        currentLicense += 1
    print >> streamToPrint , ""
    print >> streamToPrint , "};"

    print >> streamToPrint , "features f;"  
      
def generateBasicDefines(streamToPrint=stdout):
    print >> streamToPrint , "#define AIRPORTP 0"
    print >> streamToPrint , "#define AIRPORTR 1"
    print >> streamToPrint , "#define PICKUP1 2"
    print >> streamToPrint , "#define RELEASE1 3"
    print >> streamToPrint , "#define PICKUP2 4"
    print >> streamToPrint , "#define RELEASE2 5"
    
    
    print >> streamToPrint , "#define APR11 6"
    print >> streamToPrint , "#define APR12 7"
    print >> streamToPrint , "#define APR21 8"
    print >> streamToPrint , "#define P1AR1 9"
    print >> streamToPrint , "#define P1AR2 10"
    print >> streamToPrint , "#define P2AR1 11"
    
    return 11
    

def generateExtensionDefines(extensionNumber, currentMaxDefineNumber, streamToPrint=stdout):
    print >> streamToPrint , "\n"
    
    print >> streamToPrint , "#define RELEASEEXT{} {}".format(extensionNumber, currentMaxDefineNumber+1)
    currentMaxDefineNumber += 1
    print >> streamToPrint , "#define PICKUPEXT{} {}".format(extensionNumber, currentMaxDefineNumber+1) 
    currentMaxDefineNumber += 1
    
#    intermediateNumbers 
    print >> streamToPrint , "\n"
    

    intermediateStates = 3   # Assuming all extensions take 4 steps to go to/from airport
    
    # print >> streamToPrint , Airport Pickup to Extension Dropoff intermediate steps
    for i in range(1, intermediateStates +1):
        print >> streamToPrint , "#define APR{}X{} {}".format(extensionNumber, i, currentMaxDefineNumber+1)
        currentMaxDefineNumber  += 1

    # print >> streamToPrint , Airport Pickup to Extension Dropoff intermediate steps
    for i in range(1, intermediateStates +1):
        print >> streamToPrint , "#define PX{}AR{} {}".format(extensionNumber, i, currentMaxDefineNumber+1)
        currentMaxDefineNumber  += 1
        
    print >> streamToPrint , "\n"
    return currentMaxDefineNumber

def generateAirportPickupToReleaseExtension(extensionNumber, featuresFixed=False, streamToPrint=stdout):
    """
        # Airport to Release Extension
    """
    if(featuresFixed == False):
        print >> streamToPrint , """
          :: (current == AIRPORTP);
              if :: f.License{} ;
                current = APR{}X{} [60];
            :: !f.License{} ;
                skip;
            fi;""".format(extensionNumber, extensionNumber, 1, extensionNumber)
    else:
        print >> streamToPrint , """
      :: (current == AIRPORTP);
                current = APR{}X{} [60];
        """.format(extensionNumber, 1)
        
    print >> streamToPrint , """
      :: (current == APR{}X{});
            current = APR{}X{} [0];
        """.format(extensionNumber, 1, extensionNumber, 2)

    print >> streamToPrint , """
      :: (current == APR{}X{});
            current = APR{}X{} [0];
        """.format(extensionNumber, 2, extensionNumber, 3)

    print >> streamToPrint , """
      :: (current == APR{}X{});
             current = RELEASEEXT{} [0];
        """.format(extensionNumber, 3, extensionNumber)
          
def generatePickupExtensionToAirportRelease(extensionNumber, featuresFixed=False,  streamToPrint=stdout):
    """
        # Pickup Extension to Airport  Release Extension
    """
    if(featuresFixed == False):
        print >> streamToPrint , """
          :: (current == PICKUPEXT{});
              if :: f.License{} ;
                current = PX{}AR{} [60];
            :: !f.License{} ;
                skip;
            fi;""".format(extensionNumber, extensionNumber, extensionNumber, 1, extensionNumber)
    else:
        print >> streamToPrint , """
      :: (current == PICKUPEXT{});
                current = PX{}AR{} [60];
        """.format(extensionNumber,  extensionNumber, 1)
        
    print >> streamToPrint , """
      :: (current == PX{}AR{});
            current = PX{}AR{} [0];
        """.format(extensionNumber, 1, extensionNumber, 2)

    print >> streamToPrint , """
      :: (current == PX{}AR{});
            current = PX{}AR{} [0];
        """.format(extensionNumber, 2, extensionNumber, 3)

    print >> streamToPrint , """
      :: (current == PX{}AR{});
            current = AIRPORTR [0];
        """.format(extensionNumber, 3)

def generateReleaseToReleaseExt(extensionNumber, featuresFixed=False,  streamToPrint=stdout):
    if(featuresFixed==False):
        print >> streamToPrint , """
          :: (current == RELEASE1);
            if :: f.License{} && f.Shuttle ;
               current = RELEASEEXT{} [15];
            :: ! (f.License{} && f.Shuttle) ;
               skip;
            fi;""".format(extensionNumber, extensionNumber, extensionNumber)    
    else :
        print >> streamToPrint , """
          :: (current == RELEASE1);
               current = RELEASEEXT{} [15];
            """.format(extensionNumber)    
        
        
def generatePickupExtToPickup(extensionNumber, featuresFixed=False, streamToPrint=stdout):
    if(featuresFixed==False):
        print >> streamToPrint , """
            :: (current == PICKUPEXT{});
                 if :: f.License{} && f.Shuttle ;
                       current = PICKUP1 [15];
                 :: !(f.License{} && f.Shuttle);
                    skip;
                 fi;""".format(extensionNumber, extensionNumber, extensionNumber)       
    else:
        print >> streamToPrint , """
            :: (current == PICKUPEXT{});
                       current = PICKUP1 [15];
            """.format(extensionNumber)       
        
def genereateRelseaseExtToPickuExt(extensionNumber,  streamToPrint=stdout):
    print >> streamToPrint , """
          :: (current == RELEASEEXT{});
               current = PICKUPEXT{} [-2];""".format(extensionNumber, extensionNumber)


def generateTaxiStandardPickupsToReleaseExtensions(extensionNumber, featuresFixed=False,  streamToPrint=stdout):

    if (featuresFixed==False):
        print >> streamToPrint ,      """
             :: (current == PICKUP1);
                 if :: f.License{} && f.Taxi ;
                    current =  RELEASEEXT{} [30]; 
                 :: ! (f.License{} && f.Taxi);
                   skip;
                 fi;""".format(extensionNumber, extensionNumber, extensionNumber)
             
        print >> streamToPrint ,  """
             :: (current == PICKUP2);
                 if :: f.License{} && f.Taxi ;
                    current =  RELEASEEXT{} [30]; 
                 :: ! (f.License{} && f.Taxi);
                   skip;
                 fi;""".format(extensionNumber, extensionNumber, extensionNumber)
    else:
        print >> streamToPrint ,      """
             :: (current == PICKUP1);
                    current =  RELEASEEXT{} [30]; 
            """.format(extensionNumber)
             
        print >> streamToPrint ,  """
             :: (current == PICKUP2);
                    current =  RELEASEEXT{} [30]; 
                """.format(extensionNumber)
        
def generateTaxiPickupExtensionToStandardReleases(extensionNumber, featuresFixed=False, streamToPrint=stdout):
    if (featuresFixed==False):
        print >> streamToPrint , """
           :: (current == PICKUPEXT{});
             if :: f.License{} && f.Taxi ;
               current = RELEASE1 [30];
             :: !(f.License{} && f.Taxi);
                skip;
             fi;""".format(extensionNumber, extensionNumber, extensionNumber)
    
        print >> streamToPrint , """         
           :: (current == PICKUPEXT{});
             if :: f.License{} && f.Taxi ;
               current = RELEASE2 [30];
             :: !(f.License{} && f.Taxi);
                skip;
             fi;""".format(extensionNumber, extensionNumber,extensionNumber)
    else:
        print >> streamToPrint , """
           :: (current == PICKUPEXT{});
               current = RELEASE1 [30];
           """.format(extensionNumber)
    
        print >> streamToPrint , """         
           :: (current == PICKUPEXT{});
               current = RELEASE2 [30];
            """.format(extensionNumber)
        
def generateTaxiFromPickupExtentionToOtherReleaseExtension(ListAllExtensionNumbers, listFeatureValues, extensionNumber, featuresFixed=False, streamToPrint=stdout):
    for receiveExtensionPickup in ListAllExtensionNumbers:
        if (featuresFixed==False):
            if not receiveExtensionPickup == extensionNumber:            
                print >> streamToPrint , """
                       :: (current == PICKUPEXT{});
                             if :: f.License{} && f.License{} ;
                               current = RELEASEEXT{} [30];
                             :: !(f.License{} && f.License{});
                                skip;
                             fi;
                       """.format(extensionNumber, extensionNumber, receiveExtensionPickup, receiveExtensionPickup, extensionNumber, receiveExtensionPickup)
        else:
            if not receiveExtensionPickup == extensionNumber:
                if listFeatureValues[receiveExtensionPickup+2] == 1:
                    print >> streamToPrint , """
                       :: (current == PICKUPEXT{});
                           current = RELEASEEXT{} [30];
                       """.format(extensionNumber, receiveExtensionPickup)
                
            

#    print >> streamToPrint , """
#    """
def generateLicenseTransitionsForAProduct(ListAllExtensionNumbers, extensionNumber, listFeatureValues, streamToPrint=stdout): 
    ShuttleId = 1 
    TaxiId = 2
    extensionNumberId = extensionNumber +  2
    
    if listFeatureValues[extensionNumberId] == 1:
        generateAirportPickupToReleaseExtension(extensionNumber, True, streamToPrint)
        
        generatePickupExtensionToAirportRelease(extensionNumber, True, streamToPrint)
        
        if listFeatureValues[ShuttleId] == 1:
            generateReleaseToReleaseExt(extensionNumber, True, streamToPrint)
        
            generatePickupExtToPickup(extensionNumber, True, streamToPrint)
        
        genereateRelseaseExtToPickuExt(extensionNumber, streamToPrint)
        
        if listFeatureValues[TaxiId] == 1:
                    
            generateTaxiPickupExtensionToStandardReleases(extensionNumber, True, streamToPrint)
        
            generateTaxiStandardPickupsToReleaseExtensions(extensionNumber, True, streamToPrint)
        
            generateTaxiFromPickupExtentionToOtherReleaseExtension(ListAllExtensionNumbers, listFeatureValues, extensionNumber, True,  streamToPrint)


def generateLicenseTransitions(ListAllExtensionNumbers, extensionNumber,  streamToPrint=stdout):    

    generateAirportPickupToReleaseExtension(extensionNumber)
    
    generatePickupExtensionToAirportRelease(extensionNumber)
    
    generateReleaseToReleaseExt(extensionNumber)
    
    generatePickupExtToPickup(extensionNumber)
    
    genereateRelseaseExtToPickuExt(extensionNumber)
    
    generateTaxiPickupExtensionToStandardReleases(extensionNumber)
    
    generateTaxiStandardPickupsToReleaseExtensions(extensionNumber)
    
    generateTaxiFromPickupExtentionToOtherReleaseExtension(ListAllExtensionNumbers, {}, extensionNumber, False)
 

def printGlobalVariables( streamToPrint=stdout):
    print >> streamToPrint , """
    int current = 0;    
    """   

def generateFinalizationCode( streamToPrint=stdout):
    print >> streamToPrint , """          
        :: else 
             -> skip;
          fi;
   od;
}""" 


def generateUnconditionalTransitions(streamToPrint=stdout):
    print >> streamToPrint , """active proctype test() {
   do  :: 
      if 
      :: (current == AIRPORTP );
         current = APR11 [50];
      :: (current == APR11 );
         current = APR12 [0];
      :: (current == APR12 );
         current = RELEASE1 [0];
      :: (current == AIRPORTP);
         current = APR21 [45];
      :: (current == APR21 );
         current =  RELEASE2 [0];        
      :: (current == PICKUP1 );
         current = P1AR1 [40];
      :: (current == P1AR1 );
         current = P1AR2 [0];
      :: (current == P1AR2 );
         current = AIRPORTR [0];
      :: (current == AIRPORTR);
         current = AIRPORTP [-5];
      :: (current == PICKUP2);
         current = P2AR1 [35];
      :: (current == P2AR1);
         current = AIRPORTR [0];
      :: (current == RELEASE2);
        current = PICKUP2 [-2];
      :: (current == RELEASE1);
        current = PICKUP1 [-2];               
      """    
     
def generateMandatoryTransitionsForAProduct(listFeatureValues, streamToPrint=stdout):
    ShuttleId = 1 
    TaxiId = 2

    generateUnconditionalTransitions(streamToPrint)
    
    if listFeatureValues[ShuttleId] == 1:
        print >> streamToPrint, """
          :: (current == PICKUP1 );              
               current = PICKUP2 [15];
        """
    else:
        print >> streamToPrint, """
          :: (current == PICKUP1 );              
               skip;
        """
 
    if listFeatureValues[ShuttleId] == 1:
        print >> streamToPrint, """
          :: (current == RELEASE2 );              
               current = RELEASE1 [15];
        """
    else:
        print >> streamToPrint, """
          :: (current == RELEASE2 );              
               skip;
        """
        
            
    if listFeatureValues[TaxiId] == 1:
        print >> streamToPrint, """
          :: (current == PICKUP1 );              
               current = RELEASE2 [30];
        """
    else:
        print >> streamToPrint, """
          :: (current == PICKUP1 );              
               skip;
        """

    if listFeatureValues[TaxiId] == 1:
        print >> streamToPrint, """
          :: (current == PICKUP2 );              
               current = RELEASE1 [30];
        """
    else:
        print >> streamToPrint, """
          :: (current == PICKUP2 );              
               skip;
        """
    
                     
def generateMandatoryTransitions( streamToPrint=stdout):
    print >> streamToPrint , """active proctype test() {
   do  :: 
      if 
      :: (current == AIRPORTP );
         current = APR11 [50];
      :: (current == APR11 );
         current = APR12 [0];
      :: (current == APR12 );
         current = RELEASE1 [0];
      :: (current == AIRPORTP);
         current = APR21 [45];
      :: (current == APR21 );
         current =  RELEASE2 [0];        
      :: (current == PICKUP1 );
         current = P1AR1 [40];
      :: (current == P1AR1 );
         current = P1AR2 [0];
      :: (current == P1AR2 );
         current = AIRPORTR [0];
      :: (current == PICKUP1 );              
            if :: f.Shuttle ;
           current = PICKUP2 [15];
        :: !f.Shuttle;
           skip;
        fi;
      :: (current == PICKUP1);
         if :: f.Taxi ;
                current = RELEASE2 [30]; 
             :: !f.Taxi ;
                 skip;
         fi;
      :: (current == AIRPORTR);
         current = AIRPORTP [-5];
      :: (current == PICKUP2);
         current = P2AR1 [35];
      :: (current == P2AR1);
         current = AIRPORTR [0];
      :: (current == PICKUP2);
         if :: f.Taxi ;
            current =  RELEASE1 [30]; 
         :: !f.Taxi;
           skip;
         fi;
      :: (current == RELEASE2);
        current = PICKUP2 [-2];
      :: (current == RELEASE1);
        current = PICKUP1 [-2];        
      :: (current == RELEASE2);
         if :: f.Shuttle ;
           current = RELEASE1 [15];
         :: ! f.Shuttle;
             skip;
         fi;                 
      """