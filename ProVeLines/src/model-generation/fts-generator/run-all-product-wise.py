'''
Created on Oct 15, 2015

@author: rafaelolaechea
'''
import math

if __name__ == '__main__':
    for j in range(1, 12):
        numberOfProducts = int(math.pow(2, j + 2))
        
        for repetitions in range(0, 10):
            print  "echo Repetition  {} >> product-results-license-{}.txt;".format(repetitions+1, j)
            for  i in range(0,numberOfProducts):        
                productFilename = "taxi-license{}-product-{}-.pml".format(j, i)                        
                print """(time ./provelines  -silent -optimizeAlg1  -checkLimitAverage model-generation/fts-generator/taxi-license{}-product-{}-.pml ) 2>>  product-results-license-{}.txt;""".format(j,i,j)
