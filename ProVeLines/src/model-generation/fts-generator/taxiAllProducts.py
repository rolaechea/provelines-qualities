'''
Created on Oct 15, 2015

@author: rafaelolaechea
'''
import sys, math

from taxiPrinting import printFeatures, generateBasicDefines, \
generateExtensionDefines, printGlobalVariables, \
generateMandatoryTransitions, generateLicenseTransitionsForAProduct, \
generateFinalizationCode, generateMandatoryTransitionsForAProduct


#def generateMandatoryTransitionsForAProduct(listFeatureValues, fdPml):
#    pass



def getFeatureName(featureNumber):
    if featureNumber == 1:
        return "Shuttle"
    elif featureNumber == 2:
        return "Taxi"
    else:
        return "License{}".format(featureNumber)

def generateProductPML(numberOfLicenseExtensions, i, listFeatureValues):
    """
    Generates a PML file for the product described by the dictionary listFeatureValues.
    
    Generated filename is called taxi-license{#}-product-{#i}-.pml
    i ranges from 0 to (2^(numberOfLicenseExtensions+2)) -1
    """
    productFilename = "taxi-license-dense-{}-product-{}-.pml".format(numberOfLicenseExtensions, i)
    
    fdPml = open(productFilename, "w")
        
    maxDefineNumber = generateBasicDefines(fdPml)
    
    for i in range(1, numberOfLicenseExtensions+1):
        maxDefineNumber = generateExtensionDefines(i, maxDefineNumber, fdPml)    


    printGlobalVariables(fdPml)    
            
    generateMandatoryTransitionsForAProduct(listFeatureValues, fdPml)
    
    for i in range(1, int(sys.argv[1])+1):
        generateLicenseTransitionsForAProduct(range(1, int(sys.argv[1])+1), i, listFeatureValues, fdPml)
        
    generateFinalizationCode(fdPml)
    
    fdPml.close()
    
    
if __name__ == '__main__':

    numberOfLicenseExtensions = int(sys.argv[1])
    
    numberOfFeatures = numberOfLicenseExtensions + 2
    
    print "Number of Features {}".format(numberOfFeatures)
    
    numberOfProducts = int(math.pow(2, numberOfFeatures))

    for  i in range(0,numberOfProducts):
        listFeatureValues = {}
        
        productId = i
        
        for featureIndex in range(1, numberOfFeatures+1):
            listFeatureValues[featureIndex] = productId % 2
            
            productId = productId / 2
        
        generateProductPML(numberOfLicenseExtensions, i, listFeatureValues)
        print listFeatureValues
        
    print numberOfProducts