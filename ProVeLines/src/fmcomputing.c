/*
 * fmcomputing.c
 *
 *  Created on: Apr 22, 2015
 *      Author: rafaelolaechea
 */

#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>
#include <string.h>
#include "error.h"
#include "main-fm.h"
#include "main.h"
#include "list.h"
#include "stack.h"
#include "hashtableGen.h"
#include "boolFct.h"
#include "ltl.h"
#include "tvl.h"
#include "symbols.h"
#include "automata.h"
#ifdef CLOCK
	#include "clockZone.h"
	#include "federation.h"
#endif
#include "state.h"
#include "execution.h"
#include "tracing.h"
#include "qualitycomputation.h"
#include "checking.h"
#ifndef CLOCK
	#include "simulation.h"
#endif
#include "y.tab.h"
#include "lexer.h"
#include "qualityparser.h"

extern FILE* yyin;
extern int yyparse();
extern void init_lex();

// Settings defined in main
char * path;
byte trace = FULL_TRACE;
byte keepTempFiles = 1;
byte spinMode = 0;
byte optimisedSpinMode = 0;
byte exhaustive = 0;
byte fullDeadlockCheck = 0;
byte sim = 0;
byte stutterStep = 0;
long int limitExploration;

// Other global variables from main
ptSymTabNode globalSymTab = NULL;
ptSymTabNode globalSymTab2 = NULL;
ptMTypeNode mtypes = NULL;
ptMTypeNode mtypes2 = NULL;
ptSymTabNode neverClaim;
ptList props, props2;
void * _handshake_transit = NULL;

// Profiler variables
struct timeval _profileTimeBegin, _profileTimeEnd;
char* _profilerStr = NULL;


/**
 * Simply copies a file byte by byte; could be made more efficient.
 */
int copyFile(char* source, char* target) {
	FILE* fsource;
	FILE* ftarget;
	fsource = fopen(source, "r");
	ftarget = fopen(target, "w");

	if(fsource != NULL && ftarget != NULL)  {
		char buffer;
		buffer = fgetc(fsource);
		while(!feof(fsource)) {
			fputc(buffer, ftarget);
			buffer = fgetc(fsource);
		}
		fclose(fsource);
		fclose(ftarget);
		return 1;
	}

	if(fsource != NULL) fclose(fsource);
	if(ftarget != NULL) fclose(ftarget);
	return 0;
}


/*
 *
 * 1. Load Feature Model from dimacs file.
 *
 * 2. Load Constraints.
 *
 * 3. Solve and give a valid feature Model instance
 *
 *
 *
 *
 *
 *
 */
int main(int argc, char *argv[]) {




	int minNumberArguments = 2;

	if(argc < minNumberArguments){
		failure("Incorrect Number of Arguments");
	}



	const int programNameLength = 21;
	const int programNameLengthMinusOne = 20;

	path = (char *)malloc((strlen(argv[0]) - programNameLengthMinusOne) * sizeof(char));

	int i =0;
	for(i = 0; i < strlen(argv[0]) - programNameLength; i++)
		path[i] = argv[0][i];
	path[strlen(argv[0]) - programNameLength] = '\0';

	initBoolFct();

	char * pmlFile = argv[1];


	int pmlFileLen = strlen(pmlFile);

	char * tvlFile = (char *) malloc((pmlFileLen +1) * sizeof(char));

	strcpy(tvlFile, pmlFile);
	tvlFile[pmlFileLen - 3] = 't';
	tvlFile[pmlFileLen - 2] = 'v';
	tvlFile[pmlFileLen - 1] = 'l';
	tvlFile[pmlFileLen] = '\0';

    char *attFile = NULL;
    char *conFile = NULL;

	if(argc >= 3){
		attFile = argv[2];
	}

	if (argc >= 4){
		conFile = argv[3];
	}


	loadFeatureModel(tvlFile, NULL);

	initSolverWithFD(getFeatureModelClauses());

	if(attFile!=NULL){
		loadQualityAttributes(attFile);

		if(conFile!=NULL){
			createConstraints(conFile);
		}
	}

	if (argc >= 5 ){
		// Two cases either all or a filename .

		const char * commandName = argv[4];

		if (strcmp(commandName, "all")==0){
			enumerateAllSatisfyingProducts(NULL);
		} else {
			if ( argc >= 6 ){
				const char *booleanFormulaFilename = argv[5];
				enumerateAllSatisfyingProducts(booleanFormulaFilename, 1);
			} else {
				failure("You must provivde a file containing the formula to combine quality attributes with");
			}
		}

		/*
		if ( ){

		}
		else (){

		}
		*/





	} else {
		printf("Finding Single Product\n");
		byte AttributedFM = isSatisfiableWithBaseFDAndQualityConstraints(NULL);

		printf("%s ", AttributedFM ? " Is Satisfiable \n" : "Is Not Satisfiable" );
	}



}



