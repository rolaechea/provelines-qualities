if [ "$1" = '' ]; then
	TS=$(date +%Y%m%d)
else
	TS=$1
fi;

svn update > /dev/null
REV=$(svn info -r 'HEAD' |grep Revision: |cut -c11-)

mkdir ../releases/ 2> /dev/null
rm -rf ../releases/snip-r$REV-$TS
mkdir ../releases/snip-r$REV-$TS 2> /dev/null

svn export ../src ../releases/snip-r$REV-$TS/src
svn export ../test ../releases/snip-r$REV-$TS/examples
cp "../DIFFERENCES TO SPIN" "../releases/snip-r$REV-$TS/DIFFERENCES TO SPIN"
cp ../COPYING ../releases/snip-r$REV-$TS/COPYING
cp ../README ../releases/snip-r$REV-$TS/README
cp y.tab.c ../releases/snip-r$REV-$TS/src/y.tab.c
cp y.tab.h ../releases/snip-r$REV-$TS/src/y.tab.h
cp lex.yy.c ../releases/snip-r$REV-$TS/src/lex.yy.c

cd ../releases
tar -czf snip-r$REV-$TS.tar.gz snip-r$REV-$TS

echo "Packaged current working copy as snip-r$REV-$TS.zip"