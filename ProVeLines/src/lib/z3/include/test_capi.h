/*
 * test_capi.h
 *
 *  Created on: Oct 16, 2014
 *      Author: rafaelolaechea
 */
#ifndef LIB_Z3_INCLUDE_TEST_CAPI_H_
#define LIB_Z3_INCLUDE_TEST_CAPI_H_
#include<stdio.h>
#include<stdlib.h>
#include<stdarg.h>
#include<memory.h>
#include<setjmp.h>
#include<z3.h>



Z3_ast mk_bool_var(Z3_context ctx, const char * name) ;
Z3_ast mk_int(Z3_context ctx, int v) ;
Z3_ast mk_int_var(Z3_context ctx, const char * name) ;

#endif /* LIB_Z3_INCLUDE_TEST_CAPI_H_ */
