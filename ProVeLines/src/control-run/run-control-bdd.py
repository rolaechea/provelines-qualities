'''
Created on Oct 27, 2014

@author: rafaelolaechea
'''
import os

_ProvelinesExecutable = "./provelines" 

properties = ["!([]<> (stateReady && highWater && userStart))" , \
        "!([]<> stateReady)" , \
        "!([]<> stateRunning)" , \
        "!([]<> stateStopped)" , \
        "!([]<> stateMethanestop)" , \
        "!([]<> stateLowstop)" , \
        "!([]<> readCommand)" , \
        "!([]<> readAlarm)" , \
        "!([]<> readLevel)" , \
        "!(([]<> readCommand) && ([]<> readAlarm) && ([]<> readLevel)) " , \
        "!([]<>  pumpOn)" , \
        "!([]<> !pumpOn)" , \
        "!(([]<> pumpOn) && ([]<> !pumpOn))" , \
        "!([]<>  methane)" , \
        "!([]<> !methane)" , \
        "!(([]<> methane) && ([]<> !methane))" , \
        "[] (!pumpOn || stateRunning)" , \
        "[] (methane ->  (<> stateMethanestop))" , \
        "[] (methane -> !(<> stateMethanestop))" , \
        "[] (pumpOn || !methane)" , \
        "[] ((pumpOn && methane) -> <> !pumpOn)" , \
        "(([]<> readCommand) && ([]<> readAlarm) && ([]<> readLevel)) -> [] ((pumpOn && methane) -> <> !pumpOn)" , \
        "!<>[] (pumpOn && methane)" , \
        "(([]<> readCommand) && ([]<> readAlarm) && ([]<> readLevel)) -> !<>[] (pumpOn && methane)" , \
        "[] ((!pumpOn && methane && <>!methane) -> ((!pumpOn) U !methane))" , \
        "[] ((highWater && !methane) -> <>pumpOn)" , \
        "!(<> (highWater && !methane))" , \
        "(([]<> readCommand) && ([]<> readAlarm) && ([]<> readLevel)) -> ([] ((highWater && !methane) -> <>pumpOn))" , \
        "[] ((highWater && !methane) -> !<>pumpOn)" , \
        "!<>[] (!pumpOn && highWater)" , \
        "(([]<> readCommand) && ([]<> readAlarm) && ([]<> readLevel)) -> (!<>[] (!pumpOn && highWater))" , \
        "!<>[] (!pumpOn && !methane && highWater)" , \
        "(([]<> readCommand) && ([]<> readAlarm) && ([]<> readLevel)) -> (!<>[] (!pumpOn && !methane && highWater))" , \
        "[] ((pumpOn && highWater && <>lowWater) -> (pumpOn U lowWater))" , \
        "!<> (pumpOn && highWater && <>lowWater)" , \
        "[] (lowWater -> (<>!pumpOn))" , \
        "(([]<> readCommand) && ([]<> readAlarm) && ([]<> readLevel)) -> ([] (lowWater -> (<>!pumpOn)))" , \
        "!<>[] (pumpOn && lowWater)" , \
        "(([]<> readCommand) && ([]<> readAlarm) && ([]<> readLevel)) -> (!<>[] (pumpOn && lowWater))" , \
        "[] ((!pumpOn && lowWater && <>highWater) -> ((!pumpOn) U highWater))" , \
        "!<> (!pumpOn && lowWater && <>highWater)"]

_cmdParameters = " -exhaustive -check -ltl \'%s\'  -t  -nt ../test/minepump.pml"


_ouputLogFile = "output_bdd_run.txt"
_timeLogFile = "time_bdd_run.txt"

import os.path

if __name__ == '__main__':

    i =0
    print "---Running Control Benchmark for Prove-Lines---"
    print _ProvelinesExecutable + (_cmdParameters % (property,))
    
    for property in  properties:          
        i = i + 1

        print "\t Start Running Property %s" % (property,)


        fpFileContents = open(_ouputLogFile, 'a')
        fpFileContents.write('%s\n' % (property,))
        fpFileContents.close()
        
        fpFileTime = open(_timeLogFile, 'a')
        fpFileTime.write('%s\n' % (property,))
        fpFileTime.close()

        
        executionCommand = "{ time  " + _ProvelinesExecutable + (_cmdParameters % (property,))    + ( " >> %s ; }  2>> %s  " % (_ouputLogFile, _timeLogFile ))    
        os.system(executionCommand)

        if   os.path.isfile("__printbool.tmp"):
            os.system("mv __printbool.tmp bad-products-property-%d.tmp" % i )

        print "\t End Running Property %s \n " % (property,)

        
    