---Running Control Benchmark for Prove-Lines---
./provelines -exhaustive -check -ltl '<type 'property'>'  -t  -nt ../test/minepump.pml
	 Start Running Property !([]<> (stateReady && highWater && userStart))
	 End Running Property !([]<> (stateReady && highWater && userStart)) 
 
	 Start Running Property !([]<> stateReady)
	 End Running Property !([]<> stateReady) 
 
	 Start Running Property !([]<> stateRunning)
	 End Running Property !([]<> stateRunning) 
 
	 Start Running Property !([]<> stateStopped)
	 End Running Property !([]<> stateStopped) 
 
	 Start Running Property !([]<> stateMethanestop)
	 End Running Property !([]<> stateMethanestop) 
 
	 Start Running Property !([]<> stateLowstop)
	 End Running Property !([]<> stateLowstop) 
 
	 Start Running Property !([]<> readCommand)
	 End Running Property !([]<> readCommand) 
 
	 Start Running Property !([]<> readAlarm)
	 End Running Property !([]<> readAlarm) 
 
	 Start Running Property !([]<> readLevel)
	 End Running Property !([]<> readLevel) 
 
	 Start Running Property !(([]<> readCommand) && ([]<> readAlarm) && ([]<> readLevel)) 
	 End Running Property !(([]<> readCommand) && ([]<> readAlarm) && ([]<> readLevel))  
 
	 Start Running Property !([]<>  pumpOn)
	 End Running Property !([]<>  pumpOn) 
 
	 Start Running Property !([]<> !pumpOn)
	 End Running Property !([]<> !pumpOn) 
 
	 Start Running Property !(([]<> pumpOn) && ([]<> !pumpOn))
	 End Running Property !(([]<> pumpOn) && ([]<> !pumpOn)) 
 
	 Start Running Property !([]<>  methane)
	 End Running Property !([]<>  methane) 
 
	 Start Running Property !([]<> !methane)
	 End Running Property !([]<> !methane) 
 
	 Start Running Property !(([]<> methane) && ([]<> !methane))
	 End Running Property !(([]<> methane) && ([]<> !methane)) 
 
	 Start Running Property [] (!pumpOn || stateRunning)
	 End Running Property [] (!pumpOn || stateRunning) 
 
	 Start Running Property [] (methane ->  (<> stateMethanestop))
	 End Running Property [] (methane ->  (<> stateMethanestop)) 
 
	 Start Running Property [] (methane -> !(<> stateMethanestop))
	 End Running Property [] (methane -> !(<> stateMethanestop)) 
 
	 Start Running Property [] (pumpOn || !methane)
	 End Running Property [] (pumpOn || !methane) 
 
	 Start Running Property [] ((pumpOn && methane) -> <> !pumpOn)
	 End Running Property [] ((pumpOn && methane) -> <> !pumpOn) 
 
	 Start Running Property (([]<> readCommand) && ([]<> readAlarm) && ([]<> readLevel)) -> [] ((pumpOn && methane) -> <> !pumpOn)
	 End Running Property (([]<> readCommand) && ([]<> readAlarm) && ([]<> readLevel)) -> [] ((pumpOn && methane) -> <> !pumpOn) 
 
	 Start Running Property !<>[] (pumpOn && methane)
	 End Running Property !<>[] (pumpOn && methane) 
 
	 Start Running Property (([]<> readCommand) && ([]<> readAlarm) && ([]<> readLevel)) -> !<>[] (pumpOn && methane)
	 End Running Property (([]<> readCommand) && ([]<> readAlarm) && ([]<> readLevel)) -> !<>[] (pumpOn && methane) 
 
	 Start Running Property [] ((!pumpOn && methane && <>!methane) -> ((!pumpOn) U !methane))
	 End Running Property [] ((!pumpOn && methane && <>!methane) -> ((!pumpOn) U !methane)) 
 
	 Start Running Property [] ((highWater && !methane) -> <>pumpOn)
	 End Running Property [] ((highWater && !methane) -> <>pumpOn) 
 
	 Start Running Property !(<> (highWater && !methane))
	 End Running Property !(<> (highWater && !methane)) 
 
	 Start Running Property (([]<> readCommand) && ([]<> readAlarm) && ([]<> readLevel)) -> ([] ((highWater && !methane) -> <>pumpOn))
	 End Running Property (([]<> readCommand) && ([]<> readAlarm) && ([]<> readLevel)) -> ([] ((highWater && !methane) -> <>pumpOn)) 
 
	 Start Running Property [] ((highWater && !methane) -> !<>pumpOn)
	 End Running Property [] ((highWater && !methane) -> !<>pumpOn) 
 
	 Start Running Property !<>[] (!pumpOn && highWater)
	 End Running Property !<>[] (!pumpOn && highWater) 
 
	 Start Running Property (([]<> readCommand) && ([]<> readAlarm) && ([]<> readLevel)) -> (!<>[] (!pumpOn && highWater))
	 End Running Property (([]<> readCommand) && ([]<> readAlarm) && ([]<> readLevel)) -> (!<>[] (!pumpOn && highWater)) 
 
	 Start Running Property !<>[] (!pumpOn && !methane && highWater)
	 End Running Property !<>[] (!pumpOn && !methane && highWater) 
 
	 Start Running Property (([]<> readCommand) && ([]<> readAlarm) && ([]<> readLevel)) -> (!<>[] (!pumpOn && !methane && highWater))
	 End Running Property (([]<> readCommand) && ([]<> readAlarm) && ([]<> readLevel)) -> (!<>[] (!pumpOn && !methane && highWater)) 
 
	 Start Running Property [] ((pumpOn && highWater && <>lowWater) -> (pumpOn U lowWater))
	 End Running Property [] ((pumpOn && highWater && <>lowWater) -> (pumpOn U lowWater)) 
 
	 Start Running Property !<> (pumpOn && highWater && <>lowWater)
	 End Running Property !<> (pumpOn && highWater && <>lowWater) 
 
	 Start Running Property [] (lowWater -> (<>!pumpOn))
	 End Running Property [] (lowWater -> (<>!pumpOn)) 
 
	 Start Running Property (([]<> readCommand) && ([]<> readAlarm) && ([]<> readLevel)) -> ([] (lowWater -> (<>!pumpOn)))
	 End Running Property (([]<> readCommand) && ([]<> readAlarm) && ([]<> readLevel)) -> ([] (lowWater -> (<>!pumpOn))) 
 
	 Start Running Property !<>[] (pumpOn && lowWater)
	 End Running Property !<>[] (pumpOn && lowWater) 
 
	 Start Running Property (([]<> readCommand) && ([]<> readAlarm) && ([]<> readLevel)) -> (!<>[] (pumpOn && lowWater))
	 End Running Property (([]<> readCommand) && ([]<> readAlarm) && ([]<> readLevel)) -> (!<>[] (pumpOn && lowWater)) 
 
	 Start Running Property [] ((!pumpOn && lowWater && <>highWater) -> ((!pumpOn) U highWater))
	 End Running Property [] ((!pumpOn && lowWater && <>highWater) -> ((!pumpOn) U highWater)) 
 
	 Start Running Property !<> (!pumpOn && lowWater && <>highWater)
	 End Running Property !<> (!pumpOn && lowWater && <>highWater) 
 
