#define stateHighway (drivingMode == highway)

typedef features {
	bool Sportf;
	bool Gas;
	bool Diesel;
	bool Small;
	bool Large
}        
features f;

mtype = {warm, cold, city, highway}

mtype engineState = cold;
mtype drivingMode = city;
bool engineOn = false;
bool sportEngaged = false;

int previousSpeed = 0;
int speed = 0;
int temperature =  12;


active proctype carUpdate() {
	engineOn = true;
next_time:	
	// Speed Update
	if 	
		:: skip 	   ->
			previousSpeed = speed;	
			speed = speed ;
		:: speed < 205 -> 
			previousSpeed = speed; 
			speed = speed + 1;
		:: speed < 204 -> 
			previousSpeed = speed; speed = speed + 2;
		:: speed < 203 -> 
			previousSpeed = speed; speed = speed + 3;
		:: speed < 202 -> 
			previousSpeed = speed; speed = speed + 4;		
		:: speed < 201 -> 
			previousSpeed = speed; speed = speed + 5;
		:: speed < 200 -> 
			previousSpeed = speed; speed = speed + 6;
		:: speed < 199 -> 
			previousSpeed = speed; speed = speed + 7;
		:: speed < 198 -> 
			previousSpeed = speed; speed = speed + 8;
		:: speed < 197 -> 
			previousSpeed = speed; speed = speed + 9;
		:: speed < 196 -> 
			previousSpeed = speed; speed = speed + 10;
/ *
  * Speed probabilistically goes up/down by 10.
  *
  /
  

			
			
		:: speed >  9 ->  
			previousSpeed = speed; speed = speed - 10;
		:: speed >  8 ->  
			previousSpeed = speed; speed = speed - 9;
		:: speed >  7 ->  
			previousSpeed = speed; speed = speed - 8;
		:: speed >  6 ->  
			previousSpeed = speed; speed = speed - 7;
		:: speed >  5 ->  
			previousSpeed = speed; speed = speed - 6;
		:: speed >  4 ->  
			previousSpeed = speed; speed = speed - 5;
		:: speed >  3 ->  
			previousSpeed = speed; speed = speed - 4;
		:: speed >  2 ->  
			previousSpeed = speed; speed = speed - 3;
		:: speed >  1 ->  
			previousSpeed = speed; speed = speed - 2;		
		:: speed >  0 ->  
			previousSpeed = speed; speed = speed - 1;					
	fi;

	// Temperature Engine Update
	if 	
		:: skip 	   ->
			 temperature = temperature ;
		:: temperature < 125 ->
			 temperature = temperature + 1;
		:: temperature < 125 -> 
			temperature = temperature + 2;
		:: temperature < 125 -> 
			temperature = temperature + 3;
		:: temperature < 125 -> 
			temperature = temperature + 4;		
		:: temperature < 125 -> 
			temperature = temperature + 5;
	fi;
	

	// Driving Mode Update	
	if	
		:: (speed > 50 && drivingMode == city) ->
				 drivingMode = highway;
		:: (speed <= 50 && drivingMode == highway) -> 
				drivingMode = city;
		:: !(speed > 50 && drivingMode == city) && !(speed <= 50 && drivingMode == highway)  ->
				 drivingMode = drivingMode;
	fi;
	
	// Warm/Cold Update
	if	
		:: (temperature > 30 && engineState == cold) ->
				 engineState = warm;
		:: (temperature <= 30 && engineState == warm) -> 
				engineState = cold;
		:: !(temperature > 30 && engineState == cold) && !(temperature <= 30 && engineState == warm)  ->
				engineState = engineState;
	fi;

	
	// Sport Mode Switching Update 
	if 
		:: f.Sportf ->
			if  :: sportEngaged == false -> 
					sportEngaged = true;
				:: 	sportEngaged == true ->
					sportEngaged = false 
				:: skip;
					sportEngaged = sportEngaged;
			fi;			
		:: ! f.Sportf ->
			skip;
	fi;
	
	skip;
	
	Probabilistically: 
		[ ]  goto next_time
		[ ] skip;
}
