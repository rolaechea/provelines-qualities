/*
 * This model is based on the code from the article 
 *
 *   Kramer, J.; Magee, J.; Sloman, M. & Lister, A. CONIC: an integrated 
 *   approach to distributed computer control systems Computers and Digital 
 *   Techniques, IEE Proceedings E, 1983, 130, 1-10
 *
 * A similar model, formulated directly as an FTS was used in the ICSE 2010
 * paper about FTS (Classen et al., "Model Checking Lots of Systems").  See
 * also "Modelling with FTS: a Collection of Illustrative Examples".
 *
 * 
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Macros used in the above properties:
 */


typedef features {
	bool Command;
 	bool MethaneSensor
}

features f;

mtype = {stop, start, alarm, low, medium, high, ready, running, stopped, methanestop, lowstop, commandMsg, alarmMsg, levelMsg}

chan cCmd = [0] of {mtype}; 	/* stop, start			*/
chan cAlarm = [0] of {mtype}; 	/* alarm                */
chan cMethane = [0] of {mtype}; /* methanestop, ready   */
chan cLevel = [0] of {mtype}; 	/*  medium, high    */


mtype pstate = stopped; 		/* ready, running, stopped, methanestop, lowstop */
mtype readMsg = commandMsg; 		/* commandMsg, alarmMsg, levelMsg */
mtype waterLevel = medium;

bool pumpOn = false;
bool methane = false;




active proctype controller() {
	mtype pcommand = start;
	mtype level = medium;
	
	do	::	atomic {
				cCmd?pcommand;
				readMsg = commandMsg $0$; 
			};
			if	::	pcommand == stop;
							if	::	atomic {
										pstate == running ;
										pumpOn = false $0$;
									}
								::	else -> skip;
								fi;
							pstate = stopped $0$ ;
				::	pcommand == start;
							if	::	atomic {
										pstate != running;
										pstate = ready $0$;
									};
								::	else -> skip;
								fi;
				::	else -> assert(false);
			fi;
			cCmd!pstate;
			
		::	atomic { 
				cAlarm?_;
				readMsg = alarmMsg $0$;
			};
			 skip;
		::	atomic { 
				cLevel?level;
				readMsg = levelMsg $0$;
			};
			if	::	level == high;
							if	::	pstate == ready  ||  pstate == lowstop;
											skip;
											atomic {
												pstate = running $20$;
												pumpOn = true $0$;
											};
								::	else -> skip;
								fi;
				::	level == medium;
					skip;
				fi;			
		od;
}

active proctype user() {
	mtype uwants = stop; 			/* what the user wants */
	
	do	::	if	::	uwants = start $0$;
				::	uwants = stop $0$;
				fi;
			cCmd!uwants;
			cCmd?_;			/* Sends back the state; ignore it */
		od;
}

active proctype methanealarm() {
	do	:: 	methane = true $0$;
			cAlarm!alarm;
		::	methane = false $0$;
		od;
}

active proctype methanesensor() {
	do	:: 	atomic {
				cMethane?_;
				if	::	methane;
						cMethane!methanestop;
					::	!methane;
						cMethane!ready;
					fi;
			};
		od;
}


active proctype watersensor() {
	do	:: 	atomic {
				if	
					::	waterLevel == medium   && !pumpOn ->
						if	
							:: waterLevel = medium $0$;
							:: waterLevel = high $0$;
						fi;
					::	waterLevel == high && pumpOn ->
						if	:: waterLevel = medium $0$;
							:: waterLevel = high $0$;
						fi;
					::	else ->
						skip;
				fi;
				cLevel!waterLevel;
			};
		od;
};