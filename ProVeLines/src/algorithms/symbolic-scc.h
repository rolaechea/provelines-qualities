/*
 * symbolic-scc.h
 *
 *  Created on: Sep 8, 2015
 *      Author: rafaelolaechea
 */
#ifndef ALGORITHMS_SYMBOLIC_SCC_H_
#define ALGORITHMS_SYMBOLIC_SCC_H_

#include "finishing-times-adt.h"
#include "abstractSCCTree.h"


struct TripletTuple_ {
	ptState stateLabel;	 //  State Label
	ptBoolFct edgeLabel; // Edge Label.
	ptSymbolicNode TreeNode; // Tree Node
};

typedef struct TripletTuple_ tTripletTuple;
typedef struct TripletTuple_ * ptTripletTuple;


// Define ...
struct hashtable* SymbolicSCCReachability(ptState state,
		ptBoolFct stateFeatureExpression,
		struct hashtable*  ReachabilityExclude,
		struct hashtable**  newReachabilityExclude,
		ptAbstractSCCTree  * ptRetTree
);


struct hashtable* SymbolicSCCReachabilityWithAbstractTransitions(ptState state,
		ptBoolFct stateFeatureExpression,
		struct hashtable*  ReachabilityExclude,
		struct hashtable**  newReachabilityExclude);

struct stackEltTranspose {
	ptState state;
	ptBoolFct features;
	ptList OutgoingLinksTranspose;
	ptList OutgoingLinksTransposeCurrent;
};

typedef struct stackEltTranspose tStackEltTranspose;
typedef struct stackEltTranspose * ptStackEltTranspose;

ptTripletTuple createTripletTuple(ptSymbolicNode Node, ptState stateLabel, ptBoolFct edgeLabel);
void computeSymbolicScc();
struct hashtable *getSymbolicComponent(ptSymbolicNode TreeNode);
ptAbstractSCCTree getSymbolicComponentTree(ptSymbolicNode TreeNode);

int countStatesInScc(struct hashtable * sccComponent);

bool moreTransposeTransitionsAvailable(ptStackEltTranspose CurrentElt);
ptStackEltTranspose createSccStackElement(ptState state, ptBoolFct features);

#endif /* ALGORITHMS_SYMBOLIC_SCC_H_ */
