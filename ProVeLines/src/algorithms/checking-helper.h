/*
 * checking-helper.h
 *
 *  Created on: Apr 10, 2015
 *      Author: rafaelolaechea
 */
#ifndef ALGORITHMS_CHECKING_HELPER_H_
#define ALGORITHMS_CHECKING_HELPER_H_

#include <stdbool.h>
#include "automata.h"
#include "execution.h"
#include "../helper/stack.h"

bool isSafetyPropertyViolationFound(ptFsmNode neverNode);
bool areExcutableTransitionsComputed(ptStackElt current);
bool moreTransitionsAvailable(ptStackElt current);
void initializeExecutableTransitions(ptStackElt current, ptList *E,
		ptSymTabNode globalSymTab, ptMTypeNode mtypes, unsigned int _nbErrors,
		byte *hasDeadlock, ptBoolFct *noOutgoing, byte *resetExclusivity);

void checkInitialization(ptState init);


#endif /* ALGORITHMS_CHECKING_HELPER_H_ */
