/*
 * mean-cycle-structure.c

 *
 *  Created on: Oct 1, 2015
 *      Author: rafaelolaechea
 */
#include "mean-cycle-structure.h"
#include "scc-analysis.h"

/*
 * Creates a new empty D Structure.
 *
 *	Initialize empty hasthables for it but does not fill them.
 *
 */
ptDSymbolicVals createNewDStructure(int numberOfItems){

	ptDSymbolicVals ptDStructure = (ptDSymbolicVals) malloc(sizeof(tDSymbolicVals));

	if(!ptDStructure)
		failure("Out of memory (creating D structure).\n");

	ptDStructure->numberStates = numberOfItems;

	ptDStructure->KHashTables =  (struct hashtable **) malloc(sizeof(struct hashtable *) * (numberOfItems+1));

	int i = 0;

	for(i = 0; i <= numberOfItems; i++){
		ptDStructure->KHashTables[i] = create_hashtableWrapper();
	}

	return ptDStructure;
}

/*
 * Creates a new empty M Structure.
 *
 *	Initialize empty hasthables for it but does not fill them.
 *
 */
ptMSymbolicValsCollector createNewMStructure(int numberOfItems){
	ptMSymbolicValsCollector ptMStructure = (ptMSymbolicValsCollector) malloc(sizeof(tMSymbolicValsCollector));

	if(!ptMStructure)
		failure("Out of memory (creating D structure).\n");

	ptMStructure->numberStates = numberOfItems;

	ptMStructure->aHashTable = create_hashtableWrapper();

	return ptMStructure;
}

/*
 *
 * Creates a single element in M and set to Plus Infinity
 *
 */
void setAndCreateElementMSingleFExpToPlusInfinity(ptMSymbolicValsCollector M,  unsigned int Key,
		ptHtState s, ptBoolFct featureExpression){
	struct hashtable *relevantTable = M->aHashTable;

	// Inserts a new element to table
	insertInfinityStateElementIntoTable(relevantTable,  Key,  s,  featureExpression, false);
}

/*
 *
 * Creates a single element in D and set to Negative Infinity
 *
 */
void setAndCreateElementDSingleFExpToInfinity(ptDSymbolicVals D, int k, unsigned int Key,
		ptHtState s, ptBoolFct featureExpression){
	struct hashtable *relevantTable = D->KHashTables[k];

	// Inserts a new element with .... to relevant table .
	//Search in relevant table for item with key = Key
	insertInfinityStateElementIntoTable(relevantTable,  Key,  s,  featureExpression, true);

}

/*
 *
 * Performs an insert.
 *
 * Borrows code from hasthable insert, including "remove"
 *
 *If insertNegativeInfinity == true , Neg-Inf is inserted
 *If insertNegativeInfinity == false, Plus-Inf is inserted
 * Precondition
 * 	Key is not an element of relevantTable
 *
 */
void insertInfinityStateElementIntoTable(struct hashtable *relevantTable,
		unsigned int Key,
		ptHtState originalStateFromSCC,
		ptBoolFct featureExpression,
		bool insertNegativeInfinity
	){



	ptHtKey keyP = (ptHtKey) malloc(sizeof(tHtKey));
	if(!keyP)
		failure("Out of memory (creating hashtable key).\n");

	*keyP = Key;

	ptValue valP;
	valP = remove_some(relevantTable, keyP);

	if(!valP) {
		valP = (ptValue) malloc(sizeof(tValue));
		if(!valP)
			failure("Out of memory (creating hashtable value).\n");
		valP->list = NULL;
	}


	ptHtDState hs = (ptHtDState) malloc(sizeof(tHtDState));

	hs->payload = originalStateFromSCC->payload;
	hs->payloadSize = originalStateFromSCC->payloadSize;

	if(insertNegativeInfinity){
		hs->pairsOfWandF = listAdd(NULL, createNegativeInfinityAndFexpPair(featureExpression));
	} else {
		hs->pairsOfWandF = listAdd(NULL, createPositiveInfinityAndFexpPair(featureExpression));
	}

	hs->numberOfPairs = 1;


	valP->list = listAdd(valP->list, hs);

	if(!insert_some(relevantTable, keyP, valP))
		failure("Out of memory (inserting state in hashtable)\n");
}

/*
 *
 * Checks in a table holding ptHtDState if a ptHtDState with the given Key and Payload
 *
 *
 * Returns a  ptHtDState pointing to a ptHtDState and true if found.
 *
 * False otherwise.
 *
 */
bool htFindByKeyAndPayloadNoFeatureCheckTohtD(tHtKey key, void * payload,
		unsigned int payloadSize,  ptHtDState* foundState,
		struct hashtable* _abstractTable){
	if(foundState)
		*foundState = NULL;

	ptHtKey keyP = (ptHtKey) malloc(sizeof(tHtKey));

	if(!keyP)
		failure("Out of memory (creating hashtable key).\n");

	*keyP = key;

	ptValue valP;

	valP = search_some(_abstractTable, keyP);

	free(keyP);

	if(valP) {
		ptList listElement = valP->list;

		ptHtDState candidateState;

		while(listElement) {
			candidateState = (ptHtDState) listElement->value;

			if(CompareIgnoreFeaturesByKeyAndPayload(payload,
					payloadSize,
					candidateState->payload,
					candidateState->payloadSize) == true){

				*foundState = candidateState;

				return true;
			}

			listElement = listElement->next;
		}
	}

	return false;
}

/*
 *
 * Checks in a table holding ptHtDState if a ptHtDState with the given Key and Payload in a state
 *
 *
 * Returns a  ptHtDState pointing to a ptHtDState and true if found.
 *
 * False otherwise.
 *
 */
bool htFindByKeyAndStateNoFeatureCheckTohtD(tHtKey key, ptState state,  ptHtDState* foundState,
		struct hashtable* _abstractTable){

	if(foundState)
		*foundState = NULL;

	ptHtKey keyP = (ptHtKey) malloc(sizeof(tHtKey));

	if(!keyP)
		failure("Out of memory (creating hashtable key).\n");

	*keyP = key;

	ptValue valP;

	valP = search_some(_abstractTable, keyP);

	free(keyP);

	if(valP) {
		ptList listElement = valP->list;

		ptHtDState candidateState;

		while(listElement) {
			candidateState = (ptHtDState) listElement->value;

			if(stateCompareIgnoreFeatures(state, candidateState->payload) == true){

				*foundState = candidateState;

				return true;
			}

			listElement = listElement->next;
		}
	}

	return false;
}


/*
 *
 * Modifies the value for a given state/payload and set its unique state to point to zero.
 *
 * Precondition
 * 	State already exists in relevant hashtable.
 *
 */
void setElementDSingleFExpToZero(ptDSymbolicVals D, int k, unsigned int Key, ptState initialState){

	struct hashtable *relevantTable = D->KHashTables[k];

	ptHtDState foundState = NULL;

	if(htFindByKeyAndStateNoFeatureCheckTohtD(Key, initialState,  &foundState,
			relevantTable)){

		ptPairFeatureWeight ptFandW = 	(ptPairFeatureWeight) foundState->pairsOfWandF->value;

		ptFandW->W->number = 0;
		ptFandW->W->isMinusInfinity = false;
		ptFandW->W->isPlusInfinity = true;

	}else{
		failure("Tried to set a value for a non-existing element in D Structure \n");
	}
}



/*
 * Creates a new structure holding -Inf, FeatureExpression (copy) and return a pointer to it.
 *
 */
ptPairFeatureWeight createNegativeInfinityAndFexpPair(ptBoolFct featureExpression){

	ptNumberAndInfinity ptNumAndInfinity = (ptNumberAndInfinity) malloc(sizeof(tNumberAndInfinity));

	if(!ptNumAndInfinity)
		failure("Out of memory (creating number/infinity).\n");

	ptPairFeatureWeight ptFAndWPair = (ptPairFeatureWeight) malloc(sizeof(tPairFeatureWeight));

	if(!ptFAndWPair)
		failure("Out of memory (creating Feature Expression/number/inf).\n");

	ptNumAndInfinity->number = 0;
	ptNumAndInfinity->isMinusInfinity = true;
	ptNumAndInfinity->isPlusInfinity = false;

	ptFAndWPair->W = ptNumAndInfinity;
	ptFAndWPair->FExpression = copyBool(featureExpression);

	return ptFAndWPair;
}


/*
 * Creates a new structure holding +Inf, FeatureExpression (copy) and return a pointer to it.
 *
 */
ptPairFeatureWeight createPositiveInfinityAndFexpPair(ptBoolFct featureExpression){

	ptNumberAndInfinity ptNumAndInfinity = (ptNumberAndInfinity) malloc(sizeof(tNumberAndInfinity));

	if(!ptNumAndInfinity)
		failure("Out of memory (creating number/infinity).\n");

	ptPairFeatureWeight ptFAndWPair = (ptPairFeatureWeight) malloc(sizeof(tPairFeatureWeight));

	if(!ptFAndWPair)
		failure("Out of memory (creating Feature Expression/number/inf).\n");

	ptNumAndInfinity->number = 0;
	ptNumAndInfinity->isMinusInfinity = false;
	ptNumAndInfinity->isPlusInfinity = true;

	ptFAndWPair->W = ptNumAndInfinity;
	ptFAndWPair->FExpression = copyBool(featureExpression);

	return ptFAndWPair;
}


/*
 *
 * Constructor to build ptFTAndWPair
 *
 *
 */
ptPairFeatureWeight constructPairFeatureWeight(ptBoolFct featureExpression, bool  isMinusInfinity, bool isPlusInfinity, int numberW ){
	ptNumberAndInfinity ptNumAndInfinity = (ptNumberAndInfinity) malloc(sizeof(tNumberAndInfinity));

	if(!ptNumAndInfinity)
		failure("Out of memory (creating number/infinity).\n");

	ptPairFeatureWeight ptFAndWPair = (ptPairFeatureWeight) malloc(sizeof(tPairFeatureWeight));

	if(!ptFAndWPair)
		failure("Out of memory (creating Feature Expression/number/inf).\n");

	ptNumAndInfinity->number = numberW;
	ptNumAndInfinity->isMinusInfinity = isMinusInfinity;
	ptNumAndInfinity->isPlusInfinity = isPlusInfinity;

	ptFAndWPair->W = ptNumAndInfinity;
	ptFAndWPair->FExpression = featureExpression;

	return ptFAndWPair;
}

/*
 *
 * Constructor to build ptFTAndWPair for Collector M
 *
 *
 */
ptPairFeatureWeight constructPairFeatureWeightM(ptBoolFct mvANDNOTPdnvANDdkvP,
											bool previousIsMinusInfinity, bool previousIsPlusInfinity,
											int previousW,  double previousWF){
	ptNumberAndInfinity ptNumAndInfinity = (ptNumberAndInfinity) malloc(sizeof(tNumberAndInfinity));

	if(!ptNumAndInfinity)
		failure("Out of memory (creating number/infinity).\n");

	ptPairFeatureWeight ptFAndWPair = (ptPairFeatureWeight) malloc(sizeof(tPairFeatureWeight));

	if(!ptFAndWPair)
		failure("Out of memory (creating Feature Expression/number/inf).\n");

	ptNumAndInfinity->number = previousW;
	ptNumAndInfinity->isMinusInfinity = previousIsMinusInfinity;
	ptNumAndInfinity->isPlusInfinity = previousIsPlusInfinity;
	ptNumAndInfinity->numberF = previousWF;


	ptFAndWPair->W = ptNumAndInfinity;
	ptFAndWPair->FExpression = mvANDNOTPdnvANDdkvP;

	return ptFAndWPair;


}



/*
 *
 * Returns F, W in D(k, v) where v is the state with Key(v)= Key and Payload(v) = ...
 *
 * Precondition
 *
 * 		D(k, v) is defined/initialized.
 *
 */

ptList getFeatureExpressionDomainAndImage(ptDSymbolicVals D,
			int k,
			unsigned int Key,
			void  *payload,
			unsigned int paylodSize){

	struct hashtable *relevantTable = D->KHashTables[k];

	ptHtDState foundState = NULL;

	if(htFindByKeyAndPayloadNoFeatureCheckTohtD(Key, payload,  paylodSize,
			&foundState, relevantTable)){
		return foundState->pairsOfWandF ;
	} else {
		failure("Searching for D, k, Key and Payload that does not exist.");
	}
}

/*
 *
 * Updates F, W in D(k, v) where v is the state with Key(v)= Key and Payload(v) = ...
 *
 * Precondition
 *
 * 		D(k, v) is defined/initialized.
 *
 */
void setFeatureExpressionDomainAndImage(ptDSymbolicVals D,
			int k,
			unsigned int Key,
			ptList ptDomainOfDKVToUpdateTo,
			void  *payload,
			unsigned int paylodSize){

	struct hashtable *relevantTable = D->KHashTables[k];

	ptHtDState foundState = NULL;

	if(htFindByKeyAndPayloadNoFeatureCheckTohtD(Key, payload,  paylodSize,
			&foundState, relevantTable)){
		foundState->pairsOfWandF  = ptDomainOfDKVToUpdateTo;
	} else {
		failure("Searching for D, k, Key and Payload that does not exist.");
	}
}





/*
 *
 * Returns F, W in M(v) where v is the state with Key(v)= Key and Payload(v) = ...
 *
 * Precondition
 *
 * 		M(v) is defined/initialized.
 *
 */
ptList getFeatureExpressionDomainAndImageFromM(ptMSymbolicValsCollector M,
		unsigned int Key,
		void  *payload,
		unsigned int paylodSize){

	struct hashtable *relevantTable = M->aHashTable;

	ptHtDState foundState = NULL;

	if(htFindByKeyAndPayloadNoFeatureCheckTohtD(Key, payload,  paylodSize,
			&foundState, relevantTable)){
		return foundState->pairsOfWandF ;
	} else {
		failure("Searching for M, Key and Payload that does not exist.");
	}
}

/*
 *
 * Sets F, W in M(v) where v is the state with Key(v)= Key and Payload(v) = ...
 *
 * Precondition
 *
 * 		M(v) is defined/initialized.
 *
 */
void 	setFeatureExpressionDomainAndImageFromM(ptMSymbolicValsCollector M,
		unsigned int Key,
		ptList updateFAndW,
		void  *payload,
		unsigned int paylodSize){
	struct hashtable *relevantTable = M->aHashTable;

		ptHtDState foundState = NULL;

		if(htFindByKeyAndPayloadNoFeatureCheckTohtD(Key, payload,  paylodSize,
				&foundState, relevantTable)){
			foundState->pairsOfWandF = updateFAndW ;
		} else {
			failure("Searching for Updating M, Key and Payload that does not exist.");
		}
}


/*
 *
 *
 * Checks  uFAndW + W > vFandW and returns true/false
 *
 */
bool isGreaterLeftDAndWThanRightD(ptPairFeatureWeight uFandW, int W, ptPairFeatureWeight vFandW){
	if(uFandW->W->isMinusInfinity){
		return false;
	}
	else if(uFandW->W->isPlusInfinity){
		if(vFandW->W->isPlusInfinity){
			return false;
		} else {
			return true;
		}
	} else {

		int combinedLeft = uFandW->W->number + W;

		if(vFandW->W->isMinusInfinity){
			return true;
		} else if (vFandW->W->isPlusInfinity) {
			return false;
		} else {
			return combinedLeft > vFandW->W->number ;
		}
	}
}



/*
 *
 *
 * Checks  C(d1) < M(v, d2)
 * and returns true/false
 *
 */
bool isLessCThanM(ptPairFeatureWeight CFandW, ptPairFeatureWeight mvFandW){

	if(mvFandW->W->isMinusInfinity){
		return false;
	} else {
		if(mvFandW->W->isPlusInfinity){
			if(CFandW->W->isPlusInfinity){
				return false;
			}else{
				return true;
			}
		}else {
			// mvFandW->W == #
			// CFandW->W == {-Inf, #, +Inf}
			if(CFandW->W->isMinusInfinity){
				return true;
			} else if(CFandW->W->isPlusInfinity){
				return false;
			} else {
				// CFandW->W ==  W
				return CFandW->W->numberF < mvFandW->W->numberF;
			}
		}
	}
}


/*
 *
 *
 * Checks  mvFandW > (dnvFandW - dkvFandW)/(n-k)
 * and returns true/false
 *
 */
bool isGreaterLeftMThanDMinusDByNMinusK(ptPairFeatureWeight mvFandW, ptPairFeatureWeight dnvFandW,
		ptPairFeatureWeight dkvFandW, int numberOfItems, int  k){

	int divisorI = numberOfItems - k;

	if (mvFandW->W->isMinusInfinity){
		return false;
	} else {

		if(mvFandW->W->isPlusInfinity){
			if(dkvFandW->W->isMinusInfinity && !dnvFandW->W->isMinusInfinity){
				// dnvFandW - dkvFandW/ d === Number - -Inf / d = +Inf/d = +Inf
				// +Inf  > + Inf  = false
				return false;
			} else {
				// either A(dkvFandW->W->isMinusInfinity)  must be F
				//			dnvFandW - dkvFandW/ d === Number|-Inf - Number =  Number|-Inf/d =  Number|-Inf
				//			+Inf > Number|-Inf
				/// OR B(!dnvFandW->W->isMinusInfinity) must be F -> dnvFandW->W->isMinusInfinity == True
				//        dnvFandW - dkvFandW/ d === -Inf - -Number|Inf =  0|-Inf/d =  0|-Inf
				//			+Inf > 0|-Inf
				return true;
			}
		} else {
			// mvFandW->W->numberF
			double leftWf = mvFandW->W->numberF;

			if(!dkvFandW->W->isMinusInfinity && !dnvFandW->W->isMinusInfinity){
				// # - # / d = #

				double rightW =	(((double) dnvFandW->W->number) - ((double) dkvFandW->W->number))/((double) divisorI);
				return	leftWf > rightW;
			}else if  (dkvFandW->W->isMinusInfinity && dnvFandW->W->isMinusInfinity){

				//	-Inf - -Inf /d = 0/d = 0
				double rightW = 0.0 ;

				return	leftWf > rightW;

			} else if(dkvFandW->W->isMinusInfinity && !dnvFandW->W->isMinusInfinity){
				// # - -Inf / d = +Inf
				return false;

			} else if (!dkvFandW->W->isMinusInfinity && dnvFandW->W->isMinusInfinity){

				// -Inf - # / d = -Inf
				return true;
			}
		}
	}
}


/*
 *
 *
 * Computes  (dnvFandW - dkvFandW)/(n-k),
 * assuming mvFandW > (dnvFandW - dkvFandW)/(n-k) ==> (dnvFandW - dkvFandW)/(n-k) != +Inf
 *
 *
 */
tNumberAndInfinity calculateDMinusDByNMinusK(ptPairFeatureWeight dnvFandW,  ptPairFeatureWeight dkvFandW,
		int numberOfItems, int k){

	int divisorI = numberOfItems - k;

	tNumberAndInfinity toRet;

	if(!dkvFandW->W->isMinusInfinity && !dnvFandW->W->isMinusInfinity){
		// # - # / d = #

		double rightW =	(((double) dnvFandW->W->number) - ((double) dkvFandW->W->number))/((double) divisorI);
		toRet.isMinusInfinity = false;
		toRet.isPlusInfinity = false;
		toRet.numberF = rightW;


	}else if  (dkvFandW->W->isMinusInfinity && dnvFandW->W->isMinusInfinity){

		//	-Inf - -Inf /d = 0/d = 0
		double rightW = 0.0 ;
		toRet.isMinusInfinity = false;
		toRet.isPlusInfinity = false;
		toRet.numberF = rightW;

	} else if(dkvFandW->W->isMinusInfinity && !dnvFandW->W->isMinusInfinity){
		// # - -Inf / d = +Inf
		toRet.isMinusInfinity = false;
		toRet.isPlusInfinity = true;


	} else if (!dkvFandW->W->isMinusInfinity && dnvFandW->W->isMinusInfinity){

		// -Inf - # / d = -Inf

		toRet.isMinusInfinity = true;
		toRet.isPlusInfinity = false;

	}

	return toRet;
}

void printMStructureForStateByKeyAndPayload(ptMSymbolicValsCollector M, 	unsigned int tmpKey,
		void *payload,
		int payloadSize ){

		ptList ptDomainOfMV = getFeatureExpressionDomainAndImageFromM(M,
				tmpKey,
				payload,
				payloadSize );

		ptList tmpIterator = ptDomainOfMV;

		while(tmpIterator != NULL){
			ptPairFeatureWeight dkvFandW = (ptPairFeatureWeight) tmpIterator->value;

			printf("%u, ", tmpKey);
			printBool(dkvFandW->FExpression);
			printf(", ");
			printWFloat(dkvFandW->W);
			printf("\n");
			tmpIterator = tmpIterator->next;
		}
}

void printDStructure(struct hashtable *  sccComponent, ptDSymbolicVals D, int k){
	ptHtIteratorState elementPointerInTable = getEmptyHashtableIterator();
	unsigned int tmpKey;

	ptHtState s = NULL;
	tmpKey  = 0;

	while(tableHasMoreElements(sccComponent, elementPointerInTable, &s, &tmpKey)){
		ptList ptDomainOfDVKList= getFeatureExpressionDomainAndImage(D,
					k,
					tmpKey,
					s->payload,
					s->payloadSize );

		ptList iterateDVK = ptDomainOfDVKList;

		while(iterateDVK != NULL){
			ptPairFeatureWeight dkvFandW = (ptPairFeatureWeight) ptDomainOfDVKList->value;

			printf("%u, ", tmpKey);
			printBool(dkvFandW->FExpression);
			printf(", ");
			printWInteger(dkvFandW->W);
			printf("\n");
			iterateDVK = iterateDVK->next;
		}
	}
}

bool areAugmentedWEquivalent(ptNumberAndInfinity W1, ptNumberAndInfinity W2){
	if((W1->isMinusInfinity == W2->isMinusInfinity) &&
		(W1->isPlusInfinity == W2->isPlusInfinity)
		){

		if(!W1->isMinusInfinity && !W1->isPlusInfinity ){
			return W1->number == W2->number;
		}else {
			return true;
		}
	} else {
		return false;
	}
}

bool areAugmentedWEquivalentByFloat(ptNumberAndInfinity W1, ptNumberAndInfinity W2){
	if((W1->isMinusInfinity == W2->isMinusInfinity) &&
		(W1->isPlusInfinity == W2->isPlusInfinity)
		){

		if(!W1->isMinusInfinity && !W1->isPlusInfinity ){
			return W1->numberF == W2->numberF;
		}else {
			return true;
		}
	} else {
		return false;
	}
}
void printWFloat(ptNumberAndInfinity W){
	if(W->isMinusInfinity){
		printf(" -Inf");
	} else if(W->isPlusInfinity){
		printf(" +Inf");
	}else {
		printf(" %f", W->numberF);
	}

}
void printWInteger(ptNumberAndInfinity W){
	if(W->isMinusInfinity){
		printf(" -Inf");
	} else if(W->isPlusInfinity){
		printf(" +Inf");
	}else {
		printf(" %d", W->number);
	}
}
