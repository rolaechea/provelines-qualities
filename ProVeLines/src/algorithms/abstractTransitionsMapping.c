/*
 * abstractTransitionsMapping.c
 *
 *  Created on: Jan 14, 2016
 *      Author: rafaelolaechea
 */
#include "../lib/clark.hashtable/hashtable.h"
#include "../lib/clark.hashtable/hashtable_private.h"
#include "../wrapper/hash/hashtableState.h"


#include "mean-cycle-structure.h"
#include "symbolic-scc.h"
#include "abstractTransitionsMapping.h"
#include "scc-analysis.h"
#include "transpose-executables.h"

struct hashtable* htRelevantStates_ = NULL;

void addAbstractLinks(ptHtState fromState, ptHtDState toState, unsigned int toStateKey);

struct hashtable* initializeEmptySetRelevantStates(){
	htRelevantStates_ = create_hashtableWrapper();
	return htRelevantStates_;
}

struct hashtable* getRelevantStates(){
	return htRelevantStates_;
}

/*
 *
 * Sets the abstract transition mapping for the hash-state s.
 *
 *
 *
 */
void setAbstractTransitionsFromHashtable(ptHtState s, struct hashtable* DAbstractTransitions){
	ptHtIteratorState elementPointerInTable = getEmptyHashtableIterator();

	ptHtDState tmpS = NULL;
	unsigned int tmpKey ;

	while(tableHasMoreElementsHTDVersion(DAbstractTransitions, elementPointerInTable, &tmpS, &tmpKey)){
		addAbstractLinks(s, tmpS, tmpKey);
	}
}

/*
 *
 * Its called from a hasthable representation if state links, so no need to check for duplicates anymore.
 *
 */
void addAbstractLinks(ptHtState fromState, ptHtDState toState, unsigned int toStateKey
		){


	/* Look up toStateKey in Relevant States */
	ptHtState tmpFoundToStateRelevant = NULL;
	htFindByKeyAndPayloadNoFeatureCheck(toStateKey, toState->payload, toState->payloadSize, &tmpFoundToStateRelevant, getRelevantStates());

	ptList LinksToAdd =  toState->pairsOfWandF;

	while(LinksToAdd != NULL){
		ptPairFeatureWeight currentLink = (ptPairFeatureWeight) LinksToAdd->value;


		ptTransAndFExp tmpTrans = (ptTransAndFExp) malloc(sizeof(tTransAndFexp));

		if(!tmpTrans)
			failure("Out of memory (creating transition/fexp for transpose).\n");

		tmpTrans->State = tmpFoundToStateRelevant->originalState ;
		tmpTrans->features = copyBool(currentLink->FExpression);
		tmpTrans->Weight = currentLink->W->number;

		fromState->abstractTransposeLinks =  listAdd(fromState->abstractTransposeLinks, tmpTrans);

		LinksToAdd = LinksToAdd->next;
	}
}
/*
 *
 * Creates a stack element from a state and a feature expression.
 * It also looks up outgoing edges for the state in the transpose hashtable.
 *
 */
ptStackEltTranspose createSccStackElementAbstract(ptState state, ptBoolFct features){
	ptStackEltTranspose elt = (ptStackEltTranspose) malloc(sizeof(tStackEltTranspose));
	if(!elt)
		failure("Out of memory (creating SCC stack element).\n");

	elt->state = state;
	elt->features =  features;

	ptHtState transposeState = NULL;

	if( htFindNoFeatureCheck(state->payloadHash, state, &transposeState, getRelevantStates())){
		if(transposeState != NULL){
			elt->OutgoingLinksTranspose = transposeState->abstractTransposeLinks;
			elt->OutgoingLinksTransposeCurrent = elt->OutgoingLinksTranspose;

		}else {
			elt->OutgoingLinksTranspose = NULL;
			elt->OutgoingLinksTransposeCurrent = NULL;

		}

	} else {
		elt->OutgoingLinksTranspose = NULL;
		elt->OutgoingLinksTransposeCurrent = NULL;
	}

	return elt;
}




/*
 *
 * Given a Key, Payload, and Payload Size it returns set of Abstract transpose edges.
 *
 * This is equivalent to the set of incoming edges in the original graph.
 *
 * Precondition:
 *
 * 	Transpose have already been calculated/updated.
 *
 *	Hashtables hold payload sizes too.
 *
 * 	Returns:
 *
 * 	A list (possibly empty) of transpose links.
 *
 *
 */
ptList getAbstractTransposeLinksFromKeyAndPayload(tHtKey key, void * payload, unsigned int payloadSize){

	ptHtState transposeState = NULL;

	if( htFindByKeyAndPayloadNoFeatureCheck(key, payload, payloadSize,
			&transposeState, getRelevantStates())){
		return transposeState->abstractTransposeLinks;
	} else {
		return NULL;
	}
}


