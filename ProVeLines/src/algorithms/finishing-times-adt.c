/*
 * finishing-times-adt.c
 *
 *  Created on: Sep 3, 2015
 *      Author: rafaelolaechea
 */

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <stdbool.h>

#include "error.h"
#include "main.h"
#include "list.h"
#include "stack.h"
#include "boolFct.h"
#include "symbols.h"
#include "automata.h"
#include "SAT.h"
#ifdef CLOCK
	#include "clockZone.h"
	#include "federation.h"
#endif
#include "state.h"
#include "execution.h"




#include "finishing-times-adt.h"

static void _addChildSymbolicNode(ptSymbolicNode fatherNode, ptSymbolicNode childNoe, ptBoolFct edgeLabel);
/* Queue Node */

ptQueue createEmptyQueue(){
	return NULL;
}

bool isEmptyQueue(ptQueue Q){
	return NULL == Q;
}

ptQueue pushQueue(ptQueue Q, void * Node){
	if (Q == NULL){
		/* will have to create a new Queue */
		ptQueue newQ = (ptQueue) malloc(sizeof(tQueue));
		if(!newQ)
			failure("Out of memory (creating Queue).\n");

		newQ->current = Node;
		newQ->next = NULL;
		newQ->last = newQ;

		return newQ;

	} else {
		/* Add to the end of the Queue */
		if((Q->last)->next != NULL){
			failure("Last Memory of Queue doesn't have next set to null .\n");
		}
		(Q->last)->next =  (ptQueue) malloc(sizeof(tQueue));
		((Q->last)->next)->current =  Node;
		((Q->last)->next)->next = NULL;
		((Q->last)->next)->last = NULL;
		Q->last =  (Q->last)->next ;

		return Q;
	}
}

/*
 *
 * Pops an element from Q, and updates Q.
 * 	Update done by deleting an element from the front, and advancing Q and updating last.
 *
 * 		(For Q, only the front element is guaranteed to have a correct last pointer).
 *
 */
void * popQueue(ptQueue *Q){
	if((*Q)==NULL){
		failure("Trying to pop an element from an empty Queue .\n");
	}

	void * itemToReturn = (*Q)->current;

	// Advance Queue
	if ((*Q)->next == NULL) {
		// Popping from a Queue of size = 1.
		free(*Q);
		(*Q) = NULL;
	} else {
		// Update last ptr for next element in front of Queue.
		ptQueue previousQ = (*Q);

		((*Q)->next)->last = (*Q)->last;

		(*Q) =  (*Q)->next;

		free(previousQ);
	}

	return itemToReturn;
}






/* Boilerplate Functions */

bool isRootNode(ptSymbolicNode Node) {
	return Node->isRoot;
}

int getMaxMappingNode(ptSymbolicNode Node){
	return Node->maximiumNode;
}

void setMaxMappingNode(ptSymbolicNode Node, int max){
	Node->maximiumNode =  max;
}


/*
 * Creates a  Root Node, father ptr and features are set to null.
 *
 */
ptSymbolicNode createRootSymbolicFTree(){
	ptSymbolicNode Node = (ptSymbolicNode) malloc(sizeof(tSymbolicNode));
	if(!Node)
		failure("Out of memory (creating Symbolic FTree Root).\n");

	Node->ToFatherfeatures = NULL;
	Node->isRoot = true;
	Node->ListOfOutgoingEdges = NULL;
	Node->state = NULL;
	Node->ToFatherfeatures = NULL;

	Node->isVisited = false;
	Node->ptSccComponent = NULL;
	Node->hasSymbolicSccComponent = false;
	Node->ptSccUnion = NULL;

	return Node;
}


/* Boiler Plate FUnctions */

ptBoolFct FeatureExpressionFromRoot(ptSymbolicNode Node){
	if (isRootNode(Node)){
		return getTrue();
	} else {
		if(Node->father == NULL){
			failure("Incorrect Symbolic Finishing Time Tree -- Node has isRoot = false and null father.");
		}
		return addConjunction(Node->ToFatherfeatures,
				FeatureExpressionFromRoot(Node->father), 1,1);
	}
}


ptSymbolicNode createFTreeNode(ptSymbolicNode fatherNode, ptState stateLabel, ptBoolFct edgeLabel ){
	ptSymbolicNode Node = (ptSymbolicNode) malloc(sizeof(tSymbolicNode));
	if(!Node)
		failure("Out of memory (creating Symbolic FTree Node).\n");

	Node->father = fatherNode;
	Node->ToFatherfeatures = edgeLabel;
	Node->isRoot = false;
	Node->state =  stateLabel;
	Node->ListOfOutgoingEdges = NULL;

	Node->isVisited = false;
	Node->ptSccComponent = NULL;
	Node->hasSymbolicSccComponent = false;
	Node->ptSccUnion = NULL;

	_addChildSymbolicNode(fatherNode, Node, edgeLabel);

	return Node;
}

void _addChildSymbolicNode(ptSymbolicNode fatherNode, ptSymbolicNode childNode, ptBoolFct edgeLabel){

	if(fatherNode->ListOfOutgoingEdges == NULL){
		/* Create list of outgoing edges and add as first edge */
		fatherNode->ListOfOutgoingEdges = (ptEdgeListSymbolicNode) malloc(sizeof(tEdgeListSymbolicNode));
		if (! fatherNode->ListOfOutgoingEdges){
			failure("Out of memory creating a List of Edges");
		}

		// Addd Edge
		fatherNode->ListOfOutgoingEdges->next = NULL;
		fatherNode->ListOfOutgoingEdges->featureEdgeLabel = edgeLabel;
		fatherNode->ListOfOutgoingEdges->destination =childNode;

	} else {
		/* Add at the front of the list of outgoing edges. Order is inmaterial. */
		ptEdgeListSymbolicNode tmpListOfOutgoingEdges = (ptEdgeListSymbolicNode) malloc(sizeof(tEdgeListSymbolicNode));
		if (! tmpListOfOutgoingEdges){
			failure("Out of memory creating a List of Edges");
		}

		// Add Edge at front
		tmpListOfOutgoingEdges->next = fatherNode->ListOfOutgoingEdges;
		tmpListOfOutgoingEdges->featureEdgeLabel = edgeLabel;
		tmpListOfOutgoingEdges->destination =childNode;
		fatherNode->ListOfOutgoingEdges = tmpListOfOutgoingEdges;

	}

}

bool getNodeVisited(ptSymbolicNode TreeNode){
	return TreeNode->isVisited ;
}

void setNodeVisited(ptSymbolicNode TreeNode) {
	TreeNode->isVisited = true;
}
