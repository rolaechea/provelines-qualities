/*
 * limitAverage.c
 *
 *  Created on: Aug 25, 2015
 *      Author: rafaelolaechea
 */



#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <stdbool.h>

#include "error.h"
#include "main.h"
#include "list.h"
#include "stack.h"
#include "boolFct.h"
#include "symbols.h"
#include "automata.h"
#include "SAT.h"
#ifdef CLOCK
	#include "clockZone.h"
	#include "federation.h"
#endif
#include "state.h"
#include "execution.h"
#include "hashtableState.h"
#include "hashtable_private.h"
#include "tvl.h"


#include "limitAverage.h"
#include "symbolic-f-times.h"
#include "transpose-executables.h"
#include "abstractTransitionsMapping.h"
#include "scc-analysis.h"
#include "scc-debugging.h"
#include "abstractFTS.h"

byte _nbErrorsLocalLimAvg = 0;



bool isRelevantState(ptState stateToCheck){
	// Will have to check state for each user proctype values.

					//	printf("Start Checking if relevant state or not \n");

						bool atLine48 = false;
						bool userAtLine129 = false;
						bool waterSensorAtCritical = false;
						bool methaneSensorAtCritical = false;
						ptStateMask currentSM = stateToCheck->mask ;
						while(currentSM){
							ptFsmNode node = NULL;
							if(currentSM->pid != -1 ) {
								node = getNodePointer(stateToCheck, currentSM);

								if(node){
								//	printf("   pid %02d, %-27s @ NL%02d\n", currentSM->pid, currentSM->process->name, node->lineNb);

									if(strcmp(currentSM->process->name, "controller")==0){
										if(node->lineNb == 48){
											atLine48 = true;
										}
									}

									if(strcmp(currentSM->process->name, "user")==0 ){
										if( (node->lineNb == 129) && minepumpType==-1){
											userAtLine129 = true;
										}else if (( (node->lineNb == 90) && minepumpType==0)){
											userAtLine129 = true;
										}else if (( (node->lineNb == 101) && minepumpType==1)){
											userAtLine129 = true;
										}else if (( (node->lineNb == 101) && minepumpType==2)){
											userAtLine129 = true;
										}else if (( (node->lineNb == 112) && minepumpType==3)){
											userAtLine129 = true;
										}


									}

									if(strcmp(currentSM->process->name, "watersensor")==0){
										if((node->lineNb == 169 || node->lineNb == 155) && minepumpType==-1){
											waterSensorAtCritical = true;
										}else if ((node->lineNb == 130 || node->lineNb == 116) && minepumpType==0){
											waterSensorAtCritical = true;
										}else if ((node->lineNb == 141 || node->lineNb == 127) && minepumpType==1){
											waterSensorAtCritical = true;
										}else if ((node->lineNb == 141 || node->lineNb == 127) && minepumpType==2){
											waterSensorAtCritical = true;
										}else if ((node->lineNb == 152 || node->lineNb == 138) && minepumpType==3){
											waterSensorAtCritical = true;
										}
									}

									if(strcmp(currentSM->process->name, "methanesensor")==0){

										/* Structure of problem already asserts that controller = Line 48 ->
										 *  methanesensor = Line 142
										 */
										if((node->lineNb == 142 )&& minepumpType==-1){
											methaneSensorAtCritical = true;
										} else if((node->lineNb == 103 )&& minepumpType==0){
											methaneSensorAtCritical = true;
										} else if((node->lineNb == 114 )&& minepumpType==1){
											methaneSensorAtCritical = true;
										} else if((node->lineNb == 114 )&& minepumpType==2){
											methaneSensorAtCritical = true;
										} else if((node->lineNb == 125 )&& minepumpType==3){
											methaneSensorAtCritical = true;
										}

									}
								}
							}
							currentSM = currentSM->next;
						} // End-While
					//	printf("End Checking if significant state or not \n");
					//	printf("\n");




	bool nonAtomic =   stateGetValue(stateToCheck->payload, OFFSET_EXCLUSIVE, T_BYTE) == 255 ? true : false;

	return atLine48 && userAtLine129 && waterSensorAtCritical && methaneSensorAtCritical && nonAtomic;
}

void startSymbolicDepthFirstSearch(ptList props, ptSymTabNode globalSymTab, ptMTypeNode mtypes){
	/*
	 * 1. Initialize starting state.
	 *
	 *
	 */
	const int printLimit = 125000;

	int countNumberItems = 0;
	int _nbErrorsLocalLimAvgTmp = 0;


	initSolverWithFD(getFeatureModelClauses());
	htVisitedStatesInit();
	htBlackOrGreyInit();
	initHTTranpose();

	// Create initial state
	ptState init = stateCreateInitial(globalSymTab, mtypes);
	init->payloadHash = hashState(init);


	htVisitedStatesInsert(init->payloadHash, init, DFS_OUTER);
	htBlackOrGreyInsert(init->payloadHash, init);


	ptBoolFct noOutgoing = NULL;
	byte resetExclusivity = false;
	ptStackElt elt = createStackElement(init, _nbErrorsLocalLimAvgTmp);
	ptStack stack = push(NULL, elt);

	if(!silent)
		printf("Initial State is %u \n ", init->payloadHash);

	setInitialState(init->payloadHash);

//	printState(mtypes, init, NULL);
//	printf("Adding to Stack init  with payloadHash  %u \n", init->payloadHash);
//	printState(mtypes, init, NULL);
//	printf("\nEnd Printing Init \n");

	elt->E = executables(globalSymTab, mtypes, init, 1, _nbErrorsLocalLimAvgTmp, NULL, NULL, NULL, &noOutgoing, &resetExclusivity);
	elt->E_save = elt->E;


	/* Compute Transitions for transpose (init) */
	//initHTTranpose();
	computeTranposeGraph(globalSymTab, mtypes, init);

//	ptList tranposeTransitions =	executablesNoConjunctsFeatures(globalSymTab, mtypes, init, 1 );

	bool startingFromInit = true;
	bool searchForAnotherWhite = true;


	byte assertViolation = 0;
	byte hasDeadlock = 0;


	byte error = 0;

	ptStackElt current;
	ptState s_;
	ptHtState prevS_;

	ptList E = NULL;


	/*  use to iterate for unexplored states and launch a new DFS */
	ptHtState s = NULL;
	unsigned int Key ;
	ptHtIteratorState elementPointerInTable = getEmptyHashtableIterator();


	while (startingFromInit ||
			(restartWhite && searchForAnotherWhite)){
//		printf("At Start of Loop startingFromInit \n");
		bool foundAnotherWhite = false;

		if((restartWhite && startingFromInit) == false){

			E = NULL;
			s_ = NULL;
			prevS_ = NULL;
			assertViolation = 0;
			hasDeadlock = 0;
			error = 0;
			noOutgoing = NULL;
			resetExclusivity = false;

			foundAnotherWhite = false;
			searchForAnotherWhite = false;

			bool foundNextItemToSearch = false;
			while(!foundNextItemToSearch &&
					tableHasMoreElements(getBlackOrGreyTable(), elementPointerInTable, &s, &Key)
					 ){
				if (s->outerFeatures == NULL ||
						isTautology(s->outerFeatures)){
//					printf("Element Already Explored with all the products: %u, ", Key);
//					printBool(s->outerFeatures);
//					printf("\n");
				} else {
//					printf("Element not already Explored with all the products: %u, ", Key);
//					printBool(s->outerFeatures);
//					printf(" Must explore it again with ");
//					printBool(negateBool(s->outerFeatures));
//					printf("\n");

					foundNextItemToSearch = true;
					searchForAnotherWhite = true;
					foundAnotherWhite = true;

					elementPointerInTable = getEmptyHashtableIterator();  // Restart search

					s->originalState->features = copyBool(negateBool(s->outerFeatures));
					elt = createStackElement(s->originalState, _nbErrorsLocalLimAvgTmp);
					stack = push(NULL, elt);


					s->outerFeatures = getTrue();
					//copyBool(addDisjunction(s->outerFeatures, negateBool(s->outerFeatures), 1,1));

//					printf("\n s->outerFeatures should be set to true =  ");
//					printBool(copyBool(addDisjunction(s->outerFeatures, negateBool(s->outerFeatures), 1,1)));
//					printf("\n");
					//s->outerFeatures = copyBool(addDisjunction(s->outerFeatures, negateBool(s->outerFeatures), 1,1);


					elt->E = executables(globalSymTab, mtypes, s->originalState, 1, _nbErrorsLocalLimAvgTmp, NULL, NULL, NULL, &noOutgoing, &resetExclusivity);
					elt->E_save = elt->E;

				}
			} // End-While
		} // End-If  (restartWhite && startingFromInit) == false

		if(startingFromInit ||  ( restartWhite && foundAnotherWhite )){
			while(!empty(stack) ) {
				current = top(stack);

				countNumberItems++;


				if( printDFSTrace && countNumberItems % 10000000 == 0){

					struct hashtable* BgHashtable  = getBlackOrGreyTable();
					int stackSize = countStack(stack);
					printf("DFS Algorithm 1 - %d states processed, Black Grey Table is %u, Stack Size %d \n ", countNumberItems,
							BgHashtable->entrycount, stackSize );
				}

				if(printDFSTrace && countNumberItems < printLimit){
					printf("Exploring, BGTable Size = %u, state payloadHash=%u \n ", getBlackOrGreyTable()->entrycount, current->state->payloadHash);
					printState(mtypes, current->state, NULL);


				}


				if(!moreTransitionsAvailable(current) ) {
					// All Transitions have been taken, then backtrack and assign F(current, currentFeatures) -> i.
					if(printDFSTrace && countNumberItems < printLimit){
						printf("Popping after all transitions have been taken ");
						/*
						if(isRelevantState(current->state)){
							printf("Relevant State - \n");
						} else {
							printf("Irrelevant State - \n");
						}?*/
					}
					if(partialOrderReduction ){
						if(isRelevantState(current->state)){
							htSetFinishingTime(mtypes, current);
//							printf("At finishing time =  %d, state payloadh-hash is  %u, payload-size is %u \n", getSizeFinishingTime(), current->state->payloadHash, current->state->payloadSize);


							//printf("Raw-Payload ");
							//printPayload(current->state);
							//printf("\n");
							//

//							printState(mtypes, current->state, NULL);

						}
					} else {
				//		printf("Raw-Payload ");
				//		printPayload(current->state);
				//		printf("\n");
						htSetFinishingTime(mtypes, current);
					}
					// Pop from stack
					pop(&stack);
					destroyStackElementKeepState(current, processTrans);
				} else {
					// Compute Next State s_
					AppplyCombinedExecutionNoNever(globalSymTab,
							mtypes, &s_,
							 current, &E, &noOutgoing, &resetExclusivity,
							 &error, &assertViolation, &hasDeadlock);

					if(htBlackOrGreyFind(s_->payloadHash, s_, &prevS_)){
						if(printDFSTrace && countNumberItems < printLimit)
							printf("Skipping transition  \n");
//						printf("State already visited %u \n", s_->payloadHash);
						/* State already visited with f'  f' -> current->f -	Aka PX(f) subset of PX(f') - 	skip */
						stateDestroy(s_, false);
						s_ = NULL;
						destroyProcTransList(E, processTrans);
						E = NULL;
					} else {

						/* State will be visited current->f & not prevS_->f,
						 *  or
						 *
						 *  hasn't been visited at all
						 *
						 * */

						if(prevS_ == NULL){
							// State never visited at all.
								computeTranposeGraph(globalSymTab, mtypes, s_);
						}

						updateNextStateFeatureAlgebra(mtypes, s_, prevS_,  false/*countNumberItems >= 1420000 */);

//						printf("Adding to Stack  with payloadHash  %u \n", s_->payloadHash);
						if(printDFSTrace && countNumberItems < printLimit)
							printf("Adding to Stack \n");

						ptStackElt newStackElement = createStackElement(s_, _nbErrorsLocalLimAvg);

						newStackElement->E =  E;
						newStackElement->E_save = newStackElement->E;

//						if(countNumberItems >= 1420000){
//							printf(" Adding new element with hashvalue %u  to stack \n ", s_->payloadHash);
//							printState(mtypes, s_,  NULL);
//						}
						stack = push(stack, newStackElement);
					}


					advanceTransitionPointers(current);
				}
			}
		}
		startingFromInit = false;


//		printf("\n Finished a DFS-Visit - Algorithm 1 \n");
//		printHashtablesAndFeatures(getBlackOrGreyTable());
//		printf(" %s -- %s ", startingFromInit ? " fromInit " : " Not From Init",
//				searchForAnotherWhite ? "searchForAnotherWhite == True" : "searchForAnotherWhite == False");

//		printf(" Finished Printing BlackOrGrey \n");
	}





//	printf("Finished Algorithm 1 \n");
//	printHashtablesAndFeatures(getBlackOrGreyTable());
//	printf(" Finished Printing BlackOrGrey after Algorithm 1 \n");

	int SymbolicFSize = getSizeFinishingTime();

	if(statsLA){
//		printf("# Elements in Domain F = %d \n % ", SymbolicFSize);
	}

	//printf("  Printing BlackOrGrey after Algorithm 1 \n");
	//printHashtablesAndFeatures(getBlackOrGreyTable());
	unsigned int numberOfStates = getBlackOrGreyTable()->entrycount;

	if(statsLA){
		printf(" # Elements in BlackOrGrey  = %u \n", numberOfStates);
	}

	if(statsLA){
		//printf(" # States in BlackOrGreyTable = %u \n", numberOfStates);

		unsigned int numberOfStatesInTransposeTable = getTransposeHasthable()->entrycount;

				printf(" # States in TransposeTable  = %u \n", numberOfStatesInTransposeTable);


	}



	/*
	 * Extract states from finishing times list and insert into a table disregarding features.
	 *  This gives a set of htRelevantStates as only "relevant states" have a finishing time with
	 *  partialOrderReduction.
	 *
	 *  */
	struct hashtable* htRelevantStates = initializeEmptySetRelevantStates();

	int numberRelevantStates = 0;
	if(partialOrderReduction ){
		int k  =1;
		while (k <= SymbolicFSize){

			ptState FEltExampleState = NULL;
			ptBoolFct FEltExampleStateFeatures = NULL;
			ptHtState prevS_ = NULL;

			getStateAndFexpressionFInverse(k, &FEltExampleState, &FEltExampleStateFeatures);

			if(htFindNoFeatureCheck(FEltExampleState->payloadHash, FEltExampleState, &prevS_, htRelevantStates)){
				// Relevant State already inserted
			} else {
				numberRelevantStates++;
				htInsertNoFeatures(FEltExampleState->payloadHash, FEltExampleState, htRelevantStates);
			}
			k++;
		}
	}

	if(statsLA){
		printf(" Number Relevant STates = %d \n", numberRelevantStates);
	}

	if(partialOrderReduction ){
		elementPointerInTable = getEmptyHashtableIterator();
		while(tableHasMoreElements(htRelevantStates, elementPointerInTable, &s, &Key)){
			//printf("Abstract Transition for state %u \n ", Key);

			struct hashtable* DAbstractTransitions  = create_hashtableWrapper();

			computeAbstractTransitionsFromS(s, DAbstractTransitions, htRelevantStates);

			setAbstractTransitionsFromHashtable(s, DAbstractTransitions);
		}
	}



}







