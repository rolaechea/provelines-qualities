/*
 * scc-analysis.c
 *
 *  Created on: Oct 1, 2015
 *      Author: rafaelolaechea
 */
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include "../lib/clark.hashtable/hashtable.h"
#include "../lib/clark.hashtable/hashtable_private.h"
#include "../wrapper/hash/hashtableState.h"

#include "finishing-times-tree.h"
#include "finishing-times-adt.h"

#include "scc-analysis.h"

/*
 *
 * Allocates an empty iterator for a hashtables and returns it.
 */
ptHtIteratorState getEmptyHashtableIterator(){
	ptHtIteratorState iteratorElement = (ptHtIteratorState) malloc(sizeof(tHtIteratorState));

	if(!iteratorElement)
		failure("Out of memory (creating iterator for hash table).\n");

	iteratorElement->tableIndex = 0;
	iteratorElement->noSearchDone = true;

	iteratorElement->noMoreAvailable = false;
	iteratorElement->currentListStates = NULL;
	iteratorElement->currentEntry = NULL;

	return iteratorElement;
}

/*
 *
 * Code to iterate through elements of a hash table.
 *
 * 	Requires individual type of them to be a ptHtState (otherwise ... )
 *
 */
bool tableHasMoreElements(struct hashtable *sccComponent,
		ptHtIteratorState elementPointerInTable,
		ptHtState *nextState,
		unsigned int *Key){

//	printf("Executing tableHasMoreElements \n");
//	printf("elementPointerInTable=%s\n", elementPointerInTable == NULL ? "Null" : "Not NUll");
//	printElementPointerInTable(elementPointerInTable);
//	printf("\n");


	if(elementPointerInTable->noMoreAvailable == true){
		return false;
	}

	/* Iterating based on the value of elementPointerInTable */
	if(elementPointerInTable->noSearchDone == true ||
			( (elementPointerInTable->noSearchDone == false) &&
					elementPointerInTable->currentEntry == NULL &&
					elementPointerInTable->currentListStates == NULL)
			){

//		printf("Inside first if \n");
		int i = 0;

		if (elementPointerInTable->noSearchDone == false){
			i = elementPointerInTable->tableIndex + 1;
		}

		if (! (i <sccComponent->tablelength)){
//			printf("Returning due to ! i <sccComponent->tablelength, i=%d, tableLength=%d \n ", i, sccComponent->tablelength);
			elementPointerInTable->noMoreAvailable = true;
			return false;
		}

//		printf("Ready to go into loop with i=%d, tableLength=%d ", i, sccComponent->tablelength);
		for (; i < sccComponent->tablelength; i++){
			struct entry *sccEntry = sccComponent->table[i];

			if(NULL != sccEntry){
				// Found a given hashtable state

				unsigned int keyVal = * ((ptHtKey) sccEntry->k);

				ptValue valP =( (ptValue) sccEntry->v);

				if(valP != NULL){
					ptList listHashStates = valP->list;
					if(NULL != listHashStates){
							*nextState =  (ptHtState) listHashStates->value ;
							*Key = keyVal;

							// also have to fill out the key .
							elementPointerInTable->noSearchDone = false;
							elementPointerInTable->tableIndex = i;

							if(listHashStates->next != NULL){
								elementPointerInTable->currentListStates = listHashStates->next;
								elementPointerInTable->currentEntry = sccEntry;
							} else {

								elementPointerInTable->currentListStates = NULL;
								if(sccEntry->next != NULL){
									elementPointerInTable->currentEntry = sccEntry->next;
								}
							}

							return true;

					} else {
						failure ("Error in the hashtable. A key inserted with a list whose 1st element is null, or  with a reachable null element \n");
					}

				} else {
					failure ("Error in the hashtable. A key inserted with a null list of elements \n");
				}
			}
		}
		elementPointerInTable->noMoreAvailable = true;
		return false;
	} else {
		/* We performed a previous search and found remaining elements in the current index */
		if(elementPointerInTable->currentListStates == NULL){
			/* We have to obtain  element from elementPointerInTable->currentEntry */
			unsigned int keyVal = * ((ptHtKey) elementPointerInTable->currentEntry->k);

			ptValue valP =( (ptValue) elementPointerInTable->currentEntry->v);

			// Advance into the list of elements of  an entry in the HT.
			if(valP != NULL){
				ptList listHashStates = valP->list;
				if(NULL != listHashStates){
						*nextState =  (ptHtState) listHashStates->value ;
						*Key = keyVal;

						if(listHashStates->next != NULL){
							elementPointerInTable->currentListStates = listHashStates->next;
							//elementPointerInTable->currentEntry = elementPointerInTable->currentEntry;
						} else {

							elementPointerInTable->currentListStates = NULL;

							if(elementPointerInTable->currentEntry->next != NULL){
								elementPointerInTable->currentEntry = elementPointerInTable->currentEntry->next;
							} else {
								elementPointerInTable->currentEntry = NULL;
							}
						}
						return true;
				} else {
					failure ("Error in the hashtable. A key inserted with a list whose 1st element is null, or  with a reachable null element \n");
				}
			} else {
				failure ("Error in the hashtable. A key inserted with a null list of elements \n");
			}
		} else {
			// We have to obtain element from list of elements
			// By logic of the else-s elementPointerInTable->currentListStates ! =  NULL
			unsigned int keyVal = * ((ptHtKey) elementPointerInTable->currentEntry->k);

			ptList listHashStates = elementPointerInTable->currentListStates;

			if(NULL != listHashStates){
				*nextState =  (ptHtState) listHashStates->value ;

				if(listHashStates->next != NULL){
					elementPointerInTable->currentListStates = listHashStates->next;
					//elementPointerInTable->currentEntry = elementPointerInTable->currentEntry;
				} else {
					elementPointerInTable->currentListStates = NULL;

					if(elementPointerInTable->currentEntry->next != NULL){
						elementPointerInTable->currentEntry = elementPointerInTable->currentEntry->next;
					} else {
						elementPointerInTable->currentEntry = NULL;
					}
				}
			}else {
				failure("Should never happen -- Incorrect logic in if/else tableHasMoreElements gives listHashStates = NULL. ");
			}
		}
	}
}



/*
 *
 * Code to iterate through elements of a hash table.
 *
 * 	Requires individual type of them to be a ptHtDState (otherwise ... )
 *
 */
bool tableHasMoreElementsHTDVersion(struct hashtable *sccComponent,
		ptHtIteratorState elementPointerInTable,
		ptHtDState *nextState,
		unsigned int *Key){

//	printf("Executing tableHasMoreElements \n");
//	printf("elementPointerInTable=%s\n", elementPointerInTable == NULL ? "Null" : "Not NUll");
//	printElementPointerInTable(elementPointerInTable);
//	printf("\n");


	if(elementPointerInTable->noMoreAvailable == true){
		return false;
	}

	/* Iterating based on the value of elementPointerInTable */
	if(elementPointerInTable->noSearchDone == true ||
			( (elementPointerInTable->noSearchDone == false) &&
					elementPointerInTable->currentEntry == NULL &&
					elementPointerInTable->currentListStates == NULL)
			){

//		printf("Inside first if \n");
		int i = 0;

		if (elementPointerInTable->noSearchDone == false){
			i = elementPointerInTable->tableIndex + 1;
		}

		if (! (i <sccComponent->tablelength)){
//			printf("Returning due to ! i <sccComponent->tablelength, i=%d, tableLength=%d \n ", i, sccComponent->tablelength);
			elementPointerInTable->noMoreAvailable = true;
			return false;
		}

//		printf("Ready to go into loop with i=%d, tableLength=%d ", i, sccComponent->tablelength);
		for (; i < sccComponent->tablelength; i++){
			struct entry *sccEntry = sccComponent->table[i];

			if(NULL != sccEntry){
				// Found a given hashtable state

				unsigned int keyVal = * ((ptHtKey) sccEntry->k);

				ptValue valP =( (ptValue) sccEntry->v);

				if(valP != NULL){
					ptList listHashStates = valP->list;
					if(NULL != listHashStates){
							*nextState =  (ptHtDState) listHashStates->value ;
							*Key = keyVal;

							// also have to fill out the key .
							elementPointerInTable->noSearchDone = false;
							elementPointerInTable->tableIndex = i;

							if(listHashStates->next != NULL){
								elementPointerInTable->currentListStates = listHashStates->next;
								elementPointerInTable->currentEntry = sccEntry;
							} else {

								elementPointerInTable->currentListStates = NULL;
								if(sccEntry->next != NULL){
									elementPointerInTable->currentEntry = sccEntry->next;
								}
							}

							return true;

					} else {
						failure ("Error in the hashtable. A key inserted with a list whose 1st element is null, or  with a reachable null element \n");
					}

				} else {
					failure ("Error in the hashtable. A key inserted with a null list of elements \n");
				}
			}
		}
		elementPointerInTable->noMoreAvailable = true;
		return false;
	} else {
		/* We performed a previous search and found remaining elements in the current index */
		if(elementPointerInTable->currentListStates == NULL){
			/* We have to obtain  element from elementPointerInTable->currentEntry */
			unsigned int keyVal = * ((ptHtKey) elementPointerInTable->currentEntry->k);

			ptValue valP =( (ptValue) elementPointerInTable->currentEntry->v);

			// Advance into the list of elements of  an entry in the HT.
			if(valP != NULL){
				ptList listHashStates = valP->list;
				if(NULL != listHashStates){
						*nextState =  (ptHtDState) listHashStates->value ;
						*Key = keyVal;

						if(listHashStates->next != NULL){
							elementPointerInTable->currentListStates = listHashStates->next;
							//elementPointerInTable->currentEntry = elementPointerInTable->currentEntry;
						} else {

							elementPointerInTable->currentListStates = NULL;

							if(elementPointerInTable->currentEntry->next != NULL){
								elementPointerInTable->currentEntry = elementPointerInTable->currentEntry->next;
							} else {
								elementPointerInTable->currentEntry = NULL;
							}
						}
						return true;
				} else {
					failure ("Error in the hashtable. A key inserted with a list whose 1st element is null, or  with a reachable null element \n");
				}
			} else {
				failure ("Error in the hashtable. A key inserted with a null list of elements \n");
			}
		} else {
			// We have to obtain element from list of elements
			// By logic of the else-s elementPointerInTable->currentListStates ! =  NULL
			unsigned int keyVal = * ((ptHtKey) elementPointerInTable->currentEntry->k);

			ptList listHashStates = elementPointerInTable->currentListStates;

			if(NULL != listHashStates){
				*nextState =  (ptHtDState) listHashStates->value ;

				if(listHashStates->next != NULL){
					elementPointerInTable->currentListStates = listHashStates->next;
					//elementPointerInTable->currentEntry = elementPointerInTable->currentEntry;
				} else {
					elementPointerInTable->currentListStates = NULL;

					if(elementPointerInTable->currentEntry->next != NULL){
						elementPointerInTable->currentEntry = elementPointerInTable->currentEntry->next;
					} else {
						elementPointerInTable->currentEntry = NULL;
					}
				}
			}else {
				failure("Should never happen -- Incorrect logic in if/else tableHasMoreElements gives listHashStates = NULL. ");
			}
		}
	}
}


/*
 *
 * Debugging code to print contents of  elementPointerInTable.
 *
 */
void printElementPointerInTable(ptHtIteratorState elementPointerInTable){


	printf("noSearchDone=%s, noMoreAvailable=%s, ", elementPointerInTable->noSearchDone ? "True" : "False",
			elementPointerInTable->noMoreAvailable ? "True" : "False");
	printf("tableIndex=%d, ", elementPointerInTable->tableIndex);

	printf("currentListStates=%s, ", elementPointerInTable->currentListStates == NULL ?
			"Null" : "Not NUll");

	printf("currentEntry=%s, ", elementPointerInTable->currentEntry == NULL ?
			"Null" : "Not NUll");


}

