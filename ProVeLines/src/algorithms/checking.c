#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include "error.h"
#include "main.h"
#include "list.h"
#include "stack.h"
#include "boolFct.h"
#include "symbols.h"
#include "automata.h"
#include "SAT.h"
#ifdef CLOCK
	#include "clockZone.h"
	#include "federation.h"
#endif
#include "state.h"
#include "execution.h"
#include "hashState.h"
#include "hashtableState.h"
#include "tvl.h"
#include "checking.h"

PROFILER_REGISTER(pr_optimisation, "optimisation", 1000);

PROFILER_USE(pr_isSatisfiableWrtFD);
PROFILER_USE(pr_isSatisfiableWrtBaseFD);
PROFILER_USE(pr_isSatisfiable);
PROFILER_USE(pr_implies);
PROFILER_USE(pr_impliesWrtFD);
PROFILER_USE(pr_impliesWrtBaseFD);
PROFILER_USE(pr_executables);
PROFILER_USE(pr_apply);
PROFILER_USE(pr_eval);

/*
 * The following algorithms perform a nested depth first search.
 * They allow to check both safety and liveness properties.
 * A never claim (corresponding to an LTL formula) must have been declared for the model checker to work.
 */

// Statistics gathered during executions
static 		unsigned int _nbErrors = 0;				// Total number of encountered problems
static long unsigned int _nbStatesExplored = 1;		// Total of distinct states (without features) explored
static long unsigned int _nbStatesReExplored = 0;	// Total of states that had to be re-explored because new products were found to be able to reach them
static long unsigned int _nbStatesStops = 0;		// Total of states where exploration backtracked because they were already visited before
static long unsigned int _nbStatesExploredInner = 0;// As before, but for inner search.
static long unsigned int _nbStatesReExploredInner = 0;//As before, but for inner search.
static long unsigned int _nbStatesStopsInner = 0;	// As before, but for inner search.
static long unsigned int _depth = 0;				// Current exploration depth (inner and outer)

//#define DEBUG_PRINT(msg) printf(msg);
//#define DEBUG_PRINT_d(msg, d) printf(msg, d);
//#define DEBUG_PRINT_s(msg, s) printf(msg, s);
#define DEBUG_PRINT(msg)
#define DEBUG_PRINT_d(msg, d)
#define DEBUG_PRINT_s(msg, s)

/*#define TIME_REPORT \
		printf("#(Sat) : %lu\n",satCalls); \
		printf("T(Sat) : %lu\n",satTime); \
		printf("#(Boo) : %lu\n",junCalls); \
		printf("T(Boo) : %lu\n",junTime);*/

// These stats are printed regularly
#define STATS if(((_nbStatesExplored + _nbStatesReExplored) % PRINT_EXPLORED_STEP) == 0)	\
				printf(	"Explored %10lu, "												 	\
						"re-explored %10lu, "												\
						"backtracked %10lu, "												\
						"depth % 7ld, "														\
						"buckets %10lu, "													\
						"mean bucket size % 5.2Lf"  										\
						"\n",						 										\
					_nbStatesExplored,				  										\
					_nbStatesReExplored, 													\
					_nbStatesStops,															\
					_depth, 																\
					htVisitedStatesNbFilledBuckets,											\
					(long double) htVisitedStatesNbRecords / (long double) htVisitedStatesNbFilledBuckets);

// Macro used when an error is encountered (always the same stuff that's printed)
/*
			printf("%s", msg);																\
			printf(" [explored %lu states, re-explored %lu].\n",							\
					_nbStatesExplored, _nbStatesReExplored); 								\
*/


#define STOP_ERROR(msg, features, stackOuter, stackInner, loopStart) {						\
			_nbErrors++;																	\
			if(!_allProductsFail) {															\
				if(features) {																\
					_failProducts = addDisjunction(_failProducts, features, 0, 1);			\
					_allProductsFail = !addConstraintToFD(negateBool(features));			\
				} else {	 																\
					destroyBool(_failProducts);												\
					_failProducts = NULL;													\
				} 																			\
			}																				\
			if(!_failProducts) _allProductsFail = 1;										\
			if(features) {																	\
				/*printf(" - Products by which it is violated (as feature expression):\n   ");*/	\
				/* printBool(features);	*/														\
				/* printf("\n"); */																\
			} else {																		\
				 /* printf(" - Violated by all products.\n"); */									\
			}																				\
			if(trace != NO_TRACE) {															\
				printf("\n - Stack trace:\n");												\
				printStackElementStack(globalSymTab, mtypes, stackOuter, loopStart, NULL);						\
				if(stackInner) printStackElementStack(globalSymTab, mtypes, stackInner, NULL, top(stackOuter));	\
			}																				\
			if(!exhaustive) { 																\
				PROFILER_REPORT(pr_optimisation);											\
				PROFILER_REPORT(pr_executables);											\
				PROFILER_REPORT(pr_apply);													\
				PROFILER_REPORT(pr_eval);													\
				PROFILER_REPORT(pr_isSatisfiable);											\
				PROFILER_REPORT(pr_isSatisfiableWrtFD); 									\
				PROFILER_REPORT(pr_isSatisfiableWrtBaseFD); 								\
				PROFILER_REPORT(pr_implies); 												\
				PROFILER_REPORT(pr_impliesWrtFD); 											\
				PROFILER_REPORT(pr_impliesWrtBaseFD); 										\
				exit(EXIT_FOUND_PROBLEM);													\
			}																				\
			if(trace != NO_TRACE) printf("\n\n ****\n");									\
			/*printf("\n");	*/																\
		}

#define STOP_ERROR_GLOBAL {																	\
			printf("\n");																	\
			printf("Exhaustive search finished ");											\
			printf(" [explored %lu states, re-explored %lu].\n",							\
					_nbStatesExplored, _nbStatesReExplored); 								\
			if(_nbErrors == 1) printf(" -  One problem found");								\
			else printf(" - %u problems were found", _nbErrors);							\
			if(_allProductsFail || isTautology(_failProducts)) 								\
				printf(" covering every product.\n");										\
			else {																			\
				printf(" covering the following products (others are ok):\n");				\
				printBool(_failProducts);													\
				printf("\n");																\
			}																				\
			printf("\n");																	\
			if(!keepTempFiles) {															\
				remove("__workingfile.tmp");												\
				remove("__workingfile.tmp.cpp");											\
			}																				\
			PROFILER_REPORT(pr_optimisation);												\
			PROFILER_REPORT(pr_executables);												\
			PROFILER_REPORT(pr_apply);														\
			PROFILER_REPORT(pr_eval);														\
			PROFILER_REPORT(pr_isSatisfiable);												\
			PROFILER_REPORT(pr_isSatisfiableWrtFD); 										\
			PROFILER_REPORT(pr_isSatisfiableWrtBaseFD); 									\
			PROFILER_REPORT(pr_implies); 													\
			PROFILER_REPORT(pr_impliesWrtFD); 												\
			PROFILER_REPORT(pr_impliesWrtBaseFD); 											\
			exit(EXIT_FOUND_PROBLEM);														\
		}

#define EXPLORATION_LIMIT(error) 															\
			if(limitExploration && _nbStatesExplored >= limitExploration) {					\
				printf("Reached exploration limit ");										\
				printf(" [explored %lu states, re-explored %lu].\n",						\
						limitExploration, _nbStatesReExplored);								\
				if(!keepTempFiles) {														\
					remove("__workingfile.tmp");											\
					remove("__workingfile.tmp.cpp");										\
				}																			\
				if(!error) {																\
					printf("So far, no problems were found.\n");							\
					exit(EXIT_REACHED_EXPLORATION_LIMIT);									\
				} else {																	\
					printf("So far, %u problem(s) were found covering the following products:\n", _nbErrors);	\
					printBool(_failProducts);												\
					printf("\n");															\
					exit(EXIT_FOUND_PROBLEM);												\
				}																			\
			}

#define STOP_NOERROR(msg) {																	\
			printf(msg);																	\
			printf(" [explored %lu states, re-explored %lu].\n",							\
					_nbStatesExplored, _nbStatesReExplored); 								\
			if(_nbStatesExploredInner != 0) 			 									\
				printf("The inner search explored %lu states and re-explored %lu.\n",		\
						_nbStatesExploredInner, _nbStatesReExploredInner); 					\
			if(!keepTempFiles) {															\
				remove("__workingfile.tmp");												\
				remove("__workingfile.tmp.cpp");											\
			}																				\
			PROFILER_REPORT(pr_optimisation);												\
			PROFILER_REPORT(pr_executables);												\
			PROFILER_REPORT(pr_apply);														\
			PROFILER_REPORT(pr_eval);														\
			PROFILER_REPORT(pr_isSatisfiable);												\
			PROFILER_REPORT(pr_isSatisfiableWrtFD); 										\
			PROFILER_REPORT(pr_isSatisfiableWrtBaseFD); 									\
			PROFILER_REPORT(pr_implies); 													\
			PROFILER_REPORT(pr_impliesWrtFD); 												\
			PROFILER_REPORT(pr_impliesWrtBaseFD); 											\
			exit(EXIT_SUCCESS);																\
		}


// For exhaustive search we keep a boolean expression that records all products known to violate
// the property.  This is used to prevent exploration of a path that belongs to products that
// are known to violate the property (and hence, nothing new can be learned on the path).
static ptBoolFct _failProducts = NULL; // boolean function; NULL means no violations yet
static byte _allProductsFail = 0; // = 1 if all of the products violate the property.

/**
 * Checks for deadlocks in a stack element.  Returns true if there are any, false
 * otherwise.
 *
 * State->features defines the products in which the state is reachable.  For no
 * deadlock to be possible, there has to be an outgoing transition in each of these
 * products.  So let OUT be the expression defining the products for which there is
 * an outgoing transition; we need to check whether  state->features  \subset  out
 * (there should be "more" outgoing that incoming).
 */
/*byte hasDeadlock(ptStackElt elt) {
	if(!elt->E_save) return 1;
	if(elt->E_save && !fullDeadlockCheck) return 0;
	ptBoolFct featuresOut = NULL;
	byte result = 1;
	ptList transList = elt->E_save;
	while(transList && result) {
		// This has to be handled separately
		if(((ptProcessTransition) transList->value)->features) {
			featuresOut = addDisjunction(featuresOut, ((ptProcessTransition) transList->value)->features, 0, 1);
		} else {
			result = 0;
			if(featuresOut) {
				destroyBool(featuresOut);
				featuresOut = NULL;
			}
		}
		transList = transList->next;
	}
	if(result == 0) return 0;
	else {
		result = !impliesWrtBaseFD(elt->state->features, featuresOut);
		if(result) {
			// TODO integrate with STOP_ERROR
			printf("State reachable by the following products\n");
			printf(" - ");printBool(elt->state->features);printf("\n");
			printf("Outgoing transitions only for the following products\n");
			printf(" - ");printBool(featuresOut);printf("\n");
		}
		destroyBool(featuresOut);
	}
	return result;
}*/

#ifndef CLOCK
void startNestedDFS(ptList props, ptSymTabNode globalSymTab, ptMTypeNode mtypes) {
	// Initialise
	initSolverWithFD(getFeatureModelClauses());

	/* initializing Hashtable */
	htVisitedStatesInit();
	htOuterStatesInit();

	// Create initial state
	ptState init = stateCreateInitial(globalSymTab, mtypes);
	init->payloadHash = hashState(init);

	htVisitedStatesInsert(init->payloadHash, init, DFS_OUTER);
	htOuterStatesInsert(init->payloadHash, init);


	checkInitialization( init);


	ptStackElt elt = createStackElement(init, _nbErrors);
	ptStack stack = push(NULL,	elt);

	if(outerDFS(props, globalSymTab, mtypes, stack) == 0)
		STOP_NOERROR("Property satisfied")
}

/**
 * Returns true if a violation (safety, liveness) was found (the error was already printed then).
 * Returns false otherwise.
 *
 * Assert violations and deadlocks cause an immediate exit() when detected.
 */
byte outerDFS(ptList props, ptSymTabNode globalSymTab, ptMTypeNode mtypes, ptStack stackOuter) {
	ptStackElt current;
	ptFsmNode neverNode;
	ptState s_;
	ptHtState prevS_;
	ptBoolFct noOutgoing = NULL;

	byte resetExclusivity = false;
	ptList E = NULL;

	// Set if an error was found
	byte error = 0;
	byte assertViolation = 0;
	byte hasDeadlock = 0;

	byte temp; // TODO remove

	// Execution continues as long as the
	//  - stack is not empty
	//  - no error was found (except in the exhaustive case)
	//  - not all products are known to fail already
	while(!empty(stackOuter) && (!error || exhaustive) && !_allProductsFail) {
		EXPLORATION_LIMIT(error);

		current = top(stackOuter);
		neverNode = getNodePointer(current->state, current->state->never);


		DEBUG_PRINT_d(" - outer loop, state: %u", current->state->payloadHash);
		DEBUG_PRINT_d(", explored %lu", _nbStatesExplored);
		DEBUG_PRINT_d(", depth %lu\n", _depth);

		// If the current features are contained in the set of products known to violate the
		// property, then we do not have to go beyond the state
		if(0 < _nbErrors) {
			PROFILER_START(pr_optimisation);
			temp = !isSatisfiableWrtFD(current->state->features);
			PROFILER_END(pr_optimisation);
		}


		if(0 < _nbErrors && temp) {
			DEBUG_PRINT("    +-> is known to violate, backtrack.\n");
			pop(&stackOuter);
			htOuterStatesRemove(current->state->payloadHash, current->state->payload, current->state->payloadSize);
			destroyStackElement(current, processTrans);
			_depth--;


		// If the node of the never claim is NULL, then it had a loose end which was taken in the
		// previous transition.  This means that we are in a final state, which is assumed to be
		// accepting; hence:
		} else if(isSafetyPropertyViolationFound(neverNode) /*!neverNode*/) {

			// Safety property violated.
			// We have to pop two states off the stack to get to the violating state:
			//  -> the current top is a skip transition fired from an accepting state
			//  -> the state below that is the accepting state
			//  -> the state below that is the state that actually led to the accepting state to be reachable.
			//     i.e. this state is the actual violating state.
			pop(&stackOuter);
			ptStackElt newTop = pop(&stackOuter);
			STOP_ERROR("Safety property violated", ((ptStackElt) top(stackOuter))->state->features, stackOuter, NULL, NULL);
			error = 1;
			htOuterStatesRemove(current->state->payloadHash, current->state->payload, current->state->payloadSize);
			destroyStackElement(current, processTrans);
			destroyStackElement(newTop, processTrans);
			newTop = pop(&stackOuter);
			destroyStackElement(newTop, processTrans);
			_depth = _depth - 3;

		// Otherwise, the state can be explored (or exploration continue)
		} else {
			DEBUG_PRINT("    +-> exploring...\n");
			current->nbErrors = _nbErrors;

			// If the element is uninitialised; the executable transitions have to be determined
			if(!areExcutableTransitionsComputed(current) /* !current->E_save && !current->E_never_save */) {

				DEBUG_PRINT("    +-> initialising...\n");

				initializeExecutableTransitions(current, &E, globalSymTab,
						mtypes, _nbErrors, &hasDeadlock, &noOutgoing,
						&resetExclusivity);

				// Check for deadlocks
				if(hasDeadlock) {
					STOP_ERROR("Found deadlock", current->state->features, stackOuter, NULL, NULL);
					error = 1;
					current->E = NULL; // This will cause backtracking
				} else if(!current->E_never) {
					current->E = NULL;
				}

				if(fullDeadlockCheck && noOutgoing) {
					if(isSatisfiableWrtFD(noOutgoing)) {
						STOP_ERROR("Found trivially invalid end state; the following set of products can reach the state, but has no outgoing transition.", noOutgoing, stackOuter, NULL, NULL);
						error = 1;
					}
					destroyBool(noOutgoing);
					noOutgoing = NULL;
				}
			} // end-if  !areExcutableTransitionsComputed(current)

			// If we have explored all transitions of the state (!current->E_never; see "struct stackElt"
			// in stack.h), we check whether the state is accepting and start a backlink search if it is;
			// otherwise just backtrack

			if(!moreTransitionsAvailable(current) /*!current->E */) {

				DEBUG_PRINT("    +-> all transitions of state fired, acceptance check and backtracking...\n");
				// Back these values up, the inner search will free current->state before returning
				void* payloadBak = current->state->payload;
				unsigned int payloadHashBak = current->state->payloadHash;
				unsigned int payloadSizeBak = current->state->payloadSize;
				if((neverNode->flags & N_ACCEPT) == N_ACCEPT) {
					DEBUG_PRINT("    +-> found accepting, starting inner...\n");
					ptStack stackInner = push(NULL, createStackElement(current->state, _nbErrors));
					_depth++;
					_nbStatesExploredInner++;
					error = innerDFS(props, globalSymTab, mtypes, stackOuter, stackInner) || error; // error needs to be to the right, for otherwise lazy evaluation might cause the innerDFS call to be skipped
					current->state = NULL; // it will have been destroyed when the innerDFS backtracked for the last time
				}
				pop(&stackOuter);
				htOuterStatesRemove(payloadHashBak, payloadBak, payloadSizeBak);
				destroyStackElement(current, processTrans);
				_depth--;

			// ..., or there is a transition to be executed:
			} else if(moreTransitionsAvailable(current) /*current->E*/) {

				/*  Executing Transition */

				DEBUG_PRINT_d("    +-> firing transition TL%02d...", ((ptProcessTransition) current->E->value)->trans ? ((ptProcessTransition) current->E->value)->trans->lineNb : -1);


				s_ = apply(globalSymTab, mtypes, current->state, (ptProcessTransition) current->E->value, 1, &assertViolation);

				ptFsmNode neverNodeTmp =  getNodePointer(s_, s_->never);

				s_ = applyNever(globalSymTab, mtypes, s_, (ptFsmTrans) current->E_never->value);

				neverNodeTmp =  getNodePointer(s_, s_->never);


				E = executables(globalSymTab, mtypes, s_, 1, _nbErrors, &hasDeadlock, NULL, NULL, &noOutgoing, &resetExclusivity);

				if(resetExclusivity) {
					stateSetValue(s_->payload, OFFSET_EXCLUSIVE, T_BYTE, NO_PROCESS);
					DEBUG_PRINT_d("         - lost exclusivity, state became %u\n", s_->payloadHash);
				}


				s_->payloadHash = hashState(s_);
				DEBUG_PRINT_d(" got %u\n", s_->payloadHash);

				/* End Executing Transition */


				if(assertViolation) {
					char msg[40];
					sprintf(msg, "Assertion at line %d violated", ((ptProcessTransition) current->E->value)->trans->lineNb);
					STOP_ERROR(msg, s_->features, stackOuter, NULL, NULL);
					error = 1;
					stateDestroy(s_, false);
					s_ = NULL;
					destroyProcTransList(E, processTrans);
					E = NULL;

				} else { // No assert Violation.


					prevS_ = NULL;
					byte stateVisitedFind =  htVisitedStatesFind(s_->payloadHash, s_, DFS_OUTER, &prevS_);
					if( stateVisitedFind ) {
						DEBUG_PRINT("         - state already visited.\n");
						stateDestroy(s_, false);
						s_ = NULL;
						destroyProcTransList(E, processTrans);
						E = NULL;
						_nbStatesStops++;
					} else {
						if(prevS_) {
							DEBUG_PRINT("         - state visited but features fresh, pushing on stack.\n");
							// The state was visited already, but the current copy is "fresher".
							// No need to insert it into the hash table, just update the feature expression

							// Important: PrevS_ can only be a state that was fully explored with the features
							// it has now. This is because:
							//  - it has been visited already (otherwise, it wouldn't be in the hashtab)
							//  - it is not a state on the current stack (otherwise, it wouldn't be fresh)
							// This means, that all states that could be visited with prevS_->features have
							// been visited already.  So, when we continue, we use s_->features and not
							// s_->features || prevS_->features.
							ptBoolFct negPrev = negateBool(prevS_->outerFeatures);

							if(s_->features == NULL) {
								destroyBool(prevS_->outerFeatures);
								prevS_->outerFeatures = getTrue();
							} else {
								prevS_->outerFeatures = addDisjunction(prevS_->outerFeatures, s_->features, 0, 1);

								#ifdef CHECK_TAUTOLOGY
									if(prevS_->outerFeatures && isTautology(prevS_->outerFeatures)) {
										destroyBool(prevS_->outerFeatures);
										prevS_->outerFeatures = getTrue();
									}
								#endif
							}

							s_->features = addConjunction(negPrev, s_->features, 0, 0);
							free(s_->payload);
							s_->payload = prevS_->payload;

							// The state is not a new state:
							_nbStatesReExplored++;
							STATS;

						} else {
							DEBUG_PRINT("         - state fresh, pushing on stack.\n");
							// The state was not visited at all
							htVisitedStatesInsert(s_->payloadHash, s_, DFS_OUTER);
							_nbStatesExplored++;
							STATS;
						}


						stackOuter = push(stackOuter, createStackElement(s_, _nbErrors));
						htOuterStatesInsert(s_->payloadHash, s_);
						_depth++;
					} // end-if (stateVisited-Find) fresh state


				} // no assert violation

				// Simulate nested loop: when at the end of E, restart the E and advance the E_never
				// The deadlock test (E empty) is already done.  This also guarantees that E->value
				// is never NULL in the above apply(globalSymTab, mtypes, ).
				current->E_never = current->E_never->next;
				if(!current->E_never) {
					current->E = current->E->next;
					current->E_never = current->E_never_save;
				}

			} // fire transition
		} // explore state
	} // end while

	// If error is true and we end up here, then we're in exhaustive mode. A summary has to be printed
	if(error /* not needed: && exhaustive */) STOP_ERROR_GLOBAL;
	destroyStackElementStack(stackOuter, processTrans);
	return error;
}

/**
 * Returns true if a violation (liveness, deadlock) was found (the error was already printed then).
 * Returns false otherwise.
 *
 * Assert violations cause an immediate exit() when detected.
 */
byte innerDFS(ptList props, ptSymTabNode globalSymTab, ptMTypeNode mtypes, ptStack stackOuter, ptStack stackInner) {
	ptStackElt current;
	ptFsmNode neverNode;
	ptState s_;
	ptHtState prevS_, onSt;
	ptBoolFct noOutgoing = NULL;

	byte resetExclusivity = false;
	ptList E = NULL;
	byte temp;

	byte error = 0;
	byte assertViolation = 0;
	byte hasDeadlock = 0;

	// Same conditions as for the outerDFS loop
	while(!empty(stackInner) && (!error || exhaustive) && !_allProductsFail) {
		EXPLORATION_LIMIT(error);

		current = top(stackInner);
		neverNode = getNodePointer(current->state, current->state->never);

		DEBUG_PRINT_d("         - inner loop, state: %u", current->state->payloadHash);
		DEBUG_PRINT_d(", explored %lu", _nbStatesExplored);
		DEBUG_PRINT_d(", depth %lu\n", _depth);

		// If the current features are contained in the set of products known to violate the
		// property, then we do not have to go beyond the state
		temp = 0;
		if(current->nbErrors < _nbErrors) {
			PROFILER_START(pr_optimisation);
			temp = !isSatisfiableWrtFD(current->state->features);
			PROFILER_END(pr_optimisation);
		}

		if(current->nbErrors < _nbErrors && temp) {
			DEBUG_PRINT("            +-> is known to violate, backtrack.\n");
			pop(&stackInner);
			destroyStackElement(current, processTrans);
			_depth--;

		// Otherwise, explore state
		} else {
			DEBUG_PRINT("            +-> exploring.\n");
			current->nbErrors = _nbErrors;

			// If the element is uninitialised; the executable transitions have to be determined
			if(!current->E_save && !current->E_never_save) {
				DEBUG_PRINT("            +-> initialising.\n");
				if(E) {
					current->E = E;
					E = NULL;
				}
				else
					current->E = executables(globalSymTab, mtypes, current->state, 1, _nbErrors, &hasDeadlock, NULL, NULL, &noOutgoing, &resetExclusivity);
				current->E_save = current->E;
				current->E_never = executablesNever(globalSymTab, mtypes, current->state);
				current->E_never_save = current->E_never;

				// Check for deadlocks
				if(hasDeadlock) {
					STOP_ERROR("Found deadlock", current->state->features, stackOuter, stackInner, NULL);
					error = 1;
					current->E_never = NULL; // This will cause backtracking
				} else if(!current->E_never) current->E = NULL;

				if(fullDeadlockCheck && noOutgoing) {
					if(isSatisfiableWrtFD(noOutgoing)) {
						STOP_ERROR("Found trivially invalid end state; the following set of products can reach the state, but has no outgoing transition.", noOutgoing, stackOuter, stackInner, NULL);
						error = 1;
					}
					destroyBool(noOutgoing);
					noOutgoing = NULL;
				}
			}

			// When there are no more transitions to execute for the state, backtrack
			if(!current->E) {
				DEBUG_PRINT("            +-> all transitions of state fired, backtracking.\n");
				pop(&stackInner);
				destroyStackElement(current, processTrans);
				_depth--;

			// Otherwise, execute the current transition
			} else {
				DEBUG_PRINT_d("            +-> firing transition TL%02d...", ((ptProcessTransition) current->E->value)->trans ? ((ptProcessTransition) current->E->value)->trans->lineNb : -1);
				s_ = apply(globalSymTab, mtypes, current->state, (ptProcessTransition) current->E->value, 1, &assertViolation);
				s_ = applyNever(globalSymTab, mtypes, s_, (ptFsmTrans) current->E_never->value);
				E = executables(globalSymTab, mtypes, s_, 1, _nbErrors, &hasDeadlock, NULL, NULL, &noOutgoing, &resetExclusivity);
				if(resetExclusivity) {
					stateSetValue(s_->payload, OFFSET_EXCLUSIVE, T_BYTE, NO_PROCESS);
					DEBUG_PRINT_d("         - lost exclusivity, state became %u\n", s_->payloadHash);
				}
				s_->payloadHash = hashState(s_);
				DEBUG_PRINT_d(" got %u\n", s_->payloadHash);

				// Check whether back link or assert violation, if yes: do not explore further
				// onSt = stateOnStack(s_, stackOuter, STACK_ELT);
				onSt = htOuterStatesFind(s_->payloadHash, s_);
				if(onSt || assertViolation) {
					stackInner = push(stackInner, createStackElement(s_, _nbErrors)); // push on stack for printing purpose

					if(onSt){
						STOP_ERROR("Property violated", s_->features, stackOuter, stackInner, s_)
					}
					else {
						char msg[40];
						sprintf(msg, "Assertion at line %d violated", ((ptProcessTransition) current->E->value)->trans->lineNb);
						STOP_ERROR(msg, s_->features, stackOuter, stackInner, NULL);
					}

					error = 1;

					// Destroy the state and undo the push(stackInner) done for printing before
					stateDestroy(s_, false);
					s_ = NULL;
					destroyProcTransList(E, processTrans);
					E = NULL;
					((ptStackElt) top(stackInner))->state = NULL; // otherwise, destroyStackElement will access the freed state
					destroyStackElement(top(stackInner), processTrans);
					pop(&stackInner);

				// If no: continue
				} else {
					prevS_ = NULL;
					if(htVisitedStatesFind(s_->payloadHash, s_, DFS_INNER, &prevS_)) {
						DEBUG_PRINT("                 - state already visited\n");
						stateDestroy(s_, false);
						s_ = NULL;
						destroyProcTransList(E, processTrans);
						E = NULL;
						_nbStatesStopsInner++;

					} else {
						if(prevS_) {
							if(prevS_->foundInDFS == DFS_INNER) {
								DEBUG_PRINT("                 - state visited, but features fresh\n");
								_nbStatesReExploredInner++;
							} else {
								DEBUG_PRINT("                 - state only visited during outer search\n");
								prevS_->foundInDFS = DFS_INNER;
								_nbStatesExploredInner++;
							}
							STATS;

							// The state was visited already, but the current copy is "fresher".
							// No need to insert it into the hash table, just update the feature expression
							ptBoolFct negPrev = negateBool(prevS_->innerFeatures);
							if(s_->features == NULL) {
								destroyBool(prevS_->innerFeatures);
								prevS_->innerFeatures = getTrue();
							} else {
								prevS_->innerFeatures = addDisjunction(prevS_->innerFeatures, s_->features, 0, 1);
								#ifdef CHECK_TAUTOLOGY
									if(prevS_->innerFeatures && isTautology(prevS_->innerFeatures)) {
										destroyBool(prevS_->innerFeatures);
										prevS_->innerFeatures = getTrue();
									}
								#endif
							}
							s_->features = addConjunction(negPrev, s_->features, 0, 0);
							free(s_->payload);
							s_->payload = prevS_->payload;

						} else {
							printState(mtypes, s_,NULL);
							printf("Bug! The above state was found during the inner DFS but not during the outer! Aborting.\n");
							exit(EXIT_ERROR);
						}

						// Continue DFS
						stackInner = push(stackInner, createStackElement(s_, _nbErrors));
						_depth++;
					} // Fresh
				} // No back link

				// Walk through transition lists
				current->E_never = current->E_never->next;
				if(!current->E_never) {
					current->E = current->E->next;
					current->E_never = current->E_never_save;
				}
			} // fire transition
		} // explore state
	} // end while

	DEBUG_PRINT("         - done, returning to outer.\n");
	destroyStackElementStack(stackInner, processTrans);
	return error;
}
#endif


void printInitialState(ptState init, ptList props, ptSymTabNode globalSymTab, ptMTypeNode mtypes){
	printf("Printing Initial State \n");
	printf("Number of Processes: %d \n", init->nbProcesses);

	printState(mtypes, init, NULL);

	ptBoolFct noOutgoing = NULL;
	byte resetExclusivity = false;
	ptStackElt elt = createStackElement(init, _nbErrors);
	ptStack stack = push(NULL, elt);

	elt->E = executables(globalSymTab, mtypes, init, 1, _nbErrors, NULL, NULL, NULL, &noOutgoing, &resetExclusivity);
	elt->E_save = elt->E;

	while(elt->E){

		if( ((ptProcessTransition) elt->E->value)->trans){
			ptFsmTrans trans = ((ptProcessTransition) elt->E->value)->trans ;

			printf("Transition-Line Number %d ", ((ptProcessTransition) elt->E->value)->trans->lineNb );
			printf("HasFeatures=%s ", ((ptProcessTransition) elt->E->value)->trans->hasFeatures ? "true" : "false" );
			printf("FeatureExpression=", ((ptProcessTransition) elt->E->value)->trans->features ? "true" : "false" );
			printBool( ((ptProcessTransition) elt->E->value)->trans->features);
			printf(" Expression=%s", ((ptProcessTransition) elt->E->value)->trans->expression->sVal);
			printf(" Expression LineNbr=%d, Type=%s, ival=%d", ((ptProcessTransition) elt->E->value)->trans->expression->lineNb,
					getExpTypeName(((ptProcessTransition) elt->E->value)->trans->expression->type),
					((ptProcessTransition) elt->E->value)->trans->expression->iVal);

			if(E_STMNT_ASGN==trans->expression->type){
				printf("\n	Child[0]:: sVal=%s, lnbr=%d, exptype=%s, ival=%d \n",
						trans->expression->children[0]->sVal,
						trans->expression->children[0]->lineNb,
						getExpTypeName(trans->expression->children[0]->type),
						trans->expression->children[0]->iVal
						);
				printf("\n	Child[1]:: sVal=%s, lnbr=%d, exptype=%s, ival=%d \n",
						trans->expression->children[1]->sVal,
						trans->expression->children[1]->lineNb,
						getExpTypeName(trans->expression->children[1]->type),
						trans->expression->children[1]->iVal
						);
				if(E_VARREF==trans->expression->children[0]->type){
					printf("Child[0] varReference name %s", trans->expression->children[0]->children[0]->sVal);
				}
			}

			printf("\nFSM :: ");
			printFsm(((ptProcessTransition) elt->E->value)->trans->expression->fsm, 0,"ExpressionFSM");
			printf("\n Source Lnbr=%d, Target Lnbr=%d",
					((ptProcessTransition) elt->E->value)->trans->source->lineNb,

					((ptProcessTransition) elt->E->value)->trans->target->lineNb
					);

	//			printf("TransitionNode ");
//			printFsmNode(((ptProcessTransition) elt->E->value)->trans->source);
			printf(".\n");

			/*Add code for printing rest of transition */
		}
		elt->E = elt->E->next;
	}

	elt->E = elt->E_save;


}

// Simple exploration

void startExploration(ptList props, ptSymTabNode globalSymTab, ptMTypeNode mtypes) {
	// Initialise
	initSolverWithFD(getFeatureModelClauses());
	htVisitedStatesInit();

	// Create initial state
	ptState init = stateCreateInitial(globalSymTab, mtypes);
	init->payloadHash = hashState(init);

	htVisitedStatesInsert(init->payloadHash, init, DFS_OUTER);

	ptBoolFct noOutgoing = NULL;
	byte resetExclusivity = false;
	ptStackElt elt = createStackElement(init, _nbErrors);
	ptStack stack = push(NULL, elt);

	elt->E = executables(globalSymTab, mtypes, init, 1, _nbErrors, NULL, NULL, NULL, &noOutgoing, &resetExclusivity);
	elt->E_save = elt->E;

	if(!elt->E)
		STOP_ERROR("Initial state has no outgoing transition", NULL, stack, NULL, NULL);

	if(fullDeadlockCheck && noOutgoing) {
		if(isSatisfiableWrtBaseFD(noOutgoing))
			STOP_ERROR("Found trivially invalid end state; the following set of products can reach the state, but has no outgoing transition.", noOutgoing, stack, NULL, NULL);
		destroyBool(noOutgoing);
		noOutgoing = NULL;
	}

	if(explore(props, globalSymTab, mtypes, stack) == 0)
		STOP_NOERROR("No assertion violations or deadlocks found");
}

/**
 * Returns false if no assert violations or deadlocks were found.
 */
byte explore(ptList props, ptSymTabNode globalSymTab, ptMTypeNode mtypes, ptStack stackOuter) {
	ptStackElt current, new;
	ptFsmNode neverNode;
	ptState s_;
	ptHtState prevS_;
	ptBoolFct noOutgoing = NULL;

	byte resetExclusivity = false;

	// Set if an error was found
	byte error = 0;
	byte assertViolation = 0;
	byte hasDeadlock = 0;

	// Execution continues as long as the
	//  - stack is not empty
	//  - no error was found (except in the exhaustive case)
	//  - not all products are known to fail already
	while(!empty(stackOuter) && (!error || exhaustive) && !_allProductsFail) {
		EXPLORATION_LIMIT(error);

		current = top(stackOuter);


		DEBUG_PRINT_d(" - outer loop, state: %u", current->state->payloadHash);
		DEBUG_PRINT_d(", explored %lu", _nbStatesExplored);
		DEBUG_PRINT_d(", depth %lu\n", _depth);
		printState(mtypes, current->state, NULL);

		// If the current features are contained in the set of products known to violate the
		// property, then we do not have to go beyond the state
		if(current->nbErrors < _nbErrors && !isSatisfiableWrtFD(current->state->features)) {
			DEBUG_PRINT("    +-> is known to violate, backtrack.\n");
			pop(&stackOuter);
			destroyStackElement(current, processTrans);
			_depth--;

		// Otherwise, the state can be explored (or exploration continue)
		} else {
			DEBUG_PRINT("    +-> exploring...\n");
			current->nbErrors = _nbErrors;

			if(!current->E) {
				DEBUG_PRINT("    +-> all transitions of state fired, backtracking...\n");
				pop(&stackOuter);
				destroyStackElement(current, processTrans);
				_depth--;

			} else {
				DEBUG_PRINT_d("    +-> firing transition TL%02d...", ((ptProcessTransition) current->E->value)->trans ? ((ptProcessTransition) current->E->value)->trans->lineNb : -1);
				s_ = apply(globalSymTab, mtypes, current->state, (ptProcessTransition) current->E->value, 1, &assertViolation);
				s_->payloadHash = hashState(s_);
				DEBUG_PRINT_d(" got %u\n", s_->payloadHash);
				if(assertViolation) {
					char msg[40];
					sprintf(msg, "Assertion at line %d violated", ((ptProcessTransition) current->E->value)->trans->lineNb);
					STOP_ERROR(msg, s_->features, stackOuter, NULL, NULL);

					error = 1;
					stateDestroy(s_, false);
					s_ = NULL;

				} else {
#ifdef CLOCK
					byte pushS_ = 1;
					byte insertS_ = 1;
					byte destroyS_ = 1;
#endif
					prevS_ = NULL;
					new = NULL;
					resetExclusivity = false;
					if(!htVisitedStatesFind(s_->payloadHash, s_, DFS_OUTER, &prevS_)) {
						new = createStackElement(s_, _nbErrors);
						new->E = executables(globalSymTab, mtypes, s_, 1, _nbErrors, &hasDeadlock, NULL, NULL, &noOutgoing, &resetExclusivity);
						new->E_save = new->E;
						if(resetExclusivity) {
							stateSetValue(s_->payload, OFFSET_EXCLUSIVE, T_BYTE, NO_PROCESS);
							s_->payloadHash = hashState(s_);
							DEBUG_PRINT_d("         - lost exclusivity, state became %u\n", s_->payloadHash);

							prevS_ = NULL;

							if(htVisitedStatesFind(s_->payloadHash, s_, DFS_OUTER, &prevS_)) {
								new->state = NULL;
								destroyStackElement(new, processTrans);
								new = NULL;
							}
						}
					}
					if(!new) {
						DEBUG_PRINT("         - state already visited.\n");
						stateDestroy(s_, false);
						s_ = NULL;
						_nbStatesStops++;

					} else {
						stackOuter = push(stackOuter, new);
						_depth++;
						if(prevS_) {
#ifdef CLOCK
							byte freshS_ = 0;
							ptBoolFct freshProducts = NULL;
							prevS_->outerFed = addElement(prevS_->outerFed, s_->features, s_->zone, &freshS_, &freshProducts);
							s_->features = freshProducts;
							if(!freshS_) {
								pop(&stackOuter);
								_depth--;
								stateDestroy(s_, false);
								new->state = NULL;
								destroyStackElement(new, processTrans);
								new = NULL;
							}
							else {
								_nbStatesReExplored++;
								free(s_->payload);
								s_->payload = prevS_->payload;
								STATS;
							}
#else
							DEBUG_PRINT("         - state visited but features fresh, pushing on stack.\n");
							ptBoolFct negPrev = negateBool(prevS_->outerFeatures);
							if(s_->features == NULL) {
								destroyBool(prevS_->outerFeatures);
								prevS_->outerFeatures = getTrue();
							} else {
								prevS_->outerFeatures = addDisjunction(prevS_->outerFeatures, s_->features, 0, 1);
#ifdef CHECK_TAUTOLOGY
								if(prevS_->outerFeatures && isTautology(prevS_->outerFeatures)) {
									destroyBool(prevS_->outerFeatures);
									prevS_->outerFeatures = getTrue();
								}
#endif
							}
							s_->features = addConjunction(negPrev, s_->features, 0, 0);
							free(s_->payload);
							s_->payload = prevS_->payload;

							_nbStatesReExplored++;
							STATS;
#endif
						} else {
							DEBUG_PRINT("         - state fresh, pushing on stack.\n");

							// The state was not visited at all
#ifdef DEBUGSTATE
							printf("Inserting state:\n");
							printState(s_, NULL);
#endif
							htVisitedStatesInsert(s_->payloadHash, s_, DFS_OUTER);
							_nbStatesExplored++;
							STATS;
						}

						if(new && hasDeadlock) {
							STOP_ERROR("Found deadlock", s_->features, stackOuter, NULL, NULL);
							error = 1;
							pop(&stackOuter);
							_depth--;
							stateDestroy(new->state, false);
							new->state = NULL;
							destroyStackElement(new, processTrans);

						} else if(new && !new->E){
							// Even without deadlock, the list of transitions might be empty; account for that:
							pop(&stackOuter);
							_depth--;
							stateDestroy(new->state, false);
							new->state = NULL;
							destroyStackElement(new, processTrans);
						}

						if(new && fullDeadlockCheck && noOutgoing) {
							if(isSatisfiableWrtFD(noOutgoing)) {
								STOP_ERROR("Found trivially invalid end state; the following set of products can reach the state, but has no outgoing transition.", noOutgoing, stackOuter, NULL, NULL);
								error = 1;
							}
							destroyBool(noOutgoing);
							noOutgoing = NULL;
						}

					} // fresh state
				} // no assert violation

				// Next transition
				current->E = current->E->next;

			} // fire transition
		} // explore state
	} // end while

	if(error) STOP_ERROR_GLOBAL;

	destroyStackElementStack(stackOuter, processTrans);
	return error;
}
