/*
 * finishing-times-tree.c
 *
 *  Created on: Sep 2, 2015
 *      Author: rafaelolaechea
 */
#include "symbolic-f-times.h"
#include "finishing-times-adt.h"


static ptSymbolicNode treeRoot_ = NULL;

void setSymbolicTreeRoot(ptSymbolicNode T){
	treeRoot_ = T;
}

ptSymbolicNode getSymbolicTreeRoot(){
	return treeRoot_;
}

void computeSymbolicFinishingTree(){


	ptQueue Q = createEmptyQueue();
	int domainSymbolicF = getSizeFinishingTime();

	if(statsLA){
		printf(" # ( Statex x Feature Expression) == Ftimes = %d \n ", domainSymbolicF);
	}
	ptSymbolicNode T = createRootSymbolicFTree();

	Q = pushQueue(Q, T);

	setMaxMappingNode(T, domainSymbolicF+1);

	ptState FEltExampleState = NULL;
	ptBoolFct FEltExampleStateFeatures = NULL;

	bool atRootLevel = true;
	while(!isEmptyQueue(Q)){
		ptSymbolicNode CurrentNode = (ptSymbolicNode) popQueue(&Q);
		int max = getMaxMappingNode(CurrentNode);

//		printf("will obtain FeatureExpressionFromRoot, max=%d \n", max);
		ptBoolFct CurrentFeatureExpression = FeatureExpressionFromRoot(CurrentNode);


		ptBoolFct notChildren = getTrue();

		int j = max-1;


		while(j > 0){
			FEltExampleState = NULL;
			FEltExampleStateFeatures = NULL;


			getStateAndFexpressionFInverse(j, &FEltExampleState, &FEltExampleStateFeatures);


			//printf("Received Inverse for F(j)=%d \n ", j);
			if ( isSatisfiable(addConjunction(FEltExampleStateFeatures,
					addConjunction(notChildren,  CurrentFeatureExpression , 1, 1), 1 , 1))){
		//		printf("Adding Node with j=%d \n", j);
				ptSymbolicNode NewNode = createFTreeNode(CurrentNode,
						FEltExampleState,
						addConjunction(FEltExampleStateFeatures,
								notChildren, 1, 1));
				if(atRootLevel==true){
			//		printf("\t\t\t Adding node from root \n");
				}
				setMaxMappingNode(NewNode, j);

				Q = pushQueue(Q, NewNode);
				notChildren = addConjunction(notChildren,
						negateBool(FEltExampleStateFeatures), 0, 0);

				if(optimizeAlg1 && !isSatisfiable(addConjunction(notChildren,  CurrentFeatureExpression , 1, 1))){
					break;
				}
			}
			j = j -1;
 		}

		atRootLevel = false;

	}

	setSymbolicTreeRoot(T);
	/* T is root of a an F-Tree holding all possible finishing nodes */
}



ptSymbolicNode getNextChildren(ptSymbolicNode root, ptEdgeListSymbolicNode currentChildList){
	if(currentChildList != NULL){
		return currentChildList->destination ;
	} else {
		failure("Called getNextChildren when no more children where available.");
	}

}

bool hasMoreChildren(ptSymbolicNode root, ptEdgeListSymbolicNode currentChildList){
	if(currentChildList == NULL){
		return false;
	} else {
		return true;
	}
}

