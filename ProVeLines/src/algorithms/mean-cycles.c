/*
 * mean-cycles.c
 *
 *  Created on: Sep 28, 2015
 *      Author: rafaelolaechea
 */

#include <stdio.h>
#include <stdlib.h>
#include "list.h"
#include "../lib/clark.hashtable/hashtable.h"
#include "../lib/clark.hashtable/hashtable_private.h"
#include "../wrapper/hash/hashtableState.h"
#include "main.h"
#include "finishing-times-tree.h"
#include "finishing-times-adt.h"
#include "symbolic-scc.h"
#include "scc-debugging.h"
#include "scc-analysis.h"
#include "transpose-executables.h"
#include "abstractTransitionsMapping.h"
#include "mean-cycles.h"
#include "mean-cycle-structure.h"
#include "mean-cycles-tree.h"


/*
 *
 * getNewDStructure
 * 	Creates a new element of structure D.
 *
 */
//void * getNewDStructure(){
//
//}

void printSCCMeanCycle(ptList CStructure);
void combineDVFAndW(ptList *ptDomainOfCurrentStateList,
		ptPairFeatureWeight vFandW,
		ptDSymbolicVals D,
		int k ,
		unsigned int tmpKey,
		ptHtState currentState);
void combineMByW(ptList *ptDomainOfMV,
		ptPairFeatureWeight mvFandW,
		ptMSymbolicValsCollector M,
		unsigned int tmpKey,
		ptHtState s);
void combineCByW(ptList *CList,
		ptPairFeatureWeight cFandW);



void computeMeanCycle(ptMTypeNode mtypes){
	// D[k, v, R(v)	]
	// --
	// for each s in SCC

	//printf("\n Before printing SCC will print complete Graph \n");
	if (!silent)
		printTransposeAndLinks(mtypes);
	//printf("\n Printed Complete Graph \n");


//	printf("-- Initializing D[0, v, R(v)] for each SCC. \n");


	ptSymbolicNode rootT = getSymbolicTreeRoot();
	ptEdgeListSymbolicNode currentRootChildren = rootT->ListOfOutgoingEdges ;


	int indexOrder = 0;
	int levelOrder = 0;

	if(traceSCC){
		printf("# Items in SCC, Order, Time (milliseconds) \n ");
	}


	while(hasMoreChildren(rootT, currentRootChildren)){
	//	printf(" At start of hasMoreChildren \n");
		ptSymbolicNode u = getNextChildren(rootT, currentRootChildren);

		ptStack NodesToExploreStack = getEmptyStack();


		ptTripletTuple RootChildTriplet = createTripletTuple(u,  u->state,  u->ToFatherfeatures);

		NodesToExploreStack = push(NodesToExploreStack, RootChildTriplet);

		bool directlyFromRoot  = true;

		while(! empty(NodesToExploreStack)){

			ptTripletTuple currentNode = (ptTripletTuple) pop(&NodesToExploreStack);

			struct hashtable * sccComponent = getSymbolicComponent(currentNode->TreeNode);
			ptAbstractSCCTree sccComponentTree = getSymbolicComponentTree(currentNode->TreeNode);

			if(currentNode->TreeNode->hasSymbolicSccComponent == true){
				indexOrder++;

//				printf("Initial State = %u, Features = ",  currentNode->TreeNode->state->payloadHash);
//				printBool(currentNode->edgeLabel);
//				printf("\n\n ");
//				printState(mtypes, currentNode->TreeNode->state, NULL);
//				printf(" -- End Initial State \n ");
//				printSCCAllStatesAndFeatureExpression(sccComponent);




				struct timeval startSCC, endSCC;

				if(traceSCC)
					gettimeofday(&startSCC, NULL);


				if(excludeSCC ){
					if(sccComponent->entrycount > excludeSCCLessThanEqual){


					}
				}else {

//					printf("# items SCC = %d, Boolean Feature Expression = ", sccComponent->entrycount);
//					ptHtState foundInitialStateForFeatureExpression;
//					if(htFindByKeyAndPayloadNoFeatureCheck(currentNode->TreeNode->state->payloadHash, currentNode->TreeNode->state->payload,
//							currentNode->TreeNode->state->payloadSize,  &foundInitialStateForFeatureExpression,
//							sccComponent)){
//
//						printBool(foundInitialStateForFeatureExpression->outerFeatures);
//						printf(" . \n");
//
//					} else {
//						printf(" noState in Table .\n");
//					}


					//computeSCCLimitAverage(sccComponent, currentNode->TreeNode->state);

					computeAllPossibleLimitAverage(sccComponentTree, currentNode->TreeNode->state);
				}

				if(traceSCC)
						gettimeofday(&endSCC, NULL);

				if(traceSCC){
					printf("%d, %d, %ld  \n", sccComponent->entrycount, indexOrder,
							((endSCC.tv_sec * 1000000 + endSCC.tv_usec)
													- (startSCC.tv_sec * 1000000 + startSCC.tv_usec))/1000);
				}


				//printf("# Items in SCC, %d \n", sccComponent->entrycount);
//				if(sccComponent->entrycount > 200){
//					printf("# Items in SCC, %d \n", sccComponent->entrycount);
//
//					printf("Finished # Items in SCC, %d \n", sccComponent->entrycount);
//				}

			}

			directlyFromRoot =false;


			ptEdgeListSymbolicNode children  = currentNode->TreeNode->ListOfOutgoingEdges;

			while(children != NULL){

				ptTripletTuple nextTriplet = createTripletTuple(children->destination,
											children->destination->state,
											addConjunction(currentNode->edgeLabel,
													children->destination->ToFatherfeatures, 1,1));

				NodesToExploreStack = push(NodesToExploreStack, nextTriplet);

				children = children->next;
			}
		}
		currentRootChildren = currentRootChildren->next;
	}
}




ptDSymbolicVals createAndInitializeDStructure(struct hashtable * sccComponent, ptState initialState){
	int numberOfItems =  sccComponent->entrycount;

	ptDSymbolicVals  D = createNewDStructure(numberOfItems);

		ptHtIteratorState elementPointerInTable = getEmptyHashtableIterator();

		ptHtState s = NULL;
		unsigned int tmpKey ;

		/* Start Initializing */

		while(tableHasMoreElements(sccComponent, elementPointerInTable, &s, &tmpKey)){

			int k = 0;
			for(k = 0; k<= numberOfItems; k++){
				setAndCreateElementDSingleFExpToInfinity(D, k, tmpKey, s, s->outerFeatures);
			}
		}
		setElementDSingleFExpToZero(D, 0,	initialState->payloadHash, initialState);
		/* End start of Initializing */

		return D;

}

void computeSCCLimitAverage(struct hashtable * sccComponent, ptState initialState){

	//	printf("At computeSCCLimitAverage \n");
	int numberOfItems =  sccComponent->entrycount;



	ptDSymbolicVals  D = createAndInitializeDStructure(sccComponent, initialState);


	/* start of Body */
	int k = 1;
	for (k = 1;  k <= numberOfItems; k++){
		ptHtState currentState = NULL;
		unsigned int tmpKey ;
		ptHtIteratorState elementPointerInTable = getEmptyHashtableIterator();
		while(tableHasMoreElements(sccComponent, elementPointerInTable, &currentState, &tmpKey)){

			ptList incomingLinks =   partialOrderReduction ?  getAbstractTransposeLinksFromKeyAndPayload(tmpKey,
						currentState->payload, currentState->payloadSize ) :
					getTransposeLinksFromKeyAndPayload(tmpKey,
						currentState->payload, currentState->payloadSize )  ;

			while(incomingLinks != NULL){
				// For each element in tranpose link s, u
				// equivalent to all incoming links to u.

				ptTransAndFExp outgoingLink = (ptTransAndFExp) incomingLinks->value;

				ptHtState fromState = NULL;

				// Check if outgoingLink->State is an element of sccComponent .
				if( htFindNoFeatureCheck(outgoingLink->State->payloadHash,
						outgoingLink->State,
						&fromState,
						sccComponent)){
					if(fromState != NULL){

						ptList ptDomainOfCurrentStateList = getFeatureExpressionDomainAndImage(D,
									k,
									tmpKey,
									currentState->payload,
									currentState->payloadSize );


						ptList ptDomainOfUList = getFeatureExpressionDomainAndImage(D,
									k-1,
									outgoingLink->State->payloadHash,
									outgoingLink->State->payload,
									outgoingLink->State->payloadSize );


						/* Cross Multiply */

						ptList iterateDV = ptDomainOfCurrentStateList;

						while(iterateDV != NULL){
							ptPairFeatureWeight vFandW = (ptPairFeatureWeight) iterateDV->value;
							ptList iterateDU = ptDomainOfUList;
							while(iterateDU != NULL){
								ptPairFeatureWeight uFandW = (ptPairFeatureWeight) iterateDU->value;

								ptBoolFct doubleConjunction = addConjunction(vFandW->FExpression,uFandW->FExpression,1,1);

								ptBoolFct tripleConjunction = addConjunction(outgoingLink->features,
										doubleConjunction,1,1);


								if(isSatisfiable(tripleConjunction)){
									if(isGreaterLeftDAndWThanRightD(uFandW, outgoingLink->Weight, vFandW)){

										int leftSum = uFandW->W->number +  outgoingLink->Weight;

										if(implies(vFandW->FExpression, addConjunction(outgoingLink->features,	uFandW->FExpression
												,1,1))){

											vFandW->W->isMinusInfinity = false;
											vFandW->W->isPlusInfinity = false;
											vFandW->W->number = leftSum;

											/* Combining ... */
											if(combineFeatureExpressions)
												combineDVFAndW(&ptDomainOfCurrentStateList, vFandW, D, k, tmpKey, currentState );
											// END Combining


										} else {
											/* Performing Update */
											ptBoolFct D2AndD1AndD3 = tripleConjunction;
											ptBoolFct D2AndNotPD1AndD3P = addConjunction(negateBool(addConjunction(outgoingLink->features,
																								uFandW->FExpression
																								,1,1)),
																								vFandW->FExpression, 1,1);

											// Update VFAndFW = D(k, v, d1 ^ d2 ^ d3) <-- D(k-1, u, d3)
											// And Undef D(k, v,  d2)

											bool previousIsMinusInfinity = vFandW->W->isMinusInfinity ;
											bool previousIsPlusInfinity		  = vFandW->W->isPlusInfinity;
											int  previousW = 		vFandW->W->number;

											vFandW->FExpression = tripleConjunction;
											vFandW->W->isMinusInfinity = false;
											vFandW->W->isPlusInfinity = false;
											vFandW->W->number = leftSum;


											// Update D(k, v, d1 ^ ~( d2 ^ d3)) <-- D(k, v,  d2)
											ptPairFeatureWeight newSplittedVFAndW = constructPairFeatureWeight(D2AndNotPD1AndD3P,
													previousIsMinusInfinity, previousIsPlusInfinity, previousW);


											// START Combining.
											if(combineFeatureExpressions)
												combineDVFAndW(&ptDomainOfCurrentStateList, vFandW, D, k, tmpKey, currentState);
											// END Combining

											// Insert in front of current item (such that we also consider in future checks)

											listAddAfterCurrentItemNonEmptyList(iterateDV,  newSplittedVFAndW);

										} // End-if implies(vFandW->FExpression, addConjunction(outgoingLink->features,	uFandW->FExpression,1,1))

									} // End-if isGreaterLeftDAndWThanRightD(uFandW, outgoingLink->Weight, vFandW)

								} // end-if isSatisfiable(tripleConjunction)

								iterateDU = iterateDU->next;
							} // End-While( iterateDU != NULL)
							iterateDV = iterateDV->next;
						} // End-While( iterateDV != NULL)


					} else {
						failure("htFindNoFeatureCheck returns true but a null state, key used %u \n ", outgoingLink->State->payloadHash);
					}
				} else {
//					printf(" \t Link outside SCC \n");
				}

				incomingLinks = incomingLinks->next;
			}
		}

//		printf("Finished k = %d, will print k \n  ", k);
//		printDStructure(sccComponent, D, k);
//		printf("Finished printing k = %d \n", k);

	}// endfor (k = 1;  k <= numberOfItems; k++)
	/* end of Body */


	// Tail of Algorithm
	ptMSymbolicValsCollector  M = createNewMStructure(numberOfItems);
	ptHtState foundInitialState = NULL;

	ptList CStructure = NULL;
	if(htFindByKeyAndPayloadNoFeatureCheck(initialState->payloadHash, initialState->payload,
			initialState->payloadSize,  &foundInitialState,
			sccComponent)){

		CStructure = listAdd(NULL, createNegativeInfinityAndFexpPair(foundInitialState->outerFeatures));

	}else {
		failure (" Couldn't found initial state of an SCC in its table \n");
	}



	ptHtIteratorState elementPointerInTable = getEmptyHashtableIterator();
	ptHtState s = NULL;
	unsigned int tmpKey  = 0;

	while(tableHasMoreElements(sccComponent, elementPointerInTable, &s, &tmpKey)){
		setAndCreateElementMSingleFExpToPlusInfinity(M, tmpKey, s, s->outerFeatures);
		k =  0;

//		printf(" Start of Computing M(%u, f), using  k = 0 to %d   \n", tmpKey, (numberOfItems-1));
		for (k = 0; k < numberOfItems; k++ ){
			ptList ptDomainOfMV = getFeatureExpressionDomainAndImageFromM(M,
						tmpKey,
						s->payload,
						s->payloadSize );

			ptList ptDomainOfDVNList= getFeatureExpressionDomainAndImage(D,
						numberOfItems,
						tmpKey,
						s->payload,
						s->payloadSize );

			ptList ptDomainOfDVKList= getFeatureExpressionDomainAndImage(D,
						k,
						tmpKey,
						s->payload,
						s->payloadSize );



			//ptList iterateDU = ptDomainOfUList;
		//	while(iterateDU != NULL){
		//		ptPairFeatureWeight uFandW = (ptPairFeatureWeight) iterateDU->value;

			// Triple Cross Multiplication ...

			ptList iterateMV = ptDomainOfMV;

			while(iterateMV != NULL){
				ptList iterateDNV = ptDomainOfDVNList;

				ptPairFeatureWeight mvFandW = (ptPairFeatureWeight) iterateMV->value;

				while(iterateDNV != NULL){

					ptPairFeatureWeight dnvFandW = (ptPairFeatureWeight) iterateDNV->value;

					ptList iterateDKV = ptDomainOfDVKList;

					while(iterateDKV != NULL){

						ptPairFeatureWeight dkvFandW = (ptPairFeatureWeight) iterateDKV->value;

						ptBoolFct tripleConjunction = addConjunction(mvFandW->FExpression,
								addConjunction(dnvFandW->FExpression,
										dkvFandW->FExpression,
										1,1),
								1,1);

						if(isSatisfiable(tripleConjunction)){
							if(isGreaterLeftMThanDMinusDByNMinusK(mvFandW, dnvFandW,  dkvFandW, numberOfItems, k)){

								tNumberAndInfinity rightExpression = calculateDMinusDByNMinusK(dnvFandW,  dkvFandW, numberOfItems, k);

								if(implies(mvFandW->FExpression, addConjunction(dnvFandW->FExpression,
										dkvFandW->FExpression
										,1,1))){

//									printf("M --- ; d2 ^ d3 ^ d1 == d2  \n ");

									*(mvFandW->W) = rightExpression;

									// and combine by W.
								//	ptList *ptDomainOfCurrentStateList,
								//			ptPairFeatureWeight vFandW,
//											ptDSymbolicVals D,
//											unsigned int tmpKey,
//											ptHtState currentState

									if(combineFeatureExpressions)
										combineMByW(&ptDomainOfMV,
												mvFandW,
												M,
												tmpKey,
												s);



//									printf("Updating W, for ");
//									printBool(mvFandW->FExpression);
//									printf(" to ");
//									if(mvFandW->W->isMinusInfinity){
//										printf(" -Inf");
//									}else if (mvFandW->W->isPlusInfinity){
//										printf(" +Inf");
//									}else {
//										printf(" %f", mvFandW->W->numberF);
//									}
//									printf("\n");
								}else {
								// Split M

								//	printf(" Splitting M \n");
									/* Performing Update */

									ptBoolFct mvANDdnvANDdkv = tripleConjunction;

									ptBoolFct mvANDNOTPdnvANDdkvP = addConjunction(mvFandW->FExpression,
													negateBool(addConjunction(dnvFandW->FExpression,
																				dkvFandW->FExpression
																						,1,1)),
																						 1,1);

									// Update VFAndFW = D(k, v, d1 ^ d2 ^ d3) <-- D(k-1, u, d3)
									// And Undef D(k, v,  d2)

									bool previousIsMinusInfinity = mvFandW->W->isMinusInfinity ;
									bool previousIsPlusInfinity		  = mvFandW->W->isPlusInfinity;
									int  previousW = 		mvFandW->W->number;
									double previousWF = 	mvFandW->W->numberF;


									mvFandW->FExpression = tripleConjunction;
									*(mvFandW->W) =  rightExpression;


									// Update D(k, v, d1 ^ ~( d2 ^ d3)) <-- D(k, v,  d2)
									ptPairFeatureWeight newSplittedMVFAndW = constructPairFeatureWeightM(mvANDNOTPdnvANDdkvP,
											previousIsMinusInfinity, previousIsPlusInfinity, previousW, previousWF);

									if(combineFeatureExpressions)
										combineMByW(&ptDomainOfMV,
												mvFandW,
												M,
												tmpKey,
												s);
									// Insert in front of current item (such that we also consider in future checks)


									listAddAfterCurrentItemNonEmptyList(iterateMV,  newSplittedMVFAndW);


								}
							}
						}// End-While
						iterateDKV = iterateDKV->next;
					}// End-While
					iterateDNV =  iterateDNV->next;
				}// End-While
				iterateMV = iterateMV->next;
			} // End-While

//			printf("Finished Computing M at k = %d\n", k);

		} // End for (k = 0 ... )

//		printf("Finished Computing M(%u, f) with all k \n", tmpKey);
//		printf("Printing M(v)\n");
//		printMStructureForStateByKeyAndPayload(M, tmpKey, s->payload, s->payloadSize);
//		printf("End Printing M(v)\n");

//		printf(" Printing C(f) up to now \n");
//		ptList tmpC = CStructure;
//		while(tmpC != NULL){
//
//			ptPairFeatureWeight cFandW = (ptPairFeatureWeight) tmpC->value;
//
//			printBool(cFandW->FExpression);
//			if(cFandW->W->isMinusInfinity){
//				printf(" -Inf");
//			}else if (cFandW->W->isPlusInfinity){
//				printf(" +Inf");
//			}else {
//				printf(" %f", cFandW->W->numberF);
//			}
//			printf("\n");
//			tmpC = tmpC->next;
//		}
//		printf("End Printing C\n");

		// Compute C
		// Cross Multiply

//		printf("Will Update C");
		ptList iterateC = CStructure;

		while(iterateC != NULL){

			ptPairFeatureWeight cFandW = (ptPairFeatureWeight) iterateC->value;

			ptList ptDomainOfMVComputed = getFeatureExpressionDomainAndImageFromM(M,
						tmpKey,
						s->payload,
						s->payloadSize );

			ptList iterateMVComputed = ptDomainOfMVComputed;

			while(iterateMVComputed != NULL){

				ptPairFeatureWeight mvFandW = (ptPairFeatureWeight) iterateMVComputed->value;


				if(isSatisfiable(addConjunction(cFandW->FExpression,
						mvFandW->FExpression,
						1,1))){
					if (isLessCThanM(cFandW, mvFandW)){
						if(implies(cFandW->FExpression, mvFandW->FExpression)){

//							printf("Updating C\n");

							cFandW->W->isMinusInfinity = mvFandW->W->isMinusInfinity;
							cFandW->W->isPlusInfinity = mvFandW->W->isPlusInfinity;
							cFandW->W->numberF =	mvFandW->W->numberF;

							if(combineFeatureExpressions)
								combineCByW(&CStructure, cFandW);

						} else {
//							printf("Splitting C\n");
							/* Performing Update */

//							printf("M(v) is currently: ");
//							printBool(mvFandW->FExpression);
//							printf(" , ");
//							printWFloat(mvFandW->W);


							ptBoolFct cAndMv = addConjunction(cFandW->FExpression,
									mvFandW->FExpression,
									1,1);

							ptBoolFct cAndNotMv = addConjunction(cFandW->FExpression,
									negateBool(mvFandW->FExpression),
									1,1);


							bool previousIsMinusInfinity = cFandW->W->isMinusInfinity ;
							bool previousIsPlusInfinity		  = cFandW->W->isPlusInfinity;
							int  previousW = 		cFandW->W->number;
							double previousWF = 	cFandW->W->numberF;


							// Updating  C(d1^d2) in-place
							cFandW->FExpression = cAndMv;
							cFandW->W->isMinusInfinity = mvFandW->W->isMinusInfinity;
							cFandW->W->isPlusInfinity = mvFandW->W->isPlusInfinity;
							cFandW->W->numberF =	mvFandW->W->numberF;

//							printf(" New values become \n");
//							printBool(cFandW->FExpression);
//							printf(" , ");
//							printWFloat(cFandW->W);
//							printf("\n");



							// Adding  C(d1^~d2) after C(d1^d2).
							ptPairFeatureWeight newSpittedPair = constructPairFeatureWeightM(cAndNotMv,
									previousIsMinusInfinity, previousIsPlusInfinity, previousW, previousWF);

							if(combineFeatureExpressions)
								combineCByW(&CStructure, cFandW);

//							printf(" And \n");
//							printBool(newSpittedPair->FExpression);
//							printf(" , ");
//							printWFloat(newSpittedPair->W);
//							printf("\n");

							// Insert in front of current item (such that we also consider in future checks)
							listAddAfterCurrentItemNonEmptyList(iterateC,  newSpittedPair);
						}// end-if IMplies
					} // End-if is greater
				} // End-if is satisfiable

				iterateMVComputed = iterateMVComputed->next;

			}

			iterateC = iterateC->next;
		}


//		printf("\n After Updating  C(f)  \n");
//		ptList TtmpC = CStructure;
//		while(TtmpC != NULL){
//
//			ptPairFeatureWeight cFandW = (ptPairFeatureWeight) TtmpC->value;
//
//			printBool(cFandW->FExpression);
//			if(cFandW->W->isMinusInfinity){
//				printf(" -Inf");
//			}else if (cFandW->W->isPlusInfinity){
//				printf(" +Inf");
//			}else {
//				printf(" %f", cFandW->W->numberF);
//			}
//			printf("\n");
//			TtmpC = TtmpC->next;
//		}
//		printf("End Printing Updated C\n");



	}// End	while(tableHasMoreElements(sccComponent, elementPointerInTable, &s, &tmpKey)){
	// End Computing M/C


	if(!silent)
		printSCCMeanCycle(CStructure);
}


void printSCCMeanCycle(ptList CStructure){
	printf("Computed an SCC - Symbolic Max-Mean Cycle -- will print \n");
	ptList iterateCValues  = CStructure;
	printf(" Feature Expression, value \n");
	while(iterateCValues != NULL){
		ptPairFeatureWeight cPair = (ptPairFeatureWeight) iterateCValues->value;

		printBool(cPair->FExpression);
		printf(", ");

		if(cPair->W->isMinusInfinity){
			printf(" -Inf\n");
		}else if (cPair->W->isPlusInfinity){
			printf(" +Inf\n");
		} else {
			printf(" %f\n", cPair->W->numberF);
		}
		iterateCValues = iterateCValues->next;
	}
	printf("Computed an SCC - Symbolic Max-Mean Cycle -- printed \n");
}



//						printf("For transition (u,v)  from %u  to %u with W=%d, d1 = ", outgoingLink->State->payloadHash,
//								tmpKey,
//								outgoingLink->Weight );
//						printBool(outgoingLink->features);
//						printf(" ;  |D(k, v, -)| = %d , |D(k, u, -)| = %d \n",
//								listCount(ptDomainOfCurrentStateList),
//								listCount(ptDomainOfUList));
//								printf("Checking \t d2 = ");
//								printBool(vFandW->FExpression);
//								printf(" d3 = ");
//								printBool(uFandW->FExpression);

//								printf("; Checking Expression ");
//								printBool(tripleConjunction);

//								printf("  ;");




///*
// *
// * We compute the SCC limit average for a single symbolic SCC component.
// */
//void computeSCCLimitAverage(struct hashtable * sccComponent){
//
//
//
//
//	// D[k, v, R(v)]
//
//
//
//	/* --- Initialize Empty Pair */
//
//	int N = countStates(sccComponent);
//	void * D = getNewDStructure(N);
//
//	int i = 0;
//	for (i =0;i< N; i++){
//		// Go through each state of sccComponent
//		void *s = getNextState(i, sccComponent);
//
//		/* Iterate through each element*/
//
//		void *fExpAndNumberPair = getPairOrCreateEmptyPair(D, i, s);
//
//
//		if(! isPairEmpty(fExpAndNumberPair)){
//			printf("Error - should be empty");
//		}
//
//		AddNumberFeatureExpression(fExpAndNumberPair, getFeatureExpression(s), getPlustInfinity());
//	}
//
//	/* End Initialize Empty Pair */
//
//	// For each element payloadHash, payload  s in sccComponent ...
//	//	setDStructure(D, 0, s,  R(s));
//	// ----
//	// ----
//
//}


void combineDVFAndW(ptList *ptDomainOfCurrentStateList,
		ptPairFeatureWeight vFandW,
		ptDSymbolicVals D,
		int k ,
		unsigned int tmpKey,
		ptHtState currentState){


	ptList listOfDVDomainIterator = *ptDomainOfCurrentStateList;

	while(listOfDVDomainIterator != NULL){
		ptPairFeatureWeight toCompareTo = (ptPairFeatureWeight) listOfDVDomainIterator->value;


		if(toCompareTo !=   vFandW  &&
				areAugmentedWEquivalent(toCompareTo->W, vFandW->W)	){
			// Replace element.
			vFandW->FExpression = addDisjunction(toCompareTo->FExpression, vFandW->FExpression, 1, 1);

			if( listOfDVDomainIterator->prev != NULL){
				// aka not the first one .
				// just remove it  A -->  toCompareTo --> B
				// transformed into A --> B
				if(listOfDVDomainIterator->next != NULL){
					(listOfDVDomainIterator->next)->prev = listOfDVDomainIterator->prev;
				}
				(listOfDVDomainIterator->prev)->next =  listOfDVDomainIterator->next;
				break;
			} else {
				// Now have to remove toCompareTo as it subsumed by current vFandW .

				// toCompareTo --> B (size at least == 2 given toCompareTo !=   vFandW )
				// Convert to to B  ...
				(listOfDVDomainIterator->next)->prev = NULL;

				setFeatureExpressionDomainAndImage(D,
													k,
													tmpKey,
													listOfDVDomainIterator->next,
													currentState->payload,
													currentState->payloadSize );
				*ptDomainOfCurrentStateList = listOfDVDomainIterator->next;
				break;
			}

			;
		}
		listOfDVDomainIterator = listOfDVDomainIterator->next;
	}
}




void combineMByW(ptList *ptDomainOfMV,
		ptPairFeatureWeight mvFandW,
		ptMSymbolicValsCollector M,
		unsigned int tmpKey,
		ptHtState s){

	ptList listOfDVDomainIterator = *ptDomainOfMV;

	while(listOfDVDomainIterator != NULL){
		ptPairFeatureWeight toCompareTo = (ptPairFeatureWeight) listOfDVDomainIterator->value;


		if(toCompareTo !=   mvFandW  &&
				areAugmentedWEquivalentByFloat(toCompareTo->W, mvFandW->W)	){
			// Replace element.
			mvFandW->FExpression = addDisjunction(toCompareTo->FExpression, mvFandW->FExpression, 1, 1);

			if( listOfDVDomainIterator->prev != NULL){
				// aka not the first one .
				// just remove it  A -->  toCompareTo --> B
				// transformed into A --> B
				if(listOfDVDomainIterator->next != NULL){
					(listOfDVDomainIterator->next)->prev = listOfDVDomainIterator->prev;
				}
				(listOfDVDomainIterator->prev)->next =  listOfDVDomainIterator->next;
				break;
			} else {
				// Now have to remove toCompareTo as it subsumed by current vFandW .

				// toCompareTo --> B (size at least == 2 given toCompareTo !=   vFandW )
				// Convert to to B  ...
				(listOfDVDomainIterator->next)->prev = NULL;



				setFeatureExpressionDomainAndImageFromM(M,
													tmpKey,
													listOfDVDomainIterator->next,
													s->payload,
													s->payloadSize );
				*ptDomainOfMV = listOfDVDomainIterator->next;
			}

			break;
		}
		listOfDVDomainIterator = listOfDVDomainIterator->next;
	}
}



void combineCByW(ptList *CList,
		ptPairFeatureWeight cFandW){


	ptList listCIterator = *CList;

	while(listCIterator != NULL){
		ptPairFeatureWeight toCompareTo = (ptPairFeatureWeight) listCIterator->value;


		if(toCompareTo !=   cFandW  &&
				areAugmentedWEquivalentByFloat(toCompareTo->W, cFandW->W)	){
			// Replace element.
			cFandW->FExpression = addDisjunction(toCompareTo->FExpression, cFandW->FExpression, 1, 1);

			if( listCIterator->prev != NULL){
				// aka not the first one .
				// just remove it  A -->  toCompareTo --> B
				// transformed into A --> B
				if(listCIterator->next != NULL){
					(listCIterator->next)->prev = listCIterator->prev;
				}
				(listCIterator->prev)->next =  listCIterator->next;
				break;
			} else {
				// Now have to remove toCompareTo as it subsumed by current vFandW .

				// toCompareTo --> B (size at least == 2 given toCompareTo !=   vFandW )
				// Convert to to B  ...
				(listCIterator->next)->prev = NULL;

				*CList = listCIterator->next;
			}

			break;
		}
		listCIterator = listCIterator->next;
	}


}


