/*
 * abstractFTS.h
 *
 *  Created on: Dec 13, 2015
 *      Author: rafaelolaechea
 */

#ifndef ALGORITHMS_ABSTRACTFTS_H_
#define ALGORITHMS_ABSTRACTFTS_H_

#include <stdio.h>
#include <stdlib.h>
#include "list.h"
#include "../lib/clark.hashtable/hashtable.h"
#include "../lib/clark.hashtable/hashtable_private.h"
#include "../wrapper/hash/hashtableState.h"
#include "main.h"
#include "finishing-times-tree.h"
#include "finishing-times-adt.h"
#include "symbolic-scc.h"
#include "scc-debugging.h"
#include "scc-analysis.h"
#include "transpose-executables.h"
#include "mean-cycles.h"
#include "mean-cycle-structure.h"


struct bfsEltTranspose {
	ptState state;
	ptBoolFct features;
	ptList OutgoingLinksTranspose;
	ptList OutgoingLinksTransposeCurrent;
	int level;
	int accumulatedW;
	ptBoolFct accumulatedF;
};

typedef struct bfsEltTranspose tBfsEltTranspose;
typedef struct bfsEltTranspose * ptBfsEltTranspose;


bool isAbstractDInitializedForU(struct hashtable* DAbstractTransitions, ptHtDState* foundState, ptState State);
void computeAbstractTransitionsFromS(ptHtState s, struct hashtable* DAbstractTransitions, struct hashtable *htRelevantStates);
bool setAbstractTransitionValuesForVirginState(struct hashtable *relevantTable, ptState fromState, int Weight, ptBoolFct transitionFeatures,
		ptState ToState, bool fromStartRelevant);
bool htFindByKeyAndPayloadNoFeatureCheckToHTMaxWeightedR(tHtKey key, void * payload,
		unsigned int payloadSize,  ptHtDState* foundState,
		struct hashtable* _abstractTable);
void insertAbstractTransitionValuesUnvisitedState(struct hashtable *abstractTransitions, ptState ToState,
		int AccumulatedWeight, ptBoolFct accumulatedF );

ptBfsEltTranspose createBFSElement(ptState state, ptBoolFct features,
		int level,
		int AccumulatedW,
		ptBoolFct AccumulatedFeatures);

bool moreTransposeTransitionsAvailableBFS(ptBfsEltTranspose CurrentElt);

#endif /* ALGORITHMS_ABSTRACTFTS_H_ */
