

#ifndef CLOCK
// Automata-based model checking
void startNestedDFS(ptList props, ptSymTabNode globalSymTab, ptMTypeNode mtypes);
byte outerDFS(ptList props, ptSymTabNode globalSymTab, ptMTypeNode mtypes, ptStack stackOuter);
byte innerDFS(ptList props, ptSymTabNode globalSymTab, ptMTypeNode mtypes, ptStack stackOuter, ptStack stackInner);
#endif

// Exhaustive state-space exploration for assertion- and deadlock checking
void startExploration(ptList props, ptSymTabNode globalSymTab, ptMTypeNode mtypes);
byte explore(ptList props, ptSymTabNode globalSymTab, ptMTypeNode mtypes, ptStack stackOuter);
