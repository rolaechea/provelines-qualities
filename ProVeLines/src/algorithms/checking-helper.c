/*
 * checking-helper.c
 *
 *  Created on: Apr 10, 2015
 *      Author: rafaelolaechea
 */
#include <stdbool.h>
#include "automata.h"
#include "execution.h"

/* Checks if we have found a Saftety Property violated, represented by a neverNode of Null */
bool isSafetyPropertyViolationFound(ptFsmNode neverNode) {
	return !neverNode;
}


/* Checks whether executable transitions for a node have already been determined */
bool areExcutableTransitionsComputed(ptStackElt current){
	return  ! ( !current->E_save && !current->E_never_save);

}

/* Checks whether unexplored transitions remain in Current element of Stack
 * Precondition: executable transitions on Current are initialized.
 *  */
bool moreTransitionsAvailable(ptStackElt current){
	return current->E != NULL;
}


/** Initializes current element transitions. Also Updates local Variable E.
 *
 */
void initializeExecutableTransitions(ptStackElt current, ptList *E,
		ptSymTabNode globalSymTab, ptMTypeNode mtypes, unsigned int _nbErrors,
		byte *hasDeadlock, ptBoolFct *noOutgoing, byte *resetExclusivity){

	if(*E) {
		current->E = *E;
		*E = NULL;
	}
	else {
		current->E = executables(globalSymTab, mtypes, current->state, 1, _nbErrors, hasDeadlock, NULL, NULL, noOutgoing, resetExclusivity);
	}


	current->E_save = current->E;
	current->E_never = executablesNever(globalSymTab, mtypes, current->state);
	current->E_never_save = current->E_never;
}


/*
 * Check that an LTL Never clause has been set up, and that there exists outgoing
 * transitions from the initial state
 *
 */
void checkInitialization(ptState init){
	// Sanity checks
	if(!init->never)
		failure("init->never is NULL\n");


	ptFsmNode neverNode  = getNodePointer(init, init->never);
	if(!neverNode)
		failure("init->never->node is NULL\n");

	if(!neverNode->trans)
		failure("No transition leaving NL%02d\n", neverNode->lineNb);
}
