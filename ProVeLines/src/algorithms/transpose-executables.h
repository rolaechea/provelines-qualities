/*
 * transpose-executables.h
 *
 *  Created on: Sep 14, 2015
 *      Author: rafaelolaechea
 */

#ifndef ALGORITHMS_TRANSPOSE_EXECUTABLES_H_
#define ALGORITHMS_TRANSPOSE_EXECUTABLES_H_

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#include <string.h>
#include "error.h"
#include "main.h"
#include "list.h"
#include "stack.h"
#include "hashtableGen.h"
#include "boolFct.h"
#include "symbols.h"
#include "automata.h"
#include "SAT.h"
#include "tvl.h"
#ifdef CLOCK
	#include "clockZone.h"
#endif
#include "state.h"
#include "execution.h"


#include "../lib/clark.hashtable/hashtable.h"
#include "../wrapper/hash/hashtableState.h"

struct hashtable* getTransposeHasthable();
void initHTTranpose();
void computeTranposeGraph(ptSymTabNode globalSymTab, ptMTypeNode mtypes, ptState s);
ptList executablesNoConjunctsFeatures(ptSymTabNode globalSymTab, ptMTypeNode mtypes, ptState state,
		byte stutter, int nbErrors,
		byte * hasDeadlock,
		ptBoolFct* featuresOut,
		byte* allProductsOut,
		ptBoolFct* noOutgoing,
		byte* resetExclusivity);
ptList getTransposeLinks(ptState state);
ptList getTransposeLinksFromKeyAndPayload(tHtKey key, void * payload,
		unsigned int payloadSize);

#endif /* ALGORITHMS_TRANSPOSE_EXECUTABLES_H_ */
