/*
 * abstractFTS.c
 *
 *  Created on: Dec 13, 2015
 *      Author: rafaelolaechea
 */
#include <stdio.h>
#include <stdlib.h>
#include "list.h"
#include "../lib/clark.hashtable/hashtable.h"
#include "../lib/clark.hashtable/hashtable_private.h"
#include "../wrapper/hash/hashtableState.h"
#include "main.h"
#include "finishing-times-tree.h"
#include "finishing-times-adt.h"
#include "symbolic-scc.h"
#include "scc-debugging.h"
#include "scc-analysis.h"
#include "transpose-executables.h"
#include "mean-cycles.h"
#include "mean-cycle-structure.h"

#include "abstractFTS.h"



/*
 *
 * Computes abstract States for a given state.
 * Preconditions:
 * 	D is initialized to an empty hashtable.
 * 	S is a relevant state.
 *	htRelevantStates is a hashtable of relevant states
 *
 *	Algorithm
 *
 *		startEltQ =  MakeBFSNodeFromState(startState, 0)
 *		Q.push(startEltQ)
 *
 *	while(!isEmptyQueue(Q)){
 *		CNode = Q.Pop()
 *		while(hasMoreTransitions(CNode)){
 *
 *			outGoingLnk = CNode->GetCurrentLink
 *
 *
 *			if(isRelevantState(outGoingLnk->State)){
 *				if(isAbstractDInitializedForU(AbstractTranstions, outGoingLnk->State)){
 *					CrossMultiply()
 *				}else {
 *					InsertCurrentWeightAndFeature
 *				}
 *			} else {
 *				Add to Queue (if not explored to avoid loops)
 *			}
 *		}
 *	}
 *
 *
 */
void computeAbstractTransitionsFromS(ptHtState s, struct hashtable* DAbstractTransitions, struct hashtable *htRelevantStates){
	ptQueue Q = createEmptyQueue();

	ptBfsEltTranspose eltQ = createBFSElement(s->originalState, NULL, 0, 0, NULL);

	Q = pushQueue(Q, eltQ);

//	printf("Start Search \n");

	int i = 0;


	struct hashtable* htDAbstractTransitionsToNonRelevant = create_hashtableWrapper();

	while(!isEmptyQueue(Q)){

		i++;


		ptBfsEltTranspose  CurrentNode = (ptBfsEltTranspose) popQueue(&Q);

//		printf("Popping Element with level = %d\n", CurrentNode->level);

		while(moreTransposeTransitionsAvailableBFS(CurrentNode)){

			ptList OutgoingLinksTranspose = CurrentNode->OutgoingLinksTransposeCurrent;

			ptTransAndFExp outgoingLink = (ptTransAndFExp) OutgoingLinksTranspose->value;

			if(isRelevantState(outgoingLink->State)){
				ptHtDState retFoundState = NULL;

				if(isAbstractDInitializedForU(DAbstractTransitions, &retFoundState, outgoingLink->State)){
//					printf("Updating Relevant State \n");
					/***
					 *
					 * Add F, W as abstract transitions if it  doesn't exist in retFoundState list of pairs <f', w'>
					 *
					 */
					ptList fromStateWAndF = retFoundState->pairsOfWandF;

					ptBoolFct accmulatedF = CurrentNode->level == 0  ? outgoingLink->features :
							addConjunction(CurrentNode->accumulatedF, outgoingLink->features, 1, 1);

					const int accumulatedW = CurrentNode->level == 0  ? outgoingLink->Weight :
							(CurrentNode->accumulatedW + outgoingLink->Weight);

					bool foundWeight = false;
					while(fromStateWAndF != NULL){
						ptPairFeatureWeight ptFandW = (ptPairFeatureWeight) fromStateWAndF->value;

						if(accumulatedW == ptFandW->W->number){
							if(implies(accmulatedF, ptFandW->FExpression)){
								// Already included no need to or accmulatedF with ptFandW->FExpression
							} else {
								ptFandW->FExpression = addDisjunction(ptFandW->FExpression, accmulatedF, 1, 1);
							}
							foundWeight = true;
						}
						fromStateWAndF = fromStateWAndF->next;
					}

					if (!foundWeight) {
						ptPairFeatureWeight ptTmpFAndW = constructPairFeatureWeight(accmulatedF, false, false,
								accumulatedW);


						retFoundState->pairsOfWandF = listAdd(retFoundState->pairsOfWandF, ptTmpFAndW);
						retFoundState->numberOfPairs += 1 ;
					}
					/* End adding f, w as abstract transitions  */
				} else {
					//printf("Adding Relevant State \n");
					insertAbstractTransitionValuesUnvisitedState(DAbstractTransitions, outgoingLink->State,
											outgoingLink->Weight + CurrentNode->accumulatedW,
											CurrentNode->level == 0 ?  outgoingLink->features :
												addConjunction(CurrentNode->accumulatedF, outgoingLink->features, 1, 1));
				}

			} else {
				// Not relevant state -- Just add to Queue.

				//	printf("Adding Non-Relevant State to Queue \n");

				// Would add check to avoid  non-zero cycles here.
				// Check if eltNewNode exists in visited States.

				// If so then check if :
				//		- Current Weight is higher than all of the existing weights.
				// 		- Or if current weight i
				// If not then add to the Queue

				/*
				 *
				 * Have a list of non-relevant states
				 * 	List of  <F', W>  under which it has been explored
				 *
				 *	If has been explored with current W, check if F' --> implies ...
				 *
				 */

				ptHtDState retFoundStateNonRelevant = NULL;
				if(htFindByKeyAndPayloadNoFeatureCheckToHTMaxWeightedR(outgoingLink->State->payloadHash,
						outgoingLink->State->payload,
						outgoingLink->State->payloadSize,
						&retFoundStateNonRelevant,  htDAbstractTransitionsToNonRelevant)){
					// Adding visited BFS State

					ptList fromStateWAndF = retFoundStateNonRelevant->pairsOfWandF;

					ptBoolFct accmulatedF = CurrentNode->level == 0  ? outgoingLink->features :
							addConjunction(CurrentNode->accumulatedF, outgoingLink->features, 1, 1);

					const int accumulatedW = CurrentNode->level == 0  ? outgoingLink->Weight :
							(CurrentNode->accumulatedW + outgoingLink->Weight);

					bool addToQueue = false;
					bool foundWeight = false;
					bool updatedFExpression = false;

					while(fromStateWAndF != NULL){
						ptPairFeatureWeight ptFandW = (ptPairFeatureWeight) fromStateWAndF->value;

						if(accumulatedW == ptFandW->W->number){
							if(implies(accmulatedF, ptFandW->FExpression)){
								// Already included no need to or accmulatedF with ptFandW->FExpression
							} else {
								ptFandW->FExpression = addDisjunction(ptFandW->FExpression, accmulatedF, 1, 1);
								updatedFExpression = true;
							}
							foundWeight = true;
						}
						fromStateWAndF = fromStateWAndF->next;
					}

					if (!foundWeight) {
						ptPairFeatureWeight ptTmpFAndW = constructPairFeatureWeight(accmulatedF, false, false,
								accumulatedW);

						retFoundStateNonRelevant->pairsOfWandF = listAdd(retFoundStateNonRelevant->pairsOfWandF, ptTmpFAndW);
						retFoundStateNonRelevant->numberOfPairs += 1 ;

						addToQueue =  true;
					} else if(updatedFExpression){
						addToQueue =  true;
					}


					if(addToQueue){
						if(1==1 /*CurrentNode->level <= 50 */ ){
							ptBfsEltTranspose eltNewNode = createBFSElement(outgoingLink->State, NULL, CurrentNode->level + 1,
									outgoingLink->Weight + CurrentNode->accumulatedW,
									CurrentNode->level == 0 ?  outgoingLink->features :
											addConjunction(CurrentNode->accumulatedF, outgoingLink->features, 1, 1));

							Q = pushQueue(Q, eltNewNode);
						}
					}

				} else {
					// Adding univisited BFS State.
					if(1==1 /*CurrentNode->level <= 50 */){
						ptBfsEltTranspose eltNewNode = createBFSElement(outgoingLink->State, NULL, CurrentNode->level + 1,
								outgoingLink->Weight + CurrentNode->accumulatedW,
								CurrentNode->level == 0 ?  outgoingLink->features :
										addConjunction(CurrentNode->accumulatedF, outgoingLink->features, 1, 1));

						Q = pushQueue(Q, eltNewNode);


						//	insertToVisitedNotRelevantStatesBFS();

						insertAbstractTransitionValuesUnvisitedState(htDAbstractTransitionsToNonRelevant, outgoingLink->State,
																	outgoingLink->Weight + CurrentNode->accumulatedW,
																	CurrentNode->level == 0 ?  outgoingLink->features :
																		addConjunction(CurrentNode->accumulatedF, outgoingLink->features, 1, 1));

						//printf("Finished Adding to Queue \n");
					}
				}
			}

			CurrentNode->OutgoingLinksTransposeCurrent = CurrentNode->OutgoingLinksTransposeCurrent->next;
		} // End-While

		// printf("Next Element in BFS, current  level = %d \n", CurrentNode->level);
	}

	//printf("Completed Calculation of Finishing Times \n");
}


/*
 *
 * Returns true if abstract D has an entry for a state with state->payload and state->payloadSize
 *
 * False otherwise.
 *
 *
 */
bool isAbstractDInitializedForU(struct hashtable* DAbstractTransitions, ptHtDState* foundState, ptState State){
	return htFindByKeyAndPayloadNoFeatureCheckToHTMaxWeightedR(State->payloadHash, State->payload,
			State->payloadSize, foundState,  DAbstractTransitions);

}




/*
 *
 *Inserts an HTState for abstractTransitions with state and F,W
 *
 *Precondition:
 * 	abstractTransitions does not contain an entry for  ToState.
 *
 */
void insertAbstractTransitionValuesUnvisitedState(struct hashtable *abstractTransitions, ptState ToState,
		int AccumulatedWeight, ptBoolFct accumulatedF ){

	ptList pairsOfWandFToSet = NULL;

	int numberOfPairsWAndFToSet = 1;

	ptPairFeatureWeight ptFandW = constructPairFeatureWeight(accumulatedF, false, false, AccumulatedWeight);

	pairsOfWandFToSet  = listAdd(NULL, ptFandW);


	/* Start Insertion into hashtable */
	unsigned int Key = ToState->payloadHash;

	ptHtKey keyP = (ptHtKey) malloc(sizeof(tHtKey));
	if(!keyP)
		failure("Out of memory (creating hashtable key).\n");

	*keyP = Key;

	ptValue valP;
	valP = remove_some(abstractTransitions, keyP);

	if(!valP) {
		valP = (ptValue) malloc(sizeof(tValue));
		if(!valP)
			failure("Out of memory (creating hashtable value).\n");
		valP->list = NULL;
	}


	ptHtDState hs = (ptHtDState) malloc(sizeof(tHtDState));

	hs->payload = ToState->payload;
	hs->payloadSize = ToState->payloadSize;

	hs->numberOfPairs = numberOfPairsWAndFToSet;
	hs->pairsOfWandF = pairsOfWandFToSet;

	valP->list = listAdd(valP->list, hs);

	if(!insert_some(abstractTransitions, keyP, valP))
		failure("Out of memory (inserting state in hashtable)\n");

}

/*
 *
 * Inserts an HTState for abstractTransitions with state and F,W  list of ways of reaching state ToState
 * from start of abstractTransition.
 *
 *
 * Precondition:
 * 	abstractTransitions does not contain a state for ToState.
 *
 *
 * 	Returns true if an element was inserted, false otherwise.
 *
 */
bool setAbstractTransitionValuesForVirginState(struct hashtable *abstractTransitions, ptState fromState, int Weight, ptBoolFct transitionFeatures,
		ptState ToState, bool fromStartRelevant ){

	/*
	 * Start Code for initializing the list of pairs of W and F
	 *
	 *
	 *Target =  Non-Initialized
	 *
	 *	if fromState == Original
	 *
	 *		Current.pairsOfWAndF = transposeW, transposeF
	 *
	 *	else :
	 *
	 *		Precondition: Original must be initialized
	 *
	 *		Hence just cross Multiply with transposeW and transposeF.
	 *
	 *
	 *		Only add current to bfs search in case of change.
	 *
	 */

	ptList pairsOfWandFToSet = NULL;
	int numberOfPairsWAndFToSet = 0;
	bool peformInsertion = false;

	if(fromStartRelevant){

		//		printf("Initializing pairFeatureWeight fromStartRelevant \n");

		ptPairFeatureWeight ptFandW = constructPairFeatureWeight(transitionFeatures, false, false, Weight);

		pairsOfWandFToSet  = listAdd(NULL, ptFandW);

		numberOfPairsWAndFToSet = 1;

		peformInsertion = true;

	} else {
		// Original state must already be initialized with list of w, f.


		ptHtDState foundFromState;

		if( htFindByKeyAndPayloadNoFeatureCheckToHTMaxWeightedR(fromState->payloadHash, fromState->payload,
				fromState->payloadSize, &foundFromState, abstractTransitions)){

			// Start Cross Multiply Code.
			ptList fromStateWAndF =	foundFromState->pairsOfWandF;
			while(fromStateWAndF != NULL){
				ptPairFeatureWeight ptFandW = (ptPairFeatureWeight) fromStateWAndF->value;

				ptBoolFct andFromStateAndTransition = addConjunction(ptFandW->FExpression, transitionFeatures, 1, 1);
				if(isSatisfiable(andFromStateAndTransition)){
					ptPairFeatureWeight ptTmpFAndW = constructPairFeatureWeight(andFromStateAndTransition, false, false,
							ptFandW->W->number + Weight);

					pairsOfWandFToSet = listAdd(pairsOfWandFToSet, ptTmpFAndW);
					numberOfPairsWAndFToSet += 1 ;
					peformInsertion = true;
					//
				}

				fromStateWAndF = fromStateWAndF->next;
			}
			// End Cross Multiply Code.

		} else {
			failure("State with Payload  hash %u  has been added to BFS Queue for abstract transitions without being initialized \n",
					fromState->payloadHash);
		}
	}
	/* End code for initializing */



	if(peformInsertion){
		unsigned int Key = ToState->payloadHash;


		ptHtKey keyP = (ptHtKey) malloc(sizeof(tHtKey));
		if(!keyP)
			failure("Out of memory (creating hashtable key).\n");

		*keyP = Key;

		ptValue valP;
		valP = remove_some(abstractTransitions, keyP);

		if(!valP) {
			valP = (ptValue) malloc(sizeof(tValue));
			if(!valP)
				failure("Out of memory (creating hashtable value).\n");
			valP->list = NULL;
		}


		ptHtDState hs = (ptHtDState) malloc(sizeof(tHtDState));

		hs->payload = ToState->payload;
		hs->payloadSize = ToState->payloadSize;

		hs->numberOfPairs = numberOfPairsWAndFToSet;
		hs->pairsOfWandF = pairsOfWandFToSet;

		valP->list = listAdd(valP->list, hs);

		if(!insert_some(abstractTransitions, keyP, valP))
			failure("Out of memory (inserting state in hashtable)\n");


	}


	return peformInsertion;

}



/*
 *
 * Checks in a table holding ptHtDState if a ptHtDState with the given Key and Payload
 *
 *
 * Returns a  ptHtDState pointing to a ptHtDState and true if found.
 *
 * False otherwise.
 *
 */
bool htFindByKeyAndPayloadNoFeatureCheckToHTMaxWeightedR(tHtKey key, void * payload,
		unsigned int payloadSize,  ptHtDState* foundState,
		struct hashtable* _abstractTable){
	if(foundState)
		*foundState = NULL;

	ptHtKey keyP = (ptHtKey) malloc(sizeof(tHtKey));

	if(!keyP)
		failure("Out of memory (creating hashtable key).\n");

	*keyP = key;

	ptValue valP;

	valP = search_some(_abstractTable, keyP);

	free(keyP);

	if(valP) {
		ptList listElement = valP->list;

		ptHtDState candidateState;

		while(listElement) {
			candidateState = (ptHtDState) listElement->value;

			if(CompareIgnoreFeaturesByKeyAndPayload(payload,
					payloadSize,
					candidateState->payload,
					candidateState->payloadSize) == true){

				*foundState = candidateState;

				return true;
			}

			listElement = listElement->next;
		}
	}

	return false;
}



/*
 *
 * Creates a Queue (for BFS) element from a state and a feature expression.
 * It also looks up outgoing edges for the state in the transpose hashtable.
 *
 */
ptBfsEltTranspose createBFSElement(ptState state,
		ptBoolFct features,
		int level,
		int AccumulatedW,
		ptBoolFct AccumulatedFeatures){

	ptBfsEltTranspose elt = (ptBfsEltTranspose) malloc(sizeof(tBfsEltTranspose));

	if(!elt)
		failure("Out of memory (creating SCC stack element).\n");

	elt->state = state;
	elt->features =  features;


	elt->level = level;
	elt->accumulatedW = AccumulatedW ;
	elt->accumulatedF = AccumulatedFeatures;


	ptHtState transposeState = NULL;

	if( htFindNoFeatureCheck(state->payloadHash, state, &transposeState, getTransposeHasthable())){
		if(transposeState != NULL){
			elt->OutgoingLinksTranspose = transposeState->transposeLinks;
			elt->OutgoingLinksTransposeCurrent = elt->OutgoingLinksTranspose;

		}else {
			elt->OutgoingLinksTranspose = NULL;
			elt->OutgoingLinksTransposeCurrent = NULL;

		}

	} else {
		elt->OutgoingLinksTranspose = NULL;
		elt->OutgoingLinksTransposeCurrent = NULL;
	}

	return elt;

}


/*
 * Checks if a BFS  element has more transitions that haven't been explored.
 */
bool moreTransposeTransitionsAvailableBFS(ptBfsEltTranspose CurrentElt){

	bool NullCheck =  CurrentElt->OutgoingLinksTransposeCurrent != NULL;

	return NullCheck;
}
