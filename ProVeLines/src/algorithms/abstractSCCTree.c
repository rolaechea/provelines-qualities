/*
 * abstractSCCTree.c
 *
 *  Created on: Jan 22, 2016
 *      Author: rafaelolaechea
 */

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <stdbool.h>

#include "list.h"
#include "boolFct.h"
#include "state.h"
#include "tvl.h"
#include "hashtableState.h"
#include "abstractSCCTree.h"





/* Class Constructors */

/*
 *
 * Creates an empty SCC.
 *
 */
ptAbstractSCCTree createEmptyAbstractSCC(){
	//printf("createEmptyAbstractSCC called ");

	ptAbstractSCCTree NewTree = (ptAbstractSCCTree) malloc(sizeof(tAbstractSCCTree));

	NewTree->AccumulatedFExpression = getTrue();

	NewTree->chosenId = -1;
	NewTree->chosenIdSet = false;

	NewTree->htDirectStates = create_hashtableWrapper();

	NewTree->leftChild = NULL;
	NewTree->rightChild = NULL;


	NewTree->exploredInDFS = false;
	// Add all  features to list of available features.

	NewTree->availableFeaturesNbr = 0;
	NewTree->availableFeatures = NULL;

	ptLList listOfFeatures = getfeatureIDMapping();
	while(hasNextFeature(listOfFeatures)){

		if(isOriginalFeatureNonRoot(listOfFeatures)){

			int * ptNewInt = (int *) malloc(sizeof(int));
			*ptNewInt = getCurrentFeatureId(listOfFeatures);

			NewTree->availableFeatures = listAdd(NewTree->availableFeatures, ptNewInt);

			NewTree->availableFeaturesNbr += 1;
		}
		listOfFeatures = AdvanceFeature(listOfFeatures);
	}


	return NewTree;
}

/*
ptAbstractSCCTree createAbsSccFromF(ptBoolFct F){

	return NULL;
}

ptAbstractSCCTree createAbsSccFromFAndS(ptBoolFct F, ptState state){

	return NULL;
}
*/

ptAbstractSCCTree createAbsSccFromFAsChild(ptAbstractSCCTree  ptParentTree, ptBoolFct F){

	ptAbstractSCCTree blankNewTree =  (ptAbstractSCCTree) malloc(sizeof(tAbstractSCCTree));

	blankNewTree->AccumulatedFExpression = addConjunction(
			ptParentTree->AccumulatedFExpression,
			F, 1, 1);


	//printf("AccumulatedFExpression becomes ");
	//printBool(blankNewTree->AccumulatedFExpression);
	//printf("\n");

	blankNewTree->chosenId = -1;
	blankNewTree->chosenIdSet = false;

	blankNewTree->htDirectStates = create_hashtableWrapper();

	blankNewTree->leftChild = NULL;
	blankNewTree->rightChild = NULL;

	blankNewTree->exploredInDFS = false;

	// Copy available literals from parent Tree.
	blankNewTree->availableFeaturesNbr = 0;
	blankNewTree->availableFeatures = NULL;

	ptList iterateNumberFeatures = ptParentTree->availableFeatures;

	//printf("Copying List of Available literals from parent. Parent has %d literals available. \n",
	//		ptParentTree->availableFeaturesNbr );

	while(iterateNumberFeatures != NULL){
			// Add Element to blankNewTree.
			//printf("Copying Item %d . \n", blankNewTree->availableFeaturesNbr);
			//Copy Int Struct
			int tmpNum = * ((int *) iterateNumberFeatures->value);
			int * ptNewInt = (int *) malloc(sizeof(int));
			*ptNewInt = tmpNum;

			blankNewTree->availableFeatures = listAdd(blankNewTree->availableFeatures,
					ptNewInt);

			iterateNumberFeatures = iterateNumberFeatures->next;

			blankNewTree->availableFeaturesNbr += 1 ;
	}

	//printf("Copying List of Available literals from parent. Parent has %d literals available. \n",
	//		ptParentTree->availableFeaturesNbr );

	//printf("Returning New Inherited Tree, with available literals <%d> \n",
	//		blankNewTree->availableFeaturesNbr);
	return blankNewTree;
}

/* End Class Constructors */


/*
 *
 * Member Helper functions
 *
 */

/* Read only member helper functions */

bool currentFImpliesFPrime(ptAbstractSCCTree  ptTree, ptBoolFct FPrime){
	return implies(ptTree->AccumulatedFExpression, FPrime);
}
bool currentFImpliesNotFPrime(ptAbstractSCCTree  ptTree, ptBoolFct FPrime){
	return implies(ptTree->AccumulatedFExpression, negateBool(FPrime));
}

/* Search on its table whether its state is there or not. */
bool TreeDirectlyContainsState(ptAbstractSCCTree  ptTree, ptState state){

	ptHtState tmpFoundState = NULL;

	bool containsState = htFindByKeyAndPayloadNoFeatureCheck(state->payloadHash,
			state->payload, state->payloadSize, &tmpFoundState, ptTree->htDirectStates);

	return containsState;
}

bool hasChildTrees(ptAbstractSCCTree  ptTree){
	return ptTree->chosenIdSet;
}

ptAbstractSCCTree getLeftChildTree(ptAbstractSCCTree ptTree){

	return ptTree->leftChild;
}

ptAbstractSCCTree getRightChildTree(ptAbstractSCCTree ptTree){

	return ptTree->rightChild;
}


int getNumberOfUnassignedLiterals(ptAbstractSCCTree ptTree){
	return ptTree->availableFeaturesNbr;
}


bool moreUnexploredChildrensAvailableFromTree(ptAbstractSCCTree ptTree){
	bool ret_val = false;

	if(hasChildTrees(ptTree)){
		if(ptTree->leftChild->exploredInDFS == false){
			ret_val = true;
		} else if(ptTree->rightChild->exploredInDFS == false){
			ret_val = true;
		}
	}

	return ret_val;
}
/* Read only member helper functions */

/* Member Helper Functions that manipulate ptTree */


void insertDirectlyIntoTable(ptAbstractSCCTree ptTree, ptState StatePrime){

	htInsertNoFeatures(StatePrime->payloadHash, StatePrime, ptTree->htDirectStates);

}

void setLeftChildTree(ptAbstractSCCTree ptTree, ptAbstractSCCTree leftChild){
	ptTree->leftChild = leftChild;
}

void setRightChildTree(ptAbstractSCCTree ptTree, ptAbstractSCCTree rightChild){
	ptTree->rightChild = rightChild;
}


/*
 *
 * Go through the list of available literals and pick one.
 *	(later - only pick literals involved in the clause )
 *
 *
 *	Remove from list of available literals as well.
 *
 *
 *	Get list of literals / # of literals available.
 *
 *	choose randomly.
 *
 */
int ChooseNewLiteralAndGetId(ptAbstractSCCTree  ptTree){
	//printf("Choosing new Literal and Id \n");

	int numLits = getNumberOfUnassignedLiterals(ptTree);

	//printf(" NumLits = %d \n ", numLits);

	int randLitPos = rand() % numLits;

	// now have to look up in list the X rand LitPos

	//printf("Getting Literal Id  with position %d and Remove From List \n", randLitPos );
	int litId =  getLiteralIdAndRemoveFromList(ptTree, randLitPos);

	return litId;

	//	return (ptProcessTransition) currentTrans->value;
}


/*
 *
 * Gets the literal id in the ith position and returns it.
 * It also removes it from the list and updates the list and list size.
 *
 */
int getLiteralIdAndRemoveFromList(ptAbstractSCCTree  ptTree, int randLitPos){
	//printf("getLiteralIdAndRemoveFromList, randLitPost = %d ", randLitPos);

	ptList iterateListIds = ptTree->availableFeatures;

	int i = 0;

	while(i < randLitPos){

		if(iterateListIds==NULL){
			failure(" Searching for %d th element (indexing starts at 0) in a list, but list only has %d elements",
					randLitPos, i);
		} else {
			i++;
			iterateListIds = iterateListIds->next;
		}
	}

	if(iterateListIds==NULL){
		failure(" Searching for %d th element in a list (indexing starts at 0), but end up pointing to null after %d advances.",
				randLitPos, i);
	}

	/* IterateListIds pointing to current node */
	//printf("De-referencing iterateListIds \n");
	int retId =  * ( (int * ) iterateListIds->value);


	// Remove element pointed at by iterateListIds from list.
	//printf("Removing Literal, randLitPos %d \n", randLitPos);
	if(iterateListIds == ptTree->availableFeatures){
		// Remove  first item.
		ptTree->availableFeatures = ptTree->availableFeatures->next;

		if (ptTree->availableFeatures != NULL){
			ptTree->availableFeatures->prev = NULL;
		}

	} else {
		// Remove Non-first item.
		// aka iterateListIds->prev != NULL
		// and iterateListIds != ptTree->availableFeatures
		(iterateListIds->prev)->next = iterateListIds->next ;
	}
	ptTree->availableFeaturesNbr =  ptTree->availableFeaturesNbr - 1;


	return retId;

}


/*
 *
 * Returns the next available child and sets DFS visited for it to true.
 *
 * Preconditions - moreUnexploredChildrensAvailableFromTree returns true.
 */
ptAbstractSCCTree   getNextAvailableChildFromTree(ptAbstractSCCTree  ptTree){
	ptAbstractSCCTree retChild = NULL;
	if(ptTree->leftChild==NULL || ptTree->rightChild==NULL){
		failure("Called getNextAvailableChildFromTree without first checking hasNextAvailableChildFromTree = true ");
	} else {
		if(ptTree->leftChild->exploredInDFS == false){
			ptTree->leftChild->exploredInDFS = true;
			retChild = ptTree->leftChild;
		} else if (ptTree->rightChild->exploredInDFS == false){
			ptTree->rightChild->exploredInDFS = true;
			retChild = ptTree->rightChild;
		} else {
			failure("Called getNextAvailableChildFromTre after both children have been explored");
		}

	}

	return retChild;
}


/* Member Helper Functions that manipulate ptTree */


/* End Member Helper functions */


void UpdateAbstractSCC(ptAbstractSCCTree  ptTree, ptBoolFct FExpPrime, ptState StatePrime ){
	if(TreeDirectlyContainsState(ptTree, StatePrime)){
		// skip
	}else {
		UpdateAbstractSCCSNotInTable(ptTree, FExpPrime, StatePrime);
	}
}


/*
 *
 * Precondition: StatePrime not in T.TableOfStates
 *
 */
void UpdateAbstractSCCSNotInTable(ptAbstractSCCTree  ptTree, ptBoolFct FExpPrime, ptState StatePrime){
	//printf("UpdateAbstractSCCSNotInTable \n");
	if (currentFImpliesFPrime(ptTree, FExpPrime)){
		// insertIntoTable
		insertDirectlyIntoTable(ptTree, StatePrime);
	} else if(currentFImpliesNotFPrime(ptTree, FExpPrime)){
		// skip
	} else {
		//printf(" CurrentF = ");
		//printBool(ptTree->AccumulatedFExpression);
		//	printf(" doesn't imply ");
		//printBool(FExpPrime);
		//printf(" nor ");
		//printBool(negateBool(FExpPrime));
		//printf(" \n");
		PropagateToChildren(ptTree, FExpPrime, StatePrime);

	}
}

/*
 *
 * Preconditions:
 * 		StatePrime not in T.TableOfStates
 * 		Neither T.f -> FPrime nor T.f -> ~ FPrime
 *
 */
void PropagateToChildren(ptAbstractSCCTree  ptTree, ptBoolFct FExpPrime, ptState StatePrime){

	if(hasChildTrees(ptTree)){
		//printf("Propagating to Children  --with hasChildTrees == true\n");
		UpdateAbstractSCC(getLeftChildTree(ptTree), FExpPrime, StatePrime);
		UpdateAbstractSCC(getRightChildTree(ptTree), FExpPrime, StatePrime);
	} else {
		//printf("Propagating to Children  --with hasChildTrees == false\n");
		SplitAndInsert(ptTree, FExpPrime, StatePrime);
	}
}

/*
 *
 * Preconditions:
 * 		StatePrime not in T.TableOfStates
 * 		Neither T.f -> FPrime nor T.f -> ~ FPrime
 *		T is a leaf node (e.g it has no children).
 */
void SplitAndInsert(ptAbstractSCCTree  ptTree, ptBoolFct FExpPrime, ptState StatePrime){
	//printf("At SplitAndInsert,  FExpPrime features =");
	//printBool(FExpPrime);
	//printf(" Current = ");
	//printBool(ptTree->AccumulatedFExpression);
	//	printf("\n");

	int litId = ChooseNewLiteralAndGetId(ptTree);

	ptTree->chosenId = litId;
	ptTree->chosenIdSet = true;

	//printf("Creating Literal with id %d \n ", litId);

	ptBoolFct L = createLiteral(litId);
	ptBoolFct newNotL = negateBool(L);

	//printf("Setting Left children to have L = ");
	//printBool(L);
	//printf("\n");

	setLeftChildTree(ptTree, createAbsSccFromFAsChild(ptTree, L));
	UpdateAbstractSCCSNotInTable(getLeftChildTree(ptTree), FExpPrime, StatePrime);

	//printf("Setting Right children to have Not(L) = ");
	//printBool(newNotL);
	//printf("\n");


	setRightChildTree(ptTree, createAbsSccFromFAsChild(ptTree, newNotL));
	UpdateAbstractSCCSNotInTable(getRightChildTree(ptTree),  FExpPrime, StatePrime);

	//printf("Finished SplitAndInsert \n");
}



