/*
 * scc-debugging.h
 *
 *  Created on: Oct 1, 2015
 *      Author: rafaelolaechea
 */

#ifndef ALGORITHMS_SCC_DEBUGGING_H_
#define ALGORITHMS_SCC_DEBUGGING_H_

#include <stdio.h>
#include <stdlib.h>
#include "../lib/clark.hashtable/hashtable.h"
#include "../wrapper/hash/hashtableState.h"



void printHashtablesAndFeatures(struct hashtable * abstractTable);
void printSCCAllStatesAndFeatureExpression(struct hashtable* sccComponent);
void printTransposeAndLinks(ptMTypeNode mtypes);
void printGraphInDotLanguage(ptMTypeNode mtypes);

void setInitialState(unsigned int Key);
unsigned int getInitialStateKey();


#endif /* ALGORITHMS_SCC_DEBUGGING_H_ */
