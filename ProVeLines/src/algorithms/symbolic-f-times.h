/*
 * symbolic-f-times.h
 *
 *  Created on: Sep 1, 2015
 *      Author: rafaelolaechea
 */

#ifndef ALGORITHMS_SYMBOLIC_F_TIMES_H_
#define ALGORITHMS_SYMBOLIC_F_TIMES_H_

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <stdbool.h>


#include "symbols.h"
#include "execution.h"
#include "state.h"
#include "boolFct.h"




// An element on the set of f-numbers.
struct FElt {
	ptState state;			// The state itself.
	ptBoolFct features;		// The feature expression.

};

typedef struct FElt   tFElt;
typedef struct FElt* ptFElt;

void htSetFinishingTime(ptMTypeNode mtypes, ptStackElt elt);
int getSizeFinishingTime();
void getStateAndFexpressionFInverse(int i, 	ptState *s, ptBoolFct *features);


// vars = listAdd(NULL,"const");

#endif /* ALGORITHMS_SYMBOLIC_F_TIMES_H_ */
