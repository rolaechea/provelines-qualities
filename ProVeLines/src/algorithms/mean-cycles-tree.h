/*
 * mean-cycles-tree.h
 *
 *  Created on: Jan 28, 2016
 *      Author: rafaelolaechea
 */

#ifndef ALGORITHMS_MEAN_CYCLES_TREE_H_
#define ALGORITHMS_MEAN_CYCLES_TREE_H_
#include "../lib/clark.hashtable/hashtable.h"
#include "../wrapper/hash/hashtableState.h"
#include "abstractSCCTree.h"

void computeAllPossibleLimitAverage(ptAbstractSCCTree sccComponentTree, ptState initialState);


#endif /* ALGORITHMS_MEAN_CYCLES_TREE_H_ */
