/*
 * finishing-times-tree.h
 *
 *  Created on: Sep 2, 2015
 *      Author: rafaelolaechea
 */

#ifndef ALGORITHMS_FINISHING_TIMES_TREE_H_
#define ALGORITHMS_FINISHING_TIMES_TREE_H_

#include "finishing-times-adt.h"

void computeSymbolicFinishingTree();
ptSymbolicNode getSymbolicTreeRoot();
ptSymbolicNode getNextChildren(ptSymbolicNode root, ptEdgeListSymbolicNode currentChildList);
bool hasMoreChildren(ptSymbolicNode root, ptEdgeListSymbolicNode currentChildList);
void setSymbolicTreeRoot(ptSymbolicNode T);

#endif /* ALGORITHMS_FINISHING_TIMES_TREE_H_ */
