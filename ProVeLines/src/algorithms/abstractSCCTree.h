/*
 * abstractSCCTree.h
 *
 *  Created on: Jan 22, 2016
 *      Author: rafaelolaechea
 */

#ifndef ALGORITHMS_ABSTRACTSCCTREE_H_
#define ALGORITHMS_ABSTRACTSCCTREE_H_



struct abstractSCCTree {
	ptState state;
	ptBoolFct AccumulatedFExpression;
	ptList availableFeatures;
	int availableFeaturesNbr;
	struct abstractSCCTree * leftChild;
	struct abstractSCCTree * rightChild;
	struct hashtable* htDirectStates;

	int chosenId;
	bool chosenIdSet;
	bool exploredInDFS;
};

typedef struct abstractSCCTree tAbstractSCCTree;
typedef struct abstractSCCTree * ptAbstractSCCTree;

ptAbstractSCCTree createEmptyAbstractSCC();
ptAbstractSCCTree createAbsSccFromFAsChild(ptAbstractSCCTree  ptParentTree, ptBoolFct F);


bool currentFImpliesFPrime(ptAbstractSCCTree  ptTree, ptBoolFct FPrime);
bool currentFImpliesNotFPrime(ptAbstractSCCTree  ptTree, ptBoolFct FPrime);
bool TreeDirectlyContainsState(ptAbstractSCCTree  ptTree, ptState state);
bool hasChildTrees(ptAbstractSCCTree  ptTree);
bool moreUnexploredChildrensAvailableFromTree(ptAbstractSCCTree  ptTree);


ptAbstractSCCTree getLeftChildTree(ptAbstractSCCTree ptTree);
ptAbstractSCCTree getRightChildTree(ptAbstractSCCTree ptTree);
int getNumberOfUnassignedLiterals(ptAbstractSCCTree ptTree);

void insertDirectlyIntoTable(ptAbstractSCCTree ptTree, ptState StatePrime);
void setLeftChildTree(ptAbstractSCCTree ptTree, ptAbstractSCCTree leftChild);
void setRightChildTree(ptAbstractSCCTree ptTree, ptAbstractSCCTree rightChild);
int ChooseNewLiteralAndGetId(ptAbstractSCCTree  ptTree);
int getLiteralIdAndRemoveFromList(ptAbstractSCCTree  ptTree, int randLitPos);

ptAbstractSCCTree   getNextAvailableChildFromTree(ptAbstractSCCTree  ptTree);

void UpdateAbstractSCC(ptAbstractSCCTree  ptTree, ptBoolFct FExpPrime, ptState StatePrime );
void UpdateAbstractSCCSNotInTable(ptAbstractSCCTree  ptTree, ptBoolFct FExpPrime, ptState StatePrime);
void PropagateToChildren(ptAbstractSCCTree  ptTree, ptBoolFct FExpPrime, ptState StatePrime);
void SplitAndInsert(ptAbstractSCCTree  ptTree, ptBoolFct FExpPrime, ptState StatePrime);

#endif /* ALGORITHMS_ABSTRACTSCCTREE_H_ */
