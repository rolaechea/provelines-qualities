/*
 * abstractTransitionsMapping.h
 *
 *  Created on: Jan 14, 2016
 *      Author: rafaelolaechea
 */

#ifndef ALGORITHMS_ABSTRACTTRANSITIONSMAPPING_H_
#define ALGORITHMS_ABSTRACTTRANSITIONSMAPPING_H_
#include "../lib/clark.hashtable/hashtable.h"
#include "../lib/clark.hashtable/hashtable_private.h"
#include "../wrapper/hash/hashtableState.h"
#include "symbolic-scc.h"

void setAbstractTransitionsFromHashtable(ptHtState s,
		struct hashtable* DAbstractTransitions);
ptStackEltTranspose createSccStackElementAbstract(ptState state, ptBoolFct features);
ptList getAbstractTransposeLinksFromKeyAndPayload(tHtKey key, void * payload, unsigned int payloadSize);
struct hashtable* initializeEmptySetRelevantStates();

#endif /* ALGORITHMS_ABSTRACTTRANSITIONSMAPPING_H_ */
