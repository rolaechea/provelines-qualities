/*
 * scc-debugging.c
 *
 *  Created on: Oct 1, 2015
 *      Author: rafaelolaechea
 */


#include <stdio.h>
#include <stdlib.h>
#include "../lib/clark.hashtable/hashtable.h"
#include "../lib/clark.hashtable/hashtable_private.h"
#include "../wrapper/hash/hashtableState.h"

#include "finishing-times-tree.h"
#include "finishing-times-adt.h"
#include "transpose-executables.h"
#include "scc-analysis.h"
#include "symbolic-scc.h"



unsigned int privateSCCInitial = 0;

void setInitialState(unsigned int Key){
	privateSCCInitial = Key;
}
unsigned int getInitialStateKey(){
	return privateSCCInitial;
}

/*
 *
 * Includes logic to iterate through all states of a symbolic SCC represented by a hash table.
 *
 */
void printSCCAllStatesAndFeatureExpression(struct hashtable * sccComponent){
	printf("SCC\nPayload-Hash, Features, Index (for debugging) \n");
	int j = 0;

	printf("EntryCount = %u \n", sccComponent->entrycount);
	for(j = 0; j < sccComponent->tablelength; j++){

		struct entry *sccEntry = sccComponent->table[j];
		struct entry *iterateOverEntry = sccEntry;

		while(iterateOverEntry != NULL){

			ptHtKey keyP = (ptHtKey) malloc(sizeof(tHtKey));

			if(!keyP)
				failure("Out of memory (creating hashtable key).\n");

			*keyP = *((ptHtKey ) (iterateOverEntry->k ));

			ptValue valP =( (ptValue) iterateOverEntry->v);

			if(valP != NULL){
				ptList listHashStates = valP->list;
				while(NULL != listHashStates){
					ptHtState hs = (ptHtState) listHashStates->value ;


					printf("%u,  ", *keyP);
					printBool(hs->outerFeatures);
					printf(", %d" , j);
					printf("\n");
					// Each state is printed here.

					listHashStates = listHashStates->next;
				}

			}


			iterateOverEntry = iterateOverEntry->next;
		}
	}

//	printf(" Now will use iterator from scc-analysis - just to check it. \n");
//
//	ptHtState s = NULL;
//	unsigned int Key ;
//	ptHtIteratorState elementPointerInTable = getEmptyHashtableIterator();
//
//	while(tableHasMoreElements(sccComponent, elementPointerInTable, &s, &Key)){
//		printf("%u, ", Key);
//		printBool(s->outerFeatures);
//		printf("\n");
//	}
//
//	printf(" End Check \n");
}







void printHashtablesAndFeatures(struct hashtable * abstractTable){
	printf("\nPayload-Hash, Index (for debugging), Features \n");
		int j = 0;

		printf("EntryCount = %u \n", abstractTable->entrycount);
		for(j = 0; j < abstractTable->tablelength; j++){

			struct entry *sccEntry = abstractTable->table[j];
			struct entry *iterateOverEntry = sccEntry;

			while(iterateOverEntry != NULL){

				ptHtKey keyP = (ptHtKey) malloc(sizeof(tHtKey));

				if(!keyP)
					failure("Out of memory (creating hashtable key).\n");

				*keyP = *((ptHtKey ) (iterateOverEntry->k ));

				ptValue valP =( (ptValue) iterateOverEntry->v);

				if(valP != NULL){
					ptList listHashStates = valP->list;
					while(NULL != listHashStates){
						ptHtState hs = (ptHtState) listHashStates->value ;


						printf("%u,  ", *keyP);
						printf("%d ," , j);
						printBool(hs->outerFeatures);
						printf("\n");
						// Each state is printed here.

						listHashStates = listHashStates->next;
					}

				}


				iterateOverEntry = iterateOverEntry->next;
			}
		}
}


/*
 *
 * Print the state machine in dot language
 *
 */
void printGraphInDotLanguage(ptMTypeNode mtypes){
	struct hashtable * sccComponent = getTransposeHasthable();
	int j = 0;
	printf("digraph {\n");
		printf(" start -> [%u] ;\n", getInitialStateKey());
		for(j = 0; j < sccComponent->tablelength; j++){

				struct entry *sccEntry = sccComponent->table[j];
				struct entry *iterateOverEntry = sccEntry;

				while(iterateOverEntry != NULL){

					ptHtKey keyP = (ptHtKey) malloc(sizeof(tHtKey));

					if(!keyP)
						failure("Out of memory (creating hashtable key).\n");

					*keyP = *((ptHtKey ) (iterateOverEntry->k ));

					ptValue valP =( (ptValue) iterateOverEntry->v);

					if(valP != NULL){
						ptList listHashStates = valP->list;
						while(NULL != listHashStates){
							ptHtState hs = (ptHtState) listHashStates->value ;


							ptList incomingLinks =  getTransposeLinksFromKeyAndPayload(*keyP,
									hs->payload, hs->payloadSize );

							while(incomingLinks != NULL){
								ptTransAndFExp outgoingLink = (ptTransAndFExp) incomingLinks->value;
	//							printf("\t Link from %u to %u,  W = %d, ",
	//									outgoingLink->State->payloadHash,
	//									*keyP,
	//									outgoingLink->Weight
	//									);
	//							printBool(outgoingLink->features);
								printf("\t  %u -> %u [label = \"", outgoingLink->State->payloadHash, *keyP);
								printBool(outgoingLink->features);
								printf(" ,%d \"]; \n", outgoingLink->Weight);

								printf("\t %u [label=\"", *keyP);
								printGlobalVariables(mtypes, hs->originalState, NULL);
								printf("\"];\n");

								incomingLinks = incomingLinks->next;
							}
							listHashStates = listHashStates->next;
						}
					}


					iterateOverEntry = iterateOverEntry->next;
				}
			}
			printf("}\n");
}


/*
 *
 * Includes logic to iterate through all states of Transpose Table  represented by a hash table.
 *
 */
void printTransposeAndLinks(ptMTypeNode mtypes){
	struct hashtable * sccComponent = getTransposeHasthable();

	printf("\nPayload-Hash, Index (for debugging) \n");
	int j = 0;

	printf("EntryCount = %u \n", sccComponent->entrycount);
	for(j = 0; j < sccComponent->tablelength; j++){

		struct entry *sccEntry = sccComponent->table[j];
		struct entry *iterateOverEntry = sccEntry;

		while(iterateOverEntry != NULL){

			ptHtKey keyP = (ptHtKey) malloc(sizeof(tHtKey));

			if(!keyP)
				failure("Out of memory (creating hashtable key).\n");

			*keyP = *((ptHtKey ) (iterateOverEntry->k ));

			ptValue valP =( (ptValue) iterateOverEntry->v);

			if(valP != NULL){
				ptList listHashStates = valP->list;
				while(NULL != listHashStates){
					ptHtState hs = (ptHtState) listHashStates->value ;


//					printf("%u,  ", *keyP);
//					printf("%d" , j);
//					printf("\n");
//					// Each state is printed here.
//
//					printState(mtypes, hs->originalState, NULL);
//					printf("\n");
					listHashStates = listHashStates->next;
				}

			}


			iterateOverEntry = iterateOverEntry->next;
		}
	}



	printf("Links \n");
	// print links
	printf("digraph {\n");
	printf(" start -> [%u] \n", getInitialStateKey());
	for(j = 0; j < sccComponent->tablelength; j++){

			struct entry *sccEntry = sccComponent->table[j];
			struct entry *iterateOverEntry = sccEntry;

			while(iterateOverEntry != NULL){

				ptHtKey keyP = (ptHtKey) malloc(sizeof(tHtKey));

				if(!keyP)
					failure("Out of memory (creating hashtable key).\n");

				*keyP = *((ptHtKey ) (iterateOverEntry->k ));

				ptValue valP =( (ptValue) iterateOverEntry->v);

				if(valP != NULL){
					ptList listHashStates = valP->list;
					while(NULL != listHashStates){
						ptHtState hs = (ptHtState) listHashStates->value ;


						ptList incomingLinks =  getTransposeLinksFromKeyAndPayload(*keyP,
								hs->payload, hs->payloadSize );

						while(incomingLinks != NULL){
							ptTransAndFExp outgoingLink = (ptTransAndFExp) incomingLinks->value;
//							printf("\t Link from %u to %u,  W = %d, ",
//									outgoingLink->State->payloadHash,
//									*keyP,
//									outgoingLink->Weight
//									);
//							printBool(outgoingLink->features);
							printf("\t  %u -> %u [label = \"", outgoingLink->State->payloadHash, *keyP);
							printBool(outgoingLink->features);
							printf(" ,%d \"]; \n", outgoingLink->Weight);

							printf("\t %u [label=\"", *keyP);
							printGlobalVariables(mtypes, hs->originalState, NULL);
							printf("\"];\n");

							incomingLinks = incomingLinks->next;
						}
						listHashStates = listHashStates->next;
					}
				}


				iterateOverEntry = iterateOverEntry->next;
			}
		}
		printf("}\n");

//	printf(" Now will use iterator from scc-analysis - just to check it. \n");
//
//	ptHtState s = NULL;
//	unsigned int Key ;
//	ptHtIteratorState elementPointerInTable = getEmptyHashtableIterator();
//
//	while(tableHasMoreElements(sccComponent, elementPointerInTable, &s, &Key)){
//		printf("%u, ", Key);
//		printBool(s->outerFeatures);
//		printf("\n");
//	}
//
//	printf(" End Check \n");
}
