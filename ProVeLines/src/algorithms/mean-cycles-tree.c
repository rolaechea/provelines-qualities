/*
 * mean-cycles-tree.c
 *
 *  Created on: Jan 28, 2016
 *      Author: rafaelolaechea
 */
#include <stdio.h>
#include <stdlib.h>
#include "list.h"
#include "../lib/clark.hashtable/hashtable.h"
#include "../lib/clark.hashtable/hashtable_private.h"
#include "../wrapper/hash/hashtableState.h"
#include "main.h"
#include "finishing-times-tree.h"
#include "finishing-times-adt.h"
#include "symbolic-scc.h"
#include "scc-debugging.h"
#include "scc-analysis.h"
#include "transpose-executables.h"
#include "abstractTransitionsMapping.h"
#include "mean-cycles.h"
#include "mean-cycle-structure.h"



/*
 *  Go through each leaf node and check ...
 *
 *  Performing "DFS" on a tree.
 *
 */
void computeAllPossibleLimitAverage(ptAbstractSCCTree sccComponentRootTree, ptState initialState){

	ptStack stackDFS = push(NULL,	sccComponentRootTree);

	while(!empty(stackDFS)){

		ptAbstractSCCTree current = (ptAbstractSCCTree) top(stackDFS);


		if(!hasChildTrees(current)){
			// if its leaf node. then we call out limit average on the implied SCC Component and pop.
//			printf("\t\t Leaf  Node, with expression  ");
//			printBool(current->AccumulatedFExpression);
//			printf("\n");




			// Go back until parent and add each state element into the current table.
			ptAbstractSCCTree currentChildIteration = current ;
			ptStack backIterator = stackDFS;

			// ignore repeated items.
			struct hashtable* htSetOfStates = create_hashtableWrapper();

			while(currentChildIteration!=NULL){
				ptHtState currentState = NULL;
				unsigned int tmpKey ;
				ptHtIteratorState elementPointerInTable = getEmptyHashtableIterator();
				while(tableHasMoreElements(currentChildIteration->htDirectStates, elementPointerInTable, &currentState, &tmpKey)){
					// count each item.

					ptHtState tmpFoundState = NULL;

					bool containsState = htFindByKeyAndPayloadNoFeatureCheck(tmpKey,
							currentState->payload, currentState->payloadSize, &tmpFoundState, htSetOfStates);

					if(!containsState){
						htAbstractInsertExplicitFeatures(tmpKey, currentState->originalState, current->AccumulatedFExpression, htSetOfStates);
					}
				}

				if(backIterator->prev != NULL){
					backIterator = backIterator->prev;
					currentChildIteration = (ptAbstractSCCTree) backIterator->value;
				} else {
					currentChildIteration = NULL;
				}
			}

			//	printf("\t\t # of items = %u  \n", htSetOfStates->entrycount) ;

			if(htSetOfStates->entrycount > 0){
				computeSCCLimitAverage(htSetOfStates, initialState);
			}

			//			printf("\t\t Done Computing Limit Average for this subset \n");

			pop(&stackDFS);

		} else{
			// Not a leaf node.
			if(!moreUnexploredChildrensAvailableFromTree(current) /*!current->E */){
				// All children  have been explored  -- pop from stack.
				pop(&stackDFS);

			} else  if (moreUnexploredChildrensAvailableFromTree(current)){

				stackDFS = push(stackDFS, getNextAvailableChildFromTree(current));

			} else {
				failure(" moreUnexploredChildrensAvailableFromTree shall return either true or false idempotently ");
			}
		}



	}
}

