/*
 * mean-cycle-structure.h
 *
 *  Created on: Oct 1, 2015
 *      Author: rafaelolaechea
 */

#ifndef ALGORITHMS_MEAN_CYCLE_STRUCTURE_H_
#define ALGORITHMS_MEAN_CYCLE_STRUCTURE_H_
#include <stdio.h>
#include <stdlib.h>
#include "../lib/clark.hashtable/hashtable.h"
#include "../lib/clark.hashtable/hashtable_private.h"
#include "../wrapper/hash/hashtableState.h"

#include "finishing-times-tree.h"
#include "finishing-times-adt.h"
#include "symbolic-scc.h"
#include "scc-debugging.h"


// An element on the set of f-numbers.
struct DSymbolicVals {
	int numberStates;
	struct hashtable ** KHashTables;
};


typedef struct DSymbolicVals   tDSymbolicVals;
typedef struct DSymbolicVals* ptDSymbolicVals;

struct MSymbolicValsCollector {
	int numberStates;
	struct hashtable * aHashTable; // Hashtable containing htDState elements.
};

typedef struct MSymbolicValsCollector   tMSymbolicValsCollector;
typedef struct MSymbolicValsCollector* ptMSymbolicValsCollector;



struct htDState {
	void * payload;
	unsigned int payloadSize;
	ptList pairsOfWandF;
	int numberOfPairs;
};

typedef struct htDState tHtDState;
typedef struct htDState * ptHtDState;

struct numberAndInfinity {
	double numberF; // Only used in M collections
	int number;
	bool isMinusInfinity;
	bool isPlusInfinity;
};

typedef struct numberAndInfinity tNumberAndInfinity;
typedef struct numberAndInfinity * ptNumberAndInfinity;


struct pairFeatureWeight{
	ptNumberAndInfinity W;
	ptBoolFct FExpression;
};

typedef struct pairFeatureWeight tPairFeatureWeight;
typedef struct pairFeatureWeight * ptPairFeatureWeight;


ptDSymbolicVals createNewDStructure(int numberOfItems);
ptMSymbolicValsCollector createNewMStructure(int numberOfItems);


void setAndCreateElementMSingleFExpToPlusInfinity(ptMSymbolicValsCollector M,  unsigned int Key,
		ptHtState s, ptBoolFct featureExpression);


void setAndCreateElementDSingleFExpToInfinity(ptDSymbolicVals D, int k, unsigned int Key,
		ptHtState s, ptBoolFct featureExpression);
void setElementDSingleFExpToZero(ptDSymbolicVals D, int k, unsigned int Key, ptState initialState);

bool htFindByKeyAndPayloadNoFeatureCheckTohtD(tHtKey key, void * payload,
		unsigned int payloadSize,  ptHtDState* foundState,
		struct hashtable* _abstractTable);

bool htFindByKeyAndStateNoFeatureCheckTohtD(tHtKey key, ptState state,  ptHtDState* foundState,
		struct hashtable* _abstractTable);

ptList getFeatureExpressionDomainAndImage(ptDSymbolicVals D,
			int k,
			unsigned int Key,
			void  *payload,
			unsigned int paylodSize);

void setFeatureExpressionDomainAndImage(ptDSymbolicVals D,
		int k,
		unsigned int Key,
		ptList ptDomainOfDKVToUpdateTo,
		void  *payload,
		unsigned int paylodSize);

ptList getFeatureExpressionDomainAndImageFromM(ptMSymbolicValsCollector M,
		unsigned int Key,
		void  *payload,
		unsigned int paylodSize);

void 	setFeatureExpressionDomainAndImageFromM(ptMSymbolicValsCollector M,
		unsigned int Key,
		ptList updateFAndW,
		void  *payload,
		unsigned int paylodSize);

void insertInfinityStateElementIntoTable(struct hashtable *relevantTable,
		unsigned int Key,
		ptHtState s,
		ptBoolFct featureExpression,
		bool insertNegativeInfinity);

ptPairFeatureWeight createNegativeInfinityAndFexpPair(ptBoolFct featureExpression);
ptPairFeatureWeight createPositiveInfinityAndFexpPair(ptBoolFct featureExpression);

ptPairFeatureWeight constructPairFeatureWeight(ptBoolFct featureExpression, bool  isMinusInfinity, bool isPlusInfinity, int numberW );
ptPairFeatureWeight constructPairFeatureWeightM(ptBoolFct mvANDNOTPdnvANDdkvP,
											bool previousIsMinusInfinity, bool previousIsPlusInfinity,
											int previousW,  double previousWF);




bool isGreaterLeftDAndWThanRightD(ptPairFeatureWeight uFandW, int W, ptPairFeatureWeight vFandW);
bool isGreaterLeftMThanDMinusDByNMinusK(ptPairFeatureWeight mvFandW, ptPairFeatureWeight dnvFandW,
		ptPairFeatureWeight dkvFandW, int numberOfItems, int  k);

bool isLessCThanM(ptPairFeatureWeight CFandW, ptPairFeatureWeight mvFandW);
bool areAugmentedWEquivalent(ptNumberAndInfinity W1, ptNumberAndInfinity W2);
bool areAugmentedWEquivalentByFloat(ptNumberAndInfinity W1, ptNumberAndInfinity W2);

tNumberAndInfinity calculateDMinusDByNMinusK(ptPairFeatureWeight dnvFandW,  ptPairFeatureWeight dkvFandW,
		int numberOfItems, int k);
void printWFloat(ptNumberAndInfinity W);
void printWInteger(ptNumberAndInfinity W);
void printMStructureForStateByKeyAndPayload(ptMSymbolicValsCollector M,
		unsigned int tmpKey,
		void *payload,
		int payloadSize );
void printDStructure(struct hashtable *  sccComponent, ptDSymbolicVals D, int k);

#endif /* ALGORITHMS_MEAN_CYCLE_STRUCTURE_H_ */
