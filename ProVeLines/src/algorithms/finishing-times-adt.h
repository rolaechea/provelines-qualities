/*
 * finishing-times-adt.h
 *
 *  Created on: Sep 3, 2015
 *      Author: rafaelolaechea
 */

#ifndef ALGORITHMS_FINISHING_TIMES_ADT_H_
#define ALGORITHMS_FINISHING_TIMES_ADT_H_

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <stdbool.h>

#include "error.h"
#include "main.h"
#include "list.h"
#include "stack.h"
#include "boolFct.h"
#include "symbols.h"
#include "automata.h"
#include "SAT.h"
#ifdef CLOCK
	#include "clockZone.h"
	#include "federation.h"
#endif
#include "state.h"
#include "execution.h"

#include "abstractSCCTree.h"


typedef struct SymbolicNode_ tSymbolicNode;
typedef struct EdgeListSymbolicNode_ tEdgeListSymbolicNode;



struct SymbolicNode_ {
	bool isRoot;
	tEdgeListSymbolicNode *ListOfOutgoingEdges;

	int maximiumNode;
	bool isVisited;
	ptState state;			//  State Label
	struct SymbolicNode_ *father;
	ptBoolFct ToFatherfeatures; // Edge Label of father, self edge.
	struct hashtable* ptSccComponent;
	struct hashtable* ptSccUnion; // Union of scc up to (and including) tree node to exclude.
	bool hasSymbolicSccComponent;

	ptAbstractSCCTree ptTreeSccComponent;
};



struct EdgeListSymbolicNode_{
	tSymbolicNode *destination;
	ptBoolFct featureEdgeLabel;
	struct EdgeListSymbolicNode_ *next;

};

typedef struct SymbolicNode_ * ptSymbolicNode;
typedef struct  EdgeListSymbolicNode_ * ptEdgeListSymbolicNode;


struct queue_ {
	void * current;
	struct queue_ * next;
	struct queue_ * last;
};

typedef struct queue_ tQueue;
typedef struct queue_ * ptQueue;








/* API */

ptQueue createEmptyQueue();
bool isEmptyQueue(ptQueue Q);
ptQueue pushQueue(ptQueue Q, void * Node);
void * popQueue(ptQueue *Q);



ptSymbolicNode createRootSymbolicFTree();
int getMaxMappingNode(ptSymbolicNode Node);
void setMaxMappingNode(ptSymbolicNode Node, int max);
bool getNodeVisited(ptSymbolicNode Node);
void setNodeVisited(ptSymbolicNode TreeNode);
bool isRootNode(ptSymbolicNode Node);
ptBoolFct FeatureExpressionFromRoot(ptSymbolicNode Node);
ptSymbolicNode createFTreeNode(ptSymbolicNode fatherNode, ptState stateLabel, ptBoolFct edgeLabel );

#endif /* ALGORITHMS_FINISHING_TIMES_ADT_H_ */
