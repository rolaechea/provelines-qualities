/*
 * symbolic-dfs-helpers.h
 *
 *  Created on: Aug 28, 2015
 *      Author: rafaelolaechea
 */

#ifndef ALGORITHMS_SYMBOLIC_DFS_HELPERS_H_
#define ALGORITHMS_SYMBOLIC_DFS_HELPERS_H_

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <stdbool.h>

#include "main.h"
#include "execution.h"
#include "boolFct.h"



void advanceTransitionPointers(ptStackElt current);
void updateNextStateFeatureAlgebra(ptMTypeNode mtypes, 	ptState s_, ptHtState prevS_, bool enableTracing);
ptState standardInitForDFS();
void AppplyCombinedExecution(ptSymTabNode globalSymTab,
		ptMTypeNode mtypes,
		ptState *s_,
		ptStackElt current,
		ptList *E,
		ptBoolFct *noOutgoing,
		byte *resetExclusivity,
		byte *error,
		byte *assertViolation,
		byte *hasDeadlock);
void AppplyCombinedExecutionNoNever(ptSymTabNode globalSymTab,
		ptMTypeNode mtypes,
		ptState *s_,
		ptStackElt current,
		ptList *E,
		ptBoolFct *noOutgoing,
		byte *resetExclusivity,
		byte *error,
		byte *assertViolation,
		byte *hasDeadlock);

/**
 * Destroys a stack element. It means that the two full transition lists
 * will be destroyed, so will the pointer to the stack element itself.
 *
 * State, Payload and features are NOT destroyed.
 */
void destroyStackElementKeepState(ptStackElt elt, byte process_or_direct);





#endif /* ALGORITHMS_SYMBOLIC_DFS_HELPERS_H_ */
