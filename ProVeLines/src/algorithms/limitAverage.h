/*
 * limitAverage.h
 *
 *  Created on: Aug 20, 2015
 *      Author: rafaelolaechea
 */

#ifndef ALGORITHMS_LIMITAVERAGE_H_
#define ALGORITHMS_LIMITAVERAGE_H_

#include "wrapper/features/boolFct.h"
#include <stdbool.h>


/*
 * Standard Initialization including initial state, check for never claim, hashtables.
 */
ptState standardInitForDFS();

/*
 *
 * Compute partial injective Function F: S x B(N) -> N representing symbolic finishing times.
 *@ props, globalSymTab, mtypes  -- Parsed Promela Program  + Optional Never (LTL) state.
 *
 */
void startSymbolicDepthFirstSearch(ptList props, ptSymTabNode globalSymTab, ptMTypeNode mtypes);




#endif /* ALGORITHMS_LIMITAVERAGE_H_ */
