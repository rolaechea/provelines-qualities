/*
 * symbolic-dfs-helpers.c
 *
 *  Created on: Aug 28, 2015
 *      Author: rafaelolaechea
 */

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <stdbool.h>

#include "hashtableState.h"
#include "main.h"
#include "stack.h"
#include "boolFct.h"
#include "execution.h"
#include "symbols.h"

#include "symbolic-dfs-helpers.h"


byte _nbErrorsLocal = 0;

/*
 * Simulate nested loop: when at the end of E, restart the E and advance the E_never
 * The deadlock test (E empty) is already done.  This also guarantees that E->value
 * is never NULL in the above apply(globalSymTab, mtypes, ).
 *
 *
 *Two Cases  - with LTL property or without LTL Property
 */
void advanceTransitionPointers(ptStackElt current){

	current->E = current->E->next;
}

/*
 *
 * updateNextStateFeatureAlgebra - Updates the values of s_->features  depending on value prev_.
 *
 * 	If prevS_ is null then s_-> is not touched.
 *
 * 	If prevS_ is not null then:
 * 			s->features  =  	s->features ^ ~ Gray/Black[s]
 *		And
 *			Gray/Black[s]  == s->features v  Gray/Black[s]
 *
 */
void updateNextStateFeatureAlgebra(ptMTypeNode mtypes, 	ptState s_, ptHtState prevS_, bool enableTracing){
	if (prevS_){
		/* State visited with f', but not current->f -> f'						 *
		 * Aka  current->f & ~f' is satisfiable.
		 * Aka PX(f) difference PX(f') is not empty
		 *
		 *	 Implicit Update WhiteOrGray Hashtable via prevS_
		 *
		 */
		ptBoolFct negPrev = negateBool(prevS_->outerFeatures);


		if(s_->features == NULL) /* current->f == True */ {
			if(enableTracing){
				printf("prevS_->outerFeatures = ");
				printBool(prevS_->outerFeatures);
				printf(" to become true \n");
			}

			destroyBool(prevS_->outerFeatures);
			prevS_->outerFeatures = getTrue();


		} else {
			if(enableTracing){
				printf("prevS_->outerFeatures = ");
				printBool(prevS_->outerFeatures);
				printf(" to become  ");

				ptBoolFct tmpDisjunction = addDisjunction(prevS_->outerFeatures, s_->features, 0, 1);

				printBool(tmpDisjunction);
				printf("\n");
				prevS_->outerFeatures = tmpDisjunction;

			} else {

				ptBoolFct tmpDisjunction = addDisjunction(prevS_->outerFeatures, s_->features, 0, 1);
				prevS_->outerFeatures = tmpDisjunction;


			}
		}
		/* Aka WhiteOrGray[s] v= ~WhiteOrGray[s] ^ Feature
		 * If features == True (NULL) then WhiterorGray v= ~WhiteOrGray[s] = True
		 *  */
		s_->features = addConjunction(negPrev, s_->features, 0, 0);
		free(s_->payload);
		s_->payload = prevS_->payload;

	}else{
		/* Explicit Update WhiteOrGray Hashtable */
//		printf("SEarch on s_ gave prevS == NULL and False ... %d ", prevS_);
		/* State not visited at all */
		// - No Longer Using
		// htVisitedStatesInsert(s_->payloadHash, s_, DFS_OUTER);
//		printf("Adding s_ to hastable BlackOrGrey, with payloadHash of %d \n", s_->payloadHash);
//		printState(mtypes,  s_, NULL);
		htBlackOrGreyInsert(s_->payloadHash, s_);
	}

}





/*
 *
 * Perform state initialization for standard DFS, including:
 * 		Initialized FD.
 * 		Initialize Starting State.
 * 		Hash tables (Outer and Visited ) initialization
 * 		Check that never state has been initialized
 *
 */
ptState standardInitForDFS(ptSymTabNode globalSymTab, ptMTypeNode mtypes){
	printf("Initializing FD \n");
	initSolverWithFD(getFeatureModelClauses());
	printf("End Initializing FD \n");

	/* initializing Hashtable */
	printf("Initializing Visiting FD \n");
	htVisitedStatesInit();
	htOuterStatesInit();
	printf("Initializing Black/Grey \n");
	htBlackOrGreyInit();



	/*
	 * Initialize Black/Grey hasthable
	 *
	 */

	// Create initial state

	printf("Initializing Init State \n");
	ptState init = stateCreateInitial(globalSymTab, mtypes);

	printf("Return Init State \n");
	init->payloadHash = hashState(init);

	/*
	htVisitedStatesInsert(init->payloadHash, init, DFS_OUTER);
	htOuterStatesInsert(init->payloadHash, init);
	*/

	printf("Black or Grey Insert \n");

	htBlackOrGreyInsert(init->payloadHash, init);


	return init;
}


/*
 * Applies Execution
 * 	 Returns Updated next State, including hash.
 * 	 Returns Updated List of Executables E.
 */
void AppplyCombinedExecution(ptSymTabNode globalSymTab,
		ptMTypeNode mtypes,
		ptState *s_,
		ptStackElt current,
		ptList *E,
		ptBoolFct *noOutgoing,
		byte *resetExclusivity,
		byte *error,
		byte *assertViolation,
		byte *hasDeadlock){

	*s_ = apply(globalSymTab, mtypes, current->state, (ptProcessTransition) current->E->value, 1, assertViolation);

	*s_ = applyNever(globalSymTab, mtypes, (*s_), (ptFsmTrans) current->E_never->value);


	/* Update Executables */
	*E = executables(globalSymTab, mtypes, (*s_), 1, _nbErrorsLocal, hasDeadlock, NULL, NULL, noOutgoing, resetExclusivity);


	/* Start -  I don't know why  */
	if(*resetExclusivity) {
		stateSetValue((*s_)->payload, OFFSET_EXCLUSIVE, T_BYTE, NO_PROCESS);
	}
	/* End = I don't know why */

	(*s_)->payloadHash = hashState(*s_);

}


/*
 * Applies Execution
 * 	 Returns Updated next State, including hash.
 * 	 Returns Updated List of Executables E.
 */
void AppplyCombinedExecutionNoNever(ptSymTabNode globalSymTab,
		ptMTypeNode mtypes,
		ptState *s_,
		ptStackElt current,
		ptList *E,
		ptBoolFct *noOutgoing,
		byte *resetExclusivity,
		byte *error,
		byte *assertViolation,
		byte *hasDeadlock){


	*s_ = apply(globalSymTab, mtypes, current->state, (ptProcessTransition) current->E->value, 1, assertViolation);


	/* Update Executables */
	*E = executables(globalSymTab, mtypes, (*s_), 1, _nbErrorsLocal, hasDeadlock, NULL, NULL, noOutgoing, resetExclusivity);

	/* Start -  I don't know why  */
	if(*resetExclusivity) {
		stateSetValue((*s_)->payload, OFFSET_EXCLUSIVE, T_BYTE, NO_PROCESS);
	}
	/* End = I don't know why */

	(*s_)->payloadHash = hashState(*s_);

}


/**
 * Destroys a stack element. It means that the two full transition lists
 * will be destroyed, so will the pointer to the stack element itself.
 *
 * State, Payload and features are NOT destroyed.
 */
void destroyStackElementKeepState(ptStackElt elt, byte process_or_direct) {
	if(elt) {
		if(elt->E_never_save) {
			listDestroy(elt->E_never_save);
		}
		if(elt->E_save) {
			destroyProcTransList(elt->E_save, process_or_direct);
		}
		free(elt);
	}
}
