/*
 * mean-cycles.h
 *
 *  Created on: Sep 30, 2015
 *      Author: rafaelolaechea
 */

#ifndef ALGORITHMS_MEAN_CYCLES_H_
#define ALGORITHMS_MEAN_CYCLES_H_

#include <stdio.h>
#include <stdlib.h>
#include "mean-cycle-structure.h"
#include "../lib/clark.hashtable/hashtable.h"
#include "../wrapper/hash/hashtableState.h"



void computeMeanCycle(ptMTypeNode mtypes);
void printSCCAllStatesAndFeatureExpression(struct hashtable * sccComponent);
void computeSCCLimitAverage(struct hashtable * sccComponent, ptState state);

#endif /* ALGORITHMS_MEAN_CYCLES_H_ */
