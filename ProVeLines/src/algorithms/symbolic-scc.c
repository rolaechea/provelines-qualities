/*
 * symbolic-scc.c
 *
 *  Created on: Sep 4, 2015
 *      Author: rafaelolaechea
 */
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <stdbool.h>

#include "stack.h"

#include "finishing-times-tree.h"
#include "finishing-times-adt.h"

#include "symbolic-scc.h"

#include "../lib/clark.hashtable/hashtable.h"
#include "../wrapper/hash/hashtableState.h"
#include "transpose-executables.h"
#include "abstractTransitionsMapping.h"
#include "abstractSCCTree.h"



void setSymbolicComponent(ptSymbolicNode TreeNode, struct hashtable* SccComponent);
void setSymbolicComponentTree(ptSymbolicNode  TreeNode, ptAbstractSCCTree  ptTreeSccComponent);
ptSymbolicNode getUnvisitedChildren(ptSymbolicNode treeNode);

struct hashtable*  getCombinedSCCUpToNode(ptSymbolicNode TreeNode);
void setCombinedSCCUpToNode(ptSymbolicNode TreeNode, struct hashtable* mergedExclude);



/*
 *
 * Creates a stack element from a state and a feature expression.
 * It also looks up outgoing edges for the state in the transpose hashtable.
 *
 */
ptStackEltTranspose createSccStackElement(ptState state, ptBoolFct features){
	ptStackEltTranspose elt = (ptStackEltTranspose) malloc(sizeof(tStackEltTranspose));
	if(!elt)
		failure("Out of memory (creating SCC stack element).\n");

	elt->state = state;
	elt->features =  features;

	ptHtState transposeState = NULL;

	if( htFindNoFeatureCheck(state->payloadHash, state, &transposeState, getTransposeHasthable())){
		if(transposeState != NULL){
			elt->OutgoingLinksTranspose = transposeState->transposeLinks;
			elt->OutgoingLinksTransposeCurrent = elt->OutgoingLinksTranspose;

		}else {
			elt->OutgoingLinksTranspose = NULL;
			elt->OutgoingLinksTransposeCurrent = NULL;

		}

	} else {
		elt->OutgoingLinksTranspose = NULL;
		elt->OutgoingLinksTransposeCurrent = NULL;
	}

	return elt;
}






void NewReachabilityExcludeAddOrUpdate(ptState ptDestinationState, ptBoolFct  conjuctedFeatures,
				struct hashtable* originalReachabilityExclude, struct hashtable* FInalRExclude,
				bool mustExistAndBeCopy);

/*
 *
 * It incrementally creates RFinal =  RPrevious v NewSCC.
 *
 *
 *
 *Precondition
 *	if mustExistAndBeCopy = true then
 *		FInalRExclude(s) is not null and hash state already been copied such that
 *		 FInalRExclude(s)  OriginalRExclude(s) are not shared.
 */
void NewReachabilityExcludeAddOrUpdate(ptState ptDestinationState, ptBoolFct  conjuctedFeatures,
				struct hashtable* originalReachabilityExclude, struct hashtable* FInalRExclude,
				bool mustExistAndBeCopy){
	ptHtState foundState = NULL;
	ptHtState foundStateOriginal = NULL;
	ptHtState foundStateCopy = NULL;

	if(mustExistAndBeCopy == true ){
		// Look up in FInalRExclude
		if(htAbstractFind(ptDestinationState->payloadHash,
					ptDestinationState,
					conjuctedFeatures,
					&foundState,
					FInalRExclude)){
					// ptDestinationState, conjuctedFeatures adds nothing new to FInalRExclude.
					// PX(conjuctedFEatures) subset of PX(FInalRExclude(ptDestinationState))
				} else {
					if(foundState == NULL){
						//precondition failed -- FInalRExclude hasn't been udpated properly - bug.

						failure("Incorrect updating of FInalRExclude, when passed state with payload-hash = %u ",
								ptDestinationState->payloadHash );
					} else {
						// ptDestinationState already in FInalRExclude
						// -- Just have to  update (e.g. it already is a copy).
						if(conjuctedFeatures == NULL) {
							// Can't do this as copy is shallow  - (Hence memory leaks)
							//destroyBool(existingDestinationInScc->outerFeatures);
							foundState->outerFeatures = getTrue();
						} else {
							foundState->outerFeatures = \
									addDisjunction(foundState->outerFeatures,
									conjuctedFeatures, 1, 1);

							#ifdef CHECK_TAUTOLOGY
								if(foundState->outerFeatures && isTautology(foundState->outerFeatures)) {
									// Can't do this as copy is shallow  - (Hence memory leaks)
									//destroyBool(existingDestinationInScc->outerFeatures);
									foundState->outerFeatures = getTrue();
								}
							#endif
						}
					}
				}
	} else {
//		printf("Searching at mustExist=False with payloadHash %u ", ptDestinationState->payloadHash);
//		printBool(conjuctedFeatures);
//		printf("\n");
		if(htAbstractFind(ptDestinationState->payloadHash,
				ptDestinationState,
				conjuctedFeatures,
				&foundState,
				FInalRExclude)){
//				printf("htAbstractFind returned true \n");
				// ptDestinationState, conjuctedFeatures adds nothing new to FInalRExclude.
				// PX(conjuctedFEatures) subset of PX(FInalRExclude(ptDestinationState))
			} else {
//				printf("htAbstractFind returned false \n");
				if(foundState == NULL){

//					printf("foundState == NULL - Simple Insert \n");
					// ptDestinationState not in FInalRExclude, hence can do simple insert.

//					printf("Before Performing Simple Insert at FInalRExclude \n ");
//
//					printHashTableInnerDetails(originalReachabilityExclude);
//
//					printf("Printed Before Performing Simple Insert at FInalRExclude %u \n", ptDestinationState->payloadHash);

					//htAbstractInsertExplicitFeatures(ptDestinationState->payloadHash,
					//		ptDestinationState,
					//		conjuctedFeatures, FInalRExclude);

					htAbstractInsertExplicitFeaturesDebug(FInalRExclude,
							ptDestinationState->payloadHash,
							ptDestinationState,
							conjuctedFeatures,
							originalReachabilityExclude);


//					printf("After  Performing Simple Insert at FInalRExclude \n");
//
//					printHashTableInnerDetails(originalReachabilityExclude);
//
//					printf("Printed After Performing Simple Insert at FInalRExclude \n");

				} else {
//					printf("foundState != NULL - Complex Insert \n");
					// ptDestinationState is in FInalRExclude but might be shared with originalRExclude.
					// -- Might have to perform a copy or just an update.
					if(htAbstractFind(ptDestinationState->payloadHash,
							ptDestinationState,
							conjuctedFeatures,
							&foundStateOriginal,
							originalReachabilityExclude)){
						// conjuctedFeatures adds nothing to  originalReachabilityExclude
						// but adds some information to FInalRExclude.
						// -- Problem as PX(FInalRExclude(s)) should be a superset of PX(originalReachabilityExclude(s))
						failure(" FInalRExclude(s) is expanded by conjuctedFeatures but not originalReachabilityExclude(s), state-payload-hash = %u ",
								ptDestinationState->payloadHash);
					}else {
						if(foundStateOriginal == NULL){
							// ptDestinationState not in ROriginal hence can do simple update.
							if(conjuctedFeatures == NULL) {
								// Can't do this as copy is shallow  - (Hence memory leaks)
								//destroyBool(existingDestinationInScc->outerFeatures);
								foundState->outerFeatures = getTrue();
							} else {
								foundState->outerFeatures = \
										addDisjunction(foundState->outerFeatures,
										conjuctedFeatures, 1, 1);

								#ifdef CHECK_TAUTOLOGY
									if(foundState->outerFeatures && isTautology(foundState->outerFeatures)) {
										// Can't do this as copy is shallow  - (Hence memory leaks)
										//destroyBool(existingDestinationInScc->outerFeatures);
										foundState->outerFeatures = getTrue();
									}
								#endif
							}

						} else {
							// must check if foundStateOriginal == foundState
							// if so then must do a copy.
							if(foundStateOriginal == foundState){
								// must perform copy
								htAbstractReplaceByCopy(ptDestinationState->payloadHash,
										ptDestinationState,
										conjuctedFeatures,
										&foundStateCopy,
										FInalRExclude);

								// can now just do simple update (on foundStateCopy).

								if(conjuctedFeatures == NULL) {
									// Can't do this as copy is shallow  - (Hence memory leaks)
									//destroyBool(existingDestinationInScc->outerFeatures);
									foundStateCopy->outerFeatures = getTrue();
								} else {
									foundStateCopy->outerFeatures = \
											addDisjunction(foundStateCopy->outerFeatures,
											conjuctedFeatures, 1, 1);

									#ifdef CHECK_TAUTOLOGY
										if(foundStateCopy->outerFeatures && isTautology(foundStateCopy->outerFeatures)) {
											// Can't do this as copy is shallow  - (Hence memory leaks)
											//destroyBool(existingDestinationInScc->outerFeatures);
											foundStateCopy->outerFeatures = getTrue();
										}
									#endif
								}

							} else {
								// Can just do simple update.
								if(conjuctedFeatures == NULL) {
									// Can't do this as copy is shallow  - (Hence memory leaks)
									//destroyBool(existingDestinationInScc->outerFeatures);
									foundState->outerFeatures = getTrue();
								} else {
									foundState->outerFeatures = \
											addDisjunction(foundState->outerFeatures,
											conjuctedFeatures, 1, 1);

									#ifdef CHECK_TAUTOLOGY
										if(foundState->outerFeatures && isTautology(foundState->outerFeatures)) {
											// Can't do this as copy is shallow  - (Hence memory leaks)
											//destroyBool(existingDestinationInScc->outerFeatures);
											foundState->outerFeatures = getTrue();
										}
									#endif
								}
							}

						}
					}
				}
			}
	}

}


/*
 * Returns the number of states in an SCC that don't have features = bottom
 *
 */
int countStatesInScc(struct hashtable * sccComponent){
	return 0;
}


/*
 * Checks if a stack element has more transitions that haven't been explored.
 */
bool moreTransposeTransitionsAvailable(ptStackEltTranspose CurrentElt){

	bool NullCheck =  CurrentElt->OutgoingLinksTransposeCurrent != NULL;

	return NullCheck;
}


/*
 *
 * Get a children of treeNode that hasn't been "visited".
 *
 * If no such unvisited children remain, then returns NULL.
 */
ptSymbolicNode getUnvisitedChildren(ptSymbolicNode treeNode){
	ptEdgeListSymbolicNode listChildren = treeNode->ListOfOutgoingEdges;
	while(listChildren != NULL){
		if (!getNodeVisited(listChildren->destination)){
			return listChildren->destination;
		} else {
			listChildren = listChildren->next;
		}
	}

	return NULL;
}

void computeSymbolicScc(){
	if(!silent)
		printf("-- START computeALLSymbolicScc");

	ptSymbolicNode rootT = getSymbolicTreeRoot();
	ptEdgeListSymbolicNode currentRootChildren = rootT->ListOfOutgoingEdges ;

	ptStack NodesToExploreStack = getEmptyStack();

	ptStack ReachabilityStack = 	getEmptyStack();

	bool firstPop = true;

	while(hasMoreChildren(rootT, currentRootChildren)){

		ptSymbolicNode u = getNextChildren(rootT, currentRootChildren);
		if(!silent){
			printf(" --  A path has root, u with feature expression \n");
			printBool(u->ToFatherfeatures);
			printf("\n -- and state payload = %u ... \n", u->state->payloadHash);
		}


//		printf(" --  A path has root, u with feature expression \n");
//		printBool(u->ToFatherfeatures);
//		printf("\n -- and state payload = %u ... \n", u->state->payloadHash);


		struct hashtable* ReachabilityExclude =	create_hashtableWrapper();

		ptTripletTuple RootChildTriplet = createTripletTuple(u,  u->state,  u->ToFatherfeatures);


		NodesToExploreStack = push(NodesToExploreStack, RootChildTriplet);

		ReachabilityStack = push(ReachabilityStack, ReachabilityExclude);


		while(! empty(NodesToExploreStack)){
			struct hashtable* SccComponent = NULL;
			struct hashtable* ReachabilityExcludeCurrent = NULL;

			ptTripletTuple currentNode = (ptTripletTuple) top(NodesToExploreStack); // peek

			ReachabilityExcludeCurrent = (struct hashtable*) top(ReachabilityStack);

//			if(!silent)
//				printf("Top of computeSymbolicScc Stack has %u \n", currentNode->stateLabel->payloadHash);

			ptHtState prevS = NULL;

			bool createdNewComponent = false;

			struct hashtable* mergedExclude = NULL;

			if(getNodeVisited(currentNode->TreeNode)==false){
				// Check if RExclude contains currentFeatures, CurrentState
				// If it doesn't contain it --
				// Either doesn't contain it at all
				// or contains with it less features .
//				printf("Checking if we start  SCC Component  search with  =%u \n  ", currentNode->stateLabel->payloadHash);
				if(htAbstractFind(currentNode->stateLabel->payloadHash,
						currentNode->stateLabel,
						currentNode->edgeLabel,
						&prevS,
						ReachabilityExcludeCurrent)){
					/* State already visited with f'  f' -> current->f -
					 * Aka PX(f) subset of PX(f') -
					 * skip */
//					printf("Skipping Searching for an SCC Component  as excluded (from previous SCC)\n \t tried to start search with  =%u products =  ", currentNode->stateLabel->payloadHash);
//					printBool(currentNode->edgeLabel);
//					printf("\n");
					mergedExclude = ReachabilityExcludeCurrent;
				} else {
					if(prevS == NULL) {
						// * Completely Fresh
						// Aka S not an element of RExclude - */
						///printf("Searching for SCC Component of a clean Item \n");

						ptAbstractSCCTree tmpRootTree = NULL;

						SccComponent = SymbolicSCCReachability(
								currentNode->stateLabel,
								currentNode->edgeLabel,
								ReachabilityExcludeCurrent,
								&mergedExclude,
								&tmpRootTree);

						setSymbolicComponent(currentNode->TreeNode, SccComponent);
						setSymbolicComponentTree(currentNode->TreeNode, tmpRootTree);

						if(!silent)
							printSCCAllStatesAndFeatureExpression(SccComponent);

					} else {
						//  not completely fresh,
						// have to combine edgeLabel with ~RExclude(s')

						ptAbstractSCCTree tmpRootTree = NULL;

						//printf("Searching for SCC Component of a dirty Item \n");
						SccComponent = SymbolicSCCReachability(
									currentNode->stateLabel,
									addConjunction(currentNode->edgeLabel,
										negateBool(prevS->outerFeatures),
										1,1),
										ReachabilityExcludeCurrent,
										&mergedExclude,
										&tmpRootTree);

						setSymbolicComponent(currentNode->TreeNode, SccComponent);
						setSymbolicComponentTree(currentNode->TreeNode, tmpRootTree);

							if(!silent)
								printSCCAllStatesAndFeatureExpression(SccComponent);


					}


					createdNewComponent = true;
					// Create New  Reachability Exclude to push into Reachability Exclude Stack
				}

				setNodeVisited(currentNode->TreeNode);
				setCombinedSCCUpToNode(currentNode->TreeNode, mergedExclude);

			} else {

				mergedExclude = getCombinedSCCUpToNode(currentNode->TreeNode);
			}




			ptSymbolicNode v = getUnvisitedChildren(currentNode->TreeNode);

			if (v == NULL){
			//	printf("Popping Stack - SCC and Reachability \n");
				if(firstPop == true){
					//only for debugging
			//		printf("Finished Exploring first path to a leaf node \n");
					firstPop = false;
				}
				pop(&NodesToExploreStack);
				pop(&ReachabilityStack);

			} else {
//				printf("Considering Next Children\n");
				ptTripletTuple nextTriplet = createTripletTuple(v,
											v->state,
											addConjunction(currentNode->edgeLabel,
															v->ToFatherfeatures, 1,1));

				NodesToExploreStack = push(NodesToExploreStack, nextTriplet);

				ReachabilityStack = push(ReachabilityStack, mergedExclude);

			}
		} // End While (!StackIsEmpty)

		currentRootChildren = currentRootChildren->next;
	} // End While HasMoreChildren

	if(!silent)
		printf("-- End computeALLSymbolicScc");

}

ptTripletTuple createTripletTuple(ptSymbolicNode Node, ptState stateLabel, ptBoolFct edgeLabel){

	ptTripletTuple newTriplet = (ptTripletTuple) malloc(sizeof(tTripletTuple));

	if(!newTriplet)
		failure("Out of memory (creating Triplet).\n");

	newTriplet->TreeNode = Node;
	newTriplet->stateLabel = stateLabel;
	newTriplet->edgeLabel = edgeLabel;

	return newTriplet;
}


void printDebugInfoSCCComputation(ptState state,
		ptBoolFct stateFeatureExpression,
		struct hashtable*  ReachabilityExclude);

void printDebugInfoSCCComputation(ptState state,
		ptBoolFct stateFeatureExpression,
		struct hashtable*  ReachabilityExclude){
	// Start initial state
	printf("Computing SCC, starting from  state=%u \n" , state->payloadHash);
	printf("And Boolean features ... \n");
	printBool(stateFeatureExpression);

	ptHtState sourceStateTranspose;

	ptHtState prevS_;

	if( htFindNoFeatureCheck(state->payloadHash, state, &prevS_, getTransposeHasthable())){
		printf("\n Start Listing Outgoing Edges \n");
		ptList OutgoingLinksTranspose = prevS_->transposeLinks;
		while(OutgoingLinksTranspose != NULL){
			ptTransAndFExp outgoingLink = (ptTransAndFExp) OutgoingLinksTranspose->value;
			printf("\t\t Outgoing Link to %u " ,  outgoingLink->State->payloadHash);
			printf(" with features ");
			printBool(outgoingLink->features);
			printf("\n");
			OutgoingLinksTranspose = OutgoingLinksTranspose->next;
		}
		printf("End Listing Outgoing Edges \n");
	} else {
		printf("\nHas no Outgoing Edges \n");
	}



	printf("Finished Debugging SCC\n");
}

/*
 *
 * Given a state, a feature expression and a Reachability Exclude,
 *  will search for the symbolic states that can be reached in the transpose  graph.
 *
 *Precondition:
 *	state, stateFeatureExpression are not an element of ReachabilityExclude.
 *		Assured by a check before calling SymbolicSCCReachability.
 *
 *PostCondition -- a new Reachability Exclude Component is updated.
 */
struct hashtable* SymbolicSCCReachability(ptState state,
		ptBoolFct stateFeatureExpression,
		struct hashtable*  ReachabilityExclude,
		struct hashtable**  newReachabilityExclude,
		ptAbstractSCCTree  * ptRetTree) {

	if(!silent )
		printf("At SymbolicSCCReachability \n");


	//printDebugInfoSCCComputation(state, stateFeatureExpression, ReachabilityExclude);
	/* Perform a depth first search reachability ... */
	//printf("Start DFS to Compute a Symbolic SCC \n");

	struct hashtable* ptSymbolicSCC = create_hashtableWrapper();


	htAbstractInsertExplicitFeatures(state->payloadHash, state,
			stateFeatureExpression, ptSymbolicSCC);


//	printf("Inserting state with state->payloadHash %u, and features  ");
//	printBool(stateFeatureExpression);
//	printf(" \n");
//
//	printf("Creating related Tree \n ");
	ptAbstractSCCTree rootTree = createEmptyAbstractSCC();
//	printf("Creating related Tree \n ");
	UpdateAbstractSCC(rootTree, stateFeatureExpression, state );
//	printf("Finished creating related Tree \n ");




	ptStack SccStack = getEmptyStack();


	// Initialize stack element.
	ptStackEltTranspose eltSCC = partialOrderReduction ? createSccStackElementAbstract(state, stateFeatureExpression)  :
			createSccStackElement(state, stateFeatureExpression);

	//printf("Adding to SCC-DFS Stack init  with payloadHash  %u, products  ", eltSCC->state->payloadHash);
	//printBool(eltSCC->features);
	//printf("\n");

	SccStack = push(SccStack, eltSCC);


	struct hashtable* finalReachabilityExclude = shallowHashTableCopy(ReachabilityExclude);


	/*
	 * Add state, stateFeatureExpression into ReachabilityExclude.
	 */
	NewReachabilityExcludeAddOrUpdate(state, stateFeatureExpression, ReachabilityExclude, finalReachabilityExclude, false);

//	printf(" Reachability Exclude Details \n");
//
//	printHashTableInnerDetails(ReachabilityExclude);

	while(!empty(SccStack)){
		ptStackEltTranspose CurrentElt = (ptStackEltTranspose) top(SccStack);

//		printf("Looping with %u \n ", CurrentElt->state->payloadHash);
//
//		printf(" Reachability Exclude Details  in Loop \n");
//		printHashTableInnerDetails(ReachabilityExclude);
//		printf("Printed ReachabilityExclude \n");

		if(!moreTransposeTransitionsAvailable(CurrentElt)){
			//printf("No more transitions available with %u \n", CurrentElt->state->payloadHash);
			pop(&SccStack);
			free(CurrentElt);
		} else {
			// 	 Process next transition
			ptList OutgoingLinksTranspose = CurrentElt->OutgoingLinksTransposeCurrent;

			ptTransAndFExp outgoingLink = (ptTransAndFExp) OutgoingLinksTranspose->value;

			ptBoolFct conjuctedFeatures = addConjunction(CurrentElt->features,
													outgoingLink->features, 1, 1);

			ptHtState existingDestinationInScc;
			ptHtState existingDestinationInReachabilityExclude;


//			printf("Checking is Satisfiable for %u  and %u features \n", CurrentElt->state->payloadHash,
//					outgoingLink->State->payloadHash);

			if(isSatisfiable(conjuctedFeatures)){

//				printf("Executing Abstract Find at destination %u \n", outgoingLink->State->payloadHash);
				if(htAbstractFind(outgoingLink->State->payloadHash,
						outgoingLink->State,
						conjuctedFeatures,
						&existingDestinationInReachabilityExclude,
						ReachabilityExclude)){
//					printf("Abstract Find ReachabilityExclude at destination %u returned true. \n", outgoingLink->State->payloadHash);
					// State already REx(s) ->  conjunctedFeatures excluded
					// PX(conjuctedFEatures) subset of PX(REx(s))
				} else {
					//
//					printf("Abstract Find ReachabilityExclude at destination %u returned false. \n", outgoingLink->State->payloadHash);

					bool notInRExcludeAtAll;
					notInRExcludeAtAll = false;

					if(existingDestinationInReachabilityExclude == NULL){
						// destinationS not in Reachability Exclude  --> proceed as normal.
						notInRExcludeAtAll = true;
						// Update NewReachabilityRelation by inserting new element with PX(ConjuctedFeatures)
						// for R(destinationD)
					} else {
						// destination S in Reachability Exclude -->
						// proceed with conjuctedFeatures ^ ~ ReachabilityExclude

						conjuctedFeatures = addConjunction(conjuctedFeatures,
								negateBool(existingDestinationInReachabilityExclude->outerFeatures),
								1,
								1
								);

					}

					if(htAbstractFind(outgoingLink->State->payloadHash,
							outgoingLink->State,
							conjuctedFeatures,
							&existingDestinationInScc,
							ptSymbolicSCC)){
						/* State already visited with f'  f' -> current->f -	Aka PX(f) subset of PX(f') - 	skip */
					}else {
						ptStackEltTranspose eltToAdd = NULL;

						if(existingDestinationInScc == NULL){
							// First time in SCC -- will insert.


							// UPDATE TREE I
							htAbstractInsertExplicitFeatures(outgoingLink->State->payloadHash,
									outgoingLink->State,
									conjuctedFeatures, ptSymbolicSCC);

							UpdateAbstractSCC(rootTree, conjuctedFeatures, outgoingLink->State );


							eltToAdd = partialOrderReduction ? createSccStackElementAbstract(outgoingLink->State, conjuctedFeatures) :
									createSccStackElement(outgoingLink->State, conjuctedFeatures);


							// Adding an element to shallow copy.
							// Two Cases either RExclude(s') ^ originalConjunction is empty
							// or RExclude('s) has been diffed from originalConjunction
							// Case 1 -- Simple Insert into new copy of RExclude.
							// Case 2 -- Create new element and remove old one.

//							printf("Will Execute NewReachabilityExcludeAddOrUpdate existingDestinationInScc==NULL \n ");
//							printHashTableInnerDetails(ReachabilityExclude);
//							printf("Right Before \n");


//							printf("AddOrUpdate with PayloadHash = %u ", outgoingLink->State->payloadHash);

							NewReachabilityExcludeAddOrUpdate(outgoingLink->State, conjuctedFeatures,
									ReachabilityExclude, finalReachabilityExclude, false);

//							printf("Right After NewReachabilityExcludeAddOrUpdate  existingDestinationInScc==NULL  \n ");
//							printHashTableInnerDetails(ReachabilityExclude);
//							printf("Right After NewReachabilityExcludeAddOrUpdate existingDestinationInScc==NULL  End  \n");


						} else {
							ptBoolFct negPrev = negateBool(existingDestinationInScc->outerFeatures);

							// UPDATE TREE II

							if(conjuctedFeatures == NULL) {
								destroyBool(existingDestinationInScc->outerFeatures);
								existingDestinationInScc->outerFeatures = getTrue();

								UpdateAbstractSCC(rootTree, getTrue(), outgoingLink->State );
							} else {
								existingDestinationInScc->outerFeatures = \
										addDisjunction(existingDestinationInScc->outerFeatures,
										conjuctedFeatures, 1, 1);



								#ifdef CHECK_TAUTOLOGY
									if(existingDestinationInScc->outerFeatures && isTautology(existingDestinationInScc->outerFeatures)) {
										//destroyBool(existingDestinationInScc->outerFeatures);
										existingDestinationInScc->outerFeatures = getTrue();
										UpdateAbstractSCC(rootTree, getTrue(), outgoingLink->State );
									} else {
										UpdateAbstractSCC(rootTree, conjuctedFeatures, outgoingLink->State );
									}
								#else
									UpdateAbstractSCC(rootTree, conjuctedFeatures, outgoingLink->State );
								#endif
							}

							// Modifying  an element in shallow copy.
							// It must already exists and be a copy as it was already added to SCC previously.
//							printf("Will Execute NewReachabilityExcludeAddOrUpdate existingDestinationInScc !=NULL \n ");
//							printHashTableInnerDetails(ReachabilityExclude);
//							printf("Right Before \n");

							NewReachabilityExcludeAddOrUpdate(outgoingLink->State, conjuctedFeatures,
									ReachabilityExclude, finalReachabilityExclude, true);

//							printf("Right After NewReachabilityExcludeAddOrUpdate existingDestinationInScc !=NULL \n ");
//							printHashTableInnerDetails(ReachabilityExclude);
//							printf("Right After  NewReachabilityExcludeAddOrUpdate existingDestinationInScc !=NULL  End  \n");


							eltToAdd =  partialOrderReduction ?  createSccStackElementAbstract(outgoingLink->State,
									addConjunction(negPrev, conjuctedFeatures , 0, 1)) :
									createSccStackElement(outgoingLink->State,
											addConjunction(negPrev, conjuctedFeatures , 0, 1));

						} // End-if if(existingDestinationInScc == NULL)

						//printf("Adding to SCC-DFS stack  with payloadHash  %u to explore SCC  with products  ", eltToAdd->state->payloadHash);
						//printBool(eltToAdd->features);
						//printf("\n");

						SccStack = push(SccStack, eltToAdd);

					} // End-else htSCCFind


				} // End-if htAbstractFind ( ... ReachabilityExclude)

			} else {
//				printf("\n ");
//				printBool(conjuctedFeatures);
//				printf(" is not satisfiable \n");
			}



			// Advance Transition List
			CurrentElt->OutgoingLinksTransposeCurrent = CurrentElt->OutgoingLinksTransposeCurrent->next;

		} // End-else moreTransitionsAvailable
	} // End- While

//	printf("Finished Computing a Symbolic SCC \n");

	*newReachabilityExclude = finalReachabilityExclude;

	*ptRetTree = rootTree;

	return ptSymbolicSCC;
}


/*
 *
 *
 * Use a hashtable of state to list of <state, list of  <F, W>, SupersetF >.
 *
 *
 *Keep track in abstractELTStack of of such list <state, list of  <F, W>, SupersetF > for each element.
 *
 *
 *Perform depth first search on that.
 *
 */
struct hashtable* SymbolicSCCReachabilityWithAbstractTransitions(ptState state,
		ptBoolFct stateFeatureExpression,
		struct hashtable*  ReachabilityExclude,
		struct hashtable**  newReachabilityExclude){

}


void setCombinedSCCUpToNode(ptSymbolicNode TreeNode, struct hashtable* mergedExclude){
	 TreeNode->ptSccUnion = mergedExclude;
}

struct hashtable*  getCombinedSCCUpToNode(ptSymbolicNode TreeNode){
	return TreeNode->ptSccUnion;
}


void setSymbolicComponent(ptSymbolicNode TreeNode, struct hashtable* SccComponent){
	TreeNode->hasSymbolicSccComponent = true;
	TreeNode->ptSccComponent = SccComponent;

}



struct hashtable* getSymbolicComponent(ptSymbolicNode TreeNode){
	return TreeNode->ptSccComponent;
}

ptAbstractSCCTree getSymbolicComponentTree(ptSymbolicNode TreeNode){
	return TreeNode->ptTreeSccComponent;
}

void setSymbolicComponentTree(ptSymbolicNode  TreeNode, ptAbstractSCCTree  ptTreeSccComponent){
	TreeNode->ptTreeSccComponent = ptTreeSccComponent;
}
