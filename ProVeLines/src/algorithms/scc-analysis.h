/*
 * scc-analysis.h
 *
 *  Created on: Oct 1, 2015
 *      Author: rafaelolaechea
 */

#ifndef ALGORITHMS_SCC_ANALYSIS_H_
#define ALGORITHMS_SCC_ANALYSIS_H_

#include "mean-cycle-structure.h"


/* structure to allow an iterator over all entries in a hasthable */
struct htIteratorState {
	bool noSearchDone;
	int tableIndex;
	struct entry *currentEntry;
	ptList currentListStates;
	bool noMoreAvailable;
};

typedef struct htIteratorState   tHtIteratorState;
typedef struct htIteratorState* ptHtIteratorState;

ptHtIteratorState getEmptyHashtableIterator();
bool tableHasMoreElements(struct hashtable *sccComponent,
		ptHtIteratorState elementPointerInTable, ptHtState *nextState,
		unsigned int *Key);
bool tableHasMoreElementsHTDVersion(struct hashtable *sccComponent,
		ptHtIteratorState elementPointerInTable,
		ptHtDState *nextState,
		unsigned int *Key);
void printElementPointerInTable(ptHtIteratorState elementPointerInTable);
//ptHtState getNextState(ptHtIteratorState elementPointerInTable, struct hasthable * sccComponent);

#endif /* ALGORITHMS_SCC_ANALYSIS_H_ */
