/*
 * tranpose-executables.c
 *
 *  Created on: Sep 14, 2015
 *      Author: rafaelolaechea
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#include <string.h>
#include "error.h"
#include "main.h"
#include "list.h"
#include "stack.h"
#include "hashtableGen.h"
#include "boolFct.h"
#include "symbols.h"
#include "automata.h"
#include "SAT.h"
#include "tvl.h"
#ifdef CLOCK
	#include "clockZone.h"
#endif
#include "state.h"
#include "execution.h"

#include "transpose-executables.h"

static struct hashtable* htTranspose_ = NULL;



void initHTTranpose(){
	htTranspose_ = create_hashtableWrapper();
}

struct hashtable* getTransposeHasthable(){
	return htTranspose_;
}


/*
 *
 * Given a state, find the set of links leaving that state in the transpose graph.
 *
 * This is equivalent to the set of incoming edges in the original graph.
 *
 * Precondition:
 * 	Transpose have already been calculated/updated.
 *
 * 	Returns:
 *
 * 	A list (possibly empty) of transpose links.
 *
 */
ptList getTransposeLinks(ptState state){
	ptHtState transposeState = NULL;

	if( htFindNoFeatureCheck(state->payloadHash, state, &transposeState, getTransposeHasthable())){
		return transposeState->transposeLinks;
	} else {
		return NULL;
	}
}

/*
 *
 * Given a Key, Payload, and Payload Size it returns set of tranpose edges.
 *
 * This is equivalent to the set of incoming edges in the original graph.
 *
 * Precondition:
 *
 * 	Transpose have already been calculated/updated.
 *
 *	Hashtables hold payload sizes too.
 *
 * 	Returns:
 *
 * 	A list (possibly empty) of transpose links.
 *
 *
 */
ptList getTransposeLinksFromKeyAndPayload(tHtKey key, void * payload, unsigned int payloadSize){

	ptHtState transposeState = NULL;

	if( htFindByKeyAndPayloadNoFeatureCheck(key, payload, payloadSize,
			&transposeState, getTransposeHasthable())){
		return transposeState->transposeLinks;
	} else {
		return NULL;
	}
}





/*
 * It computes all outgoing transitions of s (without regard to features of s at the time).
 * 	For each s, s' it add s' into hasthable for Transpose if it is not there and then
 * 	adds s' to s into transpose links of s'.
 *
 *
 * 	Warning:
 * 		Won't insert anything in case ptState s has no outgoing transitions.
 *
 */
void computeTranposeGraph(ptSymTabNode globalSymTab, ptMTypeNode mtypes, ptState s){

	ptBoolFct noOutgoing = NULL;
	byte resetExclusivity = false;

	byte assertViolation = 0;

	byte hasDeadlock = 0;
	int _nbErrorsLocal = 0;



	//printf("Computing Transpose Graph, with %u as payload. \n", s->payloadHash );

	ptList tranposeTransitions =	executablesNoConjunctsFeatures(globalSymTab, mtypes, s,
			1,
			_nbErrorsLocal,
			&hasDeadlock,
			NULL,
			NULL,
			&noOutgoing,
			&resetExclusivity);

	// ** Will check all transpose */
	while(tranposeTransitions != NULL){
		ptProcessTransition tmpTrans = (ptProcessTransition) tranposeTransitions->value;
		int currentTransitionWeight = 0;

//		printf("Start Transition   \n");//, getExpTypeName(tmpTrans->trans->expression->type));
		if (tmpTrans->trans &&
				tmpTrans->trans->expression->type == E_STMNT_ASGN){
			currentTransitionWeight = tmpTrans->trans->expression->iVal;
//			printf(" with weight = %d \n", tmpTrans->trans->expression->iVal );
		} else {
//			printf("\n");
		}

//		printf("Source state \n");
//		printState(mtypes, s, NULL);

		ptState	newS_ = apply(globalSymTab, mtypes, s, (ptProcessTransition) tranposeTransitions->value, 1,
				&assertViolation);


		ptProcessTransition tmpTransition = ((ptProcessTransition) tranposeTransitions->value);

		/* Update Executables */


		/* Start -  I don't know why  */
		executables(globalSymTab, mtypes, newS_, 1, _nbErrorsLocal, &hasDeadlock, NULL, NULL, &noOutgoing, &resetExclusivity);

		if(resetExclusivity) {
			stateSetValue(newS_->payload, OFFSET_EXCLUSIVE, T_BYTE, NO_PROCESS);
		}
		/* End = I don't know why */

		newS_->payloadHash = hashState(newS_);

//		printf("Destintation state \n");
//		printState(mtypes, newS_, s);


//		printf("From %u to %u , with feature ", s->payloadHash, newS_->payloadHash);
//		printBool(tmpTransition->features);
//		printf("\n");

//		printf("\nEnd Transition\n");

		ptHtState prevS_ = NULL;
		struct hashtable*  _tableTranspoe = getTransposeHasthable();

		// Check if  newS_->payloadHash is in the table.
		//bool htFindNoFeatureCheck(tHtKey key, ptState state,  ptHtState* foundState,  struct hashtable* _abstractTable);

		//		printf("Seaching %u \n", newS_->payloadHash);
		if(htFindNoFeatureCheck(newS_->payloadHash, newS_, &prevS_, _tableTranspoe)){
			// Adding reverse link from destination to source.
//			printf("Found %u in transpose hashtable  \n", newS_->payloadHash);
			addReverseLink(prevS_, s, currentTransitionWeight, tmpTransition->features);
		} else {
		  //printf("Seaching %u  Returned False in _tableTranspoe \n", newS_->payloadHash);
//			printf("Didn't found %u in transpose hashtable  \n", newS_->payloadHash);
			htInsertNoFeaturesAndLink(newS_->payloadHash, newS_, s,
					tmpTransition->features, currentTransitionWeight,  _tableTranspoe);
		}


		tranposeTransitions = tranposeTransitions->next;
	}

//	exit(0);
//	printf("Finished Computing Transpose Graph, with %u as payload. \n", s->payloadHash );

}

ptList executablesNoConjunctsFeatures(ptSymTabNode globalSymTab, ptMTypeNode mtypes, ptState state,
		byte stutter, int nbErrors,
		byte * hasDeadlock,
		ptBoolFct* featuresOut,
		byte* allProductsOut,
		ptBoolFct* noOutgoing,
		byte* resetExclusivity) {

	#ifdef DEBUGEXEC
	printf("[Executables No Conjucts]\n");
#endif

	if(resetExclusivity) {
		*resetExclusivity = false;
	}

	if(!state) {
		failure("EXECUTABLE: state is null\n");
		return NULL;
	}

	#ifdef PROFILE_EXEC
		PROFILER_START(pr_executables);
	#endif

	byte noDeadlock = 0;// when 1, then we're sure that there are no deadlocks; when 0 and still in loop we don't know.  when 0 and out of loop, then there is a deadlock.
	byte noLocalDeadlock = 0;// when 1, then we're sure that there is an executable transition for the current process; when 0 and still in (inner) loop we don't know.  when 0 and out of (inner) loop, the proctype has no executable transitions.

	byte freeOut = 0; // Boolean -   1 ->  featuresOut must be free'd at the end.
	byte freeAllOut = 0; // Boolean - 1 -> allProductsOut must be free'd at the end.

	byte satFD = 0; // when 1, we KNOW that the features of considered transition satisfy the base FD.
	byte notSatFD = 0; // when 1, we KNOW that the features of considered transition do NOT satisfy the base FD.

	ptBoolFct currentFeatures;

	if (fullDeadlockCheck) {
		if(!featuresOut) {
			featuresOut = (ptBoolFct*) malloc (sizeof(ptBoolFct));
			*featuresOut = NULL;
			freeOut = 1;
		}

		if(!allProductsOut ) {
			allProductsOut = (byte*) malloc (sizeof(byte));
			*allProductsOut = 0;
			freeAllOut = 1;
		}
	}

	byte exclusive = stateGetValue(state->payload, OFFSET_EXCLUSIVE, T_BYTE);
	int handshake = stateGetValue(state->payload, OFFSET_HANDSHAKE, T_INT);

	ptStateMask maskPtr = state->mask->next; // The first mask concerns the global variables. So, we skip this one.

	ptList E = NULL; // List of all the executable transitions.
	ptList e = NULL; // List of all the executable transitions that concerns the current process.

	ptList currentTrans = NULL;
	ptProcessTransition ableTrans = NULL;
	setTimeout(0) ;

	byte endOfProgram = 1;
	ptFsmNode node;

AllProc:
	maskPtr = state->mask->next;
Cont:while (maskPtr) {
		node = getNodePointer(state, maskPtr);
		if(!node) {
			maskPtr = maskPtr->next;
			goto Cont;
		}
		endOfProgram = 0;
		e = NULL;
		if (stateGetValue(state->payload, OFFSET_EXCLUSIVE, T_BYTE) == NO_PROCESS
				|| stateGetValue(state->payload, OFFSET_HANDSHAKE, T_INT) != NO_HANDSHAKE
				|| stateGetValue(state->payload, OFFSET_EXCLUSIVE, T_BYTE) == maskPtr->pid) {

			// Else is a local variable to execution.c used exclusively in ...

			setElse(0);

OneProc:	currentTrans = node->trans;
			noLocalDeadlock = 0;
OneTrans:	while(currentTrans) {

				if(eval(globalSymTab, mtypes, state, maskPtr, ((ptFsmTrans)currentTrans->value)->expression, EVAL_EXECUTABILITY, NULL) > 0) {

					currentFeatures = ((ptFsmTrans)currentTrans->value)->features;
					// The features required for this transition to fire:
					ptBoolFct conjunct = currentFeatures; //addConjunction(currentFeatures, state->features, 1, 1);

					if((stateGetValue(state->payload, OFFSET_HANDSHAKE, T_INT) != NO_HANDSHAKE) &&
							(((ptFsmTrans)currentTrans->value)->expression->type == E_STMNT_CHAN_SND)) {

						ptBoolFct original = state->features;
						state->features = conjunct;

						ptExpNode expression = ((ptFsmTrans)currentTrans->value)->expression;
						channelSend(globalSymTab, mtypes, state, maskPtr, expression);
						// channelSend has modified the value of HANDSHAKE and one other byte in the payload.
						// These two will have to get back their original value.
						// Also, channelSend has allocated memory to handshake_transit: it will have to be free'd.
						byte responseHasDeadlock = 0;
						ptList e_ = executables(globalSymTab, mtypes, state, 0, nbErrors, &responseHasDeadlock, featuresOut, allProductsOut, noOutgoing, NULL);
						if(!responseHasDeadlock) {
							noDeadlock = 1;
							noLocalDeadlock = 1;
						}

						// After the recursive call, each transition in e_ is executable and its features
						// satisfy the modified base FD.

						// featuresOut contains all the outgoing features from now on,
						// included the ones of the response that satisfy the base FD (those may not
						//satisfy the modified FD, though).
						// *allProductsOut == 1 if the outgoing features reference all the products.

						ptList response = e_;
						while(response) {
							ableTrans = createProcessTransition(maskPtr, (ptFsmTrans)(currentTrans->value), NULL, (ptProcessTransition) response->value);

							e = listAdd(e, ableTrans);
							response = response->next;
							noDeadlock = 1;
							noLocalDeadlock = 1;
							satFD = 1;
						}
						listDestroy(e_);

						state->features = original;

						// The state must be reverted back to its original form
						// in order to ensure that the payload has not be modified.

						ptExpNode channelVar = expression->children[0];
						ptSymTabNode prefix = expressionSymbolLookUpLeft(channelVar);
						ptSymTabNode channel = expressionSymbolLookUpRight(channelVar);
						unsigned int offset;

						if(prefix->global == 0)
							offset = getVarOffset(globalSymTab, mtypes, state, maskPtr, maskPtr->offset, channelVar); // local variable
						else
							offset = getVarOffset(globalSymTab, mtypes, state, maskPtr, 0, channelVar);

						if(channel->type == T_CID)
							offset = stateGetValue(state->payload, offset, T_CID);
						stateSetValue(state->payload, offset, T_BYTE, 0);
						stateSetValue(state->payload, OFFSET_HANDSHAKE, T_INT, NO_HANDSHAKE);

						if(_handshake_transit)
							free(_handshake_transit);

						_handshake_transit = NULL;
					} else { // Not a channel send.

						satFD = 0;
						notSatFD = 0;


						if(!(currentFeatures) || isSatisfiableWrtFD(conjunct)) {

							ableTrans = createProcessTransition(maskPtr, (ptFsmTrans)(currentTrans->value), copyBool(conjunct), NULL);

#ifdef DEBUGEXEC
							printf("Transition Added.\n");
#endif
							e = listAdd(e, ableTrans);

							noDeadlock = 1;
							noLocalDeadlock = 1;
							satFD = 1;
						} // End-if  !(currentFeatures) || isSatisfiableWrtFD(conjunct)
						else if(nbErrors > 0) {
							// When the features required for firing the transition are not satisfiable wrt the FD,
							// then we have to check whether they are satisfiable wrt the base FD!
							// Only if the transition is also not satisfiable wrt the base FD could it be that we have
							// a deadlock.  Since the only reason for testing this is to prevent false deadlocks from
							// being reported, we don't do it when we already know that there is no deadlock or when
							// the base FD and the FD are identical, hence the condition:
							if(!noDeadlock) {
								if(isSatisfiableWrtBaseFD(conjunct)) {
									// The features satisfy the base FD (but not the modified one).
									noDeadlock = 1;
									noLocalDeadlock = 1;
									satFD = 1;
								}
								else notSatFD = 1; // The features do not satisfy the base FD.
							}
						}
						else {
							notSatFD = 1; // FD = Base FD and the features do not satisfy it.
						}

						if(fullDeadlockCheck && !(*allProductsOut) && !notSatFD && (satFD || isSatisfiableWrtBaseFD(conjunct))) {
							// The features satisfy the base FD -> we add it to the outgoing features.
							if(conjunct) *featuresOut = addDisjunction(*featuresOut, conjunct, 0, 1);
							else {
								destroyBool(*featuresOut);
								*featuresOut = NULL;
								*allProductsOut = 1;
							}
						}
					}// if (stateGetValue...)
					destroyBool(conjunct);
					conjunct = NULL;
				}// if eval(globalSymTab, mtypes, ...)

				currentTrans = currentTrans->next;
			} // While(currentTrans)

			if(e) 	{
				E = listConcat(E, e);
			}

			if (!noLocalDeadlock && !getElse()) {
				e = NULL;
				setElse(1);
				goto OneProc;
			}
		}
		maskPtr = maskPtr->next;
	}

	if (!noDeadlock && stateGetValue(state->payload, OFFSET_EXCLUSIVE, T_BYTE) != NO_PROCESS) {
		stateSetValue(state->payload, OFFSET_EXCLUSIVE, T_BYTE, NO_PROCESS);

		if(resetExclusivity)
			*resetExclusivity = true;

		goto AllProc;
	}

	if (!noDeadlock && !getTimeout()) {
		setTimeout(1);
		goto AllProc;
	}

	stateSetValue(state->payload, OFFSET_EXCLUSIVE, T_BYTE, exclusive);
	stateSetValue(state->payload, OFFSET_HANDSHAKE, T_INT, handshake);

	if(!noDeadlock && stutter && endOfProgram) { // We create a "blank" transition if we're in the (real) final state and if the stutter extension is asked and if there is no real transition.
		ptProcessTransition stutterTrans = (ptProcessTransition) malloc(sizeof(tProcessTransition));

		if(!stutterTrans)
			failure("Out of memory (creating processTransition).\n");

		stutterTrans->process = NULL;
		stutterTrans->features = copyBool(state->features);
		stutterTrans->trans = NULL;
		stutterTrans->response = NULL;
		E = listAdd(E, stutterTrans);
		noDeadlock = 1;
		if(fullDeadlockCheck && !(*allProductsOut)) {
			if(stutterTrans->features) *featuresOut = addDisjunction(*featuresOut, stutterTrans->features, 0, 1);
			else {
				destroyBool(*featuresOut);
				*featuresOut = NULL;
				*allProductsOut = 1;
			}
		}
	}

	#ifdef PROFILE_EXEC
		PROFILER_END(pr_executables);
	#endif

	if(hasDeadlock) {
		*hasDeadlock = (noDeadlock == 0);
	}




	if(freeOut) {
		// if freeOut, then we are in the first call to executables(globalSymTab, mtypes, ) (i.e. not a recursive call)
		// thus, we must return the features that have no outgoing transition
		if(fullDeadlockCheck && !(*allProductsOut) && *featuresOut) {
			*noOutgoing = addConjunction(state->features, negateBool(*featuresOut), 1, 1);
		}
		destroyBool(*featuresOut);
		free(featuresOut);
	}

	if(freeAllOut) {
		free(allProductsOut);
	}



#ifdef DEBUGEXEC
	printf("[/Executables No Conjuct]\n\n\n");
#endif
	return E;
}
