/*
 * symbolic-f-times.c
 *
 *  Created on: Sep 1, 2015
 *      Author: rafaelolaechea
 */
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <stdbool.h>

#include "hashtableState.h"
#include "main.h"
#include "stack.h"
#include "boolFct.h"
#include "execution.h"
#include "symbols.h"


#include "symbolic-f-times.h"


static int _FinishingTimeCounter = 0;
static ptList FTimesLst =  NULL;

static ptFElt _getNewFElement(ptState state, ptBoolFct features);


/* Creates a new F-Element and return it. */
static ptFElt _getNewFElement(ptState state, ptBoolFct features){
	ptFElt elt = (ptFElt) malloc(sizeof(tFElt));
	if(!elt)
		failure("Out of memory (creating Symbolic F element).\n");

	elt->state = state;
	elt->features = features;
	return elt;
}




/*
 * Set a finishing time for a State/feature pair.
 */
void htSetFinishingTime(ptMTypeNode mtypes, ptStackElt elt){
	/* Split Cases only for debugging */
	if (_FinishingTimeCounter == 0){
		++_FinishingTimeCounter;
		FTimesLst = listAdd(FTimesLst, _getNewFElement(elt->state, elt->state->features));
	} else {
		++_FinishingTimeCounter;
		FTimesLst = listAdd(FTimesLst, _getNewFElement(elt->state, elt->state->features));
	}

	if (!silent ){
		if(_FinishingTimeCounter % 10000 == 0){
		//	printf("Finishing time %d \n", _FinishingTimeCounter);
		}
	}

	//printf("Setting Finishing Time %d, with payloadHash  %u and features ",
	//		_FinishingTimeCounter,
	//		elt->state->payloadHash);
	//printBool(elt->state->features);
	//printf("\n");
}


int getSizeFinishingTime(){
	return _FinishingTimeCounter;
}

void getStateAndFexpressionFInverse(int i, 	ptState *s, ptBoolFct *features){
	if(i <= 0 || i > _FinishingTimeCounter ){
		failure("A call to getStateAndFexpressionFInverse(i) must have 1<=i<= |F|. \n");
	}

	ptList lstNode = FTimesLst;
	while(i > 1){
		lstNode  = lstNode->next;
		i--;
	}

	ptFElt tmpFelement = ((ptFElt) lstNode->value);

	*s = tmpFelement->state;
	*features = tmpFelement->features;

	return ;
}



