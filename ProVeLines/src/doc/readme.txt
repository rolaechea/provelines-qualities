BUILDING
- - - - -

There is no makefile, instead, type "sh build.sh".


FILES OVERVIEW
- - - - - - - -

  /            - contains the core files
  /helper      - contains generic helper functions
  /lib         - contains libraries used by the tool; each library has a  
                 wrapper (if it is used)
  /wrapper     - contains wrappers for the libraries; each wrapper 
                 consists of an interface (.h) and one or more 
                 implementations (.c) of that interface; the choice of 
                 the implementation is made at compile time (by 
                 selecting one of them)


 core:

  /main.c      - entry point
  /main.h      - defines global constants and settings, included in all 
                 other files

  /promela.l   - input file for flex
  /promela.y   - input file for bison; based on the grammar from 
                 Holzman's SPIN distribution (grammar is identical).

  /symbols.*   - functions and data types related to the construction 
                 and usage of the symbol table and of the expression 
                 syntax tree; symbols.h defines and documents the  
                 expression types

  /automata.*  - functions and data types related to the construction of 
                 finite state automata from promela source code. 

  /state.*     - functions and data types related to the representation 
                 and manipulation of execution states in memory (setting
                 and reading variables, duplicating and comparing 
                 states,..)
  
  /execution.* - functions related to the execution of a finite state  
                 automaton (eval, apply, executables,..).

  /checking.*  - implementation of model checking algorithms


 helpers:

  /helper/cnf.*   - data types and functions for manipulating boolean
                    expressions (always as CNFs)

  /helper/list.*  - generic list and labeled list implementation

  /helper/stack.* - generic stack implementation

 
 wrappers:

   /wrapper/hashState.h  - hashing a state (for use in hash table)
   
   /wrapper/hashtable.h  - hashtable for storing visited states
   
   /wrapper/ltl.h        - parsing an LTL formula and appending it as
                           a never claim to a file

   /wrapper/SAT.h        - testing a CNF against the feature 
                           diagram, testing implication of two CNF

   /wrapper/tvl.h        - parsing a TVL feature diagram and getting 
                           its DIMACS clauses


 libraries:
    
    /lib/clark.hashtable/ - hashtable implementation
    /lib/ltl2ba/          - creates never claims from an LTL formula 
    /lib/minisat/         - sat solver
    /lib/RNGS/            - random number generator
    /lib/tvl/             - JAVA-based TVL implementation





