#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "error.h"
#include "main.h"
#include "list.h"
#include "stack.h"
#include "boolFct.h"
#include "hashtable.h"
#include "symbols.h"
#include "automata.h"
#include "fdg.h"

/*
 * FEATURED DEPENDENCIES GRAPH (FDGs)
 * * * * * * * * * * * * * * * * * * */

ptFdgNode createFdgNode(char * name) {
	ptFdgNode fnode = (ptFdgNode) malloc(sizeof(tFdgNode));
	if(!fnode)
		failure("[automata/createFdgNode] Out of memory.\n");
		fnode->name = name;
	//fnode->node = node;
	fnode->trans = NULL;
	return fnode;
}

ptFdgTrans createFdgTrans(ptFdgNode source, ptFdgNode target, ptBoolFct features) {
	ptFdgTrans trans = (ptFdgTrans) malloc(sizeof(tFdgTrans));
	trans->source = source;
	trans->target = target;
	trans->features = features;
}

ptFdg computeFdg_rec(ptFdg fdg, ptFdgNode vnode, char * var, ptFsm fsm) {
	void * table = createHashtable();
	htInsert(table, fsm->init->lineNb, fsm->init);
	ptStack stack = push(NULL, fsm->init);
	while(!empty(stack)) {
		ptFsmNode node = pop(&stack);
		ptList vars = NULL;
		ptList transList = node->trans;
		while(transList) {
			ptFsmTrans trans = transList->value;
			vars = NULL;
			ptList vars = getInfluentVariables(trans->expression, var, 0);
			ptList tmp = vars;
			while(tmp) {
				ptLList foundNode = llistFind(fdg->nodes, (char *)tmp->value);
				ptFdgNode vnode_target;
				if(!foundNode) {
					vnode_target  = createFdgNode((char *)tmp->value);
					fdg->nodes = llistAdd(fdg->nodes, (char *)tmp->value, vnode_target);
				}
				else {
					vnode_target  = (ptFdgNode) foundNode->value;
				}
				foundNode = llistFind(vnode->trans, (char *)tmp->value);
				if(foundNode) {
					ptFdgTrans s2t = (ptFdgTrans) foundNode->value;
					s2t->features = addDisjunction(s2t->features, trans->features, 0, 1);
				}
				else
					vnode->trans = llistAdd(vnode->trans, (char *)tmp->value, createFdgTrans(vnode, vnode_target, trans->features));
				free((char*) tmp->value);
				tmp = tmp->next;
			}
			listDestroy(vars);
			vars = NULL;
			if(!htSearch(table, trans->target->lineNb, trans->target))
				push(stack, trans->target);
			transList = transList->next;
		}
	}
}

ptFdg computeFdg(ptFdg fdg, char * var, ptFsm fsm) {
	if(!fdg) {
		ptFdgNode init = createFdgNode(var);
		fdg = (ptFdg) malloc(sizeof(tFdg));
		fdg->init = init;
		fdg->nodes = llistAdd(NULL,var, init);
	}
	ptFsmNode node = fsm->init;
	void * table = createHashtable();
	htInsert(table, node->lineNb, node);
	ptStack stack = push(NULL, fdg->init);
	computeFdg_rec(fdg, fdg->init, var, fsm);
}

char * varexp2String(ptExpNode expression) {
	ptExpNode exp = expression;
	ptSymTabNode a_symbol;
	int size;
	char * path;
	char * attrName;

	char * tmp;
	while(exp) {	// Expression refers to a specific attribute
		a_symbol = expressionSymbolLookUpLeft(exp);
		size = strlen(a_symbol->name);
		path = (char *) malloc((size+1)*sizeof(char));
		strcpy(path,a_symbol->name);
		// Checking if an index is needed
		if(a_symbol->bound > 1) {
			if(!exp->children[0]->children[0] && exp->children[1]) // Expression of the form 'f.Feature.attr' -> forbidden!
				failure("Referring to an attribute of a complex clone-able feature without providing a clone index is forbidden (line %d).\n", expression->lineNb);
			char index[6];
			if(!exp->children[0]->children[0]) { // Expression of the form 'f.Feature' -> existential quantifier
				snprintf(index, 6, "[0]");
			}
			else { // Expression of the form f.Feature[x] or f.Feature[x].attr
				if(exp->children[0]->children[0]->type != E_EXPR_CONST) // Expression type for index
					failure("Only constants may be used for clone selection (line %d).\n", exp->lineNb);
				if(exp->children[0]->children[0]->iVal >= a_symbol->bound) // Index value
					failure("Constant refers to a non-existing clone (line %d).\n", exp->lineNb);
				snprintf(index, 6, "[%d]", exp->children[0]->children[0]->iVal);
			}
			size += strlen(index);
			tmp = (char *) realloc(path, (size+1) * sizeof(char));
			path = tmp;
			strcat(path, index);
		}

		// Checking whether an attribute is referenced
		exp = exp->children[1];
	}
	return path;
}

ptList getInfluentVariables(ptExpNode expression, char * var, byte flag) {
	if(!var || !expression) return NULL;
	ptList vars = NULL;
	char * path = NULL;
	ptExpNode exp = NULL;
	switch(expression->type) {
		case(E_DECL):
		case(E_STMNT):
			break;

		case(E_STMNT_CHAN_RCV):
			exp = expression->children[1];
			byte found = 0;
			while(exp && !found) {
				if(exp->children[0] && exp->children[0]->type == E_RARG_VAR) {
					path = varexp2String(exp->children[0]->children[0]);
					if(strcmp(var, path) == 0) {
						found = 1;
						vars = listAdd(vars, varexp2String(expression->children[0]));
					}
				}
				exp = exp->children[1];
			}
			break;

		case(E_STMNT_IF):
		case(E_STMNT_DO):
		case(E_STMNT_OPT):
		case(E_STMNT_SEQ):
		case(E_STMNT_BREAK):
		case(E_STMNT_GOTO):
		case(E_STMNT_LABEL):
			failure("Found control statement while applying an expression at line %2d\n", expression->lineNb);
			break;

		case(E_STMNT_ASGN):
		case(E_EXPR_LSHIFT):
		case(E_EXPR_RSHIFT):
		case(E_STMNT_CHAN_SND):
			path = varexp2String(expression->children[0]);
			if(strcmp(var,path) == 0) {
				vars = listConcat(vars,getInfluentVariables(expression->children[1], var, 1));
			}
			free(path);
			path = NULL;
			break;

		case(E_STMNT_PRINT):
		case(E_STMNT_PRINTM):
		case(E_STMNT_ELSE):
		case(E_STMNT_ASSERT):
			break;

		case(E_STMNT_EXPR):
		case(E_EXPR_PAR):
			vars = listConcat(vars,getInfluentVariables(expression->children[0], var, flag));
			break;

		case(E_EXPR_PLUS):
		case(E_EXPR_MINUS):
		case(E_EXPR_TIMES):
		case(E_EXPR_DIV):
		case(E_EXPR_MOD):
		case(E_EXPR_GT):
		case(E_EXPR_LT):
		case(E_EXPR_GE):
		case(E_EXPR_LE):
		case(E_EXPR_EQ):
		case(E_EXPR_NE):
		case(E_EXPR_AND):
		case(E_EXPR_OR):
		case(E_EXPR_BITWAND):
		case(E_EXPR_BITWOR):
		case(E_EXPR_BITWXOR):
		case(E_ARGLIST):
			if(flag) {
				vars = listConcat(vars,listConcat(getInfluentVariables(expression->children[0], var, flag), getInfluentVariables(expression->children[1], var, flag)));
			}
			break;

		case(E_EXPR_UMIN):
		case(E_EXPR_NEG):
		case(E_EXPR_BITWNEG):
		case(E_RARG_EVAL):
		case(E_EXPR_FULL):
		case(E_EXPR_NFULL):
		case(E_EXPR_EMPTY):
		case(E_EXPR_NEMPTY):
		case(E_STMNT_INCR):
		case(E_STMNT_DECR):
		case(E_EXPR_LEN):
		case(E_RARG_VAR):
		case(E_EXPR_VAR):

			if(flag) {
				vars = listConcat(vars,getInfluentVariables(expression->children[0], var, flag));
			}
			break;

		case(E_EXPR_CONST):
		case(E_RARG_CONST):
			if(flag) {
				vars = listAdd(NULL,"const");
			}
			break;


		case(E_EXPR_COND):
			vars = listConcat(vars,getInfluentVariables(expression->children[0], var, 1));
			vars = listConcat(vars,getInfluentVariables(expression->children[1], var, 1));
			vars = listConcat(vars,getInfluentVariables(expression->children[2], var, 1));
			break;

		case(E_EXPR_TIMEOUT):
		case(E_EXPR_RUN):
			break;

		case(E_VARREF):
			vars = listAdd(vars, varexp2String(expression));
		break;
	}

	return vars;

}

void printFdg(ptFdg fdg) {
	ptFdgNode init = fdg->init;
	printf("--> %s\n", init->name);
	ptLList trans = init->trans;
	while(trans) {
		printf("\t --> %s\n", trans->label);
		trans=trans->next;
	}
}
