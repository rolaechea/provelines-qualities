#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>
#include <string.h>
#include "error.h"
#include "main.h"
#include "list.h"
#include "boolFct.h"
#include "symbols.h"
#include "automata.h"


/*
 * SYMBOL TABLE
 * * * * * * * * * * * * * * * * * * * * * * * */

/**
 * Just creates a node with the given values.
 */
ptSymTabNode createSymTabNode(int type, char* name, int lineNb, int bound, int capacity, ptExpNode init, ptFsm fsm, ptSymTabNode child) {
#ifndef CLOCK
	if(type == T_CLOCK)
		failure("Clock typed variables cannot be used without the CLOCK option.\n");
#endif
	ptSymTabNode newNode = (ptSymTabNode) malloc(sizeof(tSymTabNode));
	if(!newNode) failure("Out of memory (creating SymTabNode).\n");
	newNode->type	= type;
	newNode->name	= name;
	newNode->global = 0;
	newNode->lineNb = lineNb;
	newNode->bound	= bound;
	newNode->capacity=capacity;
	newNode->memOffset= 0;
	newNode->memSize= 0;
	newNode->init	= init;
	newNode->fsm	= fsm;
	newNode->child	= child;
	newNode->prev	= newNode;
	newNode->next	= NULL;
	newNode->utype	= NULL;
	return newNode;
}

/**
 * Creates a node with a user-defined type (based on the type name).
 *  - It therefore needs a symbol table to look up that type.
 *  - It will return NULL if no type of this name exists!
 */
ptSymTabNode createSymTabNodeUType(ptSymTabNode symbols, char* type, char* name, int lineNb, int bound, int capacity, ptExpNode init, ptFsm fsm, ptSymTabNode child) {
	ptSymTabNode pType = lookupInSymTab(symbols, type);
	if(pType == NULL || pType->type != T_TDEF) return NULL;
	else {
		ptSymTabNode newNode = (ptSymTabNode) malloc(sizeof(tSymTabNode));
		if(!newNode) failure("Out of memory (creating SymTabNode).\n");
		newNode->type	= T_UTYPE;
		newNode->utype	= pType;
		newNode->name	= name;
		newNode->global = 0;
		newNode->lineNb = lineNb;
		newNode->bound	= bound;
		newNode->capacity=capacity;
		newNode->memOffset= 0;
		newNode->memSize= 0;
		newNode->init	= init;
		newNode->fsm	= fsm;
		newNode->child	= child;
		newNode->prev	= newNode;
		newNode->next	= NULL;
		return newNode;
	}
}

/**
 * Concatenates two symTabs (the second symTab is usually a single node).
 * The arguments can be NULL (if both are NULL, the function returns NULL).
 * Returns a pointer to the new symTab.
 */
ptSymTabNode addToSymTab(ptSymTabNode symTab, ptSymTabNode newNode) {
	if(symTab == NULL) {
		return newNode;
	} else if(newNode == NULL) {
		return symTab;
	} else {
		ptSymTabNode newlistTail = newNode->prev;
		newNode->prev = symTab->prev;
		newNode->prev->next = newNode;
		symTab->prev = newlistTail;
		return symTab;
	}
}

/**
 * Returns the symbol with the given name or NULL if there is no such symbol in the symTab.
 */
ptSymTabNode lookupInSymTab(ptSymTabNode symTab, char* name) {
	ptSymTabNode cur = symTab;
	while(cur != NULL) {
		if(cur->name != NULL && strcmp(name, cur->name) == 0) return cur;
		cur = cur->next;
	}
	return NULL;
}

/**
 * Destroys a symTab and all that's linked to it (except for the utype).
 */
void destroySymTab(ptSymTabNode symTab) {
	ptSymTabNode willy;
	while(symTab != NULL) {
		if(symTab->child) destroySymTab(symTab->child);
		if(symTab->init) destroyExpNode(symTab->init);
		if(symTab->name) free(symTab->name);
		willy  = symTab;
		symTab = symTab->next;
		free(willy);
	}
}



/**
 * Goes through the expression and all its children and looks up all references to
 * MTypes, variables or proctypes (expressions of type E_VARREF_NAME or E_EXPR_RUN)
 * and stores a pointer to the actual symTab record of the variable in the symTab
 * field of the expression; or the MType value (>=0) in the iVal field; or the magic
 * variable identifier (MVAR_* < 0) in the iVal field.
 *
 * If a variable is not found, execution aborts.
 *
 * The globalSymTab should contain all globally visible symbols, while the localSymTab
 * contains additional symbols.  Lookups are first made in the localSymTab.
 * The subFieldSymTab parameter is only set if the expression designates a subfield of
 * a user type.  If the parameter is not NULL, the lookup will be performed on it only.
 * Subsequent lookups, such as the array index, will use the other two symTabs just as
 * before.
 */
void resolveVariableNamesInExpression(ptExpNode expression, ptSymTabNode globalSymTab, ptMTypeNode mTypes, ptSymTabNode localSymTab, ptSymTabNode subFieldSymTab) {
	if(expression->type == E_VARREF || expression->type == E_EXPR_RUN) {
		// E_VARREF: 		child[0] = E_VARREF_NAME, child[1] = E_VARREF (subfield, or NULL)
		// E_VARREF_NAME:	child[0] = E_EXPR_* (index into the array, or NULL), sVal = the variable/field name
		ptSymTabNode symbol;
		char* symbolName;

		if(expression->type == E_VARREF) symbolName = expression->children[0]->sVal;
		else symbolName = expression->sVal;

		if(subFieldSymTab) symbol = lookupInSymTab(subFieldSymTab, symbolName);
		else {
			symbol = lookupInSymTab(localSymTab, symbolName);
			if(symbol == NULL) symbol = lookupInSymTab(globalSymTab, symbolName);
		}

		if(symbol != NULL) {
			if(expression->type == E_VARREF) {
#if !defined(ATTR) && !defined(MULTI)
				if(!spinMode && !optimisedSpinMode && symbol->type == T_FEAT) failure("Features (such as '%s') cannot be used in the expression at line %d'.\n", symbolName, expression->lineNb);
				else
#endif
					if(symbol->type == T_PROC || symbol->type == T_TDEF) failure("The symbol '%s' on line %d does not denote a variable.\n", symbolName, expression->lineNb);
				else {
					expression->children[0]->symTab = symbol;
					// Resolve subfields, but with the symbol table of the type
					if(expression->children[1] != NULL) {
						if((symbol->type != T_UTYPE && symbol->type != T_UFEAT) || symbol->utype == NULL) failure("The symbol '%s' on line %d does not have subfields (it's neither a user type nor a complex feature).\n", symbolName, expression->lineNb);
						resolveVariableNamesInExpression(expression->children[1], globalSymTab, mTypes, localSymTab, symbol->utype->child);
					}
					// Resolve array index
					if(expression->children[0]->children[0] != NULL) {
						resolveVariableNamesInExpression(expression->children[0]->children[0], globalSymTab, mTypes, localSymTab, NULL);
					}
				}
			} else { // if(expression->type == E_EXPR_RUN)
				if(symbol->type != T_PROC) failure("The symbol '%s' on line %d does not denote a proctype.\n", symbolName, expression->lineNb);
				else expression->symTab = symbol;
			}

		} else if(expression->type == E_EXPR_RUN) {
			failure("The proctype named '%s' on line %d could not be found.\n", symbolName, expression->lineNb);

		} else {
			// First check if its a magic variable
			int mvar = 0;
			if(mvar == 0 && strcmp(symbolName, MVARID_LAST) 	== 0) mvar = MVAR_LAST;
			if(mvar == 0 && strcmp(symbolName, MVARID_NRPR) 	== 0) mvar = MVAR_NRPR;
			if(mvar == 0 && strcmp(symbolName, MVARID_PID) 		== 0) mvar = MVAR_PID;
			if(mvar == 0 && strcmp(symbolName, MVARID_SCRATCH)	== 0) mvar = MVAR_SCRATCH;

			if(mvar < 0) {
				// MVar IDs are negative!
				if(expression->children[1] != NULL) failure("The symbol '%s' on line %d does not have subfields (it's not a user type).\n", symbolName, expression->lineNb);
				expression->children[0]->iVal = mvar;

			} else {
				// Then check if it's an mtype
				int value = getMTypeValue(mTypes, symbolName);
				if(value == -1 && !expression->iVal) {
					failure("The symbol '%s' on line %d could not be found.\n", symbolName, expression->lineNb);

				} else {
					expression->children[0]->iVal = value;
				}
			}
		}
	}

	// Children of E_VARREF_NAME were already dealt with
	if(expression->type != E_VARREF) {
		int i;
		for(i = 0; i < 3; i++) if(expression->children[i] != NULL) resolveVariableNamesInExpression(expression->children[i], globalSymTab, mTypes, localSymTab, NULL);
	}
}

/**
 * Resolves variable names in all expressions of an fsm.  See
 * resolveVariableNamesInExpression() for more info.
 */
void resolveVariableNames(ptFsm fsm, ptSymTabNode globalSymTab, ptMTypeNode mTypes) {
	ptList trans = fsm->transitions;
	while(trans != NULL) {
		if(((ptFsmTrans) trans->value)->expression != NULL) resolveVariableNamesInExpression(((ptFsmTrans) trans->value)->expression,
																							 globalSymTab,
																							 mTypes,
																							 fsm->symTab,
																							 NULL);
		trans = trans->next;
	}
}

/**
 * Removes transitions for which the featureValue is false.
 */
void filterTransitionLists(ptFsm fsm) {
	ptList node = fsm->nodes;
	ptList trans = NULL;
	ptList temp = NULL;
	while(node) {
		trans = ((ptFsmNode) node->value)->trans;
		while(trans) {
			temp = trans->next;
			if(((ptFsmTrans) trans->value)->hasFeatures && !((ptFsmTrans) trans->value)->featuresValue) {
				((ptFsmNode) node->value)->trans = listRemove(((ptFsmNode) node->value)->trans, trans->value);
			}
			trans = temp;
		}
		node = node->next;
	}
}

/**
 * For states with several transitions leaving, those leading to accepting
 * states will be placed on top of the list.
 */
void orderTransitionLists(ptFsm fsm) {
	ptList node = fsm->nodes;
	ptList trans = NULL;
	ptList newTrans = NULL;
	while(node) {
		trans = ((ptFsmNode) node->value)->trans;
		newTrans = NULL;
		while(trans) {
			if(((ptFsmTrans) trans->value)->target && ((ptFsmTrans) trans->value)->target->flags & N_ACCEPT == N_ACCEPT) {
				newTrans = listAddHead(newTrans, trans->value);
			} else {
				newTrans = listAdd(newTrans, trans->value);
			}
			trans = trans->next;
		}

		listDestroy(((ptFsmNode) node->value)->trans);
		((ptFsmNode) node->value)->trans = newTrans;
		node = node->next;
	}
}


/**
 * Initialises the values of memSize and memOffset of all variables
 * in the symTab and its descendants and starts with the node given
 * as the first parameter.  The initial memory offset is given as
 * a parameter (usually 0).
 *
 * The function will also lookup all variable references and replace
 * them by pointers.  If a variable cannot be found, execution aborts.
 *
 * The return value is the next offset to be used after the variables
 * of the symtab were added.
 */
unsigned int processVariables(ptSymTabNode node, ptSymTabNode globalSymTab, ptMTypeNode mTypes, unsigned int iOffset, byte bGlobal) {
	if(node != NULL) {
		unsigned int iMemSpace; // the space actually occupied by the variable

		node->global = bGlobal;

		switch(node->type) {
			case T_PROC:
			case T_NEVER:
				resolveVariableNames(node->fsm, globalSymTab, mTypes);
				if(node->init && node->init->type == E_EXPR_COUNT)
					resolveVariableNamesInExpression(node->init, globalSymTab, mTypes, NULL, NULL);
				if(node->type == T_NEVER) orderTransitionLists(node->fsm);
				else if(optimisedSpinMode) filterTransitionLists(node->fsm);
				node->memSize = processVariables(node->fsm->symTab, globalSymTab, mTypes, 0, 0);
				iMemSpace = 0; // A proctype does not occupy any space in the chunk of global memory
				break;

			case T_TDEF:
				if(!spinMode && strcmp(node->name, "features") == 0) {
					node->memSize = 0;
					ptSymTabNode cur = node->child;
					while(cur != NULL) {
						cur->memSize = 0;
						//cur->bound = 0;
						cur = cur->next;
					}
				} else {
					node->memSize = processVariables(node->child, globalSymTab, mTypes, 0, 0);
				}
				iMemSpace = 0;
				break;

			case T_CHAN:
				if(node->init) failure("Problem at line %d: channels cannot be initialised.\n", node->init->lineNb);
				node->memOffset = iOffset;
				node->memSize = processVariables(node->child, globalSymTab, mTypes, 0, 0);
				if(node->capacity != 0)	iMemSpace = node->memSize * node->capacity * node->bound + node->bound;
				else iMemSpace = node->bound;
				// For rendezvous channels, we need some memory space to store the values of the message request.
				if(iMemSpace == 0) iMemSpace = node->memSize * node->bound + node->bound;
				break;

			case T_UTYPE:
				if(node->init) failure("Problem at line %d: user types cannot be initialised.\n", node->init->lineNb);
				if(!spinMode && strcmp(node->utype->name, "features") == 0) {
					node->memSize = 0;
					node->bound = 0;
				} else {
					node->memSize = node->utype->memSize;
				}
				node->memOffset = iOffset;
				iMemSpace = node->memSize * node->bound;
				break;

			default:
				if(node->init && node->init->type != E_EXPR_CONST) failure("Problem at line %d: variables can only be initialised by constants.\n", node->init->lineNb);
				node->memSize = getTypeSize(node->type);
				node->memOffset = iOffset;
				iMemSpace = node->memSize * node->bound;
				break;
		}

		return processVariables(node->next, globalSymTab, mTypes, iOffset + iMemSpace, bGlobal);
	} else return iOffset;
}

/**
 * Helper function, prints "count" spaces and at each levelStep a >.
 */
void spaces(int count) {
	int i;
	for(i=0; i <= count; i++) {
		if((i % LEVEL_STEP) == 0) printf(">");
		printf(" ");
	}
}

/**
 * Returns the size (in bytes) of a basic type.
 */
int getTypeSize(int type) {
	switch(type) {
		case T_BIT: return 1;
		case T_BOOL: return 1;
		case T_BYTE: return 1;
		case T_INT: return 4;
		case T_MTYPE: return 1;
		case T_PID: return 1;
		case T_CID: return 4;
		case T_SHORT: return 2;
		case T_CLOCK: return 0; // Clocks value are registered in a DBM, not in payload.
	}
	return 0;
}

/**
 * Returns the name of a predefined type.
 */
char* getTypeName(int type) {
	switch(type) {
		case T_BIT: return "bit";
		case T_BOOL: return "bool";
		case T_BYTE: return "byte";
		case T_CHAN: return "chan";
		case T_INT: return "int";
		case T_MTYPE: return "mtype";
		case T_PID: return "pid";
		case T_PROC: return "proctype";
		case T_SHORT: return "short";
		case T_FEAT: return "feature";
		case T_UFEAT: return "ufeature";
		case T_CID: return "cid";
		case T_TDEF: return "typedef";
		case T_UNSGN: return "unsigned";
		case T_UTYPE: return "user";
		case T_NEVER: return "never";
		case T_CLOCK: return "clock";
	}
	return NULL;
}

/**
 * Prints the content of the symTab to stdout (recursively).
 *  - level denotes the number of spaces to be used to indent the output.
 *  - title is the title of the symTab.
 */
void printSymTab(ptSymTabNode symTab, int level, char* title) {
	ptSymTabNode cur = symTab;
	if(cur != NULL) {
		spaces(level);
		if(title) printf("SymTab of [%s].\n", title);
		spaces(level);
		printf("name         | lineNb |     type | bound | capacity | mOffs | mSize | init? | fsm? \n");
		while(cur != NULL) {
			spaces(level);
			printf(" %-11s | % 6d | %8s | % 5d | % 8d | % 5d | % 5d | %5s | %4s\n", cur->name,
																					cur->lineNb,
																					cur->type == T_UTYPE ? (cur->utype ? cur->utype->name : "") : getTypeName(cur->type),
																					cur->bound,
																					cur->capacity,
																					cur->memOffset,
																					cur->memSize,
																					(cur->init ? "yes" : "no"),
																					(cur->fsm  ? "yes" : "no"));
			if((cur->type == T_TDEF || cur->type == T_CHAN) && cur->child != NULL) {
				printSymTab(cur->child, level + LEVEL_STEP, cur->name);
				spaces(level);
				printf("\n");
			}
			if(cur->type == T_PROC || cur->type == T_NEVER) {
				printFsm(cur->fsm, level + LEVEL_STEP, cur->name);
				spaces(level);
				printf("\n");
			}
			cur = cur->next;
		}
	} else {
		spaces(level);
		printf("SymTab of [%s] is empty.\n", title);
	}
}


/*
 * MTYPES
 * * * * * * * * * * * * * * * * * * * * * * * */

/**
 * Adds an MType to the list (automatically assigns it a value).
 */
ptMTypeNode addMType(ptMTypeNode list, char* name) {
	ptMTypeNode newNode = (ptMTypeNode) malloc(sizeof(tMTypeNode));
	if(!newNode) failure("Out of memory (creating MTypeNode).\n");
	newNode->name = name;
	newNode->next = NULL;
	newNode->prev = newNode;
	if(list == NULL) {
		newNode->value = 0;
		return newNode;
	} else {
		newNode->value = list->prev->value + 1;
		newNode->prev = list->prev;
		list->prev->next = newNode;
		list->prev = newNode;
		return list;
	}
}

/**
 * Gets the value of an MType based on the name.
 * If the MType does not exist, it will return -1.
 */
int getMTypeValue(ptMTypeNode list, char* name) {
	ptMTypeNode cur = list;
	while(cur != NULL) {
		if(strcmp(name, cur->name) == 0) return cur->value;
		cur = cur->next;
	}
	return -1;
}

/**
 * Gets the name of the MType based on its value.
 * If the value does not exist, it will return NULL.
 */
char* getMTypeName(ptMTypeNode list, int value) {
	ptMTypeNode cur;
	if(list != NULL && value <= list->prev->value / 2) {
		cur = list;
		while(cur != NULL) {
			if(cur->value == value) return cur->name;
			cur = cur->next;
		}
	} else {
		cur = list->prev;
		while(cur != NULL) {
			if(cur->value == value) return cur->name;
			cur = cur->prev;
		}
	}
	return NULL;
}

/**
 * Prints a list of MTypes to stdout.
 */
void printMTypes(ptMTypeNode list) {
	if(list) {
		printf("MTypes:");
		ptMTypeNode cur = list;
		while(cur != NULL) {
			printf(" %s", cur->name);
			cur = cur->next;
		}
		printf("\n");
	}
}


/*
 * EXPRESSIONS
 * * * * * * * * * * * * * * * * * * * * * * * */

/**
 * Just creates a node with the given values.
 */
ptExpNode createExpNode(int type, char* sVal, int iVal, ptExpNode child0, ptExpNode child1, ptExpNode child2, int lineNb, ptFsm fsmChild, ptSymTabNode symTabChild) {
	ptExpNode newNode = (ptExpNode) malloc(sizeof(tExpNode));
	if(!newNode) failure("Out of memory (creating ExpNode).\n");
	newNode->type		= type;
	newNode->sVal   	= sVal;
	newNode->iVal		= iVal;
	newNode->fsm 		= fsmChild;
	newNode->symTab		= symTabChild;
	newNode->lineNb 	= lineNb;
	newNode->children[0]= child0;
	newNode->children[1]= child1;
	newNode->children[2]= child2;
	return newNode;
}

/**
 * Destroys an ExpNode and all that's linked to it.
 */
void destroyExpNode(ptExpNode node) {
	if(node->sVal) free(node->sVal);
	if(node->fsm) destroyFsm(node->fsm);
	if(node->symTab) destroySymTab(node->symTab);
	int i;
	for(i = 0; i < 3; i++) if(node->children[i]) destroyExpNode(node->children[i]);
	free(node);
}

/**
 * Gives a string translation for a pExpNode->type.
 */
char* getExpTypeName(int type) {
	switch(type) {
		case E_DECL            : return "Declaration wrapper (E_DECL)";
		case E_STMNT           : return "Statement wrapper (E_STMNT)";

		case E_STMNT_IF        : return "If (E_STMNT_IF)";
		case E_STMNT_DO        : return "Do (E_STMNT_DO)";
		case E_STMNT_BREAK     : return "Break (E_STMNT_BREAK)";
		case E_STMNT_GOTO      : return "Goto (E_STMNT_GOTO)";
		case E_STMNT_LABEL     : return "Label (E_STMNT_LABEL)";
		case E_STMNT_OPT       : return "Opt (E_STMNT_OPT)";
		case E_STMNT_SEQ       : return "Seq (E_STMNT_SEQ)";

		case E_STMNT_CHAN_RCV  : return "Channel receive (E_STMNT_CHAN_RCV)";
		case E_STMNT_CHAN_SND  : return "Channel send (E_STMNT_CHAN_SND)";
		case E_STMNT_ASGN      : return "Assignment (E_STMNT_ASGN)";
		case E_STMNT_INCR      : return "Increment (E_STMNT_INCR)";
		case E_STMNT_DECR      : return "Decrement (E_STMNT_DECR)";
		case E_STMNT_PRINT     : return "Print (E_STMNT_PRINT)";
		case E_STMNT_PRINTM    : return "PrintM (E_STMNT_PRINTM)";
		case E_STMNT_ASSERT    : return "Assertion (E_STMNT_ASSERT)";
		case E_STMNT_EXPR      : return "Expression wrapper (E_STMNT_EXPR)";
		case E_STMNT_ELSE      : return "Else (E_STMNT_ELSE)";
		case E_STMNT_WAIT	   : return "Time invariant (E_STMNT_WAIT)";
		case E_STMNT_WHEN	   : return "Time guard (E_STMNT_WHEN)";

		case E_EXPR_PAR        : return "Parentheses (E_EXPR_PAR)";
		case E_EXPR_PLUS       : return "Plus (E_EXPR_PLUS)";
		case E_EXPR_MINUS      : return "Minus (E_EXPR_MINUS)";
		case E_EXPR_TIMES      : return "Times (E_EXPR_TIMES)";
		case E_EXPR_DIV        : return "Divide (E_EXPR_DIV)";
		case E_EXPR_MOD        : return "Modulo (E_EXPR_MOD)";
		case E_EXPR_GT         : return "Greater than (E_EXPR_GT)";
		case E_EXPR_LT         : return "Less than (E_EXPR_LT)";
		case E_EXPR_GE         : return "Greater or equal than (E_EXPR_GE)";
		case E_EXPR_LE         : return "Less or equal than (E_EXPR_LE)";
		case E_EXPR_EQ         : return "Equal (E_EXPR_EQ)";
		case E_EXPR_NE         : return "Not equal (E_EXPR_NE)";
		case E_EXPR_AND        : return "Logical and (E_EXPR_AND)";
		case E_EXPR_OR         : return "Logical or (E_EXPR_OR)";
		case E_EXPR_COUNT	   : return "Clones count (E_EXPR_COUNT)";
		case E_EXPR_UMIN       : return "Unary minus (E_EXPR_UMIN)";
		case E_EXPR_NEG        : return "Negation (E_EXPR_NEG)";
		case E_EXPR_COND       : return "Conditional expression (E_EXPR_COND)";
		case E_EXPR_RUN        : return "Run (E_EXPR_RUN)";
		case E_EXPR_LEN        : return "Length (E_EXPR_LEN)";
		case E_EXPR_VAR        : return "Variable reference wrapper (E_EXPR_VAR)";
		case E_EXPR_CONST      : return "Constant (E_EXPR_CONST)";
		case E_EXPR_TIMEOUT    : return "Timeout (E_EXPR_TIMEOUT)";
		case E_EXPR_FULL       : return "Full probe (E_EXPR_FULL)";
		case E_EXPR_NFULL      : return "Not full probe (E_EXPR_NFULL)";
		case E_EXPR_EMPTY      : return "Empty probe (E_EXPR_EMPTY)";
		case E_EXPR_NEMPTY     : return "Not empty probe (E_EXPR_NEMPTY)";

		case E_ARGLIST         : return "Argument list (E_ARGLIST)";
		case E_VARREF          : return "Variable reference (E_VARREF)";
		case E_VARREF_NAME     : return "Variable or field name (E_VARREF_NAME)";
		case E_RARG_VAR        : return "Receive argument variable (E_RARG_VAR)";
		case E_RARG_EVAL       : return "Receive argument eval (E_RARG_EVAL)";
		case E_RARG_CONST      : return "Receive argument constant (E_RARG_CONST)";
	}
	return NULL;
}


/**
 * Returns the symbol corresponding with the far right name of an expression referencing a variable.
 * If the expression does not reference a variable, returns NULL.
 */
ptSymTabNode expressionSymbolLookUpRight(ptExpNode expression) {
	if(!expression) return NULL;
	if(expression->type == E_VARREF) {
		if (expression->children[1]) {
			// If there is a child (of type E_VARREF), then continue
			return expressionSymbolLookUpRight(expression->children[1]);
		} else {
			// Otherwise, go to the E_VARREF_NAME node
			return expressionSymbolLookUpRight(expression->children[0]);
		}
	}
	if(expression->type == E_RARG_VAR  || expression->type == E_EXPR_VAR)
		return expressionSymbolLookUpRight(expression->children[0]);
	if(expression->type == E_VARREF_NAME)
		return expression->symTab;
	return NULL;
}

/**
 * Returns the symbol corresponding with the far left name of an expression referencing a variable.
 * If the expression does not reference a variable, returns NULL.
 */
ptSymTabNode expressionSymbolLookUpLeft(ptExpNode expression) {
	if(!expression) return NULL;
	if(expression->type == E_EXPR_VAR)
		return expressionSymbolLookUpLeft(expression->children[0]);
	if(expression->type == E_VARREF)
		return expressionSymbolLookUpLeft(expression->children[0]);
	if(expression->type == E_RARG_VAR)
		return expressionSymbolLookUpLeft(expression->children[0]);
	if(expression->type == E_VARREF_NAME)
		return expression->symTab;
	return NULL;
}
