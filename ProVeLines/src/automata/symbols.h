/*
 * SYMBOL TABLE
 * * * * * * * * * * * * * * * * * * * * * * * */
#ifndef AUTOMATA_SYMBOLS_H
#define AUTOMATA_SYMBOLS_H

#include <stdbool.h>


#include "main.h"

// Primitive types:
#define T_BIT	1
#define T_BOOL	2
#define T_BYTE	3
#define T_PID	4
#define T_SHORT	5
#define T_INT	6
#define T_UNSGN	7 // not supported yet
#define T_MTYPE	8
#define T_CLOCK 30	// dense time clock

#define T_FEAT 	9
#define T_UFEAT 16

// "Special" types:
#define T_CHAN	10				// Channel: capacity used; children denote message fields
#define T_CID	15				// Channel reference; capacity and children are not used.
#define T_TDEF	11				// Type definition: children denote fields of type
#define T_PROC	12				// ProcType: fsm field used; bound denotes the number of initially active processes
#define T_UTYPE 13				// Type of variable is a user type (basically, a T_TDEF record is being used as the type): utype points to the type record
#define T_NEVER 14				// Never claim


struct symTabNode_ {
	int	type;					// The type of the symbol, one of the above
	int	lineNb;					// The line at which the symbol is declared
	char* name;					// The name of the symbol (variable name, type name, proctype name)
	byte global;				// 1 iff global variable, 0 otherwise

	struct symTabNode_* utype;	// For type->=T_UTYPE, this points to the user type symbol.

	int bound;					// Normally == 1; If > 1, the symbol denotes an array.
	int capacity;				// For T_CHAN, this denotes its capacity.  For T_FEAT, this is the feature ID.
	int memSize;				// The size in memory of the symbol table node (in bits).
	unsigned int memOffset;		// The position of the variable in the memory block of a state (in bits).
	struct expNode_* init;		// For variables, this denotes the initial value.
	struct fsm_* fsm;			// For T_PROC, this denotes the fsm corresponding to the proctype
	struct symTabNode_* child;	// For T_TDEF, this denotes the fields of the user type;
								// For T_CHAN, it denotes the fields of the messages;
	struct symTabNode_* next;	// Points to next node; the last node in a list has this pointer set to NULL.
	struct symTabNode_* prev;	// Points to previous node; the first node in a list has this pointer pointing to the last node!
};
typedef struct symTabNode_   tSymTabNode;
typedef struct symTabNode_* ptSymTabNode;

struct mTypeNode_ {
	int value;					// Automatically incremented integer value
	char* name;					// Name given by user
	struct mTypeNode_* next;	// Points to next node; the last node in a list has this pointer set to NULL.
	struct mTypeNode_* prev;	// Points to previous node; the first node in a list has this pointer pointing to the last node!
};
typedef struct mTypeNode_   tMTypeNode;
typedef struct mTypeNode_* ptMTypeNode;



/*
 * SYNTAX TREE FOR STATEMENTS
 * * * * * * * * * * * * * * * * * * * * * * * */

#define E_DECL				1	// symTab = declaration.
#define E_STMNT 			2	// child[0] = E_STMNT_*

#define E_STMNT_CHAN_RCV	3	// child[0] = E_VARREF, child[1] = E_EXPR_*
#define E_STMNT_CHAN_SND	4	// child[0] = E_VARREF, child[1] = E_EXPR_*
#define E_STMNT_IF			5	// child[0] = E_STMNT_OPT (contains an fsm)
#define E_STMNT_DO			6	// child[0] = E_STMNT_OPT (contains an fsm)
#define E_STMNT_BREAK		7	// empty
#define E_STMNT_GOTO		8	// sVal = the label to go to
#define E_STMNT_LABEL		9	// child[0] = E_STMNT_*, sVal = the label of child[0]
#define E_STMNT_OPT			19	// child[0] = E_STMNT_OPT (next option; or NULL), fsm = fsm of this option
#define E_STMNT_SEQ			18	// fsm = fsm of this sequence
#define E_STMNT_ATOMIC		20	// fsm = fsm of the atomic sequence

#define E_STMNT_ASGN		10	// child[0] = E_VARREF, child[1] = E_EXPR_*
#define E_STMNT_INCR		11	// child[0] = E_VARREF
#define E_STMNT_DECR		12	// child[0] = E_VARREF
#define E_STMNT_PRINT		13	// child[0] = E_ARGLIST, sVal = the print string
#define E_STMNT_PRINTM		14	// child[0] = E_VARREF, or iVal = constant
#define E_STMNT_ASSERT		15	// child[0] = E_EXPR_*
#define E_STMNT_EXPR		16	// child[0] = E_EXPR_*
#define E_STMNT_ELSE		17	// empty

#define E_STMNT_WAIT		30  //
#define E_STMNT_WHEN		31  // symTab = clock symbols

#define E_EXPR_PAR			50	// child[0] = E_EXPR_*
#define E_EXPR_PLUS			51	// child[0] = E_EXPR_*, child[1] = E_EXPR_*
#define E_EXPR_MINUS		52	// child[0] = E_EXPR_*, child[1] = E_EXPR_*
#define E_EXPR_TIMES		53	// child[0] = E_EXPR_*, child[1] = E_EXPR_*
#define E_EXPR_DIV			54	// child[0] = E_EXPR_*, child[1] = E_EXPR_*
#define E_EXPR_MOD			55	// child[0] = E_EXPR_*, child[1] = E_EXPR_*
#define E_EXPR_GT			56	// child[0] = E_EXPR_*, child[1] = E_EXPR_*
#define E_EXPR_LT			57	// child[0] = E_EXPR_*, child[1] = E_EXPR_*
#define E_EXPR_GE			58	// child[0] = E_EXPR_*, child[1] = E_EXPR_*
#define E_EXPR_LE			59	// child[0] = E_EXPR_*, child[1] = E_EXPR_*
#define E_EXPR_EQ			60	// child[0] = E_EXPR_*, child[1] = E_EXPR_*
#define E_EXPR_NE			61	// child[0] = E_EXPR_*, child[1] = E_EXPR_*
#define E_EXPR_AND			62	// child[0] = E_EXPR_*, child[1] = E_EXPR_*
#define E_EXPR_OR			63	// child[0] = E_EXPR_*, child[1] = E_EXPR_*
#define E_EXPR_COUNT		88	// child[0] = E_EXPR_*
#define E_EXPR_UMIN			64	// child[0] = E_EXPR_*
#define E_EXPR_NEG			65	// child[0] = E_EXPR_*
#define E_EXPR_BITWAND		66	// child[0] = E_EXPR_*, child[1] = E_EXPR_*
#define E_EXPR_BITWOR		67	// child[0] = E_EXPR_*, child[1] = E_EXPR_*
#define E_EXPR_BITWXOR		68	// child[0] = E_EXPR_*, child[1] = E_EXPR_*
#define E_EXPR_BITWNEG		69	// child[0] = E_EXPR_*
#define E_EXPR_LSHIFT		70	// child[0] = E_EXPR_*, child[1] = E_EXPR_*
#define E_EXPR_RSHIFT		71	// child[0] = E_EXPR_*, child[1] = E_EXPR_*
#define E_EXPR_COND			72	// child[0] = E_EXPR_* (the condition), child[1] = E_EXPR_* (then), child[2] = E_EXPR_* (else)
#define E_EXPR_RUN			73	// child[0] = E_ARGLIST, sVal = the procType name, and after processing: symTab = node in symbol table that represents the proctype
#define E_EXPR_LEN			74	// child[0] = E_VARREF
#define E_EXPR_VAR			75	// child[0] = E_VARREF
#define E_EXPR_CONST		76	// iVal = the value
#define E_EXPR_TIMEOUT		77	// empty
#define E_EXPR_FULL			78	// child[0] = E_VARREF
#define E_EXPR_NFULL		79	// child[0] = E_VARREF
#define E_EXPR_EMPTY		80	// child[0] = E_VARREF
#define E_EXPR_NEMPTY		81	// child[0] = E_VARREF

#define E_ARGLIST			82	// child[0] = E_EXPR_* or E_RARG_VAR or E_RARG_EVAL or E_RARG_CONST, E_ARGLIST (next argument, or NULL)
#define E_VARREF			83	// child[0] = E_VARREF_NAME, child[1] = E_VARREF (subfield, or NULL)
#define E_VARREF_NAME		84	// child[0] = E_EXPR_* (index into the array, or NULL), sVal = the variable/field name
								//						The processing step resolves all symbol names in expressions.  After this was done:
								//						  - If the symbol denotes a variable, then symTab is the node in the symbol table of that variable;
								//						  - If the symbol denotes an mtype, then symTab is NULL, and iVal >= 0 is the mtype value.
								//						  - If the symbol denotes a special variable, then iVal < 0 and is one of MVAR_*
#define E_RARG_VAR			85	// child[0] = E_VARREF
#define E_RARG_EVAL			86	// child[0] = E_EXPR_*
#define E_RARG_CONST		87	// iVal = the constant

// These constants are used to identify magic (predefined) variables inside an E_VARREF_NAME (to avoid strcmp).
#define MVAR_SCRATCH		-1
#define MVARID_SCRATCH		"_"		// A write-only variable that can be used to destroy a field read from a channel
#define MVAR_PID			-2
#define MVARID_PID			"_pid"	// The pid of the currently executing proctype
#define MVAR_LAST			-3
#define MVARID_LAST			"_last"	// The pid of the proctype that executed the last step in the current execution
#define MVAR_NRPR			-4
#define MVARID_NRPR			"_nr_pr"// The number of running proctypes

struct expNode_ {
	short int type;
	char* sVal;
	int iVal;
	int	lineNb;						// The line at which the symbol is declared
	struct expNode_* children[3];
	struct fsm_* fsm;
	struct symTabNode_* symTab;
};
typedef struct expNode_   tExpNode;
typedef struct expNode_* ptExpNode;


struct dataTuple_ {
	char* sVal;
	int iVal;
	struct symTabNode_* symTabNodeVal;
	struct fsm_* fsmVal;
};
typedef struct dataTuple_   tDataTuple;



/*
 * API
 * * * * * * * * * * * * * * * * * * * * * * * */
ptSymTabNode createSymTabNode(int type, char* name, int lineNb, int bound, int capacity, ptExpNode init, struct fsm_* fsm, ptSymTabNode child);
ptSymTabNode createSymTabNodeUType(ptSymTabNode symbols, char* type, char* name, int lineNb, int bound, int capacity, ptExpNode init, struct fsm_* fsm, ptSymTabNode child);
ptSymTabNode addToSymTab(ptSymTabNode symTab, ptSymTabNode newNode);
ptSymTabNode lookupInSymTab(ptSymTabNode symTab, char* name) ;



/* 	Member read-only functions on ptSymTabNode */
bool getIsArraySymTab(ptSymTabNode symTab);

void destroySymTab(ptSymTabNode symTab);

void spaces(int count);
void printSymTab(ptSymTabNode symTab, int level, char* title);
void printMTypes(ptMTypeNode list);

char* getTypeName(int type);

ptMTypeNode addMType(ptMTypeNode list, char* name);
int getMTypeValue(ptMTypeNode list, char* name);
char* getMTypeName(ptMTypeNode list, int value);

ptExpNode createExpNode(int type, char* sVal, int iVal, ptExpNode child0, ptExpNode child1, ptExpNode child2, int lineNb, struct fsm_* fsmChild, ptSymTabNode symTabChild);
void destroyExpNode(ptExpNode node);
int getTypeSize(int type);
char* getExpTypeName(int type);

unsigned int processVariables(ptSymTabNode node, ptSymTabNode globalSymTab, ptMTypeNode mTypes, unsigned int iOffset, byte bGlobal);

#endif
