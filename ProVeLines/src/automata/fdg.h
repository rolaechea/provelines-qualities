/*
 * FEATURED DEPENDENCIES GRAPH (FDGs)
 * * * * * * * * * * * * * * * * * * */

struct fdgNode_ {
	char * name;
	ptFsmTrans stmnt;
	ptLList trans;
};
typedef struct fdgNode_ tFdgNode;
typedef struct fdgNode_ * ptFdgNode;

struct fdgTrans_ {
	ptBoolFct features;
	struct fdgNode_ * source;
	struct fdgNode_ * target;
};
typedef struct fdgTrans_ tFdgTrans;
typedef struct fdgTrans_ * ptFdgTrans;

struct fdg_ {
	struct fdgNode_ * init;
	ptLList nodes;
};
typedef struct fdg_ tFdg;
typedef struct fdg_ * ptFdg;

ptFdgNode createFdgNode(char * name);
ptFdg computeFdg(ptFdg fdg, char * var, ptFsm fsm);
void printFdg(ptFdg fdg);
ptList getInfluentVariables(ptExpNode expression, char * var, byte conditions);
char * varexp2String(ptExpNode expression);
