#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>
#include <string.h>
#include <sys/time.h>

#include "error.h"
#include "main.h"
#include "list.h"
#include "stack.h"
#include "hashtableGen.h"
#include "boolFct.h"
#include "ltl.h"
#include "tvl.h"
#include "symbols.h"
#include "automata.h"
#ifdef CLOCK
	#include "clockZone.h"
	#include "federation.h"
#endif
#include "state.h"
#include "execution.h"
#include "tracing.h"
#include "qualitycomputation.h"
#include "checking.h"
#ifndef CLOCK
	#include "simulation.h"
#endif
#include "y.tab.h"
#include "lexer.h"

extern FILE* yyin;
extern int yydebug;
extern int yyparse();
extern void init_lex();

// Settings defined in main
char * path;
byte trace = FULL_TRACE;
byte keepTempFiles = 0;
byte spinMode = 0;
byte optimisedSpinMode = 0;
byte exhaustive = 0;
byte fullDeadlockCheck = 0;
byte sim = 0;
byte stutterStep = 0;
long int limitExploration;
int excludeSCCLessThanEqual=0;
int minepumpType=-1;

byte silent = 0;
byte statsLA =0;
byte optimizeAlg1 =0;
byte excludeSCC=0;
byte printDFSTrace =0;
byte partialOrderReduction=0;
byte combineFeatureExpressions=0;
byte restartWhite=1;
byte traceSCC=0;

// Other global variables from main
ptSymTabNode globalSymTab = NULL;
ptSymTabNode globalSymTab2 = NULL;
ptMTypeNode mtypes = NULL;
ptMTypeNode mtypes2 = NULL;
ptSymTabNode neverClaim;
ptList props, props2;
void * _handshake_transit = NULL;

// Profiler variables
struct timeval _profileTimeBegin, _profileTimeEnd;
char* _profilerStr = NULL;


/**
 * To be used for command-line parameter errors.  Does the
 * same as failure(), and prints the usage instructions.
 */
void error(char* msg, ...) {
	printf("Error: ");
	va_list args;
	va_start(args, msg);
	vfprintf(stdout, msg, args);
	va_end(args);
	printf(	"\n --\n"
			"Usage: ./provelines [options] model.pml\n"
			"\n"
			" Options for model checking:\n"
			"  (none)   Does nothing.\n"
			"  -check   Verifies the model.  When a problem (assert, deadlock or pro-\n"
			"           perty violation) is found, an error trace (counterexample) is\n"
			"           printed and execution stops. \n"
			"  -exhaustive \n"
			"           Determines also which products have *no* problems. The normal\n"
			"           check will stop at the first problem,  and does not determine\n"
			"           whether there are products that have no problems  (e.g. those\n"
			"           that satisfy a property).\n"
			"  -fdlc    Search for trivially invalid end states (more costly).\n"
			"	-generateTraces\n"
			"			Generate a trace by listing a set of transitions to execute"
			"	-dumpTransitions\n"
			"			print a summary of transitions to standard out, as well as \n"
			"			to transitions.binary in protobuf format.\n"
			"");
#ifndef CLOCK
	printf( "  -ltl <property> \n"
			"           Checks whether the specified property is satisfied (when used\n"
			"           with -check). Never claims can be specified manually, too.\n"
			"  -sim <model.pml>\n"
			"           Checks whether the specified model refines the input model\n"
			"			according to the specified properties.\n"
			"  -stutter\n"
			"           Executes non stutter steps. When combined with -sim, \n"
			"           checks stutter simulation instead of standard simulation.\n"
			"  -props  <props.pml> \n"
			"           Parses the file containing the relevant properties. Required\n"
			"           by -sim and -stutter.\n");
#endif
	printf(	"\n"
			" Options for output control:\n"
			"  (none)   Prints a full trace for every counterexample.\n"
			"  -st      Only prints states when there are no changed variables.\n"
			"  -nt      Does not print any traces, only information  about  violating\n"
			"           (or satisfying) products.\n"
			"  -silent  Deactivate all printing for limit average   \n"
			"\n"
			" Options for features and feature model:\n"
			"  (none)   Will attempt to load a TVL feature model that is named as the\n"
			"           .pml file but with extension .tvl\n"
			"  -fm <file.tvl> \n"
			"           Load the specified TVL file (only used in verification). This\n"
			"           parameter can be omitted if the TVL file is named as the .pml\n"
			"           file but with extension .tvl.\n"
			"  -fmdimacs <dimacsClauseFile.txt> <mappingFile.txt>\n"
			"           As before, but load the dimacs of the feature model directly.\n"
#ifdef Z3
			"  -at <file.att>			\n"
			"			Load the specified attributes from file.att and constraints from file.con "
#endif
			"  -filter <expression> \n"
			"           Limit the verification to a subset of the products  specified\n"
			"           in the TVL file.  The TVL syntax has to be used for this.\n"
			"  -spin    Treat features like normal variables (as SPIN would do).\n"
			"  -ospin   Similar to -spin, but statements with a bad guard are removed\n"
			"           from the  model before model checking.  The input is thus in-\n"
			"           terpreted as fPromela (not exactly as SPIN would do). This is\n"
			"           normally more efficient than -spin.\n"
			"\n"
			" Options for debugging:\n"
			"  -exec    Execute the model (does not print states, only model output).\n"
			"  -l <integer> \n"
			"           Stop when the given number of states were explored. (This op-\n"
			"           tion can also be used for model checking.)\n"
			"  -s       Parse and print static info (symbol table, FSMs, MTypes, ..).\n"
			"  -t       Do not delete the generated temporary files.\n"
			"\n"
			" --\n");
	exit(EXIT_ERROR);
}

/**
 * Simply copies a file byte by byte; could be made more efficient.
 */
int copyFile(char* source, char* target) {
	FILE* fsource;
	FILE* ftarget;
	fsource = fopen(source, "r");
	ftarget = fopen(target, "w");

	if(fsource != NULL && ftarget != NULL)  {
		char buffer;
		buffer = fgetc(fsource);
		while(!feof(fsource)) {
			fputc(buffer, ftarget);
			buffer = fgetc(fsource);
		}
		fclose(fsource);
		fclose(ftarget);
		return 1;
	}

	if(fsource != NULL) fclose(fsource);
	if(ftarget != NULL) fclose(ftarget);
	return 0;
}

int main(int argc, char *argv[]) {
	if(sizeof(int) != 4)
		failure("Bad architecture: int type must be four bytes long.\n");
	if(sizeof(short) != 2)
		failure("Bad architecture: short type must be two bytes long.\n");

	char* ltlProp = NULL;
	char* tvlFile = NULL;
	char* tvlFilter = NULL;
	char* pmlFile = NULL;
	char * propFile = NULL;
	char *traceFile = NULL;
	char *qualitiesCostFilename = NULL;

	int i, ps = 0, check = 0, exec = 0, fm = 0, ltl = 0, \
			generateTraces=0, dumpTransitions=0, computeCompatibleProducts=0 \
			,computeQualitiesCost=0, checkLimitAverage=0;
#ifdef z3
	int cn = 0;
	char *attFile = NULL;
	char *conFile = NULL;
#endif

	int maximumLengthTrace = 0;

	const int programNameLength = 10;
	const int programNameLengthMinusOne = 9;


	path = (char *)malloc((strlen(argv[0]) - programNameLengthMinusOne) * sizeof(char));
	for(i = 0; i < strlen(argv[0]) - programNameLength; i++)
		path[i] = argv[0][i];
	path[strlen(argv[0]) - programNameLength] = '\0';

//	printf("Path is <<%s>>, strlen of argv[0] is %d \n" , path, strlen(argv[0]));

	initBoolFct();

	if(argc < 2) error("No fPromela file provided.");

	//	printf("filename used <%s> \n", argv[argc - 1]);

	if(strcmp(argv[argc - 1], "pump-scc/minimal-product-0.pml")==0){
		minepumpType=0;
	}else	if(strcmp(argv[argc - 1], "pump-scc/minimal-product-1.pml")==0){
		minepumpType=1;
	}else	if(strcmp(argv[argc - 1], "pump-scc/minimal-product-2.pml")==0){
		minepumpType=2;
	}else	if(strcmp(argv[argc - 1], "pump-scc/minimal-product-3.pml")==0){
		minepumpType=3;
	}




	if(!copyFile(argv[argc - 1], "__workingfile.tmp"))
		error("The fPromela file does not exist or is not readable!");

	// Read command-line arguments
	for(i = 1; i < argc-1; i++) {
		if(strcmp(argv[i],"-s") == 0) {
			ps = 1;
		} else if (strcmp(argv[i],"-silent") == 0){
			silent = 1;
		}else if (strcmp(argv[i],"-optimizeAlg1") == 0){
			optimizeAlg1 = 1;
		} else if(strcmp(argv[i], "-excludeSCC") == 0 ){
			excludeSCC = 1;

			i++;

			if(i >= argc - 1) {
				error("The -excludeSCC option has to be followed by an integer such that all SCC smaller than such int are excluded");
			}
			else {
				excludeSCCLessThanEqual = atoi(argv[i]);
			}
		 }else if (strcmp(argv[i],"-statsLA") == 0){
			 statsLA = 1;
		 }else if (strcmp(argv[i], "-computeCompatibleProducts") == 0) {
			computeCompatibleProducts = 1;
			i++;

			if(i >= argc - 1) {
				error("The -computeCompatibleProducts has to be followed by a trace filename");
			}
			else {
				traceFile = argv[i];
			}
		} else if (strcmp(argv[i], "-computeQualitiesCost") == 0) {
			computeQualitiesCost = 1;
			i++;

			if(i >= argc - 1) {
				error("The -computeQualitiesCost has to be followed by a feature to cost mapping filename and a trace");
			}
			else {
				qualitiesCostFilename = argv[i];
			}

			i++;
			if(i >= argc - 1) {
				error("The -computeQualitiesCost has to be followed by a feature to cost mapping filename and a trace");
			}
			else {
				traceFile = argv[i];
			}


		}	else if(strcmp(argv[i], "-generateTraces") == 0 ){
			generateTraces = 1;

			i++;

			if(i >= argc - 1) {
				error("The -generateTraces has to be followed by a maximum number of transitions in each trace");
			}
			else {
				maximumLengthTrace = atoi(argv[i]);
			}
		} else if (strcmp(argv[i],"-dumpTransitions") == 0){
			dumpTransitions = 1;

		} else if (strcmp(argv[i], "-checkLimitAverage") == 0) {
			checkLimitAverage=1;

		} else if (strcmp(argv[i], "-partialOrderReduction") == 0) {
			partialOrderReduction=1;
		}else if (strcmp(argv[i], "-combineFeatureExpressions") == 0){
			combineFeatureExpressions=1;
		}else if (strcmp(argv[i], "-traceSCC") == 0){
			traceSCC=1;

		} else if(strcmp(argv[i],"-check") == 0) {
			check = 1;

		} else if(strcmp(argv[i],"-exhaustive") == 0) {
			exhaustive = 1;

		} else if(strcmp(argv[i],"-t") == 0) {
			keepTempFiles = 1;

		} else if(strcmp(argv[i],"-spin") == 0) {
			if(optimisedSpinMode) error("The options -spin and -ospin cannot be used together.");
			spinMode = 1;

		} else if(strcmp(argv[i],"-ospin") == 0) {
			if(spinMode) error("The options -spin and -ospin cannot be used together.");
			optimisedSpinMode = 1;

		} else if(strcmp(argv[i],"-fdlc") == 0) {
			fullDeadlockCheck = 1;

		} else if(strcmp(argv[i],"-exec") == 0) {
			exec = 1;

		} else if(strcmp(argv[i],"-l") == 0) {
			i++;
			if(i >= argc - 1) error("The -l option has to be followed by an integer denoting the number of steps to execute.");
			else limitExploration = atol(argv[i]);

		} else if(strcmp(argv[i],"-filter") == 0) {
			i++;
			if(i >= argc - 1) error("The -filter option has to be followed by a TVL expression.");
			else tvlFilter = argv[i];

		} else if(strcmp(argv[i],"-fm") == 0) {
			if(fm) error("The options -fm and -fmdimacs cannot be used at the same time.");
			fm = 1;
			i++;
			if(i >= argc - 1) error("The -fm option has to be followed by the feature model file name.");
			else tvlFile = argv[i];
#ifdef z3
		} else if (strcmp(argv[i], "-at") ==0){
			cn = 1;
			i++;
			if(i >= argc - 1)
				error("The -at option has to be followed by the attributes  file name.");
			else
				attFile = argv[i];
#endif
		} else if(strcmp(argv[i],"-fmdimacs") == 0) {
			if(fm) error("The options -fm and -fmdimacs cannot be used at the same time.");
			fm = 1;
			if(i+2 >= argc - 1) error("The -fmdimacs option has to be followed by the dimacs clause file and the dimacs mapping file.");
			else loadFeatureModelDimacs(argv[i+1], argv[i+2]);
			i += 2;

		} else if(strcmp(argv[i],"-nt") == 0) {
			trace = NO_TRACE;

		} else if(strcmp(argv[i],"-st") == 0) {
			if(trace != NO_TRACE) trace = SIMPLE_TRACE;
#ifndef CLOCK
		} else if(strcmp(argv[i],"-ltl") == 0) {
			ltl = 1;
			i++;
			if(i >= argc - 1) error("The -ltl option has to be followed by an LTL property.");
			else {
				ltlProp = argv[i];
				char* errorMsg = malloc(1024 * sizeof(char));

				if(!errorMsg)
					failure("Out of memory (creating string buffer).\n");

				if(!appendClaim("__workingfile.tmp", ltlProp, errorMsg))
					error("The LTL formula '%s' could not be parsed, error message: \n --\n%s\n", ltlProp, errorMsg);

				free(errorMsg);
			}

		} else if(strcmp(argv[i], "-sim") == 0) {
			if(check) error("The options -check and -sim cannot be used at the same time.");
			sim = 1;
			i++;
			pmlFile = argv[i];
		} else if(strcmp(argv[i], "-props") == 0) {
			i++;
			propFile = argv[i];
		} else if(strcmp(argv[i], "-stutter") == 0) {
			stutterStep = 1;
#endif
		} else {
			error("Unrecognised option '%s'", argv[i]);
		}
	}

	// Some basic validity checks

	if(fm && tvlFile == NULL && tvlFilter != NULL)
		error("The -filter option cannot be used with the -fmdimacs option, only with -fm.");

	if(fm && tvlFile != NULL) {
#ifdef Z3
		if(!cn){
#endif
			if(!loadFeatureModel(tvlFile, tvlFilter))
				error("Could not load the specified feature model file.");
#ifdef Z3
		} else {
			/*  Attributes file  specified */
			int attFileLen = strlen(attFile);
			char *constraintsFile = (char *) malloc((attFileLen +	1) * sizeof(char));

			strcpy(constraintsFile, attFile);


			constraintsFile[attFileLen - 3] = 'c';
			constraintsFile[attFileLen - 2] = 'o';
			constraintsFile[attFileLen - 1] = 'n';
			constraintsFile[attFileLen] = '\0';

			if(!loadFeatureModel(tvlFile, tvlFilter, attFile, constraintsFile))
				error("Could not load the specified feature model file.");
		}
#endif

	} else if(!fm && !spinMode && !optimisedSpinMode) {
		// Try to guess name of feature model file name
		char * pmlFile = argv[argc - 1];
		int pmlFileLen = strlen(pmlFile);
		tvlFile = (char *) malloc((pmlFileLen +1) * sizeof(char));
		strcpy(tvlFile, pmlFile);
		tvlFile[pmlFileLen - 3] = 't';
		tvlFile[pmlFileLen - 2] = 'v';
		tvlFile[pmlFileLen - 1] = 'l';
		tvlFile[pmlFileLen] = '\0';

#ifdef Z3
		if(!cn){
#endif
			if(!loadFeatureModel(tvlFile, tvlFilter) && tvlFilter != NULL)
				error("The -filter option can only be used when a feature model is charged.");
#ifdef Z3
		}else {
			/*  Attributes file  specified */
				int attFileLen = strlen(attFile);
				char *constraintsFile = (char *) malloc((attFileLen +	1) * sizeof(char));

				strcpy(constraintsFile, attFile);


				constraintsFile[attFileLen - 3] = 'c';
				constraintsFile[attFileLen - 2] = 'o';
				constraintsFile[attFileLen - 1] = 'n';
				constraintsFile[attFileLen] = '\0';


			if(!loadFeatureModel(tvlFile, tvlFilter, attFile, constraintsFile ) && tvlFilter != NULL)
				error("The -filter option can only be used when a feature model is charged.");
		}
#endif
	} else {
		if(!fm && (spinMode || optimisedSpinMode) && tvlFilter != NULL)
			error("The -filter option can only be used when a feature model is charged.");
	}

	if(!propFile && (sim || stutterStep)) {
		error("Simulation checking and non stutter steps require a property file.");
	}


	if (computeCompatibleProducts){
		printf("Reading a trace - will compute set of compatible products \n");
	}

	if (computeQualitiesCost){
//		printf("Computing Qualities cost \n");
	}

	// Initialise random number generator
	SelectStream(RNG_STREAM);
	PutSeed(50000);


	srand(time(0));


	// Invoke cpp
	if(system("cpp __workingfile.tmp __workingfile.tmp.cpp") != 0)
		failure("Could not run the c preprocessor (cpp).\n");

	//	yydebug = 1;

	yyin = fopen("__workingfile.tmp.cpp", "r");

	if(yyin == NULL)
		failure("Could not open temporary working file (%s).\n", argv[argc - 1]);


	init_lex();



	if(yyparse(&globalSymTab, &mtypes, NULL) != 0){
		printf("Syntax error; aborting..\n");
	}else {
		// Resolve variable names
		processVariables(globalSymTab, globalSymTab, mtypes, SYS_VARS_SIZE, 1);

		// Treat user request
		if(ps) {
			printSymTab(globalSymTab, 0, "Global");
			printMTypes(mtypes);
		}

//		printSymTab(globalSymTab, 0, "Global");
//		printMTypes(mtypes);

		if (checkLimitAverage){

			if(propFile) {
				printf("Property file is %s \n", propFile);
			}

			ptLList listOfFeatures = getfeatureIDMapping();



			while(hasNextFeature(listOfFeatures)){
				char const * featureName = getCurrentFeatureLabel(listOfFeatures);
				if(isOriginalFeatureNonRoot(listOfFeatures)){
//					printf("Feature Label <%s>, Id <%d> \n", featureName, getCurrentFeatureId(listOfFeatures));
				}
				listOfFeatures = AdvanceFeature(listOfFeatures);
			}

//			printf("# of features = %d \n", getNbFeatures());


			if(!silent)
				printf("Will start  Algorithm 1  \n");


			struct timeval startAlgorithm1, endAlgorithm1;

			if(statsLA)
				gettimeofday(&startAlgorithm1, NULL);




			startSymbolicDepthFirstSearch(props, globalSymTab, mtypes);


			if(statsLA)
				gettimeofday(&endAlgorithm1, NULL);

			if(statsLA)
				printf("Algorithm 1,  %ld milliseconds \n", ((endAlgorithm1.tv_sec * 1000000 + endAlgorithm1.tv_usec)
						- (startAlgorithm1.tv_sec * 1000000 + startAlgorithm1.tv_usec))/1000 );



			if(!silent)
				printf("Finished Algorithm  1 \n Will start building Symbolic Finishing Times Tree \n");

			struct timeval startAlgorithm2, endAlgorithm2;

			if(statsLA)
				gettimeofday(&startAlgorithm2, NULL);

			computeSymbolicFinishingTree();

			if(statsLA)
				gettimeofday(&endAlgorithm2, NULL);

			if(statsLA)
				printf("Algorithm 2,  %ld miliseconds \n", ((endAlgorithm2.tv_sec * 1000000 + endAlgorithm2.tv_usec)
					  - (startAlgorithm2.tv_sec * 1000000 + startAlgorithm2.tv_usec))/1000 );



			if(!silent)
				printf("Correctly Computed Symbolic Finishing Times Tree \n");
			if(!silent){
				printGraphInDotLanguage(mtypes);
			}


			struct timeval startAlgorithm4, endAlgorithm4;
			if(statsLA)
				gettimeofday(&startAlgorithm4, NULL);

			computeSymbolicScc();

			if(!silent){
				printf("\n Correctly Computed Symbolic SCC - Finished Algorithm 4 \n");
				printf("Will Compute Mean Cycle for each SCC - Algorithm 5\n");
			}

			if(statsLA)
				gettimeofday(&endAlgorithm4, NULL);

			if(statsLA)
				printf("Algorithm 3-4,  %ld milliseconds \n", ((endAlgorithm4.tv_sec * 1000000 + endAlgorithm4.tv_usec)
					  - (startAlgorithm4.tv_sec * 1000000 + startAlgorithm4.tv_usec))/1000 );


			struct timeval startAlgorithm5, endAlgorithm5;
			if(statsLA)
				gettimeofday(&startAlgorithm5, NULL);

			computeMeanCycle(mtypes);

			if(statsLA)
				gettimeofday(&endAlgorithm5, NULL);

			if(statsLA)
				printf("Algorithm 5,  %ld milliseconds \n", ((endAlgorithm5.tv_sec * 1000000 + endAlgorithm5.tv_usec)
					  - (startAlgorithm5.tv_sec * 1000000 + startAlgorithm5.tv_usec))/1000 );

			if(!silent)
				printf("\n Finished Computed Mean Cycle for each SCC - Algorithm 5 \n");

		} else if(check) {
#ifndef CLOCK
			if(propFile) {
				fclose(yyin);

				printf("Opening File %s . ", propFile);
				yyin = fopen(propFile, "r");

				if(yyin == NULL)
					failure("Could not open temporary working file.", argv[argc - 1]);

				if(yyparse(&globalSymTab, &mtypes, &props) != 0)
					failure("Syntax error in property file; aborting..\n");
			}

			if(check && (ltl || neverClaim)) {
				if(ltl)
					printf("Checking LTL property %s..\n", ltlProp);
				else
					printf("Checking never claim contained in input file..\n");

				if(tvlFilter)
					printf("Attention! Checks are only done for products satisfying:\n  %s!\n", tvlFilter);

				startNestedDFS(props, globalSymTab, mtypes);

			} else {
#endif
				printf("No never claim, checking only asserts and deadlocks..\n");

				if(tvlFilter)
					printf("Attention! Checks are only done for products satisfying:\n  %s!\n", tvlFilter);

				startExploration(props, globalSymTab, mtypes);
#ifndef CLOCK
			}
#endif
		} else if(exec) {
			launchExecution();
		} else if( computeCompatibleProducts){
			/* Compute trace cost
			 *
			 *For each transtion:
			 * 1.) Lookup transition feature expression.
			 * 2.) Combine with accumulated expression.
			 *  For each cost,  feature expression:
			 *  	Check if feature expression exists.
			 * 4.)
			 *
			 * */
			InitializeAndSerializeTransitions(globalSymTab, mtypes, false);
			ptList transitionIds = parseListOfTransitionIds(traceFile);
			CheckCompatibleProducts(transitionIds, globalSymTab, mtypes);
		}else if(computeQualitiesCost){

			InitializeAndSerializeTransitions(globalSymTab, mtypes, false);
			parseTransitionCost(qualitiesCostFilename);
			//printParsedTransitions(); // For Debugging
			ptList transitionIds = parseListOfTransitionIds(traceFile);
			computeCosts(transitionIds);

		} else if(generateTraces){

			InitializeAndSerializeTransitions(globalSymTab, mtypes, false);
			startTraceGeneration(globalSymTab, mtypes, maximumLengthTrace);

		} else if(dumpTransitions){
			printTransitions( globalSymTab, mtypes);
			InitializeAndSerializeTransitions(globalSymTab, mtypes, true);
		}

		if(!ps && !check && !exec && !sim && !generateTraces && \
				!dumpTransitions  && !computeCompatibleProducts && !computeQualitiesCost
				&& !checkLimitAverage) {
			error("No action specified in command-line.");
		}

	}
#ifndef CLOCK
	if(sim) {
		if(!copyFile(pmlFile, "__workingfile2.tmp")) error("The fPromela file does not exist or is not readable!");
		if(system("cpp __workingfile2.tmp __workingfile2.tmp.cpp") != 0) failure("Could not run the c preprocessor (cpp).\n");
		yyin = fopen("__workingfile2.tmp.cpp", "r");
		if(yyin == NULL) failure("Could not open temporary working file.", argv[argc - 1]);
		if(yyparse(&globalSymTab2, &mtypes2, NULL) != 0) printf("Syntax error; aborting..\n");
		else {
			processVariables(globalSymTab2, globalSymTab2, mtypes2, SYS_VARS_SIZE, 1);
			fclose(yyin);
		}

		yyin = fopen(propFile, "r");
		if(yyin == NULL) failure("Could not open temporary working file.", argv[argc - 1]);
		if(yyparse(&globalSymTab, &mtypes, &props) != 0) printf("Syntax error; aborting..\n");
		else {
			fclose(yyin);
			yyin = fopen(propFile, "r");
			if(yyin == NULL) failure("Could not open temporary working file.", argv[argc - 1]);
			if(yyparse(&globalSymTab2, &mtypes2, &props2) != 0) printf("Syntax error; aborting..\n");
			else {
				printf("Checking F-simulation..\n");
				startSimulation(props, globalSymTab, mtypes, props2, globalSymTab2, mtypes2);
			}
		}

	}
#endif

	if(yyin != NULL)
		fclose(yyin);

	if(!keepTempFiles) {
		remove("__workingfile.tmp");
		remove("__workingfile.tmp.cpp");
	}


	exit(EXIT_NO_VERIFICATION);
}
