typedef features {
	bool VendingMachine;
	bool Beverages;
	bool Soda;
	bool Tea;
	bool FreeDrinks;
	bool CancelPurchase
};

features f;

int i;

active proctype foo() {
	i = 0;
	gd	:: f.A -> i--;
		:: f.B -> i++;
		dg;
	i = 0;
	gd  :: f.A || f.B -> i++;		// The features A and B are exclusive, which means that the else cannot be executed.
	    :: else -> skip;			// The assert is thus satisfied.
		dg;
	assert(i == 1);
}
