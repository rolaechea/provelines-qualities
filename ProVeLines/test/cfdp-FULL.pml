/*
 * This model is based on the CCSDS File Delivery Protocol (CFDP)
 * recommended standard. 
 * 
 * The scenario is as follows. Two CFDP entities are connected via a space 
 * data link. One of them (the sender) wants to send a file contained
 * in its own filestore to the other one (the receiver). We are especially 
 * interested in whether or not the file will totally be registered in the 
 * file store of the receiver. 
 *
 * In our model, several simplifications have been done, in particular:
 *	- We are interested only in the transmission procedure. All the other
 *	operations (user requests, file storing, fault detection, ...) are 
 *	ignored.
 *	- A retransmitted segment cannot be lost again.
 *	- The different timers used in the protocol have been ignored. In the 
 *	standard, the control message (NAK, EOF,...) corresponding with a given 
 *	timer is send once more after the time out. If, after a given number of 
 *	attempts, the message has not been received yet, the entity throws a FAULT  
 *  and the transaction is aborted. In this model, we focus only on whether 
 *	or not the segment is eventually received.  
 *
 * Some different configurations can be applied to the two entities.
 * To take that into account, we define some features (see below). These ones
 * allow to consider a subset of the possible configurations defined in the 
 * standard. In particular, we put the emphasis on two acknowledged mode of 
 * CFDP: the 'immediate ack mode' and the 'deferred ack mode'. These 
 * functionalities are presented and illustrated on pages 2-16 to 2-20 of 
 * the CFDP 'Introduction and Overview' (Green Book - Part 1). Some state 
 * tables are also provided in the Implementers Guide of CFDP 
 * (Green Book - part 2, pages 5-1 to 5-8).
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Features
 * 
 *  x SND_MIN: The minimum set of functionalities required to send a message. 
 *	x SND_MIN_ACK: The sender can respond to a received acknowledge.
 *	x SND_MIN_PROMPT_NAK: The sender can transmit a PROMPT message to the 
 *	  receiver.
 *	x RCV_MIN: The minimum set of functionalities required to receive 
 *	  a message.
 *	x RCV_MIN_ACK: The receiver has the ability to send (negative) 
 *	  acknowledgements.
 *	x RCV_IMMEDIATE_NAK: The receiver enables the immediate acknowledge mode.
 *	x RCV_DEFERRED_NAK: The receiver enables the deferred acknowledge mode.
 *	x RCV_PROMPT_NAK: The receiver enables the prompted acknowledge mode.
 *	x RCV_ASYNCH_NAK: The receiver enables the asynchronous acknowledge mode.
 *	x Reliable: The channel is reliable.
 * 
 * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Properties
 * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 * The whole file will eventually be received.
 *	<>fileReceived
 * Violated by: (!Reliable & !SND_MIN) | (!Reliable & SND_MIN & !RCV_MIN) | (!Reliable & SND_MIN & RCV_MIN & !RCV_IMMEDIATE_NAK) | (!Reliable & SND_MIN & RCV_MIN & RCV_IMMEDIATE_NAK & !RCV_ASYNCH_NAK) | (Reliable & !SND_MIN) | (Reliable & SND_MIN & !RCV_MIN)
 *
 * Obviously, the file cannot be transmitted if the sender is not able to send
 * anything or if receiver is not able to receive anything. Furthermore, if
 * the channel is not reliable, a part of the file can be lost.
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * 
 * If the EOF is eventually received, the whole file will eventually 
 * be received.
 *	<>eofReceived -> <>fileReceived
 * Violated by: (!Reliable & SND_MIN & RCV_MIN & !RCV_DEFERRED_NAK & !RCV_IMMEDIATE_NAK & !RCV_ASYNCH_NAK) | (!Reliable & SND_MIN & RCV_MIN & !RCV_DEFERRED_NAK & !RCV_IMMEDIATE_NAK & RCV_ASYNCH_NAK & !RCV_PROMPT_NAK) | (!Reliable & SND_MIN & RCV_MIN & !RCV_DEFERRED_NAK & RCV_IMMEDIATE_NAK & !RCV_ASYNCH_NAK) | (!Reliable & SND_MIN & RCV_MIN & RCV_DEFERRED_NAK & !RCV_ASYNCH_NAK)
 * 
 * Indeed, the EOF segment can be received only if the sender is able to send
 * and the receiver is able to receive. However, when the channel is not reliable,
 * The NAK segments can still be lost. In this case, the missing segment will not
 * be transmitted once more.
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * 
 * If the EOF is eventually received and if a NAK segment is eventually received, 
 * the whole file will eventually be received.
 *	(<>eofReceived && <>nakReceived) -> <>fileReceived
 * Violated by: (!Reliable & SND_MIN & !SND_MIN_ACK & RCV_MIN & !RCV_DEFERRED_NAK & !RCV_IMMEDIATE_NAK & !RCV_ASYNCH_NAK & RCV_PROMPT_NAK) | (!Reliable & SND_MIN & !SND_MIN_ACK & RCV_MIN & !RCV_DEFERRED_NAK & !RCV_IMMEDIATE_NAK & RCV_ASYNCH_NAK & !RCV_PROMPT_NAK) | (!Reliable & SND_MIN & !SND_MIN_ACK & RCV_MIN & !RCV_DEFERRED_NAK & RCV_IMMEDIATE_NAK & !RCV_ASYNCH_NAK) | (!Reliable & SND_MIN & !SND_MIN_ACK & RCV_MIN & RCV_DEFERRED_NAK & !RCV_ASYNCH_NAK) | (!Reliable & SND_MIN & SND_MIN_ACK & !SND_PROMPT_NAK & RCV_MIN & !RCV_DEFERRED_NAK & !RCV_IMMEDIATE_NAK & RCV_ASYNCH_NAK & !RCV_PROMPT_NAK) | (!Reliable & SND_MIN & SND_MIN_ACK & !SND_PROMPT_NAK & RCV_MIN & !RCV_DEFERRED_NAK & RCV_IMMEDIATE_NAK & !RCV_ASYNCH_NAK) | (!Reliable & SND_MIN & SND_MIN_ACK & !SND_PROMPT_NAK & RCV_MIN & RCV_DEFERRED_NAK & RCV_IMMEDIATE_NAK & !RCV_ASYNCH_NAK) | (!Reliable & SND_MIN & SND_MIN_ACK & SND_PROMPT_NAK & RCV_MIN & !RCV_DEFERRED_NAK & !RCV_IMMEDIATE_NAK & !RCV_ASYNCH_NAK & RCV_PROMPT_NAK) | (!Reliable & SND_MIN & SND_MIN_ACK & SND_PROMPT_NAK & RCV_MIN & !RCV_DEFERRED_NAK & !RCV_IMMEDIATE_NAK & RCV_ASYNCH_NAK & !RCV_PROMPT_NAK) | (!Reliable & SND_MIN & SND_MIN_ACK & SND_PROMPT_NAK & RCV_MIN & !RCV_DEFERRED_NAK & RCV_IMMEDIATE_NAK & !RCV_ASYNCH_NAK) | (!Reliable & SND_MIN & SND_MIN_ACK & SND_PROMPT_NAK & RCV_MIN & RCV_DEFERRED_NAK & !RCV_IMMEDIATE_NAK & !RCV_ASYNCH_NAK & RCV_PROMPT_NAK) | (!Reliable & SND_MIN & SND_MIN_ACK & SND_PROMPT_NAK & RCV_MIN & RCV_DEFERRED_NAK & RCV_IMMEDIATE_NAK & !RCV_ASYNCH_NAK)
 *
 * The property '<>fileReceived' always hold if the channel is reliable. If the
 * sender cannot be in acknoweldge mode, the NAK segment is useless. Otherwise,
 * the property can still be violated when the acknowledge mode is 'immediate'.
 * Indeed, some segments can still be lost after the first NAK is received, 
 * as well as the following NAK.
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * 
 * If the EOF is eventually received and if a NAK segment is eventually received, 
 * the whole file will eventually be received.
 *	(<>eofReceived && []<>nakReceived) -> <>fileReceived
 * Violated by: (!Reliable & SND_MIN & !SND_MIN_ACK & RCV_MIN & !RCV_DEFERRED_NAK & !RCV_IMMEDIATE_NAK & !RCV_ASYNCH_NAK & RCV_PROMPT_NAK) | (!Reliable & SND_MIN & !SND_MIN_ACK & RCV_MIN & !RCV_DEFERRED_NAK & !RCV_IMMEDIATE_NAK & RCV_ASYNCH_NAK & !RCV_PROMPT_NAK) | (!Reliable & SND_MIN & !SND_MIN_ACK & RCV_MIN & !RCV_DEFERRED_NAK & RCV_IMMEDIATE_NAK & !RCV_ASYNCH_NAK) | (!Reliable & SND_MIN & !SND_MIN_ACK & RCV_MIN & RCV_DEFERRED_NAK & !RCV_ASYNCH_NAK)
 *
 * The property '<>fileReceived' always hold if the channel is reliable. If the sender
 * cannot be in acknoweldge mode, the NAK segment is useless. Otherwise, the
 * assumption that a NAK is always eventually received ensures that the sender
 * will eventually transmit again the lost segments.
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 * The FIN segment is not send unless all the data segments have been 
 * received.
 *	[](finSend -> fileReceived)
 * Violated by: None. 
 *
 * Obviously, if the receiver confirms that the transaction is completed,
 * it means it has received the whole file. 
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 * No deadlock occurs.
 * Violated by: None.
 * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Macros used in the above properties:
 */

/* The file is considered as transmitted as soon as all of the data segments
 * have been received.
 */ 
#define fileReceived ended

/* 
 * This macro can be used to increase the size of the model. It defines the
 * number of segments in which the file is divided. 
 */
#define MAX 3
#define SIZE f.Message.size
#define TOUT f.Entity[0].SND_MIN.tout
#define MAXTIMEOUT 3

/* The following macros are used to make the code more understandable. */

/* 
 * This constant is used to express that a memory slot contains no data at all.
 */
#define NODATA 255

/* 
 * Macro that defines the sending process.
 */
#define	SEND(d,c) if	::	f.Channel.Reliable;		\
							c!d;			\
						::	else;			\
							if	::	c!d;	\
								::	skip;	\
							fi;				\
					fi;						

/* 
 * Macro that empties a data.
 */
#define REINIT(e) 	r = 0;							\
					e.type = FD;					\
					e.content = NODATA;				\
					e.offset = NODATA;				\
					do	::	SIZE > r;				\
							e.numbers[r] = NODATA;	\
							r++;					\
						::	else -> break;			\
					od;								

/*
 * Macro that defines the sending of a NAK
 */
#define SENDNAK(e, channel, file, last)		 				\
					i = 0;									\
					j = 0;									\
					REINIT(e)								\
					e.type = NAK;							\
					do	::	if	::	SIZE > i -> gt = true;		\
								::	else -> gt = false;			\
							fi;									\
							if	::	gt && i != NODATA ->		\
								if	::	i <= last && file[i] == NODATA ->	\
										e.numbers[j] = i;		\
										j++;					\
									::	else -> skip;			\
								fi;								\
								::	else -> gt = true; break;	\
							fi;								\
							i++;							\
						::	else -> break;					\
					od;										\
					if	::	j > 0;							\
							SEND(e,channel)					\
						::	else -> skip;					\
					fi;										

/*
 * Macro that retransmit a file segment (always successful)
 */					
#define RESEND(e, channel, file, data)														\
										j = 0;												\
										do	::	j < SIZE;									\
												if	::	!fault && e.numbers[j] != NODATA;	\
														d.type = FD;						\
														d.content = fs_send[e.numbers[j]];	\
														d.offset = e.numbers[j];			\
														speak!d;							\
														resend++;							\
														if	::	TOUT < resend -> 			\
																fault = 1;					\
															::	TOUT >= resend -> skip;		\
														fi;									\
													::	else -> skip;						\
												fi;											\
												j++;										\
											::	else -> break;								\
										od;	

typedef snd_min_ack {
	bool is_in;
	bool SND_PROMPT_NAK
}

typedef rcv_min_ack {
	bool is_in;
	bool RCV_IMMEDIATE_NAK;
	bool RCV_DEFERRED_NAK;
	bool RCV_PROMPT_NAK;
	bool RCV_ASYNCH_NAK
}

typedef rcv_min {
	bool is_in;
	rcv_min_ack RCV_MIN_ACK
}

typedef snd_min {
	bool is_in;
	int tout;
	snd_min_ack SND_MIN_ACK
}

typedef entity {
	bool is_in;
	snd_min SND_MIN;
	rcv_min RCV_MIN
}

typedef channel {
	bool is_in;
	bool Reliable
}

typedef message {
	bool is_in;
	int size
}

typedef features {
	entity Entity[2];
	channel Channel;
	message Message
}

typedef Data {
	mtype type; /* Can be one of the following: FD, EOF, FIN, NAK. */
	byte content; /* When type == FD, contains the value of the segment. */
	byte offset; /* When type == FD, contains the offset of the segment. */
	short numbers[MAX] /* When type == NAK, contains the offset of the segments that have been lost. */
}

features f;

bool eofSend = false; /* true iff the EOF signal has been send. */
bool eofReceived = false; /* true iff the EOF signal has really been received. */
bool finSend = false; /* true iff the FIN signal has been send. */
bool nakReceived = false; /* true iff a file segment is received more than once. */
int fd_received = 0; /* The number of distinct segments that have been received. */
byte fs_send[MAX]; /* File store of the sender. */
byte fs_recv[MAX]; /* File store of the receiver. */
mtype = {sender, receiver, FD, EOF, FIN, NAK, PROMPT}

chan toRcver = [1] of {Data}; /* Channel to send a message to the receiver. */
chan toSnder = [1] of {Data}; /* Channel to send a message to the sender. */


byte fault = 0;
bool ended = false;
	
proctype node (mtype mode; chan speak; chan listen) {
	int i = 0;
	int j = 0;
	int r = 0;
	bool gt = true;
	bool promptTriggered = false;
	bool asynchTriggered = false;
	int lastReceived = -1;
	Data d, e;
	byte resend = 0;
	REINIT(e)
	if	::	mode == sender;
			do	::	this.SND_MIN;
					if	::	!fault ->
							if	::	listen?e; /* The sender first listen to messages that the receiver can send, e.g. a NAK segment. */
									if	::	e.type == NAK;
											nakReceived = true;
											if	::	this.SND_MIN.SND_MIN_ACK;
													nakReceived = false;
													RESEND(e, speak, fs_send, d)
												::	else -> skip;
											fi;
										::	e.type == FIN;
											finSend = true;
										::	else-> skip;
									fi;
								::	empty(listen) && !eofSend; /* No message received */
									if	::	SIZE > i -> gt = true;
										::	else -> gt = false;
									fi;	
									if	::	gt; /* All the data segments must be send. */
											d.type = FD;
											d.content = fs_send[i];
											d.offset = i;
											SEND(d, speak)
											i++;
										::	!gt; /* After sending al the data segments, an EOF segment must be send. */
											gt = true;
											d.type = EOF;
											d.content = 0;
											d.offset = 0;
											SEND(d, speak)
											eofSend = true;
										::	gt && !promptTriggered;
											promptTriggered = true;
											if	:: 	this.SND_MIN.SND_MIN_ACK.SND_PROMPT_NAK;
													d.type = PROMPT;
													d.content = NODATA;
													d.offset = NODATA;
													SEND(d, speak)
												:: else -> skip;
											fi;
									fi;
								::	timeout -> skip; /* To avoid false deadlocks. At this point, the sender has finished sending all the file segments (though some of them may not have reached the receiver). */
							fi;
						::	else -> skip;
					fi;
				:: else -> skip;	
			od;
	
		::	mode == receiver;
			do	::	this.RCV_MIN;
					if	::	(fd_received == SIZE) -> ended = true;
						::	else -> ended = false;
					fi;
					if	::	listen?d;
							ended = false;
							if	::	d.type == FD;
									/* Received a data segment. */
									if	::	fs_recv[d.offset] == NODATA;
											fs_recv[d.offset] = d.content;
											lastReceived = d.offset;
											fd_received++;
											if	::	this.RCV_MIN.RCV_MIN_ACK.RCV_IMMEDIATE_NAK;
													/* If the immediate NAK mode is enabled, the receiver looks if any previous segment has been lost. */	
													SENDNAK(e, speak, fs_recv, d.offset)
												::	else -> skip;
											fi;
										::	else -> skip;
									fi;
								::	d.type == EOF;
									eofReceived = true;
									/* In both acknowledge modes, upon receiving and EOF segments, the receiver send a NAK that ask to retransmit the missing segments. */
									if	::	(this.RCV_MIN.RCV_MIN_ACK.RCV_IMMEDIATE_NAK || this.RCV_MIN.RCV_MIN_ACK.RCV_DEFERRED_NAK || this.RCV_MIN.RCV_MIN_ACK.RCV_PROMPT_NAK);
											SENDNAK(e, speak, fs_recv, MAX - 1)
										::	else -> skip;
									fi;
								:: 	d.type == PROMPT;
									if	:: 	this.RCV_MIN.RCV_MIN_ACK.RCV_PROMPT_NAK;
											SENDNAK(e, speak, fs_recv, lastReceived)
										::	else -> skip;
									fi;
								::	else -> skip;
							fi;
						::	empty(listen) && ended && eofReceived; /* If everything has been received, the whole transaction is acknowledged as completed. */
							ended = false;
							REINIT(e)
							e.type = FIN; 
							SEND(e,speak)
						:: 	empty(listen) && !asynchTriggered;
							ended = false;
							asynchTriggered = true;
							if	::	this.RCV_MIN.RCV_MIN_ACK.RCV_ASYNCH_NAK;
									SENDNAK(e, speak, fs_recv, lastReceived)
								:: else -> skip;
							fi;
					fi;
				:: 	else -> skip;
			od;
	fi;
}


active proctype initiate() {
	/* Initializes the file stores. */
	int i = 0;
	f.Message;
	skip;
	SIZE > 0 && SIZE <= MAX && TOUT > 0 && TOUT <= MAXTIMEOUT;
	do	::	SIZE > i;
			fs_send[i] = i+1;
			fs_recv[i] = NODATA;
			i++;
		::	else -> break;
	od;
	/* Defines the mode of and launches the entities */
	atomic {
		run node[f.Entity[0]](sender, toRcver, toSnder);
		run node[f.Entity[1]](receiver, toSnder, toRcver)
	}
	
}