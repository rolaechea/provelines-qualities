This system deadlocks because the feature model allows it to have only a sender or only a 
receiver.  In either case, the launched process will block at some point.  This can be 
prevented by changing the "someOf" in the TVL file to "allOf".


$ ./snip -st -check -exhaustive ../test/deadlock-03.pml
No never claim, checking only asserts and deadlocks..
Found deadlock [explored 143 states, re-explored 0].
 - Products by which it is violated (as feature expression):
   (Send & !Receive)

 - Stack trace:
   features                            = /
   globals.buffer.s00.f00              = 0
   globals.buffer.s01.f00              = 0
   globals.buffer.s02.f00              = 0
   pid 00, init                        @ NL31
    --
   features                            = (Send)
   sender.p                            = 0
    --
   sender.p                            = 2
    --
   globals.buffer.s00.f00              = 2
    --
   globals.buffer.s01.f00              = 2
    --
   globals.buffer.s02.f00              = 2
    --
    -- Final state repeated in full:
   features                            = (Send & !Receive)
   globals.buffer.s00.f00              = 2
   globals.buffer.s01.f00              = 2
   globals.buffer.s02.f00              = 2
   pid 00, init                        @ end
   pid 01, sender                      @ NL17
   sender.p                            = 2
    --


 ****

Found deadlock [explored 206 states, re-explored 0].
 - Products by which it is violated (as feature expression):
   (!Send & Receive)

 - Stack trace:
   features                            = /
   globals.buffer.s00.f00              = 0
   globals.buffer.s01.f00              = 0
   globals.buffer.s02.f00              = 0
   pid 00, init                        @ NL31
    --
   features                            = (!Send)
    --
    -- Final state repeated in full:
   features                            = (!Send & Receive)
   globals.buffer.s00.f00              = 0
   globals.buffer.s01.f00              = 0
   globals.buffer.s02.f00              = 0
   pid 00, init                        @ end
   pid 01, receiver                    @ NL24
    --


 ****


Exhaustive search finished  [explored 206 states, re-explored 0].
 - 2 problems were found covering the following products (others are ok):
(!Send & Receive) | (Send & !Receive)
