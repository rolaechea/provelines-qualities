typedef features {
	bool A;
	bool B
};

features f;

int i;

active proctype foo() {
	i = 0;
	gd  :: f.A -> i++;
	dg;
}
