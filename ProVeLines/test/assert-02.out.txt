Only the nice sender will send flowers, so the assertion is of course violated when 
that feature is not present.  There is a second proctype "suspiciousReceiver"; in 
which the receiver will discard what's read in case the "Nice" feature is absent.
If the "receiver" proctype is commented and the "suspiciousReceiver" proctype 
uncommented, then no assertion violation will occur (output given below).  


$ ./snip -st -check -exhaustive ../test/assert-02.pml
No never claim, checking only asserts and deadlocks..
Assertion at line 20 violated [explored 7 states, re-explored 0].
 - Products by which it is violated (as feature expression):
   (!Nice)

 - Stack trace:
   features                            = /
   globals.mail.s00.f00                = nothing
   pid 00, sender                      @ NL12
   pid 01, receiver                    @ NL19
   receiver.package                    = nothing
    --
   features                            = (!Nice)
    --
   globals.mail.s00.f00                = bomb
    --
   globals.mail.s00.f00                = nothing
   receiver.package                    = bomb
    --
    -- Final state repeated in full:
   features                            = (!Nice)
   globals.mail.s00.f00                = nothing
   pid 00, sender                      @ end
   pid 01, receiver                    @ NL20
   receiver.package                    = bomb
    --


 ****


Exhaustive search finished  [explored 7 states, re-explored 0].
 -  One problem found covering the following products (others are ok):
(!Nice)


-----------------------------------------------------------------------------------------


$ ./snip -st -check -exhaustive ../test/assert-02.pml 
No never claim, checking only asserts and deadlocks..
No assertion violations or deadlocks found [explored 10 states, re-explored 1].


