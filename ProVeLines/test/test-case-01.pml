typedef features {
   bool Alarm;
   bool Canreturn
};

features f;

bool ready = true;
bool stopped = false;

active proctype toto() {
   do  :: 
       	  if :: f.Alarm; 
	      	stopped = true [ 10 ];
	     :: !f.Alarm;
	       	  if :: (stopped == true);
		      if :: f.Canreturn ;
	       	      	    stopped = false [ 0 ];
		 	  :: else 
			     -> skip;
		       fi;
	       	     :: else 
		     	-> skip;
		     if :: f.Alarm;
		     	 stopped =true [ 4 ];
		      	 :: else 
			    -> skip;
		     fi; 
	       	  fi;	     		
	     :: else 
	     	-> skip;
	 fi;
   od;
}
