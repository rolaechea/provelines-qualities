typedef feat2 {
	bool is_in;
	bool b
}
typedef feat {
      bool is_in;  /* Boolean determining feature's presence */
      int attr;     /*  Integer attribute */
      feat2 sub
}

typedef features {
       bool Simple;   /* Single Boolean feature */
       feat With_attr;   /* Single feature with attributes */
       bool Array[5]; /* Multiple Boolean features */
       feat Attr_feat[5]   /* Multiple features with attributes */
}

#define SIZE f.With_attr.attr

features f;
int x = 3;
int y = 1
chan c = [1] of {int};

active[1] proctype foo() {

do	:: gd	::	f.Simple -> skip;
					gd	::	f.With_attr -> skip;
							gd	::	f.With_attr.is_in -> skip;
									gd	::	f.With_attr.attr > 3 -> skip;
											gd	::	f.Attr_feat -> skip;
													gd	::	card(f.Attr_feat) >= 2 -> skip;
															gd	::	f.Attr_feat[1].attr < 4 -> skip;
																	gd	::	f.Attr_feat[0].attr < x -> skip;
																			gd	::	f.Attr_feat[0].attr + y > x -> skip;
																				::	else -> skip;
																			dg;
																		::	else -> skip;
																	dg;
																::	else -> skip;
															dg;
														::	else -> skip;
													dg;
												::	else -> skip;
											dg;
										::	else -> skip;
									dg;
								::	else -> skip;
							dg;
						::	else -> skip;
					dg;
				::	else -> skip;
			dg;
	od
}