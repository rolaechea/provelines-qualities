typedef features {
	bool Nice
};

features f;

chan mail = [1] of { mtype };

mtype = { nothing, flowers, bomb };

active proctype sender() {
	gd	:: f.Nice -> mail!flowers;
		:: else -> mail!bomb;
	dg;
}

active proctype receiver() {
	mtype package = nothing;
	mail?package;
	assert(package != bomb);
}

/*
active proctype suspiciousReceiver() {
	mtype package = nothing;
	if	:: f.Nice -> mail?package;
		:: else -> mail?_;
		fi;
	assert(package != bomb);
}
*/