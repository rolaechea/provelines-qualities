This is a classical mutual exclusion problem.  If things are not executed atomically
and if there is no arbiter, then the mutual exclusion property will fail, just as
expected.  See the .pml file for further properties to experiment with.


$ ./snip -st -check -ltl "[]excl" ../test/mutex.pml
Checking LTL property []excl..
Safety property violated [explored 150 states, re-explored 38].
 - Products by which it is violated (as feature expression):
   (!Atom & !Arbiter)

 - Stack trace:
   features                            = /
   never                               @ NL179 
   globals.state1                      = nothing
   globals.state2                      = nothing
   globals.coin                        = nothing
   pid 00, proc1                       @ NL80
   pid 01, proc2                       @ NL112
   pid 02, proc_arbiter                @ NL144
    --
   globals.state1                      = wait
    --
   features                            = (!Arbiter)
    --
   globals.state1                      = critical
    --
   globals.state2                      = wait
    --
   globals.state1                      = nothing
    --
   globals.state1                      = wait
    --
   globals.state1                      = critical
    --
   globals.state1                      = nothing
    --
   globals.state1                      = wait
    --
   globals.state1                      = critical
    --
   globals.state1                      = nothing
    --
   globals.state1                      = wait
    --
   globals.state1                      = critical
    --
   globals.state1                      = nothing
    --
   globals.state1                      = wait
    --
   globals.state2                      = critical
    --
   globals.state2                      = nothing
    --
   globals.state1                      = critical
    --
   globals.state1                      = nothing
    --
   globals.state1                      = wait
    --
   globals.state2                      = wait
    --
   globals.state1                      = critical
    --
   globals.state1                      = nothing
    --
   globals.state1                      = wait
    --
   globals.state1                      = critical
    --
   globals.state1                      = nothing
    --
   globals.state1                      = wait
    --
   globals.state1                      = critical
    --
   globals.state1                      = nothing
    --
   globals.state1                      = wait
    --
   globals.state1                      = critical
    --
   globals.state2                      = critical
    --
    -- Final state repeated in full:
   features                            = (!Atom & !Arbiter)
   never                               @ NL179 
   globals.state1                      = critical
   globals.state2                      = critical
   globals.coin                        = nothing
   pid 00, proc1                       @ NL106
   pid 01, proc2                       @ NL138
    --
