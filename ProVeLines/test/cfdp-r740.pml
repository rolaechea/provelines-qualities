/*
 * This model is based on the CCSDS File Delivery Protocol (CFDP)
 * recommended standard. 
 * 
 * The scenario is as follows. Two CFDP entities are connected via a space 
 * data link. One of them (the sender) wants to send a file contained
 * in its own filestore to the other one (the receiver). We are especially 
 * interested in whether or not the file will totally be registered in the 
 * file store of the receiver. 
 *
 * In our model, several simplifications have been done, in particular:
 *	- We are interested only in the transmission procedure. All the other
 *	operations (user requests, file storing, fault detection, ...) are 
 *	ignored.
 *	- A retransmitted segment cannot be lost again.
 *	- The different timers used in the protocol have been ignored. In the 
 *	standard, the control message (NAK, EOF,...) corresponding with a given 
 *	timer is send once more after the time out. If, after a given number of 
 *	attempts, the message has not been received yet, the entity throws a FAULT  
 *  and the transaction is aborted. In this model, we focus only on whether 
 *	or not the segment is eventually received.
 *	- Because of the last simplification, we do not care about the 
 *	acknowledgment of the EOF and FIN segments.  
 *
 * Some different configurations can be applied to the two entities.
 * To take that into account, we define some features (see below). These ones
 * allow to consider a subset of the possible configurations defined in the 
 * standard. In particular, we put the emphasis on two acknowledged mode of 
 * CFDP: the 'immediate ack mode' and the 'deferred ack mode'. These 
 * functionalities are presented and illustrated on pages 2-16 to 2-20 of 
 * the CFDP 'Introduction and Overview' (Green Book - Part 1). Some state 
 * tables are also provided in the Implementers Guide of CFDP 
 * (Green Book - part 2, pages 5-1 to 5-8).
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Features
 * 
 *  x SND_MIN: The minimum set of functionalities required to send a message. 
 *	x SND_MIN_ACK: The sender can respond to a received acknowledge.
 *	x RECV_MIN:The minimum set of functionalities required to receive 
 *	  a message.
 *	x RECV_IMMEDIATE_NAK: The receiver enables the immediate acknowledge mode.
 *	x RECV_DEFERRED_NAK: The receiver enables the deferred acknowledge mode.
 *	x Reliable: The channel is reliable.
 * 
 * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Properties
 * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 * The whole file will eventually be transmitted.
 *	<>transmitted
 * Violated by: (!Reliable) | (Reliable & !SND_MIN) | (Reliable & SND_MIN & !RECV_MIN)
 *
 * Obviously, the file cannot be transmitted if the sender is not able to send
 * anything or if receiver is not able to receive anything. Furthermore, if
 * the channel is not reliable, a part of the file can be lost.
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * 
 * If the EOF is eventually received, the whole file will eventually 
 * be transmitted.
 *	<>eofReceived -> <>transmitted
 * Violated by: (!Reliable & SND_MIN & RECV_MIN)
 * 
 * Indeed, the EOF segment can be received only if the sender is able to send
 * and the receiver is able to receive. However, when the channel is not reliable,
 * The NAK segments can still be lost. In this case, the missing segment will not
 * be transmitted once more.
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * 
 * If the EOF is eventually received and if a NAK segment is eventually received, 
 * the whole file will eventually be transmitted.
 *	(<>eofReceived && <>nakReceived) -> <>transmitted
 * Violated by: (!Reliable & SND_MIN & !SND_MIN_ACK & RECV_MIN & !RECV_DEFERRED_NAK & RECV_IMMEDIATE_NAK) | (!Reliable & SND_MIN & !SND_MIN_ACK & RECV_MIN & RECV_DEFERRED_NAK) | (!Reliable & SND_MIN & SND_MIN_ACK & RECV_MIN & RECV_IMMEDIATE_NAK)
 *
 * The property '<>transmitted' always hold if the channel is reliable. If the sender
 * cannot be in acknoweldge mode, the NAK segment is useless. Otherwise, the
 * property can still be violated when the acknowledge mode is 'immediate'.
 * Indeed, some segments can still be lost after the first NAK is received, 
 * as well as the following NAK.
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * 
 * If the EOF is eventually received and if a NAK segment is eventually received, 
 * the whole file will eventually be transmitted.
 *	(<>eofReceived && []<>nakReceived) -> <>transmitted
 * Violated by: (!Reliable & SND_MIN & !SND_MIN_ACK & RECV_MIN & !RECV_DEFERRED_NAK & RECV_IMMEDIATE_NAK) | (!Reliable & SND_MIN & !SND_MIN_ACK & RECV_MIN & RECV_DEFERRED_NAK)
 *
 * The property '<>transmitted' always hold if the channel is reliable. If the sender
 * cannot be in acknoweldge mode, the NAK segment is useless. Otherwise, the
 * assumption that a NAK is always eventually received ensures that the sender
 * will eventually transmit again the lost segments.
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 * The FIN segment is not lost unless all the data segments and the EOF segment
 * have been received.
 *	[](finSend -> transmitted)
 * Violated by: None. 
 *
 * Obviously, if the receiver confirms that the transaction is completed,
 * it means it has received the whole file. 
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 * No deadlock occurs.
 * Violated by: None.
 * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Macros used in the above properties:
 */
#define transmitted (fd_received == SIZE)

/* 
 * This macro can be used to increase the size of the model. It defines the
 * number of segments in which the file is divided. 
 */
#define SIZE 8

/* The following macros are used to make the code more understandable. */

/* 
 * This constant is used to express that a memory slot contains no data at all.
 */
#define NODATA 255

/* 
 * Macros that defines the sending process.
 */
#define	SEND(d,c) if	::	f.Reliable;		\
							c!d;		\
						::	else;			\
							if	::	c!d;\
								::	skip;	\
							fi;				\
					fi;						\

/* 
 * Macros that empties a data.
 */
#define REINIT(e) r = 0;						\
					e.type = FD;					\
					e.content = NODATA;				\
					e.offset = NODATA;				\
					do	::	r < SIZE;				\
							e.numbers[r] = NODATA;	\
							r++;					\
						::	else -> break;			\
					od;								\


typedef features {
	bool SND_MIN;
	bool SND_MIN_ACK;
 	bool RECV_MIN;
 	bool RECV_IMMEDIATE_NAK;
	bool RECV_DEFERRED_NAK;
 	bool Reliable
}

typedef Data {
	mtype type; /* Can be one of the following: FD, EOF, FIN, NAK. */
	byte content; /* When type == FD, contains the value of the segment. */
	byte offset; /* When type == FD, contains the offset of the segment. */
	short numbers[SIZE] /* When type == NAK, contains the offset of the segments that have been lost. */
}

features f;

bool eofSend = false; /* true iff the EOF signal has been send. */
bool eofReceived = false; /* true iff the EOF signal has really been received. */
bool finSend = false; /* true iff the FIN signal has been send. */
bool nakReceived = false; /* true iff a file segment is received more than once. */
int fd_received = 0; /* The number of distinct segments that have been received. */

byte fs_send[SIZE]; /* File store of the sender. */
byte fs_recv[SIZE]; /* File store of the receiver. */
mtype = {sender, receiver, FD, EOF, FIN, NAK}

chan toRcver = [0] of {Data}; /* Channel to send a message to the receiver. */
chan toSnder = [0] of {Data}; /* Channel to send a message to the sender. */


proctype entity (mtype mode; chan speak; chan listen) {
	int i = 0;
	int j = 0;
	int r = 0;
	Data d, e;
	REINIT(e)
	if	::	mode == sender;
			do	::	f.SND_MIN;
					do	::	i < SIZE; /* All the data segments must be send. */
							d.type = FD;
							d.content = fs_send[i];
							d.offset = i;
							SEND(d, speak)
							i++;
						::	i >= SIZE && !(eofSend); /* After sending al the data segments, an EOF segment must be send. */
							d.type = EOF;
							d.content = 0;
							d.offset = 0;
							SEND(d, speak)
							eofSend = true;
						::	listen?e; /* The sender listen to messages that the receiver can send, e.g. a NAK segment. */
								if	::	e.type == NAK;
										nakReceived = true;
										if	::	f.SND_MIN_ACK;
												nakReceived = false;
												j = 0;
												do	::	j < SIZE;
														if	::	e.numbers[j] != NODATA;
																d.type = FD;
																d.content = fs_send[e.numbers[j]];
																d.offset = e.numbers[j];
																speak!d;
															::	else -> skip;
														fi;
														j++;
													::	else -> break;
												od;
											::	else -> skip;
										fi;
									::	e.type == FIN;
										finSend = true;
									::	else-> skip;
								fi;
						
					od;
				:: else -> skip;	
			od;
	
		::	mode == receiver;
			do	::	f.RECV_MIN;
					if	::	listen?d;
							if	::	d.type == FD;
									/* Received a data segment. */
									if	::	fs_recv[d.offset] == NODATA;
											if	::	f.RECV_IMMEDIATE_NAK;
													/* If the immediate NAK mode is enabled, the receiver looks if any previous segment has been lost. */	
													i = d.offset - 1;
													j = 0;
													REINIT(e)
													e.type = NAK;
													do	::	i >= 0;
															if	::	fs_recv[i] == NODATA; 
																	e.numbers[j] = i;
																	j++;
																::	else -> skip;
															fi;
															i--;
														::	else -> break;
													od;	
													if	::	j > 0;
															SEND(e,speak)
														::	else -> skip;
													fi;
												::	else -> skip;
											fi;
											fs_recv[d.offset] = d.content;
											fd_received++;
										::	else -> skip;
									fi;
							
								::	d.type == EOF;
									eofReceived = true;
									/* In both acknowledge modes, upon receiving and EOF segments, the receiver send a NAK that ask to retransmit the missing segments. */
									if	::	f.RECV_IMMEDIATE_NAK || f.RECV_DEFERRED_NAK;
											i = 0;
											j = 0;
											e.type = NAK;
											do	::	i < SIZE;
													if	::	fs_recv[i] == NODATA -> 
															e.numbers[j] = i;
															j++;
														::	else -> skip;
													fi;	
													i++;
												::	else -> break;
											od;
											if	::	j > 0;
													SEND(e,speak)
												::	else -> skip;
											fi;
										::	else -> skip;
									fi;
						
								::	else -> skip;
							fi;
						::	transmitted && eofReceived; /* If everything has been received, the whole transaction is acknowledged as completed. */
							REINIT(e)
							e.type = FIN; 
							SEND(e,speak)
						:: timeout -> skip; /* Needed to avoid false deadlocks. */
					fi;
				:: 	else -> skip;
			od;
	fi;
}


active proctype initiate() {
	/* Initializes the file stores. */
	int i = 0;
	do	::	i < SIZE;
			fs_send[i] = i+1;
			fs_recv[i] = NODATA;
			i++;
		::	else -> break;
	od;
	/* Defines the mode of and launches the entities */
	atomic {
		run entity(sender, toRcver, toSnder);
		run entity(receiver, toSnder, toRcver)
	}
	
}