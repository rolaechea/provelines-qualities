/*
 * This model illustrates the usage of features on the resource sharing problem.
 *
 * The system is composed of two processes and one resource. Only one process can
 * have access to the resource at the same time. Before having the access granted, 
 * a process must declared its intention by entering in a "waiting state". Then,
 * he can access the resource provided the other process is not accessing it.
 * When the resource is being used by one process, this process is said to be 
 * in a "critical state".
 *
 *
 * Features:
 * 
 * 	- Atom: If this feature is active, the actions of looking if the other process
 *	is in critical state and entering its own critical section are part of a whole
 *	atomic action.
 *	- Arbiter: Determines whether or not a arbiter is present to decide which 
 *	process will be the next one to have access to the resource.
 *	- RandomizedArbiter: The arbiter grants the access randomly.
 *	- AlternateArbiter: The access is granted alternatively to the two processes.
 *
 *
 * Properties:
 *
 * Mutual exclusion: the two processes cannot access the resource at the same time.
 *  "[]excl"
 *  -> violated by: !Atom & !Arbiter
 *
 *
 * Unconditional fairness: the two processes will access the resource infinitely often.
 * (i.e. they will enter in their respective critical state infinitely often).
 *  "(([]<> crit1) && ([]<> crit2))"
 *  -> violated by: (!Arbiter) | (Arbiter & RandomizedArbiter)
 *
 *
 * Strong fairness: If the two processes want to access the resource infinitely often,
 * they will both have the access granted infinitely often.
 * (i.e. if they are in their respective waiting state infinitely often, 
 * they will enter in their respective critical state infinitely often).
 *  "([]<>wait1 && []<>wait2) -> (([]<> crit1) && ([]<> crit2))"
 *  -> violated by: (!Arbiter) | (Arbiter & RandomizedArbiter)
 *
 *
 * Weak fairness: If the two process eventually always want the access granted,
 * they will both have the access granted infinitely often.
 * (i.e. if they are eventually always in their respective waiting state, 
 * they will enter in their respective critical state infinitely often).
 *  "(<>[]wait1 && <>[]wait2) -> (([]<> crit1) && ([]<> crit2))"
 *  -> violated by: None
 *
 */


#define crit1 (state1==critical)
#define crit2 (state2==critical)
#define wait1 (state1==wait)
#define wait2 (state2==wait)
#define excl (!(crit1 && crit2))
#define head (coin == one)
#define tail (coin == two)

mtype = {nothing, wait, critical, one, two};

mtype state1;
mtype state2;
mtype coin;

typedef features {
	bool Atom;
	bool Arbiter;
	bool RandomizedArbiter;
	bool AlternateArbiter
}

features f;
chan lock = [0] of {mtype};
chan release = [0] of {mtype};

active proctype proc1() {
	do	::	state1 = wait;
			if	::	f.Arbiter;
					lock?one;
					if	::	f.Atom;
							if	::	atomic {
										state2 != critical ->
										state1 = critical
									}; 
								fi
						::	else ->	state2 != critical -> 
							state1 = critical;
						fi;
					atomic {
						state1 = nothing; 
						release!one
					};
				:: 	else ->
					if	::	f.Atom;
							if	::	atomic {
										state2 != critical -> 
										state1 = critical
									}; 
								fi
						::	else ->	state2 != critical -> 
							state1 = critical;
						fi;
					state1 = nothing;
				fi;
		od
};

active proctype proc2() {
	do	::	state2 = wait;
			if	::	f.Arbiter;
					lock?two;
					if	::	f.Atom;
							if	::	atomic {
										state1 != critical ->
										state2 = critical
									}; 
								fi
						::	else ->	state1 != critical -> 
							state2 = critical;
						fi;
					atomic {
						state2 = nothing; 
						release!two
					};
				:: 	else ->
					if	::	f.Atom;
							if	::	atomic {
										state1 != critical -> 
										state2 = critical
									}; 
								fi
						::	else ->	state1 != critical -> 
							state2 = critical;
						fi;
					state2 = nothing;
				fi;
		od
};

active proctype proc_arbiter() {
	if	::	f.Arbiter;
			if	::	f.RandomizedArbiter;
					do	::	atomic {
								coin = one;
								lock!one;
								release?one
							};
						:: atomic {
								coin = two; 
								lock!two; 
								release?two
							};
						od
				::	f.AlternateArbiter;
					do	::	atomic {
								coin != one;
								coin = one;
								lock!one;
								release?one
							};
						::	atomic {
								coin != two; 
								coin = two; 
								lock!two; 
								release?two
							};
						od
				fi
		:: else -> skip
		fi
}

