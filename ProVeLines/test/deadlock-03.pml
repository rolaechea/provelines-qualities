typedef features {
	bool Send;
	bool Receive
};

features f;

chan buffer = [3] of { int };

// The sender will non-deterministically write 0 or 1 into the buffer.
proctype sender() {
	int p;
	do	:: 	true;
			if  :: p = 0;
				:: p = 1;
				fi;
			buffer!p;
		od;
}

// The sender will read from the buffer and discard what's read.
proctype receiver() {
	do	:: 	true;
			buffer?_;
		od;
}

// Run sender only if the "Send" feature is selected.
// Run receiver only if the "Receive" feature is selected.
init {
	gd	::	f.Send	-> run sender();
		::	else	-> skip;
		dg;
	gd	::	f.Receive -> run receiver();
		::	else	  -> skip;
		dg;
}